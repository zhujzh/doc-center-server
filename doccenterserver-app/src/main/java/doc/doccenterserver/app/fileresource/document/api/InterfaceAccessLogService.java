package doc.doccenterserver.app.fileresource.document.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.InterfaceAccessLogDto;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@ApplicationService(domain = "InterfaceAccessLogResource", name = "InterfaceAccessLogResource服务")
public interface InterfaceAccessLogService {

    @ReturnValue
    @Method(name = "分页查询服务日志对象")
    PageResult<List<InterfaceAccessLogDto>> getInterfaceAccessLogList(
            HttpServletRequest request, InterfaceAccessLogDto interfaceAc);

    @ReturnValue
    @Method(name = "根据id获取服务日志对象")
    InterfaceAccessLogDto getInterfaceAccessLog(String logId);

    @ReturnValue
    @Method(name = "初始化")
    InterfaceAccessLogDto getInit(HttpServletRequest request);

    @ReturnValue
    @Method(name = "新增服务日志数据")
    InterfaceAccessLogDto insert(HttpServletRequest request, String str);
}
