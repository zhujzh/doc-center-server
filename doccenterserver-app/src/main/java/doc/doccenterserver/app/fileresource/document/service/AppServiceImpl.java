package doc.doccenterserver.app.fileresource.document.service;

import doc.doccenterserver.app.fileresource.document.api.AppService;
import doc.doccenterserver.app.fileresource.document.converter.AppConvertor;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.AppDto;
import doc.doccenterserver.domain.fileresource.document.model.AppEntity;
import doc.doccenterserver.domain.fileresource.document.service.AppDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@Component("AppServiceAS")
@Slf4j
public class AppServiceImpl implements AppService {

    @Resource
    private AppDomainService appDomainService;

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<AppDto>> getUseDetailList(HttpServletRequest request, AppDto appDto) {
        AppEntity appEntity = AppConvertor.INSTANCE.dtoToEntity(appDto);
        PageResult<List<AppEntity>> listPageResult = appDomainService.getUseDetailList(request, appEntity);
        return AppConvertor.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public AppDto editInit(String appId) {
        return AppConvertor.INSTANCE.entityToDto(appDomainService.editInit(appId));
    }

    @Override
    public AppDto save(AppDto appDto) {
        return AppConvertor.INSTANCE.entityToDto(appDomainService.save(AppConvertor.INSTANCE.dtoToEntity(appDto)));
    }

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<AppDto>> getAppList(AppDto appDto) {
        AppEntity appEntity = AppConvertor.INSTANCE.dtoToEntity(appDto);
        PageResult<List<AppEntity>> listPageResult = appDomainService.getAppList(appEntity);
        return AppConvertor.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public AppDto delete(String appIds) {
        return AppConvertor.INSTANCE.entityToDto(appDomainService.delete(appIds));
    }

    @Override
    public AppDto getAppDetail(String appId) {
        return AppConvertor.INSTANCE.entityToDto(appDomainService.getAppDetail(appId));
    }
}
