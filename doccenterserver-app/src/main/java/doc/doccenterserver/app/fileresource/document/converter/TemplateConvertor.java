package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.TemplateDto;
import doc.doccenterserver.client.fileresource.document.api.req.TemplateRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.TemplateEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface TemplateConvertor {

    TemplateConvertor INSTANCE = Mappers.getMapper(TemplateConvertor.class);

    @Mappings({ })
    TemplateEntity dtoToEntity(TemplateDto templateDto);
    @Mappings({ })
    TemplateDto entityToDto(TemplateEntity templateEntity);

    @Mappings({ })
    List<TemplateEntity> dtoToEntity(List<TemplateDto> templateDtoList);

    @Mappings({ })
    List<TemplateDto> entityToDto(List<TemplateEntity> templateEntityList);

    @Mappings({ })
    doc.doccenterserver.client.fileresource.base.PageResult<List<TemplateDto>> entityToDtoPage(PageResult<List<TemplateEntity>> template);

    @Mappings({ })
    TemplateEntity requestToEntity (TemplateRequest request);

}
