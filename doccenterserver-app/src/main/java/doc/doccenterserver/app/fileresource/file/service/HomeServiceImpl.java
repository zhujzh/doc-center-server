package doc.doccenterserver.app.fileresource.file.service;

import doc.doccenterserver.app.fileresource.file.api.HomeService;
import doc.doccenterserver.app.fileresource.file.converter.HomeConvertor;
import doc.doccenterserver.client.fileresource.file.api.dto.HomeDto;
import doc.doccenterserver.domain.fileresource.file.service.HomeDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Tangcancan
 */
@Component("HomeServiceAS")
@Slf4j
public class HomeServiceImpl implements HomeService {

    @Resource
    private HomeDomainService homeDomainService;

    @Override
    public HomeDto getDocSearch(HttpServletRequest request, HomeDto homeDto) {
        return HomeConvertor.INSTANCE.entityToDto(homeDomainService.getDocSearch(request, HomeConvertor.INSTANCE.dtoToEntity(homeDto)));
    }
}
