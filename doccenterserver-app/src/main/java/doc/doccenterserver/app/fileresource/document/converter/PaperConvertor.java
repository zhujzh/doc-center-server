package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.PaperDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface PaperConvertor {

    PaperConvertor INSTANCE = Mappers.getMapper(PaperConvertor.class);

    @Mappings({})
    PaperEntity dtoToEntity(PaperDto parameter);

    @Mappings({})
    PaperDto entityToDto(PaperEntity parameter);

    @Mappings({})
    List<PaperEntity> dtoToEntity(List<PaperDto> parameter);

    @Mappings({})
    List<PaperDto> entityToDtoList(List<PaperEntity> parameter);

    @Mappings({})
    doc.doccenterserver.client.fileresource.base.PageResult<List<PaperDto>> entityToDtoPage(PageResult<List<PaperEntity>> parameter);

    @Mappings({})
    PaperEntity requestToEntity(PaperRequest parameter);
    
}
