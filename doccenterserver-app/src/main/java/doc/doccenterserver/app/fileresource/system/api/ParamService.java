package doc.doccenterserver.app.fileresource.system.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;

import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 22:07
 */
@ApplicationService(domain = "ParamResource", name = "参数配置管理")
public interface ParamService {

    /**
     * 获取系统配置
     */
    @ReturnValue
    @Method(name = "获取系统配置")
    PageResult<List<ParamEntity>> getListParam(ParamEntity paramEntity);

    ParamEntity addParam(ParamEntity paramEntity);

    ParamEntity editParam(ParamEntity paramEntity);

    ParamEntity deleteParam(String id);

    ParamEntity getById(String id);
}
