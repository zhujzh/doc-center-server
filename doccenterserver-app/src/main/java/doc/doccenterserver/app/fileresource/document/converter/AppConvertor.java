package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.AppDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AppEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface AppConvertor {

    AppConvertor INSTANCE = Mappers.getMapper(AppConvertor.class);


    @Mappings({})
    AppEntity dtoToEntity(AppDto parameter);

    @Mappings({})
    AppDto entityToDto(AppEntity parameter);

    @Mappings({})
    List<AppEntity> dtoToEntity(List<AppDto> parameter);

    @Mappings({})
    List<AppDto> entityToDto(List<AppEntity> parameter);

    @Mappings({})
    doc.doccenterserver.client.fileresource.base.PageResult<List<AppDto>> entityToDtoPage(PageResult<List<AppEntity>> parameter);

    @Mappings({})
    AppEntity requestToEntity(PaperRequest parameter);
}
