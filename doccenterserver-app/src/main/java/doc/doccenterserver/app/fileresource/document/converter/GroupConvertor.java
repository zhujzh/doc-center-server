package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.GroupDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.GroupEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface GroupConvertor {

    GroupConvertor INSTANCE = Mappers.getMapper(GroupConvertor.class);

    @Mappings({})
    GroupEntity dtoToEntity(GroupDto parameter);

    @Mappings({})
    GroupDto entityToDto(GroupEntity parameter);

    @Mappings({})
    List<GroupEntity> dtoToEntity(List<GroupDto> parameter);

    @Mappings({})
    List<GroupDto> entityToDto(List<GroupEntity> parameter);

    @Mappings({})
    doc.doccenterserver.client.fileresource.base.PageResult<List<GroupDto>> entityToDtoPage(PageResult<List<GroupEntity>> parameter);

    @Mappings({})
    GroupEntity requestToEntity(PaperRequest parameter);
    
}
