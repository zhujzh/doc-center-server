package doc.doccenterserver.app.fileresource.system.service;

import doc.doccenterserver.app.fileresource.system.api.ParamService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.domain.fileresource.system.service.ParamDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 22:07
 */
@Component("ParamServiceAS")
@Slf4j
public class ParamServiceImpl implements ParamService {

    @Resource
    private ParamDomainService paramDomainService;


    @Override
    public PageResult<List<ParamEntity>> getListParam(ParamEntity paramEntity) {
        PageResult<List<ParamEntity>> listPageResult = paramDomainService.getListParam(paramEntity);
        return listPageResult;
    }

    @Override
    public ParamEntity addParam(ParamEntity paramEntity) {
        return paramDomainService.addParam(paramEntity);
    }

    @Override
    public ParamEntity editParam(ParamEntity paramEntity) {
        return paramDomainService.editParam(paramEntity);
    }


    @Override
    public ParamEntity deleteParam(String id) {
        return paramDomainService.deleteParam(id);
    }

    @Override
    public ParamEntity getById(String id) {
        return paramDomainService.getById(id);
    }
}
