package doc.doccenterserver.app.fileresource.document.service;

import doc.doccenterserver.app.fileresource.document.api.GroupService;
import doc.doccenterserver.app.fileresource.document.converter.GroupConvertor;
import doc.doccenterserver.client.fileresource.document.api.dto.GroupDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.GroupEntity;
import doc.doccenterserver.domain.fileresource.document.service.GroupDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@Component("GroupServiceAS")
@Slf4j
public class GroupServiceImpl implements GroupService {

    @Resource
    private GroupDomainService groupDomainService;

    @Override
    public GroupDto editInit(GroupDto dto) {
        return GroupConvertor.INSTANCE.entityToDto(groupDomainService.editInit(GroupConvertor.INSTANCE.dtoToEntity(dto)));
    }

    @Override
    public GroupDto save(GroupDto dto) {
        return GroupConvertor.INSTANCE.entityToDto(groupDomainService.save(GroupConvertor.INSTANCE.dtoToEntity(dto)));
    }

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<GroupDto>> getList(GroupDto dto) {
        GroupEntity groupEntity = GroupConvertor.INSTANCE.dtoToEntity(dto);
        PageResult<List<GroupEntity>> listPageResult = groupDomainService.getList(groupEntity);
        return GroupConvertor.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public GroupDto saveGroupMembers(GroupDto dto) {
        return GroupConvertor.INSTANCE.entityToDto(groupDomainService.saveGroupMembers(GroupConvertor.INSTANCE.dtoToEntity(dto)));
    }

    @Override
    public GroupDto delete(String groupIds) {
        return GroupConvertor.INSTANCE.entityToDto(groupDomainService.delete(groupIds));
    }

    @Override
    public GroupDto deleteGroupById(String groupId) {
        return GroupConvertor.INSTANCE.entityToDto(groupDomainService.deleteGroupById(groupId));
    }

    @Override
    public GroupDto editUserGroup(HttpServletRequest request, GroupDto dto) {
        return GroupConvertor.INSTANCE.entityToDto(groupDomainService.editUserGroup(request, GroupConvertor.INSTANCE.dtoToEntity(dto)));
    }

    @Override
    public GroupDto insertRoleGroup(GroupDto dto) {
        return GroupConvertor.INSTANCE.entityToDto(groupDomainService.insertRoleGroup(GroupConvertor.INSTANCE.dtoToEntity(dto)));
    }

    @Override
    public GroupDto selectRgByRoleId(GroupDto dto) {
        return GroupConvertor.INSTANCE.entityToDto(groupDomainService.selectRgByRoleId(GroupConvertor.INSTANCE.dtoToEntity(dto)));
    }
}
