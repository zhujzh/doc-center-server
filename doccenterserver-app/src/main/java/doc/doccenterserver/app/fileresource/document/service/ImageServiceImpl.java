package doc.doccenterserver.app.fileresource.document.service;

import doc.doccenterserver.app.fileresource.document.api.ImageService;
import doc.doccenterserver.app.fileresource.document.converter.ImageConvertor;
import doc.doccenterserver.client.fileresource.document.api.dto.ImageDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ImageEntity;
import doc.doccenterserver.domain.fileresource.document.service.ImageDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Tangcancan
 */
@Component("ImageServiceAS")
@Slf4j
public class ImageServiceImpl implements ImageService {

    @Resource
    private ImageDomainService imageDomainService;

    @Override
    public ImageDto addInit() {
        return ImageConvertor.INSTANCE.entityToDto(imageDomainService.addInit());
    }

    @Override
    public ImageDto editInit(String imgId) {
        return ImageConvertor.INSTANCE.entityToDto(imageDomainService.editInit(imgId));
    }

    @Override
    public ImageDto save(ImageDto imgDto) {
        return ImageConvertor.INSTANCE.entityToDto(imageDomainService.save(ImageConvertor.INSTANCE.dtoToEntity(imgDto)));
    }

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<ImageDto>> getList(ImageDto imgDto) {
        ImageEntity imageEntity = ImageConvertor.INSTANCE.dtoToEntity(imgDto);
        PageResult<List<ImageEntity>> listPageResult = imageDomainService.getList(imageEntity);
        return ImageConvertor.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public ImageDto delete(String imgIds) {
        return ImageConvertor.INSTANCE.entityToDto(imageDomainService.delete(imgIds));
    }

    @Override
    public ImageDto getDetail(String imgId) {
        return ImageConvertor.INSTANCE.entityToDto(imageDomainService.getDetail(imgId));
    }

    @Override
    public ImageDto downloadFile(String imgId, HttpServletRequest request, HttpServletResponse response) {
        return ImageConvertor.INSTANCE.entityToDto(imageDomainService.downloadFile(imgId, request, response));
    }

    @Override
    public ImageDto showImageTailorPage(ImageDto imgDto) {
        return ImageConvertor.INSTANCE.entityToDto(imageDomainService.showImageTailorPage(ImageConvertor.INSTANCE.dtoToEntity(imgDto)));
    }

    @Override
    public ImageDto getImageInfo(String imgId, HttpServletResponse response) {
        return ImageConvertor.INSTANCE.entityToDto(imageDomainService.getImageInfo(imgId, response));
    }

    @Override
    public ImageDto cutImage(ImageDto imgDto) {
        return ImageConvertor.INSTANCE.entityToDto(imageDomainService.cutImage(ImageConvertor.INSTANCE.dtoToEntity(imgDto)));
    }
}
