package doc.doccenterserver.app.fileresource.system.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.system.api.dto.ParameterDto;

import java.util.List;

/**
 * 系统配置服务
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@ApplicationService(domain = "FileResource", name = "系统配置服务")
public interface ParameterService {

    /**
     * 获取系统配置
     */
    @ReturnValue
    @Method(name = "获取系统配置")
    List<ParameterDto> getList();

    @ReturnValue
    @Method(name = "根据Key获取系统配置")
    ParameterDto getByKey(String key);

}
