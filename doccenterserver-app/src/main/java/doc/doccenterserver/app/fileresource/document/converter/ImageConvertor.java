package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.ImageDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ImageEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface ImageConvertor {

    ImageConvertor INSTANCE = Mappers.getMapper(ImageConvertor.class);

    @Mappings({})
    ImageEntity dtoToEntity(ImageDto parameter);

    @Mappings({})
    ImageDto entityToDto(ImageEntity parameter);

    @Mappings({})
    List<ImageEntity> dtoToEntity(List<ImageDto> parameter);

    @Mappings({})
    List<ImageDto> entityToDto(List<ImageEntity> parameter);

    @Mappings({})
    doc.doccenterserver.client.fileresource.base.PageResult<List<ImageDto>> entityToDtoPage(PageResult<List<ImageEntity>> parameter);

    @Mappings({})
    ImageEntity requestToEntity(PaperRequest parameter);
}
