package doc.doccenterserver.app.fileresource.system.api;

import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;

import java.util.Map;

/**
 * @author ch
 * @date 2023/4/20 20:20
 */
@ApplicationService(domain = "JobDetailLogResource", name = "日志管理")
public interface JobDetailLogService {

    Map<String, Object> savejobdetaillog(JobDetailLogEntity jobDetailLogEntity);
}
