package doc.doccenterserver.app.fileresource.system.service;

import doc.doccenterserver.app.fileresource.system.api.JobDetailLogService;
import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;
import doc.doccenterserver.domain.fileresource.system.service.JobDetailLogDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/20 20:25
 */
@Component("JobDetailLogServicelAS")
@Slf4j
public class JobDetailLogServiceImpl implements JobDetailLogService {
    @Resource
    private JobDetailLogDomainService jobDetailLogDomainService;

    @Override
    public Map<String, Object> savejobdetaillog(JobDetailLogEntity jobDetailLogEntity) {
        Map<String,Object> map = jobDetailLogDomainService.savejobdetaillog(jobDetailLogEntity);
        return map;
    }
}
