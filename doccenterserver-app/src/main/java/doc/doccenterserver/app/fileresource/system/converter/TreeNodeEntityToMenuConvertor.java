package doc.doccenterserver.app.fileresource.system.converter;

import doc.doccenterserver.app.fileresource.system.model.SysTreeNode;
import doc.doccenterserver.client.fileresource.document.api.dto.TreeNode;
import doc.doccenterserver.client.fileresource.system.api.dto.MenuDto;
import doc.doccenterserver.domain.fileresource.document.model.TreeNodeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author tangrongcan
 */
@Mapper
public interface TreeNodeEntityToMenuConvertor {

    TreeNodeEntityToMenuConvertor INSTANCE = Mappers.getMapper(TreeNodeEntityToMenuConvertor.class);

    @Mappings({ })
    List<MenuDto> treeNodeEntityToMenuDto(List<TreeNodeEntity> treeNodes);
}
