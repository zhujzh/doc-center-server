package doc.doccenterserver.app.fileresource.document.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.OrgTree;
import doc.doccenterserver.client.fileresource.document.api.dto.ResDto;
import doc.doccenterserver.client.fileresource.document.api.dto.RoleDto;

import java.util.List;

/**
 * @author Tangcancan
 */
@ApplicationService(domain = "RoleResource", name = "角色服务")
public interface RoleService {

    @ReturnValue
    @Method(name = "编辑初始化")
    RoleDto editInit(String roleId);

    @ReturnValue
    @Method(name = "保存操作")
    RoleDto save(RoleDto dto);

    @ReturnValue
    @Method(name = "分页查询")
    PageResult<List<RoleDto>> getList(RoleDto dto);

    @ReturnValue
    @Method(name = "根据id获取")
    RoleDto getById(String roleId);

    @ReturnValue
    @Method(name = "删除")
    RoleDto delete(String roleIds);

    @ReturnValue
    @Method(name = "获取资源树状数据")
    List<ResDto> getInitTree(String resId);

    @ReturnValue
    @Method(name = "根据角色id获取资源对象")
    RoleDto getRes(String roleId);

    @ReturnValue
    @Method(name = "更新角色的资源对象")
    RoleDto updateRes(RoleDto roleDto);

    @ReturnValue
    @Method(name = "获取角色绑定的用户对象")
    RoleDto getUser(String roleId);

    @ReturnValue
    @Method(name = "更新角色绑定的用户对象")
    RoleDto updateUser(RoleDto roleDto);

    @ReturnValue
    @Method(name = "获取组织树状数据")
    List<OrgTree> getInitOrgTree(boolean isUser);

    @ReturnValue
    @Method(name = "获取组织树状数据")
    List<OrgTree> getInitOrg(String parentId,List<String> queryType);
    @ReturnValue
    @Method(name = "获取组织树状数据")
    List<OrgTree> getInitOrgByUserName(String userName);
}
