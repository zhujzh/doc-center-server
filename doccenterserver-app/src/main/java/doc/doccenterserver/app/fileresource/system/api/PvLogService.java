package doc.doccenterserver.app.fileresource.system.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.domain.fileresource.system.model.PvLogEntity;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;

import java.util.List;

/**
 * @author zjz
 * @date 2023/4/11 22:07
 */
@ApplicationService(domain = "PvLogResource", name = "PV统计")
public interface PvLogService {

    @ReturnValue
    @Method(name = "获取PV统计")
    PageResult<List<PvLogEntity>> getListParam(PvLogEntity pvLogEntity);

    PvLogEntity add(PvLogEntity pvLogEntity);

}
