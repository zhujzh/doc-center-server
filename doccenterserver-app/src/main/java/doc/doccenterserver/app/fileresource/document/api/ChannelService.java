package doc.doccenterserver.app.fileresource.document.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.Parameter;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.ChannelDto;
import doc.doccenterserver.client.fileresource.document.api.dto.TreeNode;
import doc.doccenterserver.client.fileresource.document.api.req.ChannelRequest;

import java.util.List;

/**
 * 频道服务
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@ApplicationService(domain = "ChannelResource", name = "频道服务")
public interface ChannelService {


    @ReturnValue
    @Method(name = "编辑-初始化")
    ChannelDto getEditChannel(@Parameter(name = "频道ID") String id);

    @ReturnValue
    @Method(name = "新增-初始化")
    ChannelDto getAddChannel(String parentId);

    @ReturnValue
    @Method(name = "频道管理-列表查询")
    PageResult<List<ChannelDto>> listChannels(ChannelRequest channelRequest);

    @ReturnValue
    @Method(name = "频道管理-树结构初始化")
    List<TreeNode> findInItTree();

    @ReturnValue
    @Method(name = "频道管理-新增")
    ChannelDto addChannel(ChannelRequest channelRequest);

    @ReturnValue
    @Method(name = "频道管理-编辑")
    ChannelDto editChannel(ChannelRequest channelRequest);

    @ReturnValue
    @Method(name = "频道管理-删除")
    ChannelDto deleteChannel(String ids);

    @ReturnValue
    @Method(name = "频道管理-根据id获取频道信息")
    ChannelDto getChannel(String id);
}
