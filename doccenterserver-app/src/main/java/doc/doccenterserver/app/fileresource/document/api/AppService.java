package doc.doccenterserver.app.fileresource.document.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.AppDto;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@ApplicationService(domain = "AppResource", name = "应用服务")
public interface AppService {

    @ReturnValue
    @Method(name = "分页查询")
    PageResult<List<AppDto>> getUseDetailList(HttpServletRequest request, AppDto appDto);

    @ReturnValue
    @Method(name = "编辑初始化")
    AppDto editInit(String appId);

    @ReturnValue
    @Method(name = "保存操作")
    AppDto save(AppDto appDto);

    @ReturnValue
    @Method(name = "获取所有数据的查询")
    PageResult<List<AppDto>> getAppList(AppDto appDto);

    @ReturnValue
    @Method(name = "删除")
    AppDto delete(String appIds);

    @ReturnValue
    @Method(name = "获取明显")
    AppDto getAppDetail(String appId);

}
