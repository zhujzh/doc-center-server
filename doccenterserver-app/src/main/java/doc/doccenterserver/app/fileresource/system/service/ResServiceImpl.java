package doc.doccenterserver.app.fileresource.system.service;

import doc.doccenterserver.app.fileresource.document.converter.TreeNodeEntityToTreeNodeConvertor;
import doc.doccenterserver.app.fileresource.system.api.ResService;
import doc.doccenterserver.app.fileresource.system.converter.TreeNodeEntityToMenuConvertor;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.TreeNode;
import doc.doccenterserver.client.fileresource.system.api.dto.MenuDto;
import doc.doccenterserver.domain.fileresource.document.model.TreeNodeEntity;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import doc.doccenterserver.domain.fileresource.system.service.ResDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component("ResServiceAS")
@Slf4j
public class ResServiceImpl implements ResService {

    @Resource
    private ResDomainService resDomainService;

    @Override
    public PageResult<List<ResEntity>> getListRes(ResEntity resEntity) {
        return null;
    }

    @Override
    public List<TreeNode> findInItTree() {
        List<TreeNode> treeList = TreeNodeEntityToTreeNodeConvertor.INSTANCE.treeNodeEntityToTreeNode(
                resDomainService.findInitTree());
        return treeList.stream().filter(item -> Objects.equals(item.getPId(), "0")).peek(
                item -> item.setChildren(getChildrenList(item, treeList))).collect(Collectors.toList());
    }

    @Override
    public ResEntity getById(String id) {
        return resDomainService.getById(id);
    }

    @Override
    public ResEntity addRes(ResEntity resEntity) {
        return resDomainService.addRes(resEntity);
    }

    @Override
    public ResEntity editRes(ResEntity resEntity) {
        return resDomainService.editRes(resEntity);
    }

    @Override
    public ResEntity deleteRes(String id) {
        return resDomainService.deleteRes(id);
    }

    @Override
    public List<MenuDto> initUserTree(HttpServletRequest request) {
        List<TreeNodeEntity> treeNodeList = resDomainService.initUserTree(request);
        List<MenuDto> treeList = TreeNodeEntityToMenuConvertor.INSTANCE.treeNodeEntityToMenuDto(treeNodeList);
        return treeList.stream().filter(item -> Objects.equals(item.getPId(), "0")).peek(
                item -> item.setChildren(getChildrenMenuList(item, treeList))).collect(Collectors.toList());
    }
    private List<MenuDto> getChildrenMenuList(MenuDto menuDto, List<MenuDto> menuDtoList) {
        return menuDtoList.stream().filter(item -> item.getPId().equals(menuDto.getId())).peek(
                item -> item.setChildren(getChildrenMenuList(item, menuDtoList))).collect(Collectors.toList());
    }

    private List<TreeNode> getChildrenList(TreeNode treeNode, List<TreeNode> treeList) {
        return treeList.stream().filter(item -> item.getPId().equals(treeNode.getId())).peek(
                item -> item.setChildren(getChildrenList(item, treeList))).collect(Collectors.toList());
    }
}
