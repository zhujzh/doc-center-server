package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.OrgDto;
import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface OrgConvertor {

    OrgConvertor INSTANCE = Mappers.getMapper(OrgConvertor.class);

    @Mappings({})
    OrgEntity dtoToEntity(OrgDto parameter);

    @Mappings({})
    OrgDto entityToDto(OrgEntity parameter);

    @Mappings({})
    List<OrgEntity> dtoToEntity(List<OrgDto> parameter);

    @Mappings({})
    List<OrgDto> entityToDto(List<OrgEntity> parameter);

}
