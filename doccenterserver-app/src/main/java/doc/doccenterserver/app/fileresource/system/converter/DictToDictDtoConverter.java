package doc.doccenterserver.app.fileresource.system.converter;

import doc.doccenterserver.client.fileresource.system.api.dto.DictDto;
import doc.doccenterserver.domain.fileresource.system.model.DictEntry;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * DictToDictDOConverter
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Mapper
public interface DictToDictDtoConverter {

    DictToDictDtoConverter INSTANCE = Mappers.getMapper(DictToDictDtoConverter.class);

    /**
     * 从Dict转换到DictDO
     *
     * @param dict
     * @return DictDO
     */
    @Mappings({  })
    DictDto dictToDictDO(DictEntry dict);

    /**
     * 从DictDO转换到Dict
     *
     * @param dictDO
     * @return Dict
     */
    @Mappings({  })
    DictEntry dictDOToDict(DictDto dictDO);
}
