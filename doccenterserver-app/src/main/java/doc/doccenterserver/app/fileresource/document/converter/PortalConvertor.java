package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.PortalDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.PortalEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface PortalConvertor {

    PortalConvertor INSTANCE = Mappers.getMapper(PortalConvertor.class);

    @Mappings({})
    PortalEntity dtoToEntity(PortalDto dto);

    @Mappings({})
    PortalDto entityToDto(PortalEntity entity);

    @Mappings({})
    List<PortalEntity> dtoToEntity(List<PortalDto> dto);

    @Mappings({})
    List<PortalDto> entityToDto(List<PortalEntity> entity);

    @Mappings({})
    doc.doccenterserver.client.fileresource.base.PageResult<List<PortalDto>> entityToDtoPage(PageResult<List<PortalEntity>> page);

}
