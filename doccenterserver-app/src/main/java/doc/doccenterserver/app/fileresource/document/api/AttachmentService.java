package doc.doccenterserver.app.fileresource.document.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.AttachmentDto;

import java.util.List;

/**
 * @author Tangcancan
 */
@ApplicationService(domain = "AttachmentResource", name = "附件服务")
public interface AttachmentService {

    @ReturnValue
    @Method(name = "编辑初始化")
    AttachmentDto editInit(AttachmentDto attachmentDto);

    @ReturnValue
    @Method(name = "保存操作")
    AttachmentDto save(AttachmentDto attachmentDto);

    @ReturnValue
    @Method(name = "分页查询")
    PageResult<List<AttachmentDto>> getList(AttachmentDto attachmentDto);

    @ReturnValue
    @Method(name = "删除")
    AttachmentDto delete(AttachmentDto attachmentDto);
}
