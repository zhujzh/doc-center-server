package doc.doccenterserver.app.fileresource.system.service;

import doc.doccenterserver.app.fileresource.system.api.JoblogService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.JobLogEntry;
import doc.doccenterserver.domain.fileresource.system.service.JobLogDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/12 19:10
 */
@Component("JoblogServiceAS")
@Slf4j
public class JoblogServiceImpl implements JoblogService {

    @Resource
    private JobLogDomainService jobLogDomainService;

    @Override
    public PageResult<List<JobLogEntry>> listjobLogs(JobLogEntry jobLogEntry) {
        PageResult<List<JobLogEntry>> listPageResult = jobLogDomainService.listjobLogs(jobLogEntry);
        return listPageResult;
    }

    @Override
    public Map<String,Object> lookLogDetail(String id) {
        Map<String,Object> map = jobLogDomainService.lookLogDetail(id);
        return map;
    }

    @Override
    public Map<String, Object> viewarchmsg(String logId,Integer stepId) {
        Map<String,Object> map = jobLogDomainService.viewarchmsg(logId,stepId);
        return map;
    }

    @Override
    public Map<String, Object> rearchive(String logId) {
        Map<String,Object> map = jobLogDomainService.rearchive(logId);
        return map;
    }

    @Override
    public Map<String, Object> handlefiles(String sourcePaperId, String paperId, String appId, String optType, HttpServletRequest request) {
        Map<String,Object> map = jobLogDomainService.handlefiles(sourcePaperId,paperId,appId,optType,request);
        return map;
    }

    @Override
    public Map<String, Object> handleIndex(String logId, String paperId, String operation, String appId) {
        return jobLogDomainService.handleIndex(logId,paperId,operation,appId);
    }

    @Override
    public Map<String, Object> sendmq(String logId, Integer stepId) {
        return jobLogDomainService.sendmq(logId,stepId);
    }

    @Override
    public Map<String, Object> viewerrmsg(String logId, Integer stepId) {
        return jobLogDomainService.viewerrmsg(logId,stepId);
    }

    @Override
    public boolean retry(String logId, Integer stepId) {
        return jobLogDomainService.retry(logId,stepId);
    }
}
