package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.ChannelDto;
import doc.doccenterserver.client.fileresource.document.api.req.ChannelRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * FileToFileDtoConverter
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Mapper
public interface ChannelConverter {

    ChannelConverter INSTANCE = Mappers.getMapper(ChannelConverter.class);
    @Mappings({ })
    ChannelEntity dtoToEntity(ChannelDto parameter);
    @Mappings({ })
    ChannelDto entityToDto(ChannelEntity parameter);

    @Mappings({ })
    List<ChannelEntity> dtoToEntity(List<ChannelDto> parameter);

    @Mappings({ })
    List<ChannelDto> entityToDto(List<ChannelEntity> parameter);

    @Mappings({ })
    doc.doccenterserver.client.fileresource.base.PageResult<List<ChannelDto>> entityToDtoPage(PageResult<List<ChannelEntity>> parameter);

    @Mappings({ })
    ChannelEntity requestToEntity (ChannelRequest request);

}
