package doc.doccenterserver.app.fileresource.document.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import doc.doccenterserver.app.fileresource.document.api.RoleService;
import doc.doccenterserver.app.fileresource.document.converter.OrgConvertor;
import doc.doccenterserver.app.fileresource.document.converter.RoleConvertor;
import doc.doccenterserver.client.fileresource.document.api.dto.OrgDto;
import doc.doccenterserver.client.fileresource.document.api.dto.OrgTree;
import doc.doccenterserver.client.fileresource.document.api.dto.ResDto;
import doc.doccenterserver.client.fileresource.document.api.dto.RoleDto;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.RoleEntity;
import doc.doccenterserver.domain.fileresource.document.service.RoleDomainService;
import doc.doccenterserver.domain.fileresource.system.model.ParameterEntry;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.system.service.ParameterDomainService;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.constant.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Tangcancan
 */

@Component("RoleServiceAS")
@Slf4j
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleDomainService roleDomainService;

    @Resource
    private ParameterDomainService parameterDomainService;

    @Override
    public RoleDto editInit(String roleId) {
        return RoleConvertor.INSTANCE.entityToDto(roleDomainService.editInit(roleId));
    }

    @Override
    public RoleDto save(RoleDto dto) {
        return RoleConvertor.INSTANCE.entityToDto(roleDomainService.save(RoleConvertor.INSTANCE.dtoToEntity(dto)));
    }

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<RoleDto>> getList(RoleDto dto) {
        RoleEntity roleEntity = RoleConvertor.INSTANCE.dtoToEntity(dto);
        PageResult<List<RoleEntity>> listPageResult = roleDomainService.getList(roleEntity);
        return RoleConvertor.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public RoleDto getById(String roleId) {
        return RoleConvertor.INSTANCE.entityToDto(roleDomainService.getById(roleId));
    }

    @Override
    public RoleDto delete(String roleIds) {
        return RoleConvertor.INSTANCE.entityToDto(roleDomainService.delete(roleIds));
    }

    @Override
    public List<ResDto> getInitTree(String resId) {
        List<ResDto> resDtoList = RoleConvertor.INSTANCE.convertorTree(roleDomainService.getInitTree(resId));
        return resDtoList.stream().filter(item -> Objects.equals(item.getResId(), resId)).peek(
                item -> item.setChildren(getChildrenList(item, resDtoList))).collect(Collectors.toList());
    }

    @Override
    public RoleDto getRes(String roleId) {
        return RoleConvertor.INSTANCE.entityToDto(roleDomainService.getRes(roleId));
    }

    @Override
    public RoleDto updateRes(RoleDto roleDto) {
        return RoleConvertor.INSTANCE.entityToDto(roleDomainService.updateRes(RoleConvertor.INSTANCE.dtoToEntity(roleDto)));
    }

    @Override
    public RoleDto getUser(String roleId) {
        return RoleConvertor.INSTANCE.entityToDto(roleDomainService.getUser(roleId));
    }

    @Override
    public RoleDto updateUser(RoleDto roleDto) {
        return RoleConvertor.INSTANCE.entityToDto(roleDomainService.updateUser(RoleConvertor.INSTANCE.dtoToEntity(roleDto)));
    }

    @Override
    public List<OrgTree> getInitOrgTree(boolean isUser) {
        // 获取参数字典表里的配置值
        ParameterEntry parameterEntry = parameterDomainService.getByKey(ConstantSys.TOP_ORG_ID);
        String orgId = parameterEntry.getPValue();
        List<OrgDto> orgDtoList = OrgConvertor.INSTANCE.entityToDto(roleDomainService.getInitOrgTree(orgId));
        List<OrgTree> list = new ArrayList<>();
        orgDtoList.forEach(e -> {
            OrgTree orgTree = new OrgTree();
            orgTree.setId(e.getOrgId());
            orgTree.setLabel(e.getOrgName());
            orgTree.setPid(e.getOrgParentId());
            list.add(orgTree);
        });
        Map<String, List<UserEntity>> listMap;
        if (isUser) {
            // 获取组织下的用户信息
            List<String> orgIds = new ArrayList<>();
            for (OrgTree orgDto : list) {
                orgIds.add(orgDto.getId());
            }
            orgIds = orgIds.stream().distinct().collect(Collectors.toList());
            List<UserEntity> userList = roleDomainService.getUserByOrgIds(orgIds);
            // 用户按组织id分组
            listMap = userList.stream().collect(Collectors.groupingBy(UserEntity::getOrgId));
        } else {
            listMap = new HashMap<>();
        }
        return list.stream().filter(item -> Objects.equals(item.getId(), orgId)).peek(
                item -> item.setChildren(getOrgChildren(item, list, listMap))).collect(Collectors.toList());
    }
    @Override
    public List<OrgTree> getInitOrg(String parentId,List<String> queryType) {
        List<OrgDto> orgDtoList = OrgConvertor.INSTANCE.entityToDto(roleDomainService.getInitOrg(parentId,queryType));
        List<OrgTree> list = new ArrayList<>();
        orgDtoList.forEach(e -> {
            OrgTree orgTree = new OrgTree();
            orgTree.setId(e.getOrgId());
            orgTree.setLabel(e.getOrgName());
            orgTree.setPid(e.getOrgParentId());
            orgTree.setDeptName(e.getDeptName());
            orgTree.setLeaf(e.getOrgLeaf());
            orgTree.setDataType(e.getDataType());
            list.add(orgTree);
        });
        return list;
    }
    @Override
    public List<OrgTree> getInitOrgByUserName(String userName) {
        ParameterEntry parameterEntry = parameterDomainService.getByKey(ConstantSys.TOP_ORG_ID);
        // 判断 是否存在此配置参数
        // 组合树信息
        List<OrgTree> orgTrees = new ArrayList<>();
        if(ObjectUtil.isNotEmpty(parameterEntry) && StrUtil.isNotEmpty(parameterEntry.getPValue())){
            List<UserEntity> userList = roleDomainService.getUserByUserName(userName,Arrays.asList(parameterEntry.getPValue().split(",")));
            // 获取所有用户的组织单元id
            if(CollUtil.isNotEmpty(userList)){
                // 获取 orgUnitId 并去重
                 List<String> orgUnitIds = userList.stream().map(UserEntity::getOrgUnitId).distinct().collect(Collectors.toList());
                 List<OrgDto> orgDtoList = OrgConvertor.INSTANCE.entityToDto(roleDomainService.getOrgListTreeByUnitIds(orgUnitIds));
                // 人员
                for (UserEntity user:userList){
                    orgTrees.add(OrgTree.builder()
                            .pid(user.getOrgId())
                            .label(user.getUserName())
                            .id(user.getUserCode())
                            .deptName(user.getOrgName())
                            .build());
                }
                // 组织
                for (OrgDto org:orgDtoList){
                    orgTrees.add(OrgTree.builder()
                            .pid(org.getOrgParentId())
                            .label(org.getOrgName())
                            .id(org.getOrgId())
                            .build());
                }
            }
        }
        // 组合 Children
        if(CollUtil.isNotEmpty(orgTrees)){
            orgTrees = BuildChildren(orgTrees);
        }
        return orgTrees;
    }

    private List<OrgTree> BuildChildren(List<OrgTree> orgTrees) {
        return orgTrees.stream()
                .filter(i -> StrUtil.isEmpty(i.getPid()))
                //.filter(menu -> menu.getId() == 1)
                .peek(i -> i.setChildren(getChildrens(i, orgTrees)))
                .collect(Collectors.toList());

    }

    private List<OrgTree> getChildrens(OrgTree org, List<OrgTree> orgTrees) {

        return orgTrees.stream()
                .filter(i -> Objects.equals(i.getPid(), org.getId()))
                .peek(i -> i.setChildren(getChildrens(i, orgTrees)))
                .collect(Collectors.toList());
    }

    public List<OrgDto> filterOrg(String orgId, List<String> orgList,List<OrgDto> orgDtoList){
        List<OrgDto> list = orgDtoList.stream().filter(item->orgList.contains(item.getOrgId())).collect(Collectors.toList());
        List<String> orgListp = list.stream().map(OrgDto::getOrgParentId).collect(Collectors.toList());
        if(!orgList.contains(orgId)){
            list.addAll(filterOrg(orgId,orgListp,orgDtoList));
        }
        return list;
    }
    private List<OrgTree> getOrgChildren(OrgTree orgDto, List<OrgTree> list, Map<String, List<UserEntity>> listMap) {
        return list.stream().filter(item -> item.getPid().equals(orgDto.getId())).peek(
                item -> {
                    item.setChildren(getOrgChildren(item, list, listMap));
                    if (listMap.containsKey(item.getId())) {
                        List<OrgTree> children = item.getChildren();
                        List<UserEntity> userEntityList = listMap.get(item.getId());
                        for (UserEntity userEntity : userEntityList) {
                            OrgTree dto = new OrgTree();
                            dto.setId(userEntity.getUserCode());
                            dto.setLabel(userEntity.getUserName());
                            dto.setDeptName(userEntity.getDeptName());
                            children.add(dto);
                        }
                    }
                }).collect(Collectors.toList());
    }

    private List<ResDto> getChildrenList(ResDto resDto, List<ResDto> resDtoList) {
        return resDtoList.stream().filter(item -> item.getResParentId().equals(resDto.getResId())).peek(
                item -> item.setChildren(getChildrenList(item, resDtoList))).collect(Collectors.toList());
    }
}
