package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.AttachmentDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */

@Mapper
public interface AttachmentConvertor {

    AttachmentConvertor INSTANCE = Mappers.getMapper(AttachmentConvertor.class);

    @Mappings({})
    AttachmentEntity dtoToEntity(AttachmentDto parameter);

    @Mappings({})
    AttachmentDto entityToDto(AttachmentEntity parameter);

    @Mappings({})
    List<AttachmentEntity> dtoToEntity(List<AttachmentDto> parameter);

    @Mappings({})
    List<AttachmentDto> entityToDto(List<AttachmentEntity> parameter);

    @Mappings({})
    doc.doccenterserver.client.fileresource.base.PageResult<List<AttachmentDto>> entityToDtoPage(PageResult<List<AttachmentEntity>> parameter);

    @Mappings({})
    AttachmentEntity requestToEntity(PaperRequest parameter);
}
