package doc.doccenterserver.app.fileresource.system.service;

import doc.doccenterserver.app.fileresource.system.api.PaperManageService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.domain.fileresource.system.service.JobLogDomainService;
import doc.doccenterserver.domain.fileresource.system.service.PaperManageDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 15:58
 */
@Component("PaperManageServiceAS")
@Slf4j
public class PaperManageServiceImpl implements PaperManageService {


    @Resource
    private PaperManageDomainService paperManageDomainService;

    @Override
    public PageResult<List<PaperEntity>> listpapers(PaperEntity paperEntity) {
        PageResult<List<PaperEntity>> listPageResult = paperManageDomainService.listpapers(paperEntity);
        return listPageResult;
    }

    @Override
    public Map<String, Object> topdoc(String paperId, Boolean topStatus) {
        return paperManageDomainService.topdoc(paperId,topStatus);
    }

    @Override
    public Map<String, Object> editpaper(String paperId,HttpServletRequest request) {
        return paperManageDomainService.editpaper(paperId,request);
    }

    @Override
    public Map<String, Object> savePaper(PaperEntity paperEntity, HttpServletRequest request) {
        return paperManageDomainService.savePaper(paperEntity,request);
    }
}
