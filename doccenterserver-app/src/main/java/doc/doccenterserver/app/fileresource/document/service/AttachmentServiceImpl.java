package doc.doccenterserver.app.fileresource.document.service;

import doc.doccenterserver.app.fileresource.document.api.AttachmentService;
import doc.doccenterserver.app.fileresource.document.converter.AttachmentConvertor;
import doc.doccenterserver.client.fileresource.document.api.dto.AttachmentDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import doc.doccenterserver.domain.fileresource.document.service.AttachmentDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Component("AttachmentServiceAS")
@Slf4j
public class AttachmentServiceImpl implements AttachmentService {

    @Resource
    private AttachmentDomainService attachmentDomainService;

    @Override
    public AttachmentDto editInit(AttachmentDto attachmentDto) {
        return AttachmentConvertor.INSTANCE.entityToDto(attachmentDomainService.editInit(AttachmentConvertor.INSTANCE.dtoToEntity(attachmentDto)));
    }

    @Override
    public AttachmentDto save(AttachmentDto attachmentDto) {
        return AttachmentConvertor.INSTANCE.entityToDto(attachmentDomainService.save(AttachmentConvertor.INSTANCE.dtoToEntity(attachmentDto)));
    }

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<AttachmentDto>> getList(AttachmentDto attachmentDto) {
        AttachmentEntity attachmentEntity = AttachmentConvertor.INSTANCE.dtoToEntity(attachmentDto);
        PageResult<List<AttachmentEntity>> listPageResult = attachmentDomainService.getList(attachmentEntity);
        return AttachmentConvertor.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public AttachmentDto delete(AttachmentDto attachmentDto) {
        return AttachmentConvertor.INSTANCE.entityToDto(attachmentDomainService.delete(AttachmentConvertor.INSTANCE.dtoToEntity(attachmentDto)));
    }
}
