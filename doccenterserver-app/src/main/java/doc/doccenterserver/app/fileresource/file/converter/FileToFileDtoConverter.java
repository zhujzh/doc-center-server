package doc.doccenterserver.app.fileresource.file.converter;

import doc.doccenterserver.client.fileresource.file.api.dto.FileDto;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * FileToFileDtoConverter
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Mapper
public interface FileToFileDtoConverter {

    FileToFileDtoConverter INSTANCE = Mappers.getMapper(FileToFileDtoConverter.class);


    /**
     * 从FileDto转换到File
     *
     * @param fileDto
     * @return File
     */
    @Mappings({ @Mapping(source = "filename", target = "filename") })
    FileEntity fileDtoToFile(FileDto fileDto);

    /**
     * 从File转换到FileDto
     *
     * @param file
     * @return FileDto
     */
    @Mappings({ @Mapping(source = "filename", target = "filename") })
    FileDto fileToDictFile(FileEntity file);
}
