package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.ResDto;
import doc.doccenterserver.client.fileresource.document.api.dto.RoleDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.RoleEntity;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface RoleConvertor {

    RoleConvertor INSTANCE = Mappers.getMapper(RoleConvertor.class);


    @Mappings({})
    RoleEntity dtoToEntity(RoleDto parameter);

    @Mappings({})
    RoleDto entityToDto(RoleEntity parameter);

    @Mappings({})
    List<RoleEntity> dtoToEntity(List<RoleDto> parameter);

    @Mappings({})
    List<RoleDto> entityToDto(List<RoleEntity> parameter);

    @Mappings({})
    doc.doccenterserver.client.fileresource.base.PageResult<List<RoleDto>> entityToDtoPage(PageResult<List<RoleEntity>> parameter);

    @Mappings({})
    RoleEntity requestToEntity(PaperRequest parameter);

    @Mappings({})
    ResDto convertorTree(ResEntity parameter);

    @Mappings({})
    List<ResDto> convertorTree(List<ResEntity> parameter);
}
