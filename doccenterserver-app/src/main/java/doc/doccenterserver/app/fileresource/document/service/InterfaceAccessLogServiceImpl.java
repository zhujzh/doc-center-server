package doc.doccenterserver.app.fileresource.document.service;

import doc.doccenterserver.app.fileresource.document.api.InterfaceAccessLogService;
import doc.doccenterserver.app.fileresource.document.converter.AppConvertor;
import doc.doccenterserver.app.fileresource.document.converter.InterfaceAccessLogConvertor;
import doc.doccenterserver.client.fileresource.document.api.dto.AppDto;
import doc.doccenterserver.client.fileresource.document.api.dto.InterfaceAccessLogDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AppEntity;
import doc.doccenterserver.domain.fileresource.document.model.InterfaceAccessLogEntity;
import doc.doccenterserver.domain.fileresource.document.service.InterfaceAccessLogDomainService;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author Tangcancan
 */
@Component("InterfaceAccessLogServiceAS")
@Slf4j
public class InterfaceAccessLogServiceImpl implements InterfaceAccessLogService {

    @Resource
    private InterfaceAccessLogDomainService interfaceAccessLogDomainService;

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<InterfaceAccessLogDto>> getInterfaceAccessLogList(
            HttpServletRequest request, InterfaceAccessLogDto interfaceAc) {
        InterfaceAccessLogEntity interfaceAccessLogEntity = InterfaceAccessLogConvertor.INSTANCE.dtoToEntity(interfaceAc);
        PageResult<List<InterfaceAccessLogEntity>> listPageResult = interfaceAccessLogDomainService.getInterfaceAccessLogList(
                request, interfaceAccessLogEntity);
        return InterfaceAccessLogConvertor.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public InterfaceAccessLogDto getInterfaceAccessLog(String logId) {
        return InterfaceAccessLogConvertor.INSTANCE.entityToDto(interfaceAccessLogDomainService.getInterfaceAccessLog(logId));
    }

    @Override
    public InterfaceAccessLogDto getInit(HttpServletRequest request) {
        InterfaceAccessLogEntity init = interfaceAccessLogDomainService.getInit(request);
        InterfaceAccessLogDto interfaceAccessLogDto = InterfaceAccessLogConvertor.INSTANCE.entityToDto(init);
        List<AppEntity> appEntityList = init.getAppEntityList();
        List<AppDto> appDtoList = AppConvertor.INSTANCE.entityToDto(appEntityList);
        interfaceAccessLogDto.setAppDtoList(appDtoList);
        return interfaceAccessLogDto;
    }

    @Override
    public InterfaceAccessLogDto insert(HttpServletRequest request, String str) {
        InterfaceAccessLogDto interfaceAccessLogDto = new InterfaceAccessLogDto();
        interfaceAccessLogDto.setParams(str);
        interfaceAccessLogDto.setCallTime(new Date());
        interfaceAccessLogDto.setResultStatus("Y");
        if (StringUtil.isEmpty(request.getParameter("appname"))) {
            interfaceAccessLogDto.setAppname("文档中心_集团");
        } else {
            interfaceAccessLogDto.setAppname(request.getParameter("appname"));
        }
        interfaceAccessLogDto.setRequestUrl(request.getRequestURL().toString());
        interfaceAccessLogDto.setRequestIp(getIpAddress(request));
        return InterfaceAccessLogConvertor.INSTANCE.entityToDto(interfaceAccessLogDomainService.insert(
                InterfaceAccessLogConvertor.INSTANCE.dtoToEntity(interfaceAccessLogDto)));
    }

    private String getIpAddress(HttpServletRequest request) {
        String sourceIp = null;

        String ipAddresses = request.getHeader("x-forwarded-for");

        if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            ipAddresses = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            ipAddresses = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            ipAddresses = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            ipAddresses = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses)) {
            ipAddresses = request.getRemoteAddr();
        }
        if (!StringUtil.isEmpty(ipAddresses)) {
            sourceIp = ipAddresses.split(",")[0];
        }

        return sourceIp;
    }

}
