package doc.doccenterserver.app.fileresource.document.service;

import doc.doccenterserver.app.fileresource.document.api.ChannelService;
import doc.doccenterserver.app.fileresource.document.converter.ChannelConverter;
import doc.doccenterserver.app.fileresource.document.converter.TreeNodeEntityToTreeNodeConvertor;
import doc.doccenterserver.client.fileresource.document.api.dto.ChannelDto;
import doc.doccenterserver.client.fileresource.document.api.dto.TreeNode;
import doc.doccenterserver.client.fileresource.document.api.req.ChannelRequest;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.document.service.ChannelDomainService;
import doc.doccenterserver.domain.fileresource.system.model.ParameterEntry;
import doc.doccenterserver.domain.fileresource.system.service.ParameterDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Tangcancan
 */
@Component("ChannelServiceAS")
@Slf4j
public class ChannelServiceImpl implements ChannelService {

    @Resource
    private ChannelDomainService channelDomainService;

    @Resource
    private ParameterDomainService parameterDomainService;

    @Override
    public ChannelDto getEditChannel(String id) {
        return ChannelConverter.INSTANCE.entityToDto(channelDomainService.getEditChannel(id));
    }

    @Override
    public ChannelDto getAddChannel(String parentId) {
        return ChannelConverter.INSTANCE.entityToDto(channelDomainService.getAddChannel(parentId));
    }

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<ChannelDto>> listChannels(ChannelRequest channelRequest) {
        ChannelEntity channelEntity = ChannelConverter.INSTANCE.requestToEntity(channelRequest);
        PageResult<List<ChannelEntity>> listPageResult = channelDomainService.selectByPage(channelEntity);
        return ChannelConverter.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public List<TreeNode> findInItTree() {
        // 获取参数字典表里的配置值
        ParameterEntry parameterEntry = parameterDomainService.getByKey(ConstantSys.TOP_CHANNEL_ID);
        String parentId = parameterEntry.getPValue();
        ChannelEntity channel = channelDomainService.getChannel(parentId);
        String topChannelId = channel.getParentId();
        List<TreeNode> treeList = TreeNodeEntityToTreeNodeConvertor.INSTANCE.treeNodeEntityToTreeNode(
                channelDomainService.findInitTree(parentId));
        // 优化树状结构
        return treeList.stream().filter(item -> Objects.equals(item.getPId(), topChannelId) && Objects.equals(item.getId(), parentId)).peek(
                item -> item.setChildren(getChildrenList(item, treeList))).collect(Collectors.toList());
    }

    private List<TreeNode> getChildrenList(TreeNode treeNode, List<TreeNode> treeList) {
        return treeList.stream().filter(item -> item.getPId().equals(treeNode.getId())).peek(
                item -> item.setChildren(getChildrenList(item, treeList))).collect(Collectors.toList());
    }

    @Override
    public ChannelDto addChannel(ChannelRequest channelRequest) {
        ChannelEntity channelEntity = ChannelConverter.INSTANCE.requestToEntity(channelRequest);
        return ChannelConverter.INSTANCE.entityToDto(channelDomainService.addChannel(channelEntity));
    }

    @Override
    public ChannelDto editChannel(ChannelRequest channelRequest) {
        ChannelEntity channelEntity = ChannelConverter.INSTANCE.requestToEntity(channelRequest);
        return ChannelConverter.INSTANCE.entityToDto(channelDomainService.editChannel(channelEntity));
    }

    @Override
    public ChannelDto deleteChannel(String ids) {
        return ChannelConverter.INSTANCE.entityToDto(channelDomainService.deleteChannel(ids));
    }

    @Override
    public ChannelDto getChannel(String id) {
        return ChannelConverter.INSTANCE.entityToDto(channelDomainService.getChannel(id));
    }
}
