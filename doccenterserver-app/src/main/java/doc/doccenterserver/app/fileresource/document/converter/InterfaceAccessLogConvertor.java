package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.InterfaceAccessLogDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.InterfaceAccessLogEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface InterfaceAccessLogConvertor {

    InterfaceAccessLogConvertor INSTANCE = Mappers.getMapper(InterfaceAccessLogConvertor.class);

    @Mappings({})
    InterfaceAccessLogEntity dtoToEntity(InterfaceAccessLogDto parameter);

    @Mappings({})
    InterfaceAccessLogDto entityToDto(InterfaceAccessLogEntity parameter);

    @Mappings({})
    List<InterfaceAccessLogEntity> dtoToEntity(List<InterfaceAccessLogDto> parameter);

    @Mappings({})
    List<InterfaceAccessLogDto> entityToDto(List<InterfaceAccessLogEntity> parameter);

    @Mappings({})
    doc.doccenterserver.client.fileresource.base.PageResult<List<InterfaceAccessLogDto>> entityToDtoPage(PageResult<List<InterfaceAccessLogEntity>> parameter);

    @Mappings({})
    InterfaceAccessLogEntity requestToEntity(PaperRequest parameter);

}
