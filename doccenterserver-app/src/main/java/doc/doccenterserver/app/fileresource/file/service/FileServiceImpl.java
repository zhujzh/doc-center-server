package doc.doccenterserver.app.fileresource.file.service;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import doc.doccenterserver.app.fileresource.file.api.FileService;
import doc.doccenterserver.app.fileresource.file.converter.FileToFileDtoConverter;
import doc.doccenterserver.client.fileresource.file.api.dto.FileDto;
import doc.doccenterserver.client.fileresource.file.api.req.FileOldUploadRequest;
import doc.doccenterserver.client.fileresource.file.api.req.FileRequest;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import doc.doccenterserver.domain.fileresource.file.service.FileDomainService;
import doc.doccenterserver.domain.fileresource.system.service.ParameterDomainService;
import doc.doccenterserver.infrastructure.fileresource.file.utils.MultipartFileUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.rmi.ServerException;
import java.util.Date;
import java.util.List;

/**
 * 字典服务实现类
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Component("FILEFileServiceAS")
@Slf4j
public class FileServiceImpl implements FileService {

    @Resource
    private FileDomainService dictDomainService;
    @Resource
    private ParameterDomainService parameterDomainService;
    @Override
    public FileDto getFileById(String fileId) {
        return FileToFileDtoConverter.INSTANCE.fileToDictFile(dictDomainService.getFileById(fileId));
    }

    @Override
    public String md5Check(FileRequest fileRequest) throws Exception {
        String info = "";
        boolean flag = true;
        if (StringUtils.isEmpty(fileRequest.getAppId())) {
            flag = false;
            info = "appid:应用编号不能为空；";
        }
        if (StringUtils.isEmpty(fileRequest.getMd5())) {
            flag = false;
            info += "md5:文件md5值不能为空";
        }
        if (!flag) {
            throw new ServerException(info);
        }
        List<FileEntity> fileList = dictDomainService.md5Check(fileRequest.getMd5());
        if (fileList.isEmpty()) {
            throw new ServerException("文件在服务器上不存在");
        }
        FileEntity file = fileList.get(0);
        return file.getFileId();
    }

    @Override
    public FileDto upload(MultipartFile mFile , FileOldUploadRequest fileOldUploadRequest) throws Exception {
        if(StringUtils.isEmpty(fileOldUploadRequest.getMd5())){
            //throw new ServerException("md5:文件md5值不能为空");
        }
        //上传文件
        File file = MultipartFileUtil.multipartFileToFIle(mFile);
        if (file == null) {
            throw new ServerException("请选择上传文件");
        }
        String fileId = IdUtil.simpleUUID();
        String bigdataFileId = dictDomainService.uploadFile(mFile);
        MultipartFileUtil.deleteTempFile(file);
        String fileName = mFile.getOriginalFilename();
        fileName = java.net.URLDecoder.decode(fileName, "UTF-8");
        if (fileName.length() > 100) {
            throw new ServerException("文件名称超长");
        }
        String md5 = fileOldUploadRequest.getMd5();
        String ext = FilenameUtils.getExtension(fileName);
        String filePath = md5 + "." + ext;
        String fileUrl = parameterDomainService.getByKeyValue("BIG_DATA_DOWNLOAD_URL") + fileId;
        FileDto fileDto = new FileDto();
        fileDto.setFileMd5(md5);
        fileDto.setFileId(fileId);
        fileDto.setFileFormat(ext);
        fileDto.setFileSize((double) mFile.getSize());
        fileDto.setFileUrl(fileUrl);
        fileDto.setFilename(fileName);
        fileDto.setFilePath(fileOldUploadRequest.getAppId() + "/" + filePath);
        fileDto.setAppId(fileOldUploadRequest.getAppId());
        fileDto.setLabelName(fileOldUploadRequest.getLabelName());
        fileDto.setBigdataFileId(bigdataFileId);
        fileDto.setCreateDate(new Date());
        dictDomainService.insertFile(FileToFileDtoConverter.INSTANCE.fileDtoToFile(fileDto));
        return fileDto;
    }

    @Override
    public void download(FileOldUploadRequest fileOldUploadRequest, HttpServletResponse response) throws Exception {
        if (StrUtil.isEmpty(fileOldUploadRequest.getFileId())) {
            throw new ServerException("请填写文件ID");
        }
        List<FileEntity> fileList = dictDomainService.downloadFileList(fileOldUploadRequest.getFileId());
        if (fileList.isEmpty()) {
            throw new ServerException("文件不存在");
        }
        FileEntity fileEntity = fileList.get(0);
        String ext = fileEntity.getFileFormat();
        String name = fileOldUploadRequest.getFileName();
        String type = fileOldUploadRequest.getDownload();
        if (StrUtil.isBlank(name)) {
            name = fileEntity.getFilename();
        }
        if ("jpg".equalsIgnoreCase(ext) || "png".equalsIgnoreCase(ext) || "bmp".equalsIgnoreCase(ext)
                || "jpeg".equalsIgnoreCase(ext)) {
            type = "no";
        } else if (StrUtil.isBlank(type)) {
            type = "yes";
        }
        dictDomainService.downloadFile(response, fileEntity.getBigdataFileId(), type, name);
    }

}
