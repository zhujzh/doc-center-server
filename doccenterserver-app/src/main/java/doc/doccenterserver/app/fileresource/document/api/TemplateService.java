package doc.doccenterserver.app.fileresource.document.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.TemplateDto;
import doc.doccenterserver.client.fileresource.document.api.req.TemplateRequest;

import java.util.List;

/**
 * @author Tangcancan
 */
@ApplicationService(domain = "TemplateResource", name = "模板服务")
public interface TemplateService {

    @ReturnValue
    @Method(name = "获取模板-列表")
    PageResult<List<TemplateDto>> getListTemplates(TemplateRequest templateRequest);

    @ReturnValue
    @Method(name = "获取模板-单个")
    TemplateDto getTemplate(Integer id);

    @ReturnValue
    @Method(name = "新增模板")
    TemplateDto addTemplate(TemplateRequest templateRequest);

    @ReturnValue
    @Method(name = "编辑模板")
    TemplateDto editTemplate(TemplateRequest templateRequest);

    @ReturnValue
    @Method(name = "删除模板")
    TemplateDto deleteTemplate(String ids);
}
