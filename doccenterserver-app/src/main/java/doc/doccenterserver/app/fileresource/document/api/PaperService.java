package doc.doccenterserver.app.fileresource.document.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.PaperDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@ApplicationService(domain = "PaperResource", name = "频道服务")
public interface PaperService {

    @ReturnValue
    @Method(name = "文档管理，根据id，分页查询paper数据-列表查询")
    PageResult<List<PaperDto>> listPapers(HttpServletRequest request, PaperRequest paperRequest);

    @ReturnValue
    @Method(name = "主页-查询初始化")
    PaperDto getListPaper(HttpServletRequest request, String classId);

    @ReturnValue
    @Method(name = "查询文档内容 - 详细")
    PaperDto getPaperDetail(String paperId, HttpServletRequest request);

    @ReturnValue
    @Method(name = "新增初始化")
    PaperDto getAddPaper(String classId, HttpServletRequest request);

    @ReturnValue
    @Method(name = "新增Paper对象")
    PaperDto addPaper(HttpServletRequest request, PaperRequest paperRequest);

    @ReturnValue
    @Method(name = "编辑初始化")
    PaperDto getEditPaper(String paperId, HttpServletRequest request);

    @ReturnValue
    @Method(name = "编辑Paper对象")
    PaperDto editPaper(HttpServletRequest request, PaperRequest paperRequest);

    @ReturnValue
    @Method(name = "置顶或取消置顶文档")
    PaperDto topDoc(PaperRequest paperRequest);

    @ReturnValue
    @Method(name = "置顶排序")
    PaperDto topDocSort(PaperRequest paperRequest);

    @ReturnValue
    @Method(name = "预览文档")
    PaperDto previewPaper(PaperRequest paperRequest, HttpServletRequest request);

    @ReturnValue
    @Method(name = "批量删除Paper对象")
    PaperDto deletePaper(String ids);

    @ReturnValue
    @Method(name = "门户页面统一查询入口")
    PageResult<List<PaperDto>> listfrontpapers(PaperRequest paperRequest, String classid, String ccid, HttpServletRequest request);

    @ReturnValue
    @Method(name = "门户页面统一查询入口")
    PaperDto papersQueryRet(PaperRequest paperRequest, String classId, String ccId, HttpServletRequest request);

    PaperDto getInit(String classId);

    PaperDto getPreviewInfo(String paperId, HttpServletRequest request);

}
