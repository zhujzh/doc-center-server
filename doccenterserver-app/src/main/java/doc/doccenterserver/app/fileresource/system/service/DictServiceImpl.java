package doc.doccenterserver.app.fileresource.system.service;

import doc.doccenterserver.app.fileresource.document.converter.TreeNodeEntityToTreeNodeConvertor;
import doc.doccenterserver.app.fileresource.system.model.SysTreeNode;
import doc.doccenterserver.app.fileresource.system.converter.DictToDictDtoConverter;
import doc.doccenterserver.app.fileresource.system.api.DictService;
import doc.doccenterserver.client.fileresource.system.api.dto.DictDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.DictEntry;
import doc.doccenterserver.domain.fileresource.system.service.DictDomainService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.rmi.ServerException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 字典服务实现类
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Component("SYSTEMDictServiceAS")
public class DictServiceImpl implements DictService {

    @Resource
    private DictDomainService dictDomainService;

    @Override
    public DictDto getDictList(String pid) {
        DictEntry dict = dictDomainService.selectDict(pid);
        return DictToDictDtoConverter.INSTANCE.dictToDictDO(dict);
    }

    @Override
    public List<SysTreeNode> findInItTree(String pid) {
        List<SysTreeNode> treeList = TreeNodeEntityToTreeNodeConvertor.INSTANCE.sysTreeNodeEntityToTreeNode(dictDomainService.findInitTree(pid));
        return treeList.stream().filter(item -> Objects.equals(item.getPId(), pid)).peek(
                item -> item.setChildren(getChildrenList(item, treeList))).collect(Collectors.toList());
    }

    @Override
    public PageResult<List<DictEntry>> listDict(DictEntry dictEntry) {
        PageResult<List<DictEntry>> listPageResult = dictDomainService.getListDict(dictEntry);
        return listPageResult;
    }

    @Override
    public DictEntry getById(String id) {
        return dictDomainService.getById(id);
    }

    @Override
    public DictEntry editDict(DictEntry dictEntry) {
        return dictDomainService.editDict(dictEntry);
    }

    @Override
    public DictEntry deleteDict(String id) {
        return dictDomainService.deleteDict(id);
    }

    @Override
    public DictEntry addDict(DictEntry dictEntry) throws ServerException {
        DictEntry dict= dictDomainService.getById(dictEntry.getId());
        if(dict != null){
            throw new ServerException("字典编码重复，保存失败！");
        }
        return dictDomainService.addDict(dictEntry);
    }

    @Override
    public List<DictEntry> getByPId(String pid) {
        return dictDomainService.getByPId(pid);
    }

    @Override
    public DictEntry getByPIdAndVal(String pid, String val) {
        return dictDomainService.getByPIdAndVal(pid,val);
    }

    private List<SysTreeNode> getChildrenList(SysTreeNode treeNode, List<SysTreeNode> treeList) {
        return treeList.stream().filter(item -> item.getPId().equals(treeNode.getId())).peek(
                item -> item.setChildren(getChildrenList(item, treeList))).collect(Collectors.toList());
    }
}
