package doc.doccenterserver.app.fileresource.system.converter;

import doc.doccenterserver.client.fileresource.system.api.dto.ParameterDto;
import doc.doccenterserver.domain.fileresource.system.model.ParameterEntry;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * ParameterEntryToParameterDtoConverter
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Mapper
public interface ParameterEntryToParameterDtoConverter {

    ParameterEntryToParameterDtoConverter INSTANCE = Mappers.getMapper(ParameterEntryToParameterDtoConverter.class);

    @Mappings({})
    ParameterDto parameterEntryToParameterDto(ParameterEntry parameterEntry);
    @Mappings({})
    ParameterEntry parameterDtoToParameterEntry(ParameterDto parameterDO);

    @Mappings({})
    List<ParameterDto> parameterEntryToParameterDto(List<ParameterEntry> parameterEntry);
    @Mappings({})
    List<ParameterEntry> parameterDtoToParameterEntry(List<ParameterDto> parameterDO);
}
