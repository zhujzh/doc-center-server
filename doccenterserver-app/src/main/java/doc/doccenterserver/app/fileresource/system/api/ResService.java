package doc.doccenterserver.app.fileresource.system.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.TreeNode;
import doc.doccenterserver.client.fileresource.system.api.dto.MenuDto;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 频道服务
 *
 * @author changgao
 * @date 2023年4月10日17:18:28
 */
@ApplicationService(domain = "ResResource", name = "资源管理")
public interface ResService {
    @ReturnValue
    @Method(name = "资源管理-列表")
    PageResult<List<ResEntity>> getListRes(ResEntity resEntity);

    @ReturnValue
    @Method(name = "资源管理-树结构初始化")
    List<TreeNode> findInItTree();

    ResEntity getById(String id);

    ResEntity addRes(ResEntity resEntity);

    ResEntity editRes(ResEntity resEntity);

    ResEntity deleteRes(String id);

    List<MenuDto> initUserTree(HttpServletRequest request);
}
