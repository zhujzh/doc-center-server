package doc.doccenterserver.app.fileresource.system.model;

import com.alibaba.bizworks.core.specification.StructureObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "TreeNode", desc = "TreeNode")
public class SysTreeNode {
    private String id;
    private String pId;
    private String name;
    private Boolean checked;
    private Boolean open;
    private Boolean isParent;
    private String type;
    private String isAccessTopMenu;
    private String iconSkin = "orgIcon";
    //使用图片路径引发相对路径不一致问题，统一改良为图标样式引用，样式在ztree主样式文件中定义即可
    public static final String ORG_ICON = "orgIcon";
    public static final String USER_ICON = "userIcon";
    public static final String GROUP_ICON = "groupIcon";
    private String mobiles;
    private String mobileCount;
    private String orgLevel;
    private String companyId;//所属公司id
    private String companyName;//所属公司名称
    private String deptId;//所属一级部门id
    private String deptName;//所属一级部门名称
    private String orgId;//所属部门id
    private String orgName;//所属部门名称

    private List<SysTreeNode> children;

}
