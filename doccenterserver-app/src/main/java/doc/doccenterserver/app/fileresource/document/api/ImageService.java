package doc.doccenterserver.app.fileresource.document.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.ImageDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Tangcancan
 */
@ApplicationService(domain = "ImageResource", name = "xx服务")
public interface ImageService {

    @ReturnValue
    @Method(name = "新增初始化")
    ImageDto addInit();

    @ReturnValue
    @Method(name = "编辑初始化")
    ImageDto editInit(String imgId);

    @ReturnValue
    @Method(name = "保存操作")
    ImageDto save(ImageDto imgDto);

    @ReturnValue
    @Method(name = "分页查询")
    PageResult<List<ImageDto>> getList(ImageDto imgDto);

    @ReturnValue
    @Method(name = "删除")
    ImageDto delete(String imgIds);

    @ReturnValue
    @Method(name = "获取明细")
    ImageDto getDetail(String imgId);

    @ReturnValue
    @Method(name = "下载文件")
    ImageDto downloadFile(String imgId, HttpServletRequest request, HttpServletResponse response);

    @ReturnValue
    @Method(name = "展示")
    ImageDto showImageTailorPage(ImageDto imgDto);

    @ReturnValue
    @Method(name = "获取信息")
    ImageDto getImageInfo(String imgId, HttpServletResponse response);

    @ReturnValue
    @Method(name = "裁剪")
    ImageDto cutImage(ImageDto imgDto);
}
