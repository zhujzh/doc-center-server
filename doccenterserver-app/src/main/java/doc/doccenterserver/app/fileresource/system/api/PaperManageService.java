package doc.doccenterserver.app.fileresource.system.api;

import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 15:57
 */
@ApplicationService(domain = "PaperManageResource", name = "文档维护")
public interface PaperManageService {
    PageResult<List<PaperEntity>> listpapers(PaperEntity paperEntity);

    Map<String, Object> topdoc(String paperId, Boolean topStatus);

    Map<String, Object> editpaper(String paperId, HttpServletRequest request);

    Map<String, Object> savePaper(PaperEntity paperEntity, HttpServletRequest request);
}
