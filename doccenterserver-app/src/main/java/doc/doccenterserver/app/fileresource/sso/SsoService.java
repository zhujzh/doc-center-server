package doc.doccenterserver.app.fileresource.sso;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.app.fileresource.sso.model.SsoDTO;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wangxp
 * @description 认证服务
 * @date 2023/3/29 19:50
 */
@ApplicationService(domain = "USER", name = "认证服务")
public interface SsoService {


    @Method(desc = "sso认证回调", name = "sso认证回调")
    SsoDTO ssoCallback(String code, String requestUrl);

    @Method(desc = "退出登录", name = "退出登录")
    void loginOut(HttpServletRequest request);

}
