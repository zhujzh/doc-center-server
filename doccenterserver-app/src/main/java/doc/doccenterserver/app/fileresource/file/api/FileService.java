package doc.doccenterserver.app.fileresource.file.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.Parameter;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.file.api.dto.FileDto;
import doc.doccenterserver.client.fileresource.file.api.req.FileOldUploadRequest;
import doc.doccenterserver.client.fileresource.file.api.req.FileRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 文件服务
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@ApplicationService(domain = "FileResource", name = "文件服务")
public interface FileService {

    /**
     * 获取当个文件
     *
     * @param fileId
     * @return FileDto
     */
    @ReturnValue
    @Method(name = "获取当个文件")
    FileDto getFileById(@Parameter(name = "文件ID", required = false) String fileId);

    /**
     * 文件校验
     * @param fileRequest
     * @return String
     */
    @ReturnValue
    @Method(name = "文件校验")
    String md5Check(@Parameter(name = "文件REQ", required = false) FileRequest fileRequest) throws Exception;

    /**
     * 文件上传
     * @param fileOldUploadRequest
     * @return String
     */
    @ReturnValue
    @Method(name = "文件上传")
    FileDto upload(@Parameter(name = "选择文件", required = false) MultipartFile multipartFile, @Parameter(name = "旧文档中心REQ", required = false) FileOldUploadRequest fileOldUploadRequest) throws Exception;

    /**
     * 文件下载
     * @param fileOldUploadRequest
     * @return String
     */
    @ReturnValue
    @Method(name = "文件下载")
    void download(@Parameter(name = "旧文档中心REQ", required = false) FileOldUploadRequest fileOldUploadRequest, HttpServletResponse response) throws Exception;

}
