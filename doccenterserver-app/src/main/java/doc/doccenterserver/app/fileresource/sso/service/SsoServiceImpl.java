package doc.doccenterserver.app.fileresource.sso.service;

import cn.hutool.core.bean.BeanUtil;
import doc.doccenterserver.app.fileresource.sso.SsoService;
import doc.doccenterserver.app.fileresource.sso.model.SsoDTO;
import doc.doccenterserver.domain.fileresource.sso.vo.SsoResVo;
import doc.doccenterserver.domain.fileresource.system.service.SsoDomainService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author wangxp
 * @description sso认证回调
 * @date 2023/3/29 19:52
 */
@Component("ssoServiceImpl")
public class SsoServiceImpl implements SsoService {


    @Resource
    private SsoDomainService userDomainService;

    @Override
    public SsoDTO ssoCallback(String code, String requestUrl) {
        SsoResVo ssoResVo = userDomainService.ssoCallback(code,requestUrl);
        return BeanUtil.copyProperties(ssoResVo,SsoDTO.class);
    }

    @Override
    public void loginOut(HttpServletRequest request) {
        userDomainService.loginOut(request);
    }

}
