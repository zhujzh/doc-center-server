package doc.doccenterserver.app.fileresource.document.service;

import doc.doccenterserver.app.fileresource.document.api.PortalService;
import doc.doccenterserver.app.fileresource.document.converter.PortalConvertor;
import doc.doccenterserver.client.fileresource.document.api.dto.PortalDto;
import doc.doccenterserver.domain.fileresource.document.model.PortalEntity;
import doc.doccenterserver.domain.fileresource.document.service.PortalDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */

@Component("PortalServiceAS")
@Slf4j
public class PortalServiceImpl implements PortalService {

    @Resource
    private PortalDomainService portalDomainService;

    @Override
    public List<PortalDto> getPortal(PortalDto portalDto, HttpServletRequest request) {
        List<PortalEntity> result = portalDomainService.getPortal(PortalConvertor.INSTANCE.dtoToEntity(portalDto), request);
        return PortalConvertor.INSTANCE.entityToDto(result);
    }
}
