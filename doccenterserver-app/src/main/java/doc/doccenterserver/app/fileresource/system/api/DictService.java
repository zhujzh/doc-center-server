package doc.doccenterserver.app.fileresource.system.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.Parameter;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.app.fileresource.system.model.SysTreeNode;
import doc.doccenterserver.client.fileresource.system.api.dto.DictDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.DictEntry;

import java.rmi.ServerException;
import java.util.List;

/**
 * 字典服务
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@ApplicationService(domain = "FileResource", name = "字典服务")
public interface DictService {

    /**
     * 获取字典集合
     *
     * @param pid
     * @return Dict
     */
    @ReturnValue
    @Method(name = "获取字典集合")
    DictDto getDictList(@Parameter(name = "父ID", required = false) String pid);

    List<SysTreeNode> findInItTree(String pid);

    PageResult<List<DictEntry>> listDict(DictEntry dictEntry);

    DictEntry getById(String id);

    DictEntry editDict(DictEntry dictEntry);

    DictEntry deleteDict(String id);

    DictEntry addDict(DictEntry dictEntry) throws ServerException;

    List<DictEntry> getByPId(String pid);

    DictEntry getByPIdAndVal(String pid, String val);
}
