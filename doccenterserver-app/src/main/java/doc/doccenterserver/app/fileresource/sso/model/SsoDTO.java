package doc.doccenterserver.app.fileresource.sso.model;

import com.alibaba.bizworks.core.specification.StructureObject;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangxp
 * @description sso 认证 返回提
 * @date 2023/4/4 9:48
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "sso认证响应结果")
public class SsoDTO {

    private String token;

    private String requestUrl;
    private UserEntity user;
}
