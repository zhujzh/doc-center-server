package doc.doccenterserver.app.fileresource.file.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.file.api.dto.HomeDto;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Tangcancan
 */

@ApplicationService(domain = "HomeResource", name = "文档检索服务")
public interface HomeService {

    @ReturnValue
    @Method(name = "检索查询")
    HomeDto getDocSearch(HttpServletRequest request, HomeDto homeDto);
}
