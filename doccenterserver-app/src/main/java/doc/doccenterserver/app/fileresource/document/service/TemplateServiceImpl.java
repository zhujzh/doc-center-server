package doc.doccenterserver.app.fileresource.document.service;

import doc.doccenterserver.app.fileresource.document.api.TemplateService;
import doc.doccenterserver.app.fileresource.document.converter.TemplateConvertor;
import doc.doccenterserver.client.fileresource.document.api.dto.TemplateDto;
import doc.doccenterserver.client.fileresource.document.api.req.TemplateRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.TemplateEntity;
import doc.doccenterserver.domain.fileresource.document.service.TemplateDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Component("TemplateServiceAS")
@Slf4j
public class TemplateServiceImpl implements TemplateService {


    @Resource
    private TemplateDomainService templateDomainService;

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<TemplateDto>> getListTemplates(TemplateRequest templateRequest) {
        TemplateEntity templateEntity = TemplateConvertor.INSTANCE.requestToEntity(templateRequest);
        PageResult<List<TemplateEntity>> listPageResult = templateDomainService.getListTemplates(templateEntity);
        return TemplateConvertor.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public TemplateDto getTemplate(Integer id) {
        return TemplateConvertor.INSTANCE.entityToDto(templateDomainService.getTemplate(id));
    }

    @Override
    public TemplateDto addTemplate(TemplateRequest templateRequest) {
        return TemplateConvertor.INSTANCE.entityToDto(templateDomainService.addTemplate(
                TemplateConvertor.INSTANCE.requestToEntity(templateRequest)));
    }

    @Override
    public TemplateDto editTemplate(TemplateRequest templateRequest) {
        return TemplateConvertor.INSTANCE.entityToDto(templateDomainService.editTemplate(
                TemplateConvertor.INSTANCE.requestToEntity(templateRequest)));
    }

    @Override
    public TemplateDto deleteTemplate(String ids) {
        return TemplateConvertor.INSTANCE.entityToDto(templateDomainService.deleteTemplate(ids));
    }
}
