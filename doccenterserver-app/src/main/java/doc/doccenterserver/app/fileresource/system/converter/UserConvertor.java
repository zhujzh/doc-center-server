package doc.doccenterserver.app.fileresource.system.converter;

import doc.doccenterserver.client.fileresource.document.api.dto.UserDto;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface UserConvertor {

    UserConvertor INSTANCE = Mappers.getMapper(UserConvertor.class);

    @Mappings({})
    UserDto entityToDto(UserEntity user);

    @Mappings({})
    UserEntity dtoToEntity(UserDto user);

    @Mappings({})
    List<UserDto> entityToDto(List<UserEntity> user);

    @Mappings({})
    List<UserEntity> dtoToEntity(List<UserDto> user);

}
