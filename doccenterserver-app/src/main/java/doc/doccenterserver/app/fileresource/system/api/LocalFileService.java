package doc.doccenterserver.app.fileresource.system.api;

import com.alibaba.bizworks.core.specification.Parameter;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.file.api.dto.FileDto;
import doc.doccenterserver.client.fileresource.file.api.req.FileOldUploadRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 19:18
 */
@ApplicationService(domain = "LocalFileResource", name = "文件维护")
public interface LocalFileService {
    PageResult<List<FileEntity>> listfiles(FileEntity fileEntity);

    int reUpload(String fileId);

    //@描述:显示文件详情
    Map<String, Object> showfiledetail(String fileId, String appName);

    //删除File对象
    String deletefiles(String fileIds);

    FileDto upload(@Parameter(name = "选择文件", required = false) MultipartFile multipartFile,@Parameter(name = "旧文档中心REQ", required = false) FileOldUploadRequest fileOldUploadRequest)throws Exception;
}
