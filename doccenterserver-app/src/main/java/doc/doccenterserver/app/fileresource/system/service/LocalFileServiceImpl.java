package doc.doccenterserver.app.fileresource.system.service;

import cn.hutool.core.util.IdUtil;
import doc.doccenterserver.app.fileresource.file.converter.FileToFileDtoConverter;
import doc.doccenterserver.app.fileresource.system.api.LocalFileService;
import doc.doccenterserver.client.fileresource.file.api.dto.FileDto;
import doc.doccenterserver.client.fileresource.file.api.req.FileOldUploadRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import doc.doccenterserver.domain.fileresource.file.service.FileDomainService;
import doc.doccenterserver.domain.fileresource.system.service.LocalFileDomainService;
import doc.doccenterserver.infrastructure.fileresource.file.utils.MultipartFileUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.rmi.ServerException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 19:19
 */
@Component("LocalFileServiceAS")
@Slf4j
public class LocalFileServiceImpl implements LocalFileService {
    @Resource
    private LocalFileDomainService localFileDomainService;
    @Resource
    private FileDomainService fileDomainService;

    @Override
    public PageResult<List<FileEntity>> listfiles(FileEntity fileEntity) {
        return localFileDomainService.listfiles(fileEntity);
    }

    @Override
    public int reUpload(String fileId) {
        return localFileDomainService.reUpload(fileId);
    }

    //@描述:显示文件详情
    @Override
    public Map<String, Object> showfiledetail(String fileId, String appName) {
        return localFileDomainService.showfiledetail(fileId,appName);
    }

    @Override
    public String deletefiles(String fileIds) {
        return localFileDomainService.deletefiles(fileIds);
    }

    @Override
    public FileDto upload(MultipartFile mFile, FileOldUploadRequest fileOldUploadRequest) throws Exception {
        if(StringUtils.isEmpty(fileOldUploadRequest.getMd5())){
            //throw new ServerException("md5:文件md5值不能为空");
        }
        //上传文件
        File file = MultipartFileUtil.multipartFileToFIle(mFile);
        if (file == null) {
            throw new ServerException("请选择上传文件");
        }
        String fileId = null;
        if(fileOldUploadRequest.getFileId() ==null){
            fileId = IdUtil.simpleUUID();
        }else{
            fileId = fileOldUploadRequest.getFileId();
        }
//
        String bigdataFileId = fileDomainService.uploadFile(mFile);
        MultipartFileUtil.deleteTempFile(file);
        String fileName = mFile.getOriginalFilename();
        fileName = java.net.URLDecoder.decode(fileName, "UTF-8");
        if (fileName.length() > 100) {
            throw new ServerException("文件名称超长");
        }
        String md5 = fileOldUploadRequest.getMd5();
        String ext = FilenameUtils.getExtension(fileName);
        String filePath = md5 + "." + ext;
        String fileUrl = "下载地址" + "file/download/" + fileId;
        FileDto fileDto = new FileDto();
        fileDto.setFileMd5(md5);
        fileDto.setFileId(fileId);
        fileDto.setFileFormat(ext);
        fileDto.setFileSize((double) mFile.getSize());
        fileDto.setFileUrl(fileUrl);
        fileDto.setFilename(fileName);
        fileDto.setFilePath(fileOldUploadRequest.getAppId() + "/" + filePath);
        fileDto.setAppId(fileOldUploadRequest.getAppId());
        fileDto.setLabelName(fileOldUploadRequest.getLabelName());
        fileDto.setBigdataFileId(bigdataFileId);
        fileDomainService.updateFile(FileToFileDtoConverter.INSTANCE.fileDtoToFile(fileDto));
        return fileDto;
    }

}
