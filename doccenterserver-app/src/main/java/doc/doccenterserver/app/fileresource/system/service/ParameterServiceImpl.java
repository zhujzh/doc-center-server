package doc.doccenterserver.app.fileresource.system.service;

import doc.doccenterserver.app.fileresource.system.converter.ParameterEntryToParameterDtoConverter;
import doc.doccenterserver.app.fileresource.system.api.ParameterService;
import doc.doccenterserver.client.fileresource.system.api.dto.ParameterDto;
import doc.doccenterserver.domain.fileresource.system.service.ParameterDomainService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 字典服务实现类
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Component("SYSTEMParameteServiceAS")
public class ParameterServiceImpl implements ParameterService {

    @Resource
    private ParameterDomainService parameterDomainService;

    @Override
    public List<ParameterDto> getList() {
        return ParameterEntryToParameterDtoConverter.INSTANCE.parameterEntryToParameterDto(parameterDomainService.selectList());
    }

    @Override
    public ParameterDto getByKey(String key) {
        return ParameterEntryToParameterDtoConverter.INSTANCE.parameterEntryToParameterDto(parameterDomainService.getByKey(key));
    }
}
