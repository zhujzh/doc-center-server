package doc.doccenterserver.app.fileresource.file.converter;

import doc.doccenterserver.client.fileresource.file.api.dto.HomeDto;
import doc.doccenterserver.domain.fileresource.file.model.HomeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface HomeConvertor {

    HomeConvertor INSTANCE = Mappers.getMapper(HomeConvertor.class);

    @Mappings({})
    HomeEntity dtoToEntity(HomeDto parameter);

    @Mappings({})
    HomeDto entityToDto(HomeEntity parameter);

    @Mappings({})
    List<HomeEntity> dtoToEntity(List<HomeDto> parameter);

    @Mappings({})
    List<HomeDto> entityToDto(List<HomeEntity> parameter);
}
