package doc.doccenterserver.app.fileresource.document.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.GroupDto;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@ApplicationService(domain = "GroupResource", name = "群组服务")
public interface GroupService {

    @ReturnValue
    @Method(name = "编辑初始化")
    GroupDto editInit(GroupDto dto);

    @ReturnValue
    @Method(name = "保存操作")
    GroupDto save(GroupDto dto);

    @ReturnValue
    @Method(name = "分页查询")
    PageResult<List<GroupDto>> getList(GroupDto dto);

    @ReturnValue
    @Method(name = "保存群组成员")
    GroupDto saveGroupMembers(GroupDto dto);

    @ReturnValue
    @Method(name = "批量删除")
    GroupDto delete(String groupIds);

    @ReturnValue
    @Method(name = "删除群组")
    GroupDto deleteGroupById(String groupId);

    @ReturnValue
    @Method(name = "编辑群组成员")
    GroupDto editUserGroup(HttpServletRequest request, GroupDto dto);

    @ReturnValue
    @Method(name = "新增角色群组")
    GroupDto insertRoleGroup(GroupDto dto);

    @ReturnValue
    @Method(name = "根据id获取")
    GroupDto selectRgByRoleId(GroupDto dto);

}
