package doc.doccenterserver.app.fileresource.document.converter;

import doc.doccenterserver.app.fileresource.system.model.SysTreeNode;
import doc.doccenterserver.client.fileresource.document.api.dto.TreeNode;
import doc.doccenterserver.domain.fileresource.document.model.TreeNodeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author tangrongcan
 */
@Mapper
public interface TreeNodeEntityToTreeNodeConvertor {

    TreeNodeEntityToTreeNodeConvertor INSTANCE = Mappers.getMapper(TreeNodeEntityToTreeNodeConvertor.class);

    @Mappings({ })
    List<TreeNode> treeNodeEntityToTreeNode(List<TreeNodeEntity> treeNodes);

    @Mappings({ })
    List<SysTreeNode> sysTreeNodeEntityToTreeNode(List<TreeNodeEntity> treeNodes);
}
