package doc.doccenterserver.app.fileresource.document.api;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ApplicationService;
import doc.doccenterserver.client.fileresource.document.api.dto.PortalDto;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@ApplicationService(domain = "PortalResource", name = "Portal")
public interface PortalService {

    @ReturnValue
    @Method(name = "门户页面，查询paper数据，按条件筛选")
    List<PortalDto> getPortal(PortalDto portalDto, HttpServletRequest request);
}
