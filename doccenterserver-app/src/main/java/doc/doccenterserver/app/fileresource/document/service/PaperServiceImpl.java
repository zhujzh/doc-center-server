package doc.doccenterserver.app.fileresource.document.service;

import doc.doccenterserver.app.fileresource.document.api.PaperService;
import doc.doccenterserver.app.fileresource.document.converter.PaperConvertor;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.PaperDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.document.service.PaperDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@Component("PaperServiceAS")
@Slf4j
public class PaperServiceImpl implements PaperService {

    @Resource
    private PaperDomainService paperDomainService;

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<PaperDto>> listPapers(
            HttpServletRequest request, PaperRequest paperRequest) {
        PaperEntity paperEntity = PaperConvertor.INSTANCE.requestToEntity(paperRequest);
        PageResult<List<PaperEntity>> listPageResult = paperDomainService.listPapers(request, paperEntity);
        return PaperConvertor.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public PaperDto getListPaper(HttpServletRequest request, String classId) {
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.getListPaper(request, classId));
    }

    @Override
    public PaperDto getPaperDetail(String paperId, HttpServletRequest request) {
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.getPaperDetail(paperId, request));
    }

    @Override
    public PaperDto getAddPaper(String classId, HttpServletRequest request) {
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.getAddPaper(classId, request));
    }

    @Override
    public PaperDto addPaper(HttpServletRequest request, PaperRequest paperRequest) {
        PaperEntity paperEntity = PaperConvertor.INSTANCE.requestToEntity(paperRequest);
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.addPaper(request, paperEntity));
    }

    @Override
    public PaperDto getEditPaper(String paperId, HttpServletRequest request) {
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.getEditPaper(paperId, request));
    }

    @Override
    public PaperDto editPaper(HttpServletRequest request, PaperRequest paperRequest) {
        PaperEntity paperEntity = PaperConvertor.INSTANCE.requestToEntity(paperRequest);
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.editPaper(request, paperEntity));
    }

    @Override
    public PaperDto topDoc(PaperRequest paperRequest) {
        PaperEntity paperEntity = PaperConvertor.INSTANCE.requestToEntity(paperRequest);
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.topDoc(paperEntity));
    }

    @Override
    public PaperDto topDocSort(PaperRequest paperRequest) {
        PaperEntity paperEntity = PaperConvertor.INSTANCE.requestToEntity(paperRequest);
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.topDocSort(paperEntity));
    }

    @Override
    public PaperDto previewPaper(PaperRequest paperRequest, HttpServletRequest request) {
        PaperEntity paperEntity = PaperConvertor.INSTANCE.requestToEntity(paperRequest);
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.previewPaper(paperEntity, request));
    }

    @Override
    public PaperDto deletePaper(String ids) {
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.deletePaper(ids));
    }

    @Override
    public doc.doccenterserver.client.fileresource.base.PageResult<List<PaperDto>> listfrontpapers(PaperRequest paperRequest, String classid, String ccid, HttpServletRequest request) {
        PaperEntity paperEntity = PaperConvertor.INSTANCE.requestToEntity(paperRequest);
        PageResult<List<PaperEntity>> listPageResult = paperDomainService.listfrontpapers(paperEntity,classid,ccid,request);
        return PaperConvertor.INSTANCE.entityToDtoPage(listPageResult);
    }

    @Override
    public PaperDto papersQueryRet(PaperRequest paperRequest, String classId, String ccId, HttpServletRequest request) {
        PaperEntity paperEntity = PaperConvertor.INSTANCE.requestToEntity(paperRequest);
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.papersQueryRet(paperEntity,classId,ccId,request));
    }

    @Override
    public PaperDto getInit(String classId) {
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.getInit(classId));
    }

    @Override
    public PaperDto getPreviewInfo(String paperId, HttpServletRequest request) {
        return PaperConvertor.INSTANCE.entityToDto(paperDomainService.getPreviewInfo(paperId, request));
    }
}
