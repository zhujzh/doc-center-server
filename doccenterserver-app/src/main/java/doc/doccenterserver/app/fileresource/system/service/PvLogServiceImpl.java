package doc.doccenterserver.app.fileresource.system.service;


import doc.doccenterserver.app.fileresource.system.api.PvLogService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.PvLogEntity;
import doc.doccenterserver.domain.fileresource.system.service.PvLogDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 22:07
 */
@Component("PvLogServiceAS")
@Slf4j
public class PvLogServiceImpl implements PvLogService {
    @Resource
    private PvLogDomainService pvLogDomainService;
    @Override
    public PageResult<List<PvLogEntity>> getListParam(PvLogEntity paramEntity) {
        return pvLogDomainService.getListParam(paramEntity);
    }

    @Override
    public PvLogEntity add(PvLogEntity pvLogEntity) {
        return pvLogDomainService.add(pvLogEntity);
    }

}
