package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.User2roleEntity;

import java.util.List;

/**
 * @author Tangcancan
 */
public interface User2RoleRepository {
    List<User2roleEntity> findAccountInfoListByUserId(String userId);

    List<User2roleEntity> getUserByRoleId(String roleId);

    Integer delete(String roleId);

    Integer insert(User2roleEntity user2role);

}
