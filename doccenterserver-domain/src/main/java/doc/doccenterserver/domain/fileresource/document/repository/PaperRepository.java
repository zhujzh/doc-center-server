package doc.doccenterserver.domain.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
public interface PaperRepository {
    PageResult<List<PaperEntity>> listPapers(HttpServletRequest request, PaperEntity paperEntity);
    List<PaperEntity> getPortalList(HttpServletRequest request, PaperEntity paperEntity);

    List<PaperEntity> listPaper(HttpServletRequest request, PaperEntity paperEntity, String[] split);

    PaperEntity getListPaper(HttpServletRequest request, String classId);

    PaperEntity getPaperDetail(String paperId, HttpServletRequest request);

    PaperEntity getAddPaper(String classId, HttpServletRequest request);

    void setPaperType(PaperEntity paperEntity);

    PaperEntity addPaper(HttpServletRequest request, PaperEntity paperEntity);

    PaperEntity getEditPaper(String paperId, HttpServletRequest request);

    PaperEntity topDoc(PaperEntity paperEntity);

    PaperEntity topDocSort(PaperEntity paperEntity);

    PaperEntity previewPaper(PaperEntity paperEntity, HttpServletRequest request);

    PaperEntity deletePaper(String ids);

    Double findsumpaperfilesize(String appId);

    Double sumtpanfilesize(String appId);

    PaperEntity findPaperById(String paperId);

    PageResult<List<PaperEntity>> listfrontpapers(PaperEntity paperEntity, String classid, String ccid, HttpServletRequest request);

    PaperEntity papersQueryRet(PaperEntity paperEntity, String classid, String ccid, HttpServletRequest request);

    PaperEntity getInit(String classId);

    PaperEntity getById(String paperId);

    List<PaperEntity> getByIds(List<String> paperIds);

}
