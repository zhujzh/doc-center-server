package doc.doccenterserver.domain.fileresource.document.service;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.InterfaceAccessLogEntity;
import doc.doccenterserver.domain.fileresource.document.repository.InterfaceAccessLogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("InterfaceAccessLogDS")
@DomainService(name = "InterfaceAccessLogDS")
public class InterfaceAccessLogDomainService {

    @Resource
    private InterfaceAccessLogRepository interfaceAccessLogRepository;

    @ReturnValue
    @Method(name = "分页查询")
    public PageResult<List<InterfaceAccessLogEntity>> getInterfaceAccessLogList(
            HttpServletRequest request, InterfaceAccessLogEntity interfaceAccessLogEntity) {
        return interfaceAccessLogRepository.getInterfaceAccessLogList(request, interfaceAccessLogEntity);
    }

    @ReturnValue
    @Method(name = "获取明细详情")
    public InterfaceAccessLogEntity getInterfaceAccessLog(String logId) {
        return interfaceAccessLogRepository.getInterfaceAccessLog(logId);
    }

    @ReturnValue
    @Method(name = "初始化")
    public InterfaceAccessLogEntity getInit(HttpServletRequest request) {
        return interfaceAccessLogRepository.getInit(request);
    }

    @ReturnValue
    @Method(name = "新增")
    public InterfaceAccessLogEntity insert(InterfaceAccessLogEntity interfaceAccessLogEntity) {
        return interfaceAccessLogRepository.insert(interfaceAccessLogEntity);
    }
}
