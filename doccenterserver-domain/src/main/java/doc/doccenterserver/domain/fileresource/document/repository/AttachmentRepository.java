package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;

import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
public interface AttachmentRepository {
    Integer deleteAttachmentByPaperId(String paperId);

    Integer insertAttachment(AttachmentEntity attachmentEntity);

    Integer findAttCountByPaperId(String paperId);

    List<AttachmentEntity> getAttachmentList(AttachmentEntity attachmentEntity);

    /**
     * @方法名: findMainAttachmentByPaperId
     * @描述:查找主附件
     * @参数:@param paperId
     * @参数:@return
     * @返回值：AttachmentEntry
     */
    AttachmentEntity findMainAttachmentByPaperId(String paperId);
    /**
     * 根据条件查询Attachment列表(不分页)
     */
    List<AttachmentEntity> findAttachmentListByParams(Map<String,Object> attachment);

    AttachmentEntity getAttachmentByPaperId(String paperId);

    List<AttachmentEntity> getAttachmentsByPaperIdList(List<String> paperIdList);


}
