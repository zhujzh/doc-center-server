package doc.doccenterserver.domain.fileresource.document.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChannelAuthUtil {
    public final static int[] NUMS = {2, 4, 8, 16};
    public static List<List<Integer>> findSum(int total,int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == total) {
                List<Integer> sub = new ArrayList<>();
                sub.add(nums[i]);
                res.add(sub);
            } else {
                int[] subNums = Arrays.copyOfRange(nums, i + 1, nums.length);
                List<List<Integer>> subRes = findSum(total - nums[i], subNums);
                for (List<Integer> sub : subRes) {
                    sub.add(0, nums[i]);
                    res.add(sub);
                }
            }
        }
        return res;
    }
}
