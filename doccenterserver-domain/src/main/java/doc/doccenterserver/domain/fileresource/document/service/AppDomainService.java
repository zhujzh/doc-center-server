package doc.doccenterserver.domain.fileresource.document.service;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AppEntity;
import doc.doccenterserver.domain.fileresource.document.repository.AppRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("AppDS")
@DomainService(name = "AppDomainService")
public class AppDomainService {

    @Resource
    private AppRepository appRepository;

    @ReturnValue
    @Method(name = "分页查询")
    public PageResult<List<AppEntity>> getUseDetailList(HttpServletRequest request, AppEntity appEntity) {
        return appRepository.getUseDetailList(request, appEntity);
    }

    @ReturnValue
    @Method(name = "编辑初始化")
    public AppEntity editInit(String appId) {
        return appRepository.editInit(appId);
    }

    @ReturnValue
    @Method(name = "保存操作")
    public AppEntity save(AppEntity appEntity) {
        return appRepository.save(appEntity);
    }

    @ReturnValue
    @Method(name = "获取全部数据查询")
    public PageResult<List<AppEntity>> getAppList(AppEntity appEntity) {
        return appRepository.getAppLists(appEntity);
    }

    @ReturnValue
    @Method(name = "删除")
    public AppEntity delete(String appIds) {
        return appRepository.delete(appIds);
    }

    @ReturnValue
    @Method(name = "获取明细")
    public AppEntity getAppDetail(String appId) {
        return appRepository.getAppDetail(appId);
    }
}
