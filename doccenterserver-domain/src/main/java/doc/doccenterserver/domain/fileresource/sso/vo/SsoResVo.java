package doc.doccenterserver.domain.fileresource.sso.vo;

import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangxp
 * @description 认证返回对象
 * @date 2023/4/4 9:55
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SsoResVo {

    private String token;

    private String requestUrl;
    private UserEntity user;
}
