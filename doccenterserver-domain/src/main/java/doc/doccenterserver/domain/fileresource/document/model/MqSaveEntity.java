package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "统一消息暂存日志实体")
public class MqSaveEntity {

    @Field(name = "日志流水号")
    private String logId;

    @Field(name = "信息的XML内容")
    private String content;

    @Field(name = "源应用文档编号")
    private String sourcePaperId;

    @Field(name = "MQ信息的发送方")
    private String sourceApp;

    @Field(name = "信息归档操作类型")
    private String operation;

    @Field(name = "信息产生时间")
    private Date createTime;

    @Field(name = "归档组织ID")
    private String archiveOrgCode;

    public MqSaveEntity(String logId, String content, Date createTime,
                        String operation, String sourceApp, String sourcePaperId, String archiveOrgCode) {
        super();
        this.setLogId(logId);
        this.setContent(content);
        this.setCreateTime(createTime);
        this.setOperation(operation);
        this.setSourceApp(sourceApp);
        this.setSourcePaperId(sourcePaperId);
        this.setArchiveOrgCode(archiveOrgCode);
    }
}
