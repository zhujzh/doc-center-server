package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;

import java.util.List;

/**
 * 文件
 * repository
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
public interface ChannelRepository {
    ChannelEntity getById(String id);

    List<ChannelEntity> selectList(ChannelEntity channel);

    String getChannelId(String name, String parentId, String fromAppName);

    PageResult<List<ChannelEntity>> selectByPage(ChannelEntity channel);

    List<ChannelEntity> getInItTree(String parentId);

    String getLastestChannelId(String parentId);

    String initChannelPath(String parentId, String curId);

    List<ChannelEntity> initChannelPath(String classId);

    ChannelEntity deleteChannel(String ids);

    ChannelEntity addChannel(ChannelEntity channelEntity);

    ChannelEntity editChannel(ChannelEntity channelEntity);

    String queryRefChannels();

    List<ChannelEntity> queryChildChannlesById(String classid);

    ChannelEntity findChannelById(String classid);

    boolean isChannelAuthList(String classId, UserModel curUser);

    List<String> initChannelPathName(String classId);

    Boolean isHasChildrenChannel(String classId);
}
