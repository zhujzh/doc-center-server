package doc.doccenterserver.domain.fileresource.file.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.Parameter;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import doc.doccenterserver.domain.fileresource.file.repository.FileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.ServerException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件服务
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Slf4j
@Component("FILEFileDS")
@DomainService(name = "文件服务")
public class FileDomainService {

    @Autowired
    private FileRepository dictDtoRepository;

    @ReturnValue
    @Method(name = "根据ID查询文件")
    public FileEntity getFileById(@Parameter(name = "fileId", required = false) String fileId) {
    	return dictDtoRepository.getFileById(fileId);
	}
    @ReturnValue
    @Method(name = "根据md5查询文件")
    public List<FileEntity> md5Check(@Parameter(name = "md5", required = false) String md5) {
        return dictDtoRepository.md5Check(md5);
    }

    @ReturnValue
    @Method(name = "上传文件-HTTP")
    public String uploadFileOld(File file) throws ServerException {
        String fileId;
        String url="http://10.158.227.187:31671/mp/dc/c/file/uploadFile/v1";
//        String url="http://192.168.1.169:9000/mock/494/mp/dc/c/file/uploadFile/v1";
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("file", file);
        String result= HttpUtil.post(url, paramMap);
        JSONObject jsonObject= JSONUtil.parseObj(result);
        if(jsonObject.getBool("success") && StrUtil.isNotBlank(jsonObject.getStr("data"))){
            fileId=jsonObject.getStr("data");
        }else{
            throw new ServerException("上传基线版本接口错误:"+jsonObject.getStr("message"));
        }
        return fileId;
    }
    @ReturnValue
    @Method(name = "下载文件-HTTP")
    public void downloadFileOld(HttpServletResponse response, String fileId, String type, String downloadName) throws Exception {
//        String urlPath = "http://localhost:8080/file/download";
        String urlPath = "http://10.158.227.187:31671/mp/dc/q/file/downloadFile/v1";
        URL url = new URL(urlPath);
        // 连接类的父类，抽象类
        URLConnection urlConnection = url.openConnection();
        // http的连接类
        HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
        // 设定请求的方法，默认是GET（对于知识库的附件服务器必须是GET，如果是POST会返回405。流程附件迁移功能里面必须是POST，有所区分。）
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setRequestProperty("Accept-Charset", "UTF-8");
        httpURLConnection.setRequestProperty("contentType", "UTF-8");
        downloadName= new String(downloadName.getBytes("UTF-8"), "ISO8859-1");
        if(type.equals("no")){
            response.setHeader("Content-disposition", "inline; filename=\""
                    + downloadName+"\"");
        }else{
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=\""
                    + downloadName+"\"");
        }
        Map<String,Object> map=new HashMap<>();
        map.put("fileId",fileId);
        // 设置字符编码
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        httpURLConnection.setUseCaches(false);
        httpURLConnection.connect();
        httpURLConnection.getOutputStream().write(JSONUtil.toJsonStr(map).getBytes());
        httpURLConnection.getOutputStream().flush();
        httpURLConnection.getOutputStream().close();
        // 打开到此 URL 引用的资源的通信链接（如果尚未建立这样的连接）。
        int code = httpURLConnection.getResponseCode();
        log.info("下载文件：-------------"+code);
        if(code==200){
            InputStream inputStream = httpURLConnection.getInputStream();
            OutputStream out = response.getOutputStream();
            int size = 0;
            int lent = 0;
            byte[] buf = new byte[1024];
            while ((size = inputStream.read(buf)) != -1) {
                lent += size;
                out.write(buf, 0, size);
            }
            inputStream.close();
            out.close();
        }else {
            throw new ServerException("调用基线版接口异常CODE:"+code);
        }
    }


    @ReturnValue
    @Method(name = "上传文件-SDK")
    public String uploadFile(MultipartFile file) throws ServerException {
        return dictDtoRepository.uploadFile(file);
    }
    @ReturnValue
    @Method(name = "下载文件-SDK")
    public void downloadFile(HttpServletResponse response, String fileId, String type, String downloadName) throws IOException {
        downloadName= new String(downloadName.getBytes("UTF-8"), "ISO8859-1");
        if(type.equals("no")){
            response.setHeader("Content-disposition", "inline; filename=\""
                    + downloadName+"\"");
        }else{
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=\""
                    + downloadName+"\"");
        }
        dictDtoRepository.downloadFile(response,fileId);
    }

    @ReturnValue
    @Method(name = "保存文件信息")
    public boolean insertFile(FileEntity fileEntity) {
        return dictDtoRepository.insertFile(fileEntity);
    }

    @ReturnValue
    @Method(name = "更新文件信息")
    public boolean updateFile(FileEntity fileEntity) {
        return dictDtoRepository.updateFile(fileEntity);
    }

    @ReturnValue
    @Method(name = "下载文件时查询FileList")
    public List<FileEntity> downloadFileList(@Parameter(name = "fileId", required = false) String fileId) {
        return dictDtoRepository.downloadFileList(fileId);
    }

}
