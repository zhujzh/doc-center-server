package doc.doccenterserver.domain.fileresource.system.service;

import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import doc.doccenterserver.domain.fileresource.system.repository.LocalFileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 19:22
 */
@Slf4j
@Component("LocalFileDS")
@DomainService(name = "文件维护")
public class LocalFileDomainService {
    @Resource
    private LocalFileRepository localFileRepository;
    public PageResult<List<FileEntity>> listfiles(FileEntity fileEntity) {
        return localFileRepository.listfiles(fileEntity);
    }

    public int reUpload(String fileId) {
        return localFileRepository.reUpload(fileId);
    }

    public Map<String, Object> showfiledetail(String fileId, String appName) {
        return localFileRepository.showfiledetail(fileId,appName);
    }

    public String deletefiles(String fileIds) {
        return localFileRepository.deletefiles(fileIds);
    }

}
