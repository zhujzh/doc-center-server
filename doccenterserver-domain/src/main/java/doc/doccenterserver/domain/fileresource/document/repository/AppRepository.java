package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AppEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
public interface AppRepository {
    PageResult<List<AppEntity>> getUseDetailList(HttpServletRequest request, AppEntity appEntity);

    List<AppEntity> getAppList(AppEntity appEntity);

    AppEntity editInit(String appId);

    AppEntity save(AppEntity appEntity);

    PageResult<List<AppEntity>> getAppLists(AppEntity appEntity);

    AppEntity delete(String appIds);

    AppEntity getAppDetail(String appId);

}
