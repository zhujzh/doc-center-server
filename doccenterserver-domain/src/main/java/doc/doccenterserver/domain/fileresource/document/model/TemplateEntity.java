package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "模板实体")
public class TemplateEntity extends BaseDto {

    @Field(name = "模板id")
    private Integer id;

    @Field(name = "模板名称")
    private String name;

    @Field(name = "类型")
    private Integer type;

    @Field(name = "描述")
    private String descript;

    @Field(name = "模板内容")
    private String content;

    @Field(name = "生成文件名称")
    private String pathname;

    private Integer mode;
}
