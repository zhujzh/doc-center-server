package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "图片实体")
public class ImageEntity extends BaseDto {

    @Field(name = "附件id")
    private String imgId;

    @Field(name = "附件名称")
    private String imgName;
    private String name;

    @Field(name = "描述")
    private String imgDesc;

    @Field(name = "所属文档id")
    private String paperId;

    @Field(name = "文件保存路径")
    private String imgPath;

    @Field(name = "上传时间")
    private Date uploadDate;

    @Field(name = "是否为首页图片")
    private String isMain;

    @Field(name = "排序号")
    private Integer orderBy;

    @Field(name = "裁剪参数")
    private String tailParams;

    @Field(name = "大数据平台文件ID")
    private String fileId;

    @Field(name = "租户id")
    private String appId;

    @Field(name = "是否上传")
    private String isUpload;

    @Field(name = "大数据平台文件MD5")
    private String fileMd5;


    // cutImage
    private int x;
    private int y;
    private int width;
    private int height;
    private Map<String, Object> result;
    private String fromType;
    private String minwidth;
    private String minheight;
    // cutImage
    private String uid;
}
