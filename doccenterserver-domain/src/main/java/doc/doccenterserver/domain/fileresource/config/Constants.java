package doc.doccenterserver.domain.fileresource.config;

public class Constants {

    //空字符
    public static final String EMPTY = "";

    //扩展属性 员工编制类型 key
    public static final String EXTEND_PROPS_KIND = "kind";
    /**
     * 员工编制类型 BZN：编制内  BZW：编制外
     */
    public static final String BZN = "BZN";
    public static final String BZW = "BZW";

    /**
     * 员工岗位状态 ZZ：在职  LZ：离职 TZ：停职  TX：退休
     */
    public static final String STAFF_JOB_STATUS_ZZ = "ZZ";
    public static final String STAFF_JOB_STATUS_LZ = "LZ";
    public static final String STAFF_JOB_STATUS_TZ = "TZ";
    public static final String STAFF_JOB_STATUS_TX = "TX";

    /**
     * 员工性别  N 男  V 女
     */
    public static final String SEX_N = "N";
    public static final String SEX_V = "V";


    public static final String ORG_ROOT = "root";


    public static final String HTTPS = "HTTPS";

    public static final String GM_PROTOCOL = "GMTLS";
    public static final String INTERNATIONAL_PROTOCOL = "TLSv1.2";
    public static final String SIGNATURE_ALGORITHM_SDK_HMAC_SHA256 = "SDK-HMAC-SHA256";
    public static final String SIGNATURE_ALGORITHM_SDK_HMAC_SM3 = "SDK-HMAC-SM3";

    // 用户信息 redis key前缀
    public static final String LOGIN_USER_KEY = "dc_user_key_";

    // refreshToken  redis 前缀
    public static final String LOGIN_REFRESH_KEY = "dc_refresh_key_";

    // 用户类型：GYGS：工业公司, ，SYGS：商业公司， LSH：零售户(场所) ，XFZ：消费者 ，GYS：供应商
    public static final String USER_TYPE_GYGS = "GYGS";
    public static final String USER_TYPE_SYGS = "SYGS";
    public static final String USER_TYPE_LSH = "LSH";
    public static final String USER_TYPE_XFZ = "XFZ";
    public static final String USER_TYPE_GYS = "GYS";

}
