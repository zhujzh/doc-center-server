package doc.doccenterserver.domain.fileresource.system.repository;

import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;

import java.util.Map;

/**
 * @author ch
 * @date 2023/4/19 9:33
 */
public interface JobDetailLogManageRepository {

    Map<String, Object> savejobdetaillog(JobDetailLogEntity jobDetailLogEntity);
}
