package doc.doccenterserver.domain.fileresource.file.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "TPanObjEntity")
public class TPanObjEntity {

    @Field(name = "OBJ_ID")
    private String objId;

    @Field(name = "OBJ_NAME")
    private String objName;

    @Field(name = "OBJ_TYPE")
    private String objType;

    @Field(name = "OBJ_PATH")
    private String objPath;

    @Field(name = "PARENT_OBJ_ID")
    private String parentObjId;

    @Field(name = "USER_ID")
    private String userId;

    @Field(name = "OBJ_AUTH")
    private Double objAuth;

    @Field(name = "OBJ_SORT")
    private Double objSort;

    @Field(name = "OBJ_SIZE")
    private Double objSize;

    @Field(name = "IS_DELETE")
    private String isDelete;

    @Field(name = "OBJ_ICON")
    private String objIcon;

    @Field(name = "CREATE_TIME")
    private Date createTime;

    @Field(name = "UPDATE_TIME")
    private Date updateTime;

    @Field(name = "RESERVED_1")
    private String reserved1;

    @Field(name = "RESERVED_2")
    private String reserved2;

    @Field(name = "RESERVED_14")
    private String reserved14;

    @Field(name = "RESERVED_13")
    private String reserved13;

    @Field(name = "RESERVED_12")
    private String reserved12;

    @Field(name = "RESERVED_11")
    private String reserved11;

    @Field(name = "RESERVED_10")
    private String reserved10;

    @Field(name = "RESERVED_9")
    private String reserved9;

    @Field(name = "RESERVED_8")
    private String reserved8;

    @Field(name = "RESERVED_7")
    private String reserved7;

    @Field(name = "RESERVED_6")
    private String reserved6;

    @Field(name = "RESERVED_5")
    private String reserved5;

    @Field(name = "RESERVED_4")
    private String reserved4;

    @Field(name = "RESERVED_3")
    private String reserved3;

    @Field(name = "OBJ_TAG")
    private String objTag;

    @Field(name = "THUMB_PATH")
    private String thumbPath;

    @Field(name = "OBJ_DESC")
    private String objDesc;


    private String fileUrl;

    public TPanObjEntity(String objId, String objName) {
        this.objId = objId;
        this.objName = objName;
    }
}
