package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "Attachment实体")
public class AttachmentEntity {

    @Field(name = "附件名称")
    private String fileName;
    private String name;

    @Field(name = "附件描述")
    private String fileDesc;

    @Field(name = "所属实体的id")
    private String paperId;

    @Field(name = "文件保存路径")
    private String filePath;

    @Field(name = "大数据平台文件ID")
    private String fileId;

    @Field(name = "大数据平台对应文件访问地址")
    private String fileUrl;

    @Field(name = "是否已经被转换成PDF")
    private Integer isPdf;

    @Field(name = "附件大小")
    private Double fileSize;

    @Field(name = "附件格式")
    private String fileFormat;

    @Field(name = "附件下载次数")
    private Integer downloadCount;

    @Field(name = "是否是主附件")
    private String isMain;

    @Field(name = "PDF文件路径")
    private String pdfUrl;

    @Field(name = "排序号")
    private Integer orderBy;

    @Field(name = "上传时间")
    private Date uploadDate;

    @Field(name = "PDF文档大数据平台主键")
    private String pdfFileId;

    @Field(name = "附件id")
    private String attaId;

    @Field(name = "租户id")
    private String appId;

    @Field(name = "是否上传")
    private String isUpload;

    @Field(name = "SWF文件id")
    private String swfFileId;

    @Field(name = "文件MD5")
    private String fileMd5;

    @Field(name = "PDF文件MD5")
    private String pdfFileMd5;

    private String ext;

    private String uid;
}
