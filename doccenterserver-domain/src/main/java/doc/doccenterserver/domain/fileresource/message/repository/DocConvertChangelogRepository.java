package doc.doccenterserver.domain.fileresource.message.repository;

import doc.doccenterserver.domain.fileresource.document.model.DocConvertChangelogEntity;

/**
 * 文档转换
 */
public interface DocConvertChangelogRepository {

    /**
     * 文件转换
     */
    void docConvert();

    Integer insertDocConvertChangelog(DocConvertChangelogEntity docConvertChangelog);
}
