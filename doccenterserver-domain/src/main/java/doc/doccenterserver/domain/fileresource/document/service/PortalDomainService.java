package doc.doccenterserver.domain.fileresource.document.service;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.document.model.ImageEntity;
import doc.doccenterserver.domain.fileresource.document.model.MeetingEntity;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.document.model.PortalEntity;
import doc.doccenterserver.domain.fileresource.document.repository.ChannelRepository;
import doc.doccenterserver.domain.fileresource.document.repository.ImageRepository;
import doc.doccenterserver.domain.fileresource.document.repository.MeetingRepository;
import doc.doccenterserver.domain.fileresource.document.repository.PaperRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tangcancan
 */

@Slf4j
@Component("PortalDS")
@DomainService(name = "门户查询服务")
public class PortalDomainService {

    @Resource
    private ChannelRepository channelRepository;
    @Resource
    private PaperRepository paperRepository;

    @Resource
    private ImageRepository imageRepository;

    @Resource
    private MeetingRepository meetingRepository;
    public List<PortalEntity> getPortal(PortalEntity portalEntity, HttpServletRequest request) {
        List<PortalEntity> result = new ArrayList<>();
        // 判断是否需要 获取截止日期之前的数据（针对于公告、通知、会议通知数据）
        String msgType = portalEntity.getMsgType();
        // 频道id
        String id = portalEntity.getId();
        PaperEntity paperEntity = new PaperEntity();
        paperEntity.setDelFlag(ConstantSys.DELETE_FLAG_ZERO);
        paperEntity.setPublicFlag(ConstantSys.PUBLIC_FLAG_TWO);
        paperEntity.setPageNum(portalEntity.getPageNum());
        paperEntity.setPageSize(portalEntity.getPageSize());
        if (null != msgType && !"".equals(msgType)) {
            paperEntity.setEndTime(LocalDateTime.now());
        }
        // 获取频道集合
        String[] split = id.split(ConstantSys.SEPARATE_COMMA);
        paperEntity.setClassIdList(Arrays.asList(split));
        List<PaperEntity> listPapers = paperRepository.getPortalList(request, paperEntity);
        for (String str : split) {
            PortalEntity portal = new PortalEntity();
            List<PaperEntity> resultData = listPapers.stream().filter((p)->p.getClassId().equals(str)).collect(Collectors.toList());
            if (CollectionUtil.isNotEmpty(resultData)) {
                List<String> paperIds = new ArrayList<>();
                resultData.forEach(e -> paperIds.add(e.getPaperId()));
                List<MeetingEntity> meeting = meetingRepository.getMeeting(paperIds);
                if (CollectionUtil.isNotEmpty(meeting)) {
                    for (MeetingEntity meet : meeting) {
                        for (PaperEntity paper : resultData) {
                            if (meet.getPaperId().equals(paper.getPaperId())) {
                                meet.setPaperName(paper.getPaperName());
                            }
                        }
                    }
                    portal.setMeetingList(meeting);
                }
                List<ImageEntity> images = imageRepository.getImages(paperIds);
                if (CollectionUtil.isNotEmpty(images)) {
                    for (ImageEntity image : images) {
                        for (PaperEntity paper : resultData) {
                            if (image.getPaperId().equals(paper.getPaperId())) {
                                image.setName(paper.getPaperName());
                            }
                        }
                    }
                    portal.setImageList(images);
                }
            }
            portal.setPaperList(resultData);
            result.add(portal);
        }
        return result;
    }
    public List<PortalEntity> getPortal_old(PortalEntity portalEntity, HttpServletRequest request) {
        List<PortalEntity> result = new ArrayList<>();
        // 判断是否需要 获取截止日期之前的数据（针对于公告、通知、会议通知数据）
        String msgType = portalEntity.getMsgType();
        // 频道id
        String id = portalEntity.getId();
        PaperEntity paperEntity = new PaperEntity();
        paperEntity.setDelFlag(ConstantSys.DELETE_FLAG_ZERO);
        paperEntity.setPublicFlag(ConstantSys.PUBLIC_FLAG_TWO);
        paperEntity.setPageNum(portalEntity.getPageNum());
        paperEntity.setPageSize(portalEntity.getPageSize());
        if (null != msgType && !"".equals(msgType)) {
            paperEntity.setEndTime(LocalDateTime.now());
        }
        // 获取频道
        String[] split = id.split(ConstantSys.SEPARATE_COMMA);
        for (String str : split) {
            PortalEntity portal = new PortalEntity();
            List<PaperEntity> resultData = new ArrayList<>();

                paperEntity.setClassId(str);
                resultData = paperRepository.listPaper(request, paperEntity, split);
                if (CollectionUtil.isNotEmpty(resultData)) {
                    List<String> paperIds = new ArrayList<>();
                    resultData.forEach(e -> paperIds.add(e.getPaperId()));

                    List<MeetingEntity> meeting = meetingRepository.getMeeting(paperIds);
                    if (CollectionUtil.isNotEmpty(meeting)) {
                        for (MeetingEntity meet : meeting) {
                            for (PaperEntity paper : resultData) {
                                if (meet.getPaperId().equals(paper.getPaperId())) {
                                    meet.setPaperName(paper.getPaperName());
                                }
                            }
                        }
                        portal.setMeetingList(meeting);
                    }

                    List<ImageEntity> images = imageRepository.getImages(paperIds);
                    if (CollectionUtil.isNotEmpty(images)) {
                        for (ImageEntity image : images) {
                            for (PaperEntity paper : resultData) {
                                if (image.getPaperId().equals(paper.getPaperId())) {
                                    image.setName(paper.getPaperName());
                                }
                            }
                        }
                        portal.setImageList(images);
                    }
                    // }
                }
            // }
            portal.setPaperList(resultData);
            result.add(portal);
        }
        return result;
    }
}
