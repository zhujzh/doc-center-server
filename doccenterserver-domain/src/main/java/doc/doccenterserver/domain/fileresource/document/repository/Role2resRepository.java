package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.Role2resEntity;

import java.util.List;

/**
 * @author Tangcancan
 */
public interface Role2resRepository {
    List<Role2resEntity> getRes(String roleId);

    Integer delete(String roleId);

    Integer insert(Role2resEntity role2resEntity);
}
