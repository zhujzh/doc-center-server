package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "job详细日志实体")
public class JobDetailLogEntity {

    @Field(name = "任务日_日志流水号")
    private String logId;

    @Field(name = "步骤 标识")
    private Integer stepId;

    @Field(name = "步骤状态")
    private Integer stepStatus;

    @Field(name = "消息体")
    private String content;

    @Field(name = "步骤处理日志信息")
    private String message;

    @Field(name = "日志处理意见")
    private String logIdea;

    @Field(name = "日志创建时间")
    private Date createTime;

    @Field(name = "日志更新时间")
    private Date updateTime;

    @Field(name = "是否当前环节")
    private String currActFlag;

    @Field(name = "备用字段1")
    private String reserved1;

    @Field(name = "备用字段2")
    private String reserved2;

    @Field(name = "备用字段3")
    private String reserved3;

    @Field(name = "备用字段4")
    private String reserved4;

    @Field(name = "备用字段5")
    private String reserved5;

    @Field(name = "备用字段6")
    private String reserved6;

    @Field(name = "备用字段7")
    private String reserved7;

    @Field(name = "备用字段8")
    private String reserved8;

    @Field(name = "备用字段9")
    private String reserved9;

    @Field(name = "备用字段10")
    private String reserved10;

    @Field(name = "备用字段11")
    private String reserved11;

    @Field(name = "备用字段12")
    private String reserved12;

    @Field(name = "备用字段13")
    private String reserved13;

    @Field(name = "备用字段14")
    private String reserved14;

    // 当前日志的所在环节
    public final static Integer STEP_ID_1 = 1;// 1.MQ消息入库
    public final static Integer STEP_ID_2 = 2;//2.MQ消息处理
    public final static Integer STEP_ID_3 = 3;// 3.消息归档
    public final static Integer STEP_ID_4 = 4;//4.文档转换
    public final static Integer STEP_ID_5 = 5;//5.文档发布
    public final static Integer STEP_ID_6 = 6; //6.创建索引
    public final static Integer STEP_ID_7 = 7;//7.发送ESB消息
    public final static Integer STEP_ID_8 = 8;//8.消息异动
    public final static Integer STEP_ID_9 = 9;//9.网盘文档转换
    public final static Integer STEP_ID_10 = 10;//10.网盘创建索引
    public final static Integer STEP_ID_11 = 11;//11.网盘索引删除

    //当前日志的状态（0：失败；）
    public final static Integer STEP_STATUS_FAILD = 0;
    //当前日志的状态（1：成功；）
    public final static Integer STEP_STATUS_SUCCESS = 1;
    //当前日志的状态（2：待查；）
    public final static Integer STEP_STATUS_WATING = 2;
    //当前日志的状态（3：忽略；）
    public final static Integer STEP_STATUS_IGNOR = 3;
    //当前日志的状态（4：处理中）
    public final static Integer STEP_STATUS_HANDLING = 4;

    public JobDetailLogEntity(String logId, String content, Date createTime,
                              String logIdea, String message, Integer stepId, Integer stepStatus,
                              Date updateTime) {
        super();
        this.setLogId(logId);
        this.setContent(content);
        this.setCreateTime(createTime);
        this.setLogIdea(logIdea);
        this.setMessage(message);
        this.setStepId(stepId);
        this.setStepStatus(stepStatus);
        this.setUpdateTime(updateTime);
    }
}
