package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;
import doc.doccenterserver.domain.fileresource.document.model.TreeNodeEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;

import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
public interface UserRepository {
    UserEntity getUserInfo(String userId);

    List<Map<String, Object>> querySelectedData(String authObjIds);

    List<UserEntity> getUserListByGroupId(List<String> groupUserId);

    List<UserEntity> getUserListByOrgIds(List<String> orgIds);
    List<UserEntity> getUserByUserName(String userName,List<String> orgIds);

    List<OrgEntity> getOrgListTreeByUnitIds(List<String> orgUntIds);
}
