package doc.doccenterserver.domain.fileresource.message.repository;

/**
 * 文件发布
 * repository
 */
public interface DocPublishChangelogRepository {

    /**
     * 文件发布
     */
    void docPublish();
}
