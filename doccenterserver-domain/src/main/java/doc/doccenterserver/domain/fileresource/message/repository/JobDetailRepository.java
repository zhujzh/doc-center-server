package doc.doccenterserver.domain.fileresource.message.repository;


import doc.doccenterserver.domain.fileresource.message.model.JobDetailLogEntity;

import java.rmi.ServerException;

/**
 * 任务详情
 * repository
 */
public interface JobDetailRepository {

    boolean updateStatus(String logId,Integer stepId, Integer stepStatus);
    boolean updateByStepId(JobDetailLogEntity jobDetailLogEntity) throws ServerException;
    boolean  insert(String logId, String mqmsg,String message, Integer stepId, Integer stepStatus);
}
