package doc.doccenterserver.domain.fileresource.base;

/**
 * 信息 提示 枚举类
 *
 * @author Tangcancan
 */

public enum ResponseEnum {

    OPERATE_SUCCESS("200", "操作成功"),
    QUERY_SUCCESS("200", "查询成功"),
    DELETE_SUCCESS("200", "删除成功"),
    SAVE_SUCCESS("200", "保存成功");

    /**
     * 错误码
     */
    private final String code;

    /**
     * 错误描述
     */
    private final String msg;

    ResponseEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
