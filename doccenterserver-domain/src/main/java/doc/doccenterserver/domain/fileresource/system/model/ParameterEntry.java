package doc.doccenterserver.domain.fileresource.system.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统配置参数
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "系统配置参数")
public class ParameterEntry {

    @Field(name = "主键")
    private String pId;

    @Field(name = "定义标识符")
    private String pKey;

    @Field(name = "值")
    private String pValue;

    @Field(name = "描述")
    private String description;
}
