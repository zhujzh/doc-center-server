package doc.doccenterserver.domain.fileresource.system.service;

import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.JobLogEntry;
import doc.doccenterserver.domain.fileresource.system.repository.JobLogManageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/12 19:12
 */
@Slf4j
@Component("JobLogDS")
@DomainService(name = "资源日志")
public class JobLogDomainService {

    @Resource
    private JobLogManageRepository jobLogManageRepository;

    public PageResult<List<JobLogEntry>> listjobLogs(JobLogEntry jobLogEntry) {
        return jobLogManageRepository.listJobLogs(jobLogEntry);
    }

    public Map<String,Object> lookLogDetail(String id) {
        return jobLogManageRepository.lookLogDetail(id);
    }

    public Map<String, Object> viewarchmsg(String logId, Integer stepId) {
        return jobLogManageRepository.viewarchmsg(logId,stepId);
    }

    public Map<String, Object> rearchive(String logId) {
        return jobLogManageRepository.rearchive(logId);
    }

    public Map<String, Object> handlefiles(String sourcePaperId, String paperId, String appId, String optType, HttpServletRequest request) {
        return jobLogManageRepository.handlefiles(sourcePaperId,paperId,appId,optType,request);
    }

    public Map<String, Object> handleIndex(String logId, String paperId, String operation, String appId) {
        return jobLogManageRepository.handleIndex(logId,paperId,operation,appId);
    }

    public Map<String, Object> sendmq(String logId, Integer stepId) {
        return jobLogManageRepository.sendmq(logId,stepId);
    }

    public Map<String, Object> viewerrmsg(String logId, Integer stepId) {
        return jobLogManageRepository.viewerrmsg(logId,stepId);
    }

    public boolean retry(String logId, Integer stepId) {
        return jobLogManageRepository.retry(logId,stepId);
    }
}
