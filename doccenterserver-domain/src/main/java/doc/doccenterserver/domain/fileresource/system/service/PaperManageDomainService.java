package doc.doccenterserver.domain.fileresource.system.service;

import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.system.repository.PaperManageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 15:59
 */
@Slf4j
@Component("PaperManageDS")
@DomainService(name = "文档维护")
public class PaperManageDomainService {
    @Resource
    private PaperManageRepository paperManageRepository;

    public PageResult<List<PaperEntity>> listpapers(PaperEntity paperEntity) {
        return paperManageRepository.listpapers(paperEntity);
    }

    public Map<String, Object> topdoc(String paperId, Boolean topStatus) {
        return paperManageRepository.topdoc(paperId,topStatus);
    }

    public Map<String, Object> editpaper(String paperId, HttpServletRequest request) {
        return paperManageRepository.editpaper(paperId,request);
    }

    public Map<String, Object> savePaper(PaperEntity paperEntity, HttpServletRequest request) {
        return paperManageRepository.savePaper(paperEntity,request);
    }
}
