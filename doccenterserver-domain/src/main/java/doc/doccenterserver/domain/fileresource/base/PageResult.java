package doc.doccenterserver.domain.fileresource.base;

import lombok.Data;

/**
 * @author Tangcancan
 */
@Data
public class PageResult<T> {

    /**
     * 数据结果集
     */
    private T data;

    /**
     * 分页结果
     */
    private BaseDto page;

    public PageResult(T data, BaseDto page) {
        this.data = data;
        this.page = page;
    }
}
