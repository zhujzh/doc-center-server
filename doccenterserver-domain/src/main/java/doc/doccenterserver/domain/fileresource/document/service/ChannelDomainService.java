package doc.doccenterserver.domain.fileresource.document.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.Parameter;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ChannelAuthEntity;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.document.model.EnumEntity;
import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;
import doc.doccenterserver.domain.fileresource.document.model.TemplateEntity;
import doc.doccenterserver.domain.fileresource.document.model.TreeNodeEntity;
import doc.doccenterserver.domain.fileresource.document.repository.*;
import doc.doccenterserver.domain.fileresource.document.utils.ChannelAuthUtil;
import doc.doccenterserver.domain.fileresource.system.model.ParameterEntry;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.system.repository.ParameterRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;


/**
 * @author Tangcancan
 */
@Slf4j
@Component("ChannelDS")
@DomainService(name = "频道服务")
public class ChannelDomainService {

    @Resource
    private ChannelRepository channelRepository;

    @Resource
    private ChannelAuthRepository channelAuthRepository;

    @Resource
    private TemplateRepository templateRepository;

    @Resource
    private OrgRepository orgRepository;
    @Resource
    private UserRepository userRepository;
    @Resource
    private ParameterRepository parameterRepository;


    @ReturnValue
    @Method(name = "根据ID查询频道")
    public ChannelEntity getEditChannel(@Parameter(name = "id") String id) {
        ChannelEntity channelEntity = channelRepository.getById(id);
        String authObjectNames = channelEntity.getAuthObjectNames();
        List<String> authObjectIds = new ArrayList<>();
        List<Integer> authList = new ArrayList<>();
        List<Map<String,Object>> authObjectList=  new ArrayList<>();
        List<Map<String,Object>> authObjectOrgList=  new ArrayList<>();
        if (null != authObjectNames && !"".equals(authObjectNames)) {
            List<ChannelAuthEntity> paperAuthList = channelAuthRepository.getChannelAuthLists(id);
            for (ChannelAuthEntity item : paperAuthList) {
                authObjectIds.add(item.getAuthObject());
                Map<String,Object> map=new HashMap<>();
                map.put("ID",item.getAuthObject());
                if(ConstantSys.USER.equals(item.getObjectType())){
                    UserEntity userEntity = userRepository.getUserInfo(item.getAuthObject());
                    if(ObjectUtil.isEmpty(userEntity)){
                        map.put("NAME",userEntity.getUserName());
                    }
                }else{
                    OrgEntity orgEntity = orgRepository.getById(item.getAuthObject());
                    if(ObjectUtil.isNotEmpty(orgEntity)){
                        map.put("NAME",orgEntity.getOrgName());
                    }
                }
                map.put("DATA_TYPE",item.getObjectType());
                authObjectList.add(map);
                List<List<Integer>> res = ChannelAuthUtil.findSum(item.getAuthValue(),ChannelAuthUtil.NUMS);
                for (List<Integer> sub : res) {
                    authList=sub;
                }
            }
            authList=authList.stream().distinct().collect(Collectors.toList());
        }
        if (null != channelEntity.getOrgId() && !"".equals(channelEntity.getOrgId())) {
            OrgEntity orgEntity = orgRepository.getById(channelEntity.getOrgId());
            Map<String,Object> map=new HashMap<>();
            if (null != orgEntity) {
                channelEntity.setOrgName(orgEntity.getOrgName());
                map.put("NAME",orgEntity.getOrgName());
            }

            map.put("ID",channelEntity.getOrgId());
            map.put("DATA_TYPE",ConstantSys.ORG);
            authObjectOrgList.add(map);
        }
        channelEntity.setAuthValue(authList);
        channelEntity.setAuthObjectIds(authObjectIds);
        channelEntity.setAuthObjectList(authObjectList);
        channelEntity.setAuthObjectOrgList(authObjectOrgList);
        getTemplateForChannel(channelEntity);
        return channelEntity;
    }
    private void getTemplateForChannel(ChannelEntity channelEntity) {
        // 获取模板
        List<TemplateEntity> templateEntityList = templateRepository.getAllTemplate();
        if (CollectionUtil.isNotEmpty(templateEntityList) && templateEntityList.size() > 0) {
            Map<Integer, List<TemplateEntity>> listMap = templateEntityList.stream().collect(Collectors.groupingBy(TemplateEntity::getType));
            if (listMap.containsKey(EnumEntity.TEMPLATE_TYPE_ONE)) {
                channelEntity.setOverviewTemplate(listMap.get(EnumEntity.TEMPLATE_TYPE_ONE));
            }
            if (listMap.containsKey(EnumEntity.TEMPLATE_TYPE_TWO)) {
                channelEntity.setViewTemplate(listMap.get(EnumEntity.TEMPLATE_TYPE_TWO));
            }
        }
    }

    @ReturnValue
    @Method(name = "根据ID查询频道")
    public ChannelEntity getAddChannel(String parentId) {
        ChannelEntity chan = new ChannelEntity();
        chan.setParentId(parentId);
        // 自动生成的频道编号
        String curId = channelRepository.getLastestChannelId(parentId);
        // 截取频道id去除父节点id的那一部分，如1010101,10101是父节点，则cId为01
        chan.setCId(parentId + curId);
        // 初始化频道路径
        chan.setPath(channelRepository.initChannelPath(parentId, curId));
        // 获取模板
        getTemplateForChannel(chan);
        chan.setCurId(curId);
        return chan;
    }

    @ReturnValue
    @Method(name = "查询频道")
    public List<ChannelEntity> selectList(@Parameter(name = "channel") ChannelEntity channel) {
        return channelRepository.selectList(channel);
    }

    @ReturnValue
    @Method(name = "查询频道")
    public PageResult<List<ChannelEntity>> selectByPage(@Parameter(name = "channel") ChannelEntity channel) {
        return channelRepository.selectByPage(channel);
    }

    @ReturnValue
    @Method(name = "频道管理-初始化树结构")
    public List<TreeNodeEntity> findInitTree(String parentId) {
        List<TreeNodeEntity> treeNodeList = new ArrayList<>();
        if (parentId != null) {
            List<ChannelEntity> channelList = channelRepository.getInItTree(parentId);
            if (channelList.size() > 0) {
                for (ChannelEntity channel : channelList) {
                    TreeNodeEntity node = new TreeNodeEntity(channel.getId(), channel.getParentId(), channel.getName());
                    node.setOrgId(channel.getOrgId());
                    treeNodeList.add(node);
                }
            }
        }
        return treeNodeList;
    }

    @ReturnValue
    @Method(name = "新增频道")
    public ChannelEntity addChannel(ChannelEntity channelEntity) {
        ChannelEntity channel = channelRepository.addChannel(channelEntity);
        updateChannelAuth(channelEntity);
        return channel;
    }

    @ReturnValue
    @Method(name = "编辑频道")
    public ChannelEntity editChannel(ChannelEntity channelEntity) {
        ChannelEntity channel = channelRepository.editChannel(channelEntity);
        updateChannelAuth(channelEntity);
        return channel;
    }

    @ReturnValue
    @Method(name = "删除、编辑频道验证")
    private void updateChannelAuth(ChannelEntity channelEntity) {
        String channelId = channelEntity.getId();
        // 1、删除频道现有权限
        channelAuthRepository.deleteChannelAuthById(channelId);
        // 2、插入最新权限
        List<String> authObjectArr = channelEntity.getAuthObjectIds();
        if (CollectionUtil.isNotEmpty(authObjectArr) && authObjectArr.size() > 0) {
            List<Integer> authValue = channelEntity.getAuthValue();
            Integer sum=authValue.stream().reduce(Integer::sum).orElse(0);
            for (String authObject : authObjectArr) {
                ChannelAuthEntity channelAuthEntity = new ChannelAuthEntity();
                channelAuthEntity.setChannelId(channelId);
                channelAuthEntity.setAuthObject(authObject);
                channelAuthEntity.setAuthValue(sum);
                channelAuthRepository.addChannelAuth(channelAuthEntity);
            }
        }
    }

    @ReturnValue
    @Method(name = "删除频道")
    public ChannelEntity deleteChannel(String ids) {
        return channelRepository.deleteChannel(ids);
    }

    @ReturnValue
    @Method(name = "查询单个频道")
    public ChannelEntity getChannel(String id) {
        return channelRepository.getById(id);
    }
}
