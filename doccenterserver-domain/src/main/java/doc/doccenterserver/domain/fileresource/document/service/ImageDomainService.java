package doc.doccenterserver.domain.fileresource.document.service;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ImageEntity;
import doc.doccenterserver.domain.fileresource.document.repository.ImageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("ImageDS")
@DomainService(name = "ImageDomainService")
public class ImageDomainService {

    @Resource
    private ImageRepository imageRepository;

    @ReturnValue
    @Method(name = "新增初始化")
    public ImageEntity addInit() {
        return imageRepository.addInit();
    }

    @ReturnValue
    @Method(name = "编辑初始化")
    public ImageEntity editInit(String imgId) {
        return imageRepository.editInit(imgId);
    }

    @ReturnValue
    @Method(name = "保存操作")
    public ImageEntity save(ImageEntity imageEntity) {
        return imageRepository.save(imageEntity);
    }

    @ReturnValue
    @Method(name = "分页查询")
    public PageResult<List<ImageEntity>> getList(ImageEntity imageEntity) {
        return imageRepository.getList(imageEntity);
    }

    @ReturnValue
    @Method(name = "删除")
    public ImageEntity delete(String imgIds) {
        return imageRepository.delete(imgIds);
    }

    @ReturnValue
    @Method(name = "获取明细信息")
    public ImageEntity getDetail(String imgId) {
        return imageRepository.getDetail(imgId);
    }

    @ReturnValue
    @Method(name = "下载")
    public ImageEntity downloadFile(String imgId, HttpServletRequest request, HttpServletResponse response) {
        return imageRepository.downloadFile(imgId, request, response);
    }

    @ReturnValue
    @Method(name = "展示")
    public ImageEntity showImageTailorPage(ImageEntity imageEntity) {
        return imageRepository.showImageTailorPage(imageEntity);
    }

    @ReturnValue
    @Method(name = "图片信息")
    public ImageEntity getImageInfo(String imgId, HttpServletResponse response) {
        return imageRepository.getImageInfo(imgId, response);
    }

    @ReturnValue
    @Method(name = "裁剪")
    public ImageEntity cutImage(ImageEntity imageEntity) {
        return imageRepository.cutImage(imageEntity);
    }
}
