package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Tangcancan
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "PortalEntity")
public class PortalEntity extends BaseDto {

    // 类别
    private String msgType;

    // 频道id
    private String id;

    // 频道父id
    private String parentId;

    // 图片新闻
    private List<ImageEntity> imageList;

    // 频道
    private List<ChannelEntity> channelList;

    // 文档
    private List<PaperEntity> paperList;

    // 会议
    private List<MeetingEntity> meetingList;

    // 分页
    private BaseDto page;

    // 搜索条件
    private String content;

}
