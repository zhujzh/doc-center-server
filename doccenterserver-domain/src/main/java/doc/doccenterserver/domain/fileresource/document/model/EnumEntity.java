package doc.doccenterserver.domain.fileresource.document.model;

/**
 * @author Tangcancan
 */
public class EnumEntity {

    public static final Integer TEMPLATE_TYPE_ONE = 1;
    public static final Integer TEMPLATE_TYPE_TWO = 2;
}
