package doc.doccenterserver.domain.fileresource.message.repository;

/**
 * 发送消息
 * repository
 */
public interface SendMqChangelogRepository {
    /**
     * 发送消息
     */
    void sendMq();
}
