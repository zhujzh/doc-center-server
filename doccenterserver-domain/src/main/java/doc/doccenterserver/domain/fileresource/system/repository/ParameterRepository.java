package doc.doccenterserver.domain.fileresource.system.repository;


import doc.doccenterserver.domain.fileresource.system.model.ParameterEntry;

import java.util.List;

/**
 * 系统配置参数
 * repository
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
public interface ParameterRepository {

    List<ParameterEntry> selectList();

    ParameterEntry getByKey(String key);
}
