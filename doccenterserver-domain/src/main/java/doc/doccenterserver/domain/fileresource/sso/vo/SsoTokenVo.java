package doc.doccenterserver.domain.fileresource.sso.vo;

import lombok.Data;

/**
 * @author wangxp
 * @description ssoVo对象
 * @date 2023/3/30 9:15
 */
@Data
public class SsoTokenVo {


    private String openid;

    //访问令牌
    private String access_token;

    //刷新令牌，30天未使用则失效
    private String refresh_token;

    // 访问令牌多少秒后超时
    private Long expires_in;

    //授权类型
    private String scope;
}
