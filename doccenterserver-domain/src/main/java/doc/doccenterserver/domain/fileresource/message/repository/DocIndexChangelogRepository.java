package doc.doccenterserver.domain.fileresource.message.repository;

/**
 * 创建索引
 */
public interface DocIndexChangelogRepository {

    /**
     * 创建索引
     */
    void docIndex();


    //后台维护索引创建
    boolean insertDocIndexChange(String logId, String paperId, String operation);
}
