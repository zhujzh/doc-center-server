package doc.doccenterserver.domain.fileresource.system.repository;

import doc.doccenterserver.domain.fileresource.system.model.ResEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ResRepository {
    ResEntity getByParentId(String s);

    Integer findChildrenNumById(String resId);

    List<ResEntity> getInItTree(String s);

    ResEntity getById(String id);

    ResEntity addRes(ResEntity resEntity);

    ResEntity editRes(ResEntity resEntity);

    ResEntity deleteRes(String id);

    List<ResEntity> getAllRes();

    List<ResEntity> getInItUserTree(HttpServletRequest request);
}
