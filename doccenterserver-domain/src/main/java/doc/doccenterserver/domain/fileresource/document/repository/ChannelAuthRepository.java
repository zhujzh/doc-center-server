package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.ChannelAuthEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;

import java.util.List;

/**
 * @author Tangcancan
 */
public interface ChannelAuthRepository {
    List<ChannelAuthEntity> getChannelAuthLists(String id);

    Integer deleteChannelAuthById(String id);

    ChannelAuthEntity addChannelAuth(ChannelAuthEntity channelAuthEntity);

    List<ChannelAuthEntity> isChannelAuthList(UserEntity userEntity);

}
