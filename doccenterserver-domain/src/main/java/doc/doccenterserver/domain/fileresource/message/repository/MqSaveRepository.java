package doc.doccenterserver.domain.fileresource.message.repository;

/**
 * 异常任务
 * repository
 */
public interface MqSaveRepository {
    /**
     * 异常任务处理
     */
    void msgErr();
}
