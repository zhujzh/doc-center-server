package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;

import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
public interface OrgRepository {
    OrgEntity getById(String id);

    List<Map<String, Object>> querySelectedOrgs(List<List<String>> orgMutilList);

    List<OrgEntity> getAllOrg();
    List<OrgEntity> getOrgByOrgId(String parentId,List<String> queryType);

    List<OrgEntity> getOrgByUserName(String name, List<String> bizIds);
}
