package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.PaperAuthEntity;

import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
public interface PaperAuthRepository {
    Integer deletePaperAuthById(String paperId);

    Integer insertPaperAuth(PaperAuthEntity paperAuthEntity);

    List<PaperAuthEntity> getPaperAuthList(String paperId);

    Boolean getPaperAuth(String userId, String paperId);

}
