package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.TemplateEntity;

import java.util.List;

/**
 * @author Tangcancan
 */
public interface TemplateRepository {
    PageResult<List<TemplateEntity>> getListTemplates(TemplateEntity templateEntity);

    TemplateEntity getTemplate(Integer id);

    TemplateEntity addTemplate(TemplateEntity templateEntity);

    TemplateEntity editTemplate(TemplateEntity templateEntity);

    TemplateEntity deleteTemplate(String ids);

    List<TemplateEntity> getAllTemplate();

}
