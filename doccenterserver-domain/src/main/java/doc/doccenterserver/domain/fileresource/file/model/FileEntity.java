package doc.doccenterserver.domain.fileresource.file.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 文件
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "文件实体")
public class FileEntity extends BaseDto {

    @Field(name = "主键")
    private String fileId;

    @Field(name = "文件名称")
    private String filename;

    @Field(name = "文件保存路径")
    private String filePath;

    @Field(name = "文件大小")
    private Double fileSize;

    @Field(name = "文件后缀")
    private String fileFormat;

    @Field(name = "文件MD5值")
    private String fileMd5;

    @Field(name = "应用ID")
    private String appId;

    @Field(name = "上传时间")
    private Date createDate;

    @Field(name = "修改时间")
    private Date updateDate;

    @Field(name = "删除标记")
    private String deleteFlag;

    @Field(name = "删除时间")
    private Date deleteDate;

    @Field(name = "bigdataFileId")
    private String bigdataFileId;

    @Field(name = "LABEL_NAME")
    private String labelName;

    @Field(name = "IS_UPLOAD")
    private String isUpload;

    private String uploadStartDate;
    private String uploadEndDate;
    private String appName;
    private String fileSizeStr;
}
