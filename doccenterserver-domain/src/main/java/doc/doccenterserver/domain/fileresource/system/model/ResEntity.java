package doc.doccenterserver.domain.fileresource.system.model;

import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import com.baomidou.mybatisplus.annotation.TableId;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "资源管理实体")
public class ResEntity extends BaseDto {

    @TableId
    private String resId;

    private String resKey;

    private String resType;

    private String resValue;

    private String resParentId;

    private Integer resSort;

    private Integer authYesNo;

    private String resIcon;

}
