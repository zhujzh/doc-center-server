package doc.doccenterserver.domain.fileresource.system.repository;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;
import doc.doccenterserver.domain.fileresource.system.model.JobLogEntry;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/19 9:33
 */
public interface JobLogManageRepository {
    PageResult<List<JobLogEntry>> listJobLogs(JobLogEntry jobLogEntry);

    Map<String, Object> lookLogDetail(String id);

    Map<String, Object> viewarchmsg(String logId, Integer stepId);

    Map<String, Object> rearchive(String logId);

    Map<String, Object> handlefiles(String sourcePaperId, String paperId, String appId, String optType, HttpServletRequest request);

    Map<String, Object> handleIndex(String logId, String paperId, String operation, String appId);

    Map<String, Object> sendmq(String logId, Integer stepId);

    Map<String, Object> viewerrmsg(String logId, Integer stepId);

    boolean retry(String logId, Integer stepId);
}
