package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.MeetingEntity;

import java.util.List;

/**
 * @author Tangcancan
 */
public interface MeetingRepository {
    Integer saveMeeting(MeetingEntity meeting);

    MeetingEntity getMeetingById(String paperId);

    List<MeetingEntity> getMeeting(List<String> paperIds);
}
