package doc.doccenterserver.domain.fileresource.file.repository;

import doc.doccenterserver.domain.fileresource.file.model.HomeEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Tangcancan
 */
public interface HomeRepository {
    HomeEntity getDocSearch(HttpServletRequest request, HomeEntity homeEntity);

}
