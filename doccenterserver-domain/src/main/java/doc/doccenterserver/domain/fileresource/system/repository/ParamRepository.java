package doc.doccenterserver.domain.fileresource.system.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;

import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 22:04
 */
public interface ParamRepository {
    PageResult<List<ParamEntity>> getListParam(ParamEntity paramEntity);

    ParamEntity addParam(ParamEntity paramEntity);

    ParamEntity editParam(ParamEntity paramEntity);

    ParamEntity deleteParam(String id);

    ParamEntity getById(String id);
}
