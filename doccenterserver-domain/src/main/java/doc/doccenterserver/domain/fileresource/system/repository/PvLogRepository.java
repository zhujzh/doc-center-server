package doc.doccenterserver.domain.fileresource.system.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.domain.fileresource.system.model.PvLogEntity;

import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 22:04
 */
public interface PvLogRepository {
    PageResult<List<PvLogEntity>> getListParam(PvLogEntity paramEntity);

    PvLogEntity add(PvLogEntity pvLogEntity);

}
