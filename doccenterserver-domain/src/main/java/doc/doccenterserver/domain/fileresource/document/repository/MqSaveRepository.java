package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.MqSaveEntity;

import java.util.List;

/**
 * @author Tangcancan
 */
public interface MqSaveRepository {
    List<MqSaveEntity> getMqSaveList(MqSaveEntity mqSaveEntity);

    Integer insertMqSave(MqSaveEntity mqSave);
}
