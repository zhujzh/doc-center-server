package doc.doccenterserver.domain.fileresource.message.repository;

/**
 * 消息处理
 * repository
 */
public interface MqChangelogRepository {

    /**
     * 消息处理
     * @return
     */
    void msgHandle();
}
