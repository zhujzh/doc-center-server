package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.GroupEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
public interface GroupRepository {
    List<Map<String, Object>> querySelectedGroups(List<List<String>> groupMutilList);

    GroupEntity editInit(GroupEntity groupEntity);

    GroupEntity save(GroupEntity groupEntity);

    PageResult<List<GroupEntity>> getList(GroupEntity groupEntity);

    GroupEntity saveGroupMembers(GroupEntity groupEntity);

    GroupEntity delete(String groupIds);

    GroupEntity deleteGroupById(String groupId);

    GroupEntity editUserGroup(HttpServletRequest request, GroupEntity groupEntity);

    GroupEntity insertRoleGroup(GroupEntity groupEntity);

    GroupEntity selectRgByRoleId(GroupEntity groupEntity);

    List<GroupEntity> queryGroups(String groupUserId);

}
