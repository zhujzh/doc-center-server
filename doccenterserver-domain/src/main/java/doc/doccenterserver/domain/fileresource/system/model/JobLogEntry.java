package doc.doccenterserver.domain.fileresource.system.model;

import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author ch
 * @date 2023/4/12 19:05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "日志")
public class JobLogEntry extends BaseDto {
    //主状态为完成
    public final static String STATUS_Y = "Y";
    //主状态为 处理中
    public final static String STATUS_N = "N";
    //主状态为排队
    public final static String STATUS_Q = "Q";
    //主状态为异常
    public final static String STATUS_E = "E";
    //主状态为 忽略
    public final static String STATUS_IGNOR = "I";

    //文档操作 创建
    public final static String OPERATION_CREATE = "create";
    //文件操作 更新，网盘独有操作
    public final static String OPERATION_UPDATE = "update";
    //文档操作 删除
    public final static String OPERATION_RETRIEVE = "retrieve";

    private String logId; //LOG_ID <日志流水号>

    private String content; //CONTENT <消息体(XML格式MQ信息)>

    private String logName; //LOG_NAME <日志名称>

    private String operation; //OPERATION <文档操作（create/retrieve）>


    private String reserved1; //RESERVED_1 <备用字段1>


    private String reserved10; //RESERVED_10 <>


    private String reserved11; //RESERVED_11 <>


    private String reserved12; //RESERVED_12 <>


    private String reserved13; //RESERVED_13 <>


    private String reserved14; //RESERVED_14 <>


    private String reserved2; //RESERVED_2 <>


    private String reserved3; //RESERVED_3 <>


    private String reserved4; //RESERVED_4 <>


    private String reserved5; //RESERVED_5 <>


    private String reserved6; //RESERVED_6 <>


    private String reserved7; //RESERVED_7 <>


    private String reserved8; //RESERVED_8 <>


    private String reserved9; //RESERVED_9 <>


    private String sourceAppId; //SOURCE_APP_ID <消息来源的应用编号>


    private String sourceAppName; //SOURCE_APP_NAME <消息来源（OA、JT_ECM、WZ_ECM......）>


    private String sourcePaperId; //SOURCE_PAPER_ID <源应用文档编号>


    private String sourceSeq; //SOURCE_SEQ <消息来源的应用流水号>


    private String status; //STATUS <Y:完成，N:未完成>

    private String paperId;//PAPER_ID<文档id>

    private String archiveOrgCode;//ARCHIVE_ORG_CODE<归档组织编码>

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date createTime; //CREATE_TIME <日志创建时间>

    private Integer stepId;
    private Integer stepStatus;

    private String orderByColumns;
    private String createStartDate;
    private String createEndDate;
}
