package doc.doccenterserver.domain.fileresource.system.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 任务日志详细表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "日志")
public class JobDetailLogEntry {

    @Field(name = "任务日_日志流水号")
    @TableId(type = IdType.INPUT, value = "log_id")
    private String logId;

    @Field(name = "步骤 标识")
    @TableField("step_id")
    private Integer stepId;

    @Field(name = "步骤状态")
    @TableField("step_status")
    private Integer stepStatus;

    @Field(name = "消息体")
    @TableField("content")
    private String content;

    @Field(name = "步骤处理日志信息")
    @TableField("message")
    private String message;

    @Field(name = "日志处理意见")
    @TableField("log_idea")
    private String logIdea;

    @Field(name = "日志创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @Field(name = "日志更新时间")
    @TableField("update_time")
    private Date updateTime;

    @Field(name = "是否当前环节")
    @TableField("curr_act_flag")
    private String currActFlag;

    @Field(name = "备用字段1")
    @TableField("reserved_1")
    private String reserved1;

    @Field(name = "备用字段2")
    @TableField("reserved_2")
    private String reserved2;

    @Field(name = "备用字段3")
    @TableField("reserved_3")
    private String reserved3;

    @Field(name = "备用字段4")
    @TableField("reserved_4")
    private String reserved4;

    @Field(name = "备用字段5")
    @TableField("reserved_5")
    private String reserved5;

    @Field(name = "备用字段6")
    @TableField("reserved_6")
    private String reserved6;

    @Field(name = "备用字段7")
    @TableField("reserved_7")
    private String reserved7;

    @Field(name = "备用字段8")
    @TableField("reserved_8")
    private String reserved8;

    @Field(name = "备用字段9")
    @TableField("reserved_9")
    private String reserved9;

    @Field(name = "备用字段10")
    @TableField("reserved_10")
    private String reserved10;

    @Field(name = "备用字段11")
    @TableField("reserved_11")
    private String reserved11;

    @Field(name = "备用字段12")
    @TableField("reserved_12")
    private String reserved12;

    @Field(name = "备用字段13")
    @TableField("reserved_13")
    private String reserved13;

    @Field(name = "备用字段14")
    @TableField("reserved_14")
    private String reserved14;

}
