package doc.doccenterserver.domain.fileresource.system.repository;


import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.DictEntry;

import java.util.List;

/**
 * 字典
 * repository
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
public interface DictRepository {
    DictEntry selectDict(String pid);

    DictEntry getByParentId(String pid);

    List<DictEntry> getInItTree(String pid);

    Integer findChildrenNumById(String id);

    PageResult<List<DictEntry>> getListDict(DictEntry dictEntry);

    DictEntry getById(String id);

    DictEntry editDict(DictEntry dictEntry);

    DictEntry deleteDict(String id);

    DictEntry addDict(DictEntry dictEntry);

    List<DictEntry> getByPId(String pid);

    DictEntry getByPIdAndVal(String pid, String val);

    DictEntry getDictByIdAndPid(DictEntry dict);

}
