package doc.doccenterserver.domain.fileresource.system.service;

import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;
import doc.doccenterserver.domain.fileresource.system.model.JobLogEntry;
import doc.doccenterserver.domain.fileresource.system.repository.JobDetailLogManageRepository;
import doc.doccenterserver.domain.fileresource.system.repository.JobLogManageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/12 19:12
 */
@Slf4j
@Component("JobDetailLogDS")
@DomainService(name = "资源日志")
public class JobDetailLogDomainService {

    @Resource
    private JobDetailLogManageRepository jobDetailLogManageRepository;


    public Map<String, Object> savejobdetaillog(JobDetailLogEntity jobDetailLogEntity) {
        return jobDetailLogManageRepository.savejobdetaillog(jobDetailLogEntity);
    }
}
