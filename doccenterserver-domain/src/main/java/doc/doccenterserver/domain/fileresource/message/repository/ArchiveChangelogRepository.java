package doc.doccenterserver.domain.fileresource.message.repository;

/**
 * 消息归档
 * repository
 */
public interface ArchiveChangelogRepository {

    /**
     * 消息归档
     * @return
     */
    void msgArchive();
}
