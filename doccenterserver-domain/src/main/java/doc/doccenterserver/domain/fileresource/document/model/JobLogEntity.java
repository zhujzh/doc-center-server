package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "job日志实体")
public class JobLogEntity {

    //主状态为完成
    public final static String STATUS_Y = "Y";
    //主状态为 处理中
    public final static String STATUS_N = "N";
    //主状态为排队
    public final static String STATUS_Q = "Q";
    //主状态为异常
    public final static String STATUS_E = "E";
    //主状态为 忽略
    public final static String STATUS_IGNOR = "I";

    @Field(name = "日志流水号")
    private String logId;

    @Field(name = "日志名称")
    private String logName;

    @Field(name = "文档ID")
    private String paperId;

    @Field(name = "消息来源")
    private String sourceAppName;

    @Field(name = "消息来源的应用编号")
    private String sourceAppId;

    @Field(name = "源应用文档编号")
    private String sourcePaperId;

    @Field(name = "消息来源的应用流水号")
    private String sourceSeq;

    @Field(name = "文档操作")
    private String operation;

    @Field(name = "消息体")
    private String content;

    @Field(name = "日志主状态")
    private String status;

    @Field(name = "归档组织ID")
    private String archiveOrgCode;

    @Field(name = "日志创建时间")
    private Date createTime;

    @Field(name = "备用字段1")
    private String reserved1;

    @Field(name = "备用字段2")
    private String reserved2;

    @Field(name = "备用字段3")
    private String reserved3;

    @Field(name = "备用字段4")
    private String reserved4;

    @Field(name = "备用字段5")
    private String reserved5;

    @Field(name = "备用字段6")
    private String reserved6;

    @Field(name = "备用字段7")
    private String reserved7;

    @Field(name = "备用字段8")
    private String reserved8;

    @Field(name = "备用字段9")
    private String reserved9;

    @Field(name = "备用字段10")
    private String reserved10;

    @Field(name = "备用字段11")
    private String reserved11;

    @Field(name = "备用字段12")
    private String reserved12;

    @Field(name = "备用字段13")
    private String reserved13;

    @Field(name = "备用字段14")
    private String reserved14;

    // 文档操作 创建
    public final static String OPERATION_CREATE = "create";
    // 文件操作 更新，网盘独有操作
    public final static String OPERATION_UPDATE = "update";
    // 文档操作 删除
    public final static String OPERATION_RETRIEVE = "retrieve";

    public JobLogEntity(String logId, String content, String logName,
                        String operation, String sourceAppId, String sourceAppName,
                        String sourcePaperId, String status, String archiveOrgCode) {
        super();
        this.setLogId(logId);
        this.setContent(content);
        this.setLogName(logName);
        this.setOperation(operation);
        this.setSourceAppId(sourceAppId);
        this.setSourceAppName(sourceAppName);
        this.setSourcePaperId(sourcePaperId);
        this.setStatus(status);
        this.setArchiveOrgCode(archiveOrgCode);
    }
}
