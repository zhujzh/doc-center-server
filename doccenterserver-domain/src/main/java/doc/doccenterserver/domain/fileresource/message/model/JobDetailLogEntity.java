package doc.doccenterserver.domain.fileresource.message.model;

import com.alibaba.bizworks.core.specification.Field;
import lombok.Data;

import java.util.Date;

/**
 * 任务日志详细表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
public class JobDetailLogEntity {

    @Field(name = "任务日_日志流水号")
    private String logId;

    @Field(name = "步骤 标识")
    private Integer stepId;

    @Field(name = "步骤状态")
    private Integer stepStatus;

    @Field(name = "消息体")
    private String content;

    @Field(name = "步骤处理日志信息")
    private String message;

    @Field(name = "日志处理意见")
    private String logIdea;

    @Field(name = "日志创建时间")
    private Date createTime;

    @Field(name = "日志更新时间")
    private Date updateTime;

    @Field(name = "是否当前环节")
    private String currActFlag;

    @Field(name = "备用字段1")
    private String reserved1;

    @Field(name = "备用字段2")
    private String reserved2;

    @Field(name = "备用字段3")
    private String reserved3;

    @Field(name = "备用字段4")
    private String reserved4;

    @Field(name = "备用字段5")
    private String reserved5;

    @Field(name = "备用字段6")
    private String reserved6;

    @Field(name = "备用字段7")
    private String reserved7;

    @Field(name = "备用字段8")
    private String reserved8;

    @Field(name = "备用字段9")
    private String reserved9;

    @Field(name = "备用字段10")
    private String reserved10;

    @Field(name = "备用字段11")
    private String reserved11;

    @Field(name = "备用字段12")
    private String reserved12;

    @Field(name = "备用字段13")
    private String reserved13;

    @Field(name = "备用字段14")
    private String reserved14;

    public JobDetailLogEntity(String logId, String content, Date createTime,
                              String logIdea, String message, Integer stepId, Integer stepStatus,
                              Date updateTime) {
        super();
        this.setLogId ( logId);
        this.setContent ( content);
        this.setCreateTime ( createTime);
        this.setLogIdea ( logIdea);
        this.setMessage ( message);
        this.setStepId ( stepId);
        this.setStepStatus ( stepStatus);
        this.setUpdateTime ( updateTime);
    }

    public JobDetailLogEntity(String logId, String message, Integer stepId, Integer stepStatus,
                              Date updateTime) {
        super();
        this.setLogId ( logId);
        this.setMessage ( message);
        this.setStepId ( stepId);
        this.setStepStatus ( stepStatus);
        this.setUpdateTime ( updateTime);
    }
    public JobDetailLogEntity(){
        super();
    }
}
