package doc.doccenterserver.domain.fileresource.system.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author ch
 * @date 2023/4/11 22:05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "参数配置管理")
public class PvLogEntity extends BaseDto {
    @Field(name = "主键")
    private String pvId;

    @Field(name = "当前操作的用户id")
    private String userId;

    @Field(name = "当前操作的用户名称")
    private String userName;

    @Field(name = "当前操作用户的部门id")
    private String orgId;

    @Field(name = "当前操作用户的部门名称")
    private String orgName;

    @Field(name = "当前操作用户所操作的菜单id")
    private String menuId;

    @Field(name = "操作时间")
    private Date visitTime;

    @Field(name = "当前用户所使用的浏览器类型")
    private String browse;

    @Field(name = "操作类型")
    private String operateType;
    private String resKey;
    private String menuValue;
    private String issueStartDate;
    private String issueEndDate;


}
