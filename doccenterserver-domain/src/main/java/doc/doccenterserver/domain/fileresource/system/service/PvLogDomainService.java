package doc.doccenterserver.domain.fileresource.system.service;

import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.domain.fileresource.system.model.PvLogEntity;
import doc.doccenterserver.domain.fileresource.system.repository.ParamRepository;
import doc.doccenterserver.domain.fileresource.system.repository.PvLogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zjz
 * @date 2023/4/11 22:03
 */
@Slf4j
@Component("PvLogDS")
@DomainService(name = "pv日志")
public class PvLogDomainService {

    @Resource
    private PvLogRepository pvLogRepository;


    public PageResult<List<PvLogEntity>> getListParam(PvLogEntity pvLogEntity) {
        return pvLogRepository.getListParam(pvLogEntity);
    }

    public PvLogEntity add(PvLogEntity pvLogEntity) {
        return pvLogRepository.add(pvLogEntity);
    }

}
