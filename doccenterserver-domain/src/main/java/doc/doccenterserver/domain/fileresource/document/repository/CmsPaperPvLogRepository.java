package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.CmsPaperPvLogEntity;

/**
 * @author Tangcancan
 */
public interface CmsPaperPvLogRepository {
    Integer saveCmsPaperPvLog(CmsPaperPvLogEntity cmsPaperPvLogEntity);
}
