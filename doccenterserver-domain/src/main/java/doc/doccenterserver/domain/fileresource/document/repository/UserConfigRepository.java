package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.UserConfigEntity;

/**
 * @author Tangcancan
 */
public interface UserConfigRepository {
    UserConfigEntity getUserConfigById(String userId);

}
