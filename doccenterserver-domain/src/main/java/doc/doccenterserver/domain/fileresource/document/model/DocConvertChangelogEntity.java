package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "待文档转换任务处理实体")
public class DocConvertChangelogEntity {

    @Field(name = "日志流水号")
    private String logId;

    @Field(name = "文档ID")
    private String paperId;

    @Field(name = "信息归档操作类型")
    private String operation;

    @Field(name = "错误重试次数")
    private Integer retryTimes;

    @Field(name = "信息产生时间")
    private Date createTime;

    public DocConvertChangelogEntity(String logId, Date createTime,
                                     String operation, String paperId, Integer retryTimes) {
        super();
        this.setLogId(logId);
        this.setCreateTime(createTime);
        this.setOperation(operation);
        this.setPaperId(paperId);
        this.setRetryTimes(retryTimes);
    }

}
