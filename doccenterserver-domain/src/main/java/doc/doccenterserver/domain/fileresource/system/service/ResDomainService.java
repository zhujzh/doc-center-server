package doc.doccenterserver.domain.fileresource.system.service;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.document.model.TreeNodeEntity;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import doc.doccenterserver.domain.fileresource.system.repository.ResRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 */
@Slf4j
@Component("ResDS")
@DomainService(name = "资源管理")
public class ResDomainService {
    private static final String ICON = "pIcon01";
    @Resource
    private ResRepository resRepository;

    @ReturnValue
    @Method(name = "资源管理-初始化树结构")
    public List<TreeNodeEntity> findInitTree() {
        List<TreeNodeEntity> treeNodeList = new ArrayList<TreeNodeEntity>();
        //顶级节点
        ResEntity topMenu = resRepository.getByParentId("0");
        if (topMenu != null) {
            if ("1".equals(topMenu.getResId())) {
                addTreeNode(treeNodeList, topMenu, true);
            }
            List<ResEntity> resList = resRepository.getInItTree("0");
            if (resList.size() > 0) {
                for (ResEntity res : resList) {
                    addTreeNode(treeNodeList, res, false);
                }
            }
        }
        return treeNodeList;
    }

    private void addTreeNode(List<TreeNodeEntity> list, ResEntity res, boolean isOpen) {
        Integer childrenNum = resRepository.findChildrenNumById(res.getResId());
        boolean hasChildren = (childrenNum > 0);
        TreeNodeEntity node = null;
        if (hasChildren) {
            node = new TreeNodeEntity(res.getResId(), res.getResParentId(), res.getResKey(), true, isOpen, hasChildren);
        } else {
            node = new TreeNodeEntity(res.getResId(), res.getResParentId(), res.getResKey(), true, isOpen, hasChildren);
        }
        node.setIconSkin(ICON);
        list.add(node);
    }

    public ResEntity getById(String id) {
        ResEntity resEntity = resRepository.getById(id);
        return resEntity;
    }

    public ResEntity addRes(ResEntity resEntity) {
        return resRepository.addRes(resEntity);
    }

    public ResEntity editRes(ResEntity resEntity) {
        return resRepository.editRes(resEntity);
    }

    public ResEntity deleteRes(String id) {
        return resRepository.deleteRes(id);
    }

    public List<TreeNodeEntity> initUserTree(HttpServletRequest request) {
        List<TreeNodeEntity> treeNodeList = new ArrayList<TreeNodeEntity>();
        //顶级节点
        ResEntity topMenu = resRepository.getByParentId("0");
        if (topMenu != null) {
            if ("1".equals(topMenu.getResId())) {
                addUserTreeNode(treeNodeList, topMenu, true);
            }
            List<ResEntity> resList = resRepository.getInItUserTree(request);
            if (resList.size() > 0) {
                for (ResEntity res : resList) {
                    addUserTreeNode(treeNodeList, res, false);
                }
            }
        }
        return treeNodeList;
    }

    private void addUserTreeNode(List<TreeNodeEntity> list, ResEntity res, boolean isOpen) {
        Integer childrenNum = resRepository.findChildrenNumById(res.getResParentId());
        boolean hasChildren = (childrenNum > 0);
        TreeNodeEntity node = null;
        if (hasChildren) {
            node = new TreeNodeEntity(res.getResId(), res.getResParentId(), res.getResKey(), res.getResValue(),res.getResIcon(),true, true, false);
        } else {
            node = new TreeNodeEntity(res.getResId(), res.getResParentId(), res.getResKey(), res.getResValue(),res.getResIcon(),true, true, true);
        }
        list.add(node);
    }
}
