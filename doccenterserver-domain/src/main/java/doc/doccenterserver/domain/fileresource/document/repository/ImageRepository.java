package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ImageEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Tangcancan
 */
public interface ImageRepository {

    Integer deleteImagesByPaperId(String paperId);

    Integer insertImage(ImageEntity imageEntity);

    List<ImageEntity> getImageList(ImageEntity imageEntity);

    ImageEntity addInit();

    ImageEntity editInit(String imgId);

    ImageEntity save(ImageEntity imageEntity);

    PageResult<List<ImageEntity>> getList(ImageEntity imageEntity);

    ImageEntity delete(String imgIds);

    ImageEntity getDetail(String imgId);

    ImageEntity downloadFile(String imgId, HttpServletRequest request, HttpServletResponse response);

    ImageEntity showImageTailorPage(ImageEntity imageEntity);

    ImageEntity getImageInfo(String imgId, HttpServletResponse response);

    ImageEntity cutImage(ImageEntity imageEntity);

    List<ImageEntity> getImages(List<String> paperIds);
}
