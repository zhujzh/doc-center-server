package doc.doccenterserver.domain.fileresource.document.service;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import doc.doccenterserver.domain.fileresource.document.repository.AttachmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */

@Slf4j
@Component("AttachmentDS")
@DomainService(name = "AttachmentDomainService")
public class AttachmentDomainService {

    @Resource
    private AttachmentRepository attachmentRepository;

    @ReturnValue
    @Method(name = "编辑初始化")
    public AttachmentEntity editInit(AttachmentEntity attachmentEntity) {
        return null;
    }

    @ReturnValue
    @Method(name = "保存操作")
    public AttachmentEntity save(AttachmentEntity attachmentEntity) {
        return null;
    }

    @ReturnValue
    @Method(name = "分页查询")
    public PageResult<List<AttachmentEntity>> getList(AttachmentEntity attachmentEntity) {
        return null;
    }

    @ReturnValue
    @Method(name = "删除")
    public AttachmentEntity delete(AttachmentEntity attachmentEntity) {
        return null;
    }
}
