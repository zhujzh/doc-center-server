package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.GroupMemberEntity;

import java.util.List;

/**
 * @author Tangcancan
 */
public interface GroupMemberRepository {
    List<GroupMemberEntity> getGroupAuth(String groupId);

    Integer deleteGroupMemberByGroupId(String groupId);

    Integer insertGroupMember(GroupMemberEntity member);

    Integer updateGroupMember(GroupMemberEntity member);

}
