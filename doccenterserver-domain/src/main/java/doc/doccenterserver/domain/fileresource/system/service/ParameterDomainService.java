package doc.doccenterserver.domain.fileresource.system.service;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.system.model.ParameterEntry;
import doc.doccenterserver.domain.fileresource.system.repository.ParameterRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 查询系统配置
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Component("SYSTEMParameterDS")
@DomainService(name = "查询系统配置")
public class ParameterDomainService {
    @Resource
    private ParameterRepository parameterRepository;
    @ReturnValue
    @Method(name = "查询系统配置")
    public List<ParameterEntry> selectList() {
    	return parameterRepository.selectList();
	}

    @ReturnValue
    @Method(name = "根据key查询系统配置")
    public ParameterEntry getByKey(String key) {
        return parameterRepository.getByKey(key);
    }

    @ReturnValue
    @Method(name = "根据key查询系统配置")
    public String getByKeyValue(String key) {
        return parameterRepository.getByKey(key).getPValue();
    }


}
