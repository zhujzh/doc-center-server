package doc.doccenterserver.domain.fileresource.document.service;


import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;
import doc.doccenterserver.domain.fileresource.document.model.RoleEntity;
import doc.doccenterserver.domain.fileresource.document.repository.RoleRepository;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("RoleDS")
@DomainService(name = "RoleDomainService")
public class RoleDomainService {

    @Resource
    private RoleRepository roleRepository;

    @ReturnValue
    @Method(name = "编辑初始化")
    public RoleEntity editInit(String roleId) {
        return roleRepository.editInit(roleId);
    }

    @ReturnValue
    @Method(name = "保存操作")
    public RoleEntity save(RoleEntity roleEntity) {
        return roleRepository.save(roleEntity);
    }

    @ReturnValue
    @Method(name = "分页查询")
    public PageResult<List<RoleEntity>> getList(RoleEntity roleEntity) {
        return roleRepository.getList(roleEntity);
    }

    @ReturnValue
    @Method(name = "根据id获取角色")
    public RoleEntity getById(String roleId) {
        return roleRepository.getById(roleId);
    }

    @ReturnValue
    @Method(name = "删除角色")
    public RoleEntity delete(String roleIds) {
        return roleRepository.delete(roleIds);
    }

    @ReturnValue
    @Method(name = "获取资源树状数据")
    public List<ResEntity> getInitTree(String resId) {
        return roleRepository.getInitTree(resId);
    }

    @ReturnValue
    @Method(name = "获取角色绑定的资源")
    public RoleEntity getRes(String roleId) {
        return roleRepository.getRes(roleId);
    }

    @ReturnValue
    @Method(name = "更新角色绑定的资源")
    public RoleEntity updateRes(RoleEntity roleDto) {
        return roleRepository.updateRes(roleDto);
    }

    @ReturnValue
    @Method(name = "获取角色绑定的用户")
    public RoleEntity getUser(String roleId) {
        return roleRepository.getUser(roleId);
    }

    @ReturnValue
    @Method(name = "更新角色绑定用户")
    public RoleEntity updateUser(RoleEntity roleEntity) {
        return roleRepository.updateUser(roleEntity);
    }

    @ReturnValue
    @Method(name = "组织树状数据")
    public List<OrgEntity> getInitOrgTree(String orgId) {
        return roleRepository.getInitOrgTree(orgId);
    }
    @ReturnValue
    @Method(name = "组织树状数据")
    public List<OrgEntity> getInitOrg(String parentId,List<String> queryType) {
        return roleRepository.getInitOrg(parentId,queryType);
    }
    @ReturnValue
    @Method(name = "根据组织id获取用户")
    public List<UserEntity> getUserByOrgIds(List<String> orgIds) {
        return roleRepository.getUserByOrgIds(orgIds);
    }
    @ReturnValue
    @Method(name = "根据组织id获取用户")
    public List<UserEntity> getUserByUserName(String userName,List<String> orgIds) {
        return roleRepository.getUserByUserName(userName,orgIds);
    }

    @ReturnValue
    @Method(name = "根据基线版UnitIds查询所有上级组织")
    public List<OrgEntity> getOrgListTreeByUnitIds(List<String> orgUnitIds) {
        return roleRepository.getOrgListTreeByUnitIds(orgUnitIds);
    }

    @ReturnValue
    @Method(name = "根据名称与根组织节点bizCode查询组织")
    public List<OrgEntity> getOrgByUserName(String name, List<String> bizIds) {
        return roleRepository.getOrgByUserName(name,bizIds);
    }
}
