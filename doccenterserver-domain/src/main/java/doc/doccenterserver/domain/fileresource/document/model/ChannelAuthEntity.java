package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "频道验证实体")
public class ChannelAuthEntity {

    @Field(name = "频道编号")
    private String channelId;

    @Field(name = "授权对象")
    private String authObject;

    @Field(name = "对象类型")
    private String objectType;

    @Field(name = "相应的权限值")
    private Integer authValue;

}
