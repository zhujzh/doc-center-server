package doc.doccenterserver.domain.fileresource.message.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.system.model.JobLogEntry;
import org.jdom2.Document;
import org.jdom2.JDOMException;

import java.rmi.ServerException;
import java.util.List;
import java.util.Map;

/**
 * 主任务
 * repository
 */
public interface JobLogRepository {

    /**
     * 消息入库
     */
    boolean msgIn(String mqmsg)throws Exception;
    Boolean checkMqSaveExist(String sourceApp,String sourcePaperId,String archiveOrgCode);
    Boolean checkMqSaveExist(String paperId);
    /**
     * 消息重复异常处理
     */
    void clearDate4Repeated(String logId, Integer stepId) throws ServerException;
    /**
     * CMS频道处理
     */
    ChannelEntity cmsCl(Document doc) throws ServerException, JDOMException;
    void clearChangeLog(String logId);

    Boolean updateStatus(String logId,String status);

    void updateError(String logId, String msg, Integer stepId, Integer stepStatus, Integer retryTimes);

}
