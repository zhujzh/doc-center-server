package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "GroupMember实体")
public class GroupMemberEntity {

    @Field(name = "主键")
    private String groupMemberid;

    @Field(name = "排序号")
    private Integer memberSort;

    @Field(name = "组id")
    private String groupId;

    @Field(name = "成员ID")
    private String uniquemember;

    @Field(name = "成员类型")
    private String uniquememberType;

}
