package doc.doccenterserver.domain.fileresource.file.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "TPanCollectFolderEntity")
public class TPanCollectFolderEntity {

    @Field(name = "collect_id")
    private String collectId;

    @Field(name = "folder_name")
    private String folderName;

    @Field(name = "user_id")
    @TableField("user_id")
    private String userId;

    @Field(name = "parent_collect_id")
    private String parentCollectId;

    @Field(name = "create_time")
    private Date createTime;

    @Field(name = "obj_id")
    private String objId;

    @Field(name = "doc_type")
    private String docType;

    @Field(name = "obj_name")
    private String objName;

}
