package doc.doccenterserver.domain.fileresource.document.service;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.Parameter;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.TemplateEntity;
import doc.doccenterserver.domain.fileresource.document.repository.TemplateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("TemplateDS")
@DomainService(name = "模板服务")
public class TemplateDomainService {

    @Resource
    private TemplateRepository templateRepository;

    @ReturnValue
    @Method(name = "根据模板名称查询模板")
    public PageResult<List<TemplateEntity>> getListTemplates(TemplateEntity templateEntity) {
        return templateRepository.getListTemplates(templateEntity);
    }

    @ReturnValue
    @Method(name = "根据模板id查询模板")
    public TemplateEntity getTemplate(Integer id) {
        TemplateEntity template = templateRepository.getTemplate(id);
        if (null != template) {
            if (template.getPathname() != null && !"".equals(template.getPathname())) {
                template.setMode(ConstantSys.NUMBER_ONE);
            } else {
                template.setMode(ConstantSys.NUMBER_TWO);
            }
        }
        return template;
    }

    @ReturnValue
    @Method(name = "新增模板")
    public TemplateEntity addTemplate(TemplateEntity templateEntity) {
        return templateRepository.addTemplate(templateEntity);
    }

    @ReturnValue
    @Method(name = "编辑模板")
    public TemplateEntity editTemplate(TemplateEntity templateEntity) {
        return templateRepository.editTemplate(templateEntity);
    }

    @ReturnValue
    @Method(name = "根据ids删除模板")
    public TemplateEntity deleteTemplate(String ids) {
        return templateRepository.deleteTemplate(ids);
    }
}
