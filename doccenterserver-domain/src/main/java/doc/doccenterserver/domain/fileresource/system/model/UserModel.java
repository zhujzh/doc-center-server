package doc.doccenterserver.domain.fileresource.system.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.ValueObject;
import com.baomidou.mybatisplus.annotation.TableField;
import doc.doccenterserver.domain.fileresource.system.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangxp
 * @description 用户域
 * @date 2023/3/29 20:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ValueObject(name = "用户模型")
public class UserModel {

    @Field(name = "用户名称")
    private String name;

  /*  @Field(name = "手机号码")
    private String mobile;
    @Field(name = "邮箱账号")
    private String email;*/
    @Field(name = "用户账号")
    private String userid;
    @Field("dept_id")
    private String deptId;
    @Field(name = "ORG_ID")
    private String orgId;
    @Field(name = "COMPANY_ID")
    private String companyId;

    private UserRepository userRepository;

    // 构造方法
    public UserModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Method(name = "根据token获取用户信息")
    @ReturnValue
    public UserModel getUserInfoByToken(String accessToken) {
       return userRepository.getUserInfoByToken(accessToken);
    }
}
