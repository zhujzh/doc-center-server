package doc.doccenterserver.domain.fileresource.system.service;

import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.domain.fileresource.system.repository.ParamRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 22:03
 */
@Slf4j
@Component("ParamDS")
@DomainService(name = "参数配置管理")
public class ParamDomainService {

    @Resource
    private ParamRepository paramRepository;


    public PageResult<List<ParamEntity>> getListParam(ParamEntity paramEntity) {
        return paramRepository.getListParam(paramEntity);
    }

    public ParamEntity addParam(ParamEntity paramEntity) {
        return paramRepository.addParam(paramEntity);
    }

    public ParamEntity editParam(ParamEntity paramEntity) {
        return paramRepository.editParam(paramEntity);
    }

    public ParamEntity deleteParam(String id) {
        return paramRepository.deleteParam(id);
    }

    public ParamEntity getById(String id) {
        return paramRepository.getById(id);
    }
}
