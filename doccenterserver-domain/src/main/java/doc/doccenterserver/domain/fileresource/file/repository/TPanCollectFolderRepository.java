package doc.doccenterserver.domain.fileresource.file.repository;

import doc.doccenterserver.domain.fileresource.file.model.TPanCollectFolderEntity;

import java.util.List;

/**
 * @author Tangcancan
 */
public interface TPanCollectFolderRepository {
    Integer getCountByObjId(TPanCollectFolderEntity tpanCol);

}
