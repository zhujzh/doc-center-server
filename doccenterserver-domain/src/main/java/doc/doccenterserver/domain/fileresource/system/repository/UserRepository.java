package doc.doccenterserver.domain.fileresource.system.repository;


import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;

/**
 * @author wangxp
 * @description 用户接口
 * @date 2023/3/29 20:44
 */
public interface UserRepository {

    UserModel getUserInfoByToken(String accessToken);

    UserEntity getUserInfo(String userId);
}
