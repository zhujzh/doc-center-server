package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "用户配置实体")
public class UserConfigEntity {

    @Field(name = "用户ID")
    private String userId;

    @Field(name = "页面缩放尺寸")
    private String paperScale;

    @Field(name = "主题皮肤ID")
    private String skinId;

}
