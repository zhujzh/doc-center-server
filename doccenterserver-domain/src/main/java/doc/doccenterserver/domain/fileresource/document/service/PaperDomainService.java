package doc.doccenterserver.domain.fileresource.document.service;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.document.model.ImageEntity;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.document.repository.AttachmentRepository;
import doc.doccenterserver.domain.fileresource.document.repository.ChannelRepository;
import doc.doccenterserver.domain.fileresource.document.repository.ImageRepository;
import doc.doccenterserver.domain.fileresource.document.repository.PaperRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("PaperDS")
@DomainService(name = "文档服务")
public class PaperDomainService {

    @Resource
    private PaperRepository paperRepository;
    @Resource
    private ImageRepository imageRepository;
    @Resource
    private AttachmentRepository attachmentRepository;

    @Resource
    private ChannelRepository channelRepository;

    @ReturnValue
    @Method(name = "文档管理，根据id，分页查询paper数据-列表查询")
    public PageResult<List<PaperEntity>> listPapers(HttpServletRequest request, PaperEntity paperEntity) {
        return paperRepository.listPapers(request, paperEntity);
    }

    @ReturnValue
    @Method(name = "主页-查询初始化")
    public PaperEntity getListPaper(HttpServletRequest request, String classId) {
        return paperRepository.getListPaper(request, classId);
    }

    @ReturnValue
    @Method(name = "查询文档内容 - 详细")
    public PaperEntity getPaperDetail(String paperId, HttpServletRequest request) {
        return paperRepository.getPaperDetail(paperId, request);
    }

    @ReturnValue
    @Method(name = "新增初始化")
    public PaperEntity getAddPaper(String classId, HttpServletRequest request) {
        return paperRepository.getAddPaper(classId, request);
    }

    @ReturnValue
    @Method(name = "新增Paper对象")
    public PaperEntity addPaper(HttpServletRequest request, PaperEntity paperEntity) {
        return paperRepository.addPaper(request, paperEntity);
    }

    @ReturnValue
    @Method(name = "编辑初始化")
    public PaperEntity getEditPaper(String paperId, HttpServletRequest request) {
        return paperRepository.getEditPaper(paperId, request);
    }

    @ReturnValue
    @Method(name = "编辑Paper对象")
    public PaperEntity editPaper(HttpServletRequest request, PaperEntity paperEntity) {
        return paperRepository.addPaper(request, paperEntity);
    }

    @ReturnValue
    @Method(name = "置顶或取消置顶文档")
    public PaperEntity topDoc(PaperEntity paperEntity) {
        return paperRepository.topDoc(paperEntity);
    }

    @ReturnValue
    @Method(name = "置顶排序")
    public PaperEntity topDocSort(PaperEntity paperEntity) {
        return paperRepository.topDocSort(paperEntity);
    }

    @ReturnValue
    @Method(name = "预览文档")
    public PaperEntity previewPaper(PaperEntity paperEntity, HttpServletRequest request) {
        return paperRepository.previewPaper(paperEntity, request);
    }

    @ReturnValue
    @Method(name = "批量删除Paper对象")
    public PaperEntity deletePaper(String ids) {
        return paperRepository.deletePaper(ids);
    }

    @ReturnValue
    @Method(name = "门户页面统一查询入口")
    public PageResult<List<PaperEntity>> listfrontpapers(PaperEntity paperEntity, String classid, String ccid, HttpServletRequest request) {
        return paperRepository.listfrontpapers(paperEntity,classid,ccid,request);
    }


    @ReturnValue
    @Method(name = "门户页面统一查询返回")
    public PaperEntity papersQueryRet(PaperEntity paperEntity, String classid, String ccid, HttpServletRequest request) {
        return paperRepository.papersQueryRet(paperEntity,classid,ccid,request);
    }

    @ReturnValue
    @Method(name = "初始化信息")
    public PaperEntity getInit(String classId) {
        return paperRepository.getInit(classId);
    }

    @ReturnValue
    @Method(name = "获取预览文件时的文档信息展示")
    public PaperEntity getPreviewInfo(String paperId, HttpServletRequest request) {
        // 获取文档信息
        PaperEntity paperEntity = paperRepository.getById(paperId);
        if(paperEntity==null){
            return null;
        }
        // 文件大小
        AttachmentEntity attachment = attachmentRepository.getAttachmentByPaperId(paperId);
        // 获取文件所在的频道路径
        List<ChannelEntity> channel = channelRepository.initChannelPath(paperEntity.getClassId());
        paperEntity.setChannelList(channel);
        List<AttachmentEntity> list = new ArrayList<>();
        list.add(attachment);
        paperEntity.setAttachList(list);
        if (0 == paperEntity.getPaperType()) {
            ImageEntity imageEntity = new ImageEntity();
            imageEntity.setPaperId(paperId);
            List<ImageEntity> imgList = imageRepository.getImageList(imageEntity);
            paperEntity.setImgList(imgList);
        }
        return paperEntity;
    }
}
