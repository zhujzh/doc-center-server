package doc.doccenterserver.domain.fileresource.system.service;


import cn.hutool.core.util.ObjectUtil;
import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.exception.BusinessException;
import doc.doccenterserver.domain.fileresource.exception.ExceptionEnum;
import doc.doccenterserver.domain.fileresource.sso.vo.SsoResVo;
import doc.doccenterserver.domain.fileresource.sso.vo.SsoTokenVo;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.domain.fileresource.system.repository.SsoRepository;
import doc.doccenterserver.domain.fileresource.system.repository.UserRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Tangcancan
 */
@Component("SYSTEMSsoDS")
@DomainService(name = "用户")
public class SsoDomainService {

    @Resource
    private UserRepository userRepository;
    @Resource
    private SsoRepository ssoRepository;

    @ReturnValue
    @Method(name = "用户登录认证")
    public SsoResVo ssoCallback(String code, String requestUrl) {
        // 获取token
        SsoTokenVo ssoVo = ssoRepository.getTokenByCode(code);
        if (ObjectUtil.isEmpty(ssoVo)) {
            throw new BusinessException(ExceptionEnum.UNAUTHORIZED.getCode(), ExceptionEnum.UNAUTHORIZED.getMsg());
        }
        // 获取token
        UserModel userModel = new UserModel(userRepository);
        UserModel user = userModel.getUserInfoByToken(ssoVo.getAccess_token());
        if (ObjectUtil.isEmpty(user)) {
            throw new BusinessException(ExceptionEnum.UNAUTHORIZED.getCode(), "未获取到用户信息，认证失败!");
        }
        UserEntity userEntity = userRepository.getUserInfo(ssoVo.getOpenid());
        if (userEntity == null) {
            throw new BusinessException(ExceptionEnum.UNAUTHORIZED.getCode(), "未获取到文档中心用户信息，认证失败!");
        }
        user.setDeptId(userEntity.getDeptId());
        user.setCompanyId(userEntity.getCompanyId());
        user.setOrgId(userEntity.getOrgId());
        //存储信
        ssoRepository.saveRedisInfo(ssoVo, user);
        return SsoResVo.builder()
                .token(ssoVo.getAccess_token())
                .user(userEntity)
                .requestUrl(requestUrl)
                .build();
    }
    @ReturnValue
    @Method(name = "退出登录")
    public void loginOut(HttpServletRequest request) {
        ssoRepository.loginOut(request);
    }

}
