package doc.doccenterserver.domain.fileresource.pan.repository;

import doc.doccenterserver.domain.fileresource.file.model.TPanObjEntity;

import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
public interface TPanObjRepository {
    Double getTpanObjSize();

    TPanObjEntity findTPanObjById(String objId);

    List<TPanObjEntity> findChildObjsByParentId(TPanObjEntity temp);

}
