package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DomainObject(isAggregateRoot = true, name = "组织机构实体")
public class OrgEntity {

    @Field(name = "ORG_ID")
    private String orgId;

    @Field(name = "ORG_NAME")
    private String orgName;

    @Field(name = "ORG_FULL_NAME")
    private String orgFullName;

    @Field(name = "ORG_FULL_PATH_NAME")
    private String orgFullPathName;

    @Field(name = "ORG_FULL_PATH_ID")
    private String orgFullPathId;

    @Field(name = "ORG_PARENT_ID")
    private String orgParentId;

    @Field(name = "ORG_TYPE")
    private String orgType;

    @Field(name = "ORG_LEVEL")
    private Integer orgLevel;

    @Field(name = "ORG_AREA_TYPE")
    private String orgAreaType;

    @Field(name = "ORG_SORT")
    private Integer orgSort;

    @Field(name = "ORG_WORK_PHONE")
    private String orgWorkPhone;

    @Field(name = "ORG_WORK_ADDRESS")
    private String orgWorkAddress;

    @Field(name = "ORG_PRINCIPAL")
    private String orgPrincipal;

    @Field(name = "ORG_STATUS")
    private String orgStatus;

    @Field(name = "ORG_CREATE_TIME")
    private Date orgCreateTime;

    @Field(name = "REMARK")
    private String remark;

    @Field(name = "FUND_CODE")
    private String fundCode;

    @Field(name = "FUND_NAME")
    private String fundName;

    @Field(name = "COMPANY_ID")
    private String companyId;

    @Field(name = "DEPT_ID")
    private String deptId;

    @Field(name = "DEPT_NAME")
    private String deptName;

    @Field(name = "COMPANY_NAME")
    private String companyName;

    @Field(name = "ORG_BRANCH_LEADER")
    private String orgBranchLeader;


    // 上级 标识
    @Field(name = "ORG_LEAF")
    private Boolean orgLeaf;

    // 组织节点id
    @Field(name = "ORG_UNIT_ID")
    private String orgUnitId;



    // 父级组织单元id
    private String parentOrgUnitId;


    // org 组织 user 用户
    // 树形结构 数据类型
    private String dataType;

}
