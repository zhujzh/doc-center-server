package doc.doccenterserver.domain.fileresource.file.repository;

import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.rmi.ServerException;
import java.util.List;

/**
 * 文件
 * repository
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
public interface FileRepository {
    FileEntity getFileById(String fileId);

    List<FileEntity> md5Check(String md5);

    boolean insertFile(FileEntity fileEntity);

    List<FileEntity> downloadFileList(String fileId);

    /**
     * 基线版文档中心上传文件
     *
     * @param file
     * @return
     * @throws ServerException
     */
    String uploadFile(MultipartFile file) throws ServerException;

    /**
     * 基线版文档中心下载文件
     *
     * @param response
     * @param fileId
     * @throws IOException
     */
    void downloadFile(HttpServletResponse response, String fileId) throws IOException;

    void downloadFilePath(String filePath, String fileName, String fileId) throws IOException;

    /**
     * 从基线版下载文件，并将文件转换为byte[]
     *
     * @param fileId 基线版文件ID
     * @return
     * @throws IOException
     */
    byte[] downloadFile(String fileId) throws IOException;

    Boolean deleteFile(String fileId) throws IOException;

    boolean updateFile(FileEntity fileEntity);
}
