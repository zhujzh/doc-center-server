package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "AppEntity")
public class AppEntity extends BaseDto {

    @Field(name = "应用ID")
    private String appId;

    @Field(name = "应用名称")
    private String appName;

    @Field(name = "应用承建厂商")
    private String appBelong;

    @Field(name = "应用运维接口人")
    private String userName;

    @Field(name = "应用运维接口人联系方式")
    private String userMobtel;

    @Field(name = "应用系统磁盘配额")
    private Double quota;

    @Field(name = "应用系统权限")
    private String authValue;

    @Field(name = "应用系统状态")
    private String status;

    @Field(name = "应用密码")
    private String appPassword;

    @Field(name = "USER_ID")
    private String userId;

    @Field(name = "是否启用文件服务")
    private String fileserviceStatus;

    private Double usedQuota;

    private Double surplusQuota;

    private Double sumquota;

    private String operate;

}
