package doc.doccenterserver.domain.fileresource.system.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author Tangcancan
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "用户实体")
public class UserEntity {

    @Field(name = "USER_ID")
    private String userId; //有

    @Field(name = "USER_CODE")
    private String userCode;

    @Field(name = "USER_NAME")
    private String userName; // 有

    @Field(name = "USER_SEX")
    private String userSex;

    @Field(name = "USER_AGE")
    private Integer userAge;

    @Field(name = "COMPANY_ID")
    private String companyId; // 有

    @Field(name = "ORG_ID")
    private String orgId; // 有

    @Field(name = "USER_MOBILE")
    private String userMobile; //有

    @Field(name = "USER_MAIL")
    private String userMail; // 有

    @Field(name = "USER_WORK_ADDRESS")
    private String userWorkAddress;

    @Field(name = "USER_WORK_PHONE")
    private String userWorkPhone;

    @Field(name = "USER_HOME_ADDREE")
    private String userHomeAddree;

    @Field(name = "USER_HOME_PHONE")
    private String userHomePhone;

    @Field(name = "POSITION_ID")
    private String positionId; // 有

    @Field(name = "PLURALITY_POSITION_ID")
    private String pluralityPositionId;

    @Field(name = "TITLE_ID")
    private String titleId;

    @Field(name = "PLURALITY_TITLE_ID")
    private String pluralityTitleId;

    @Field(name = "USER_TYPE")
    private String userType;

    @Field(name = "USER_STATUS")
    private String userStatus;

    @Field(name = "USER_SORT")
    private Integer userSort;

    @Field(name = "USER_PWD")
    private String userPwd;

    @Field(name = "USER_CREATE_TIME")
    private Date userCreateTime;

    @Field(name = "USER_UPDATE_TIME")
    private Date userUpdateTime;

    @Field(name = "USER_CREATOR")
    private String userCreator;

    @Field(name = "REMARK")
    private String remark;
    @TableField("dept_id")
    private String deptId;
    @Field(name = "COMPANY_NAME")
    private String companyName;
    @Field(name = "DEPT_NAME")
    private String deptName;
    @Field(name = "ORG_NAME")
    private String orgName;

    private String channelId;

    private String skinId;

    private List<String> authList;

    @Field(
            value = "基线版员工id",
            name = "employeeId"
    )
    private String employeeId;


    @Field(
            value = "基线版所属组织id",
            name = "orgUnitId"
    )
    private String orgUnitId;



}
