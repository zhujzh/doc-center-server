//package doc.doccenterserver.domain.fileresource.security;
//
//import com.blueland.framework.model.RoleEntry;
//import com.blueland.framework.model.UserEntry;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//
//public class SecurityUserHolder {
//	public static UserSession getCurrentUser() {
//		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//		if (authentication == null) {
//			return null;
//		}
//		Object principal = authentication.getPrincipal();
//		if (principal == null || !(principal instanceof User) ) {
//			return null;
//		}
//		return  (UserSession)principal;
//	}
//
//	public static void setCurrentUser(UserDetails loadedUser){
//		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(
//				loadedUser, loadedUser.getPassword(),
//				loadedUser.getAuthorities());
//		result.setDetails(loadedUser);
//		SecurityContextHolder.getContext().setAuthentication(result);
//	}
//
//	public static void ChangeCurrentUser(UserEntry user,List<RoleEntry> rolelist){
//
//		Collection<RoleGrantedAuthority> authorities = new ArrayList<RoleGrantedAuthority>();
//		for(RoleEntry role:rolelist){
//			RoleGrantedAuthority roleGrantedAuthority = new RoleGrantedAuthority(role.getRoleId(), role.getRoleName());
//		    authorities.add(roleGrantedAuthority);
//		}
//		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//		String password = user.getUserPwd()==null?"123456":user.getUserPwd();
//		UserSession userSession = new UserSession(user.getUserId(), password, true, true,
//				true, true, authorities);
//		userSession.setUser(user);
//		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(
//				userSession, user.getUserPwd(),
//				authorities);
//		result.setDetails(authentication.getDetails());
//		SecurityContextHolder.getContext().setAuthentication(result);
//	}
//
//	public static Collection<GrantedAuthority> getAuthorityList() {
//		return (Collection<GrantedAuthority>)SecurityContextHolder.getContext().getAuthentication().getAuthorities();
//	}
//
//	/**
//	* @方法名: hasRole
//	* @描述:判断用户是否有某个角色的权限
//	* @作者:高星
//	* @时间:2016年7月8日 下午4:07:23
//	* @参数:@param roleId
//	* @参数:@return
//	* @返回值：boolean
//	*/
//	public static boolean hasRole(String roleId){
//		Collection<GrantedAuthority> rolelist = getAuthorityList();
//		for(GrantedAuthority role:rolelist){
//			if(roleId.equals(role.getAuthority())){
//				return true;
//			}else{
//				continue;
//			}
//		}
//		return false;
//	}
//}
