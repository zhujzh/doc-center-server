package doc.doccenterserver.domain.fileresource.base;

import lombok.*;

import java.io.Serializable;

/**
 * 基础实体类
 *
 * @author tangrongcan
 */
@Data
public class BaseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    public BaseDto() {
        this.pageNum = 1L;
        this.pageSize = 10L;
        this.total = 0L;
        this.totalPage = 1L;
    }

    /**
     * 当前页
     */
    private Long pageNum;

    /**
     * 分页大小
     */
    private Long pageSize;

    /**
     * 总记录数
     */
    private Long total;

    /**
     * 总页数
     */
    private Long totalPage;


}
