package doc.doccenterserver.domain.fileresource.file.model;

import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Tangcancan
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "首页全文检索实体")
public class HomeEntity {

    // 搜索类型 - 0：按标题搜索  1：按内容搜索  2：按标题、内容搜索
    private String type;
    // 搜索内容
    private String content;
    private String fileName;
    private String titleName;
    private String issueUserName;
    // 当前页数
    private String page;
    // 搜索频道id
    private String classId;
    // 时间
    private String issueStartDate;
    private String issueEndDate;
    // 文件类型
    private String objsuffix;
    // 搜索大类型
    private String personType;
    private String channelId;


    private String appId;//索引所属应用id

    private List<SearchResultPack> docList;

    private Integer totalNum;
    private Integer loadedDataNum;
    private Double timeConsuming;
    private Integer pageNum;


    public static final String TYPE_ZERO = "0";
    public static final String TYPE_ONE = "1";
}
