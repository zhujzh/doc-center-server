package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;

/**
 * @author Tangcancan
 */
public interface JobDetailLogRepository {
    Integer insertJobDetailLog(JobDetailLogEntity jobDetailLogEntity);

}
