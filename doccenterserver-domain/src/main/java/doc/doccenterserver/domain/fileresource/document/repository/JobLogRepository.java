package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.JobLogEntity;

import java.util.List;

/**
 * @author Tangcancan
 */
public interface JobLogRepository {
    Integer findJobLogCountByPaperId(String paperId);

    Integer insertJobLog(JobLogEntity jobLog);

    List<JobLogEntity> getJobLogList(JobLogEntity jobLogEntity);

}
