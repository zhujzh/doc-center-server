package doc.doccenterserver.domain.fileresource.document.model;

import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

/**
 * @author Tangcancan
 */
@Data
public class TreeNodeEntity {

    private String id;
    private String pId;
    private String name;
    private Boolean checked;
    private Boolean open;
    private Boolean isParent;
    private Boolean isHytz;
    private Boolean isTzgg;
    private Boolean isTpxw;
    private String type;
    private String isAccessTopMenu;
    private String iconSkin = "orgIcon";
    public static final String ORG_ICON = "orgIcon";
    public static final String USER_ICON = "userIcon";
    public static final String GROUP_ICON = "groupIcon";
    private String mobiles;
    private String mobileCount;
    private String orgLevel;
    private UserEntity user;
    private OrgEntity org;
    private String companyId;//所属公司id
    private String companyName;//所属公司名称
    private String deptId;//所属一级部门id
    private String deptName;//所属一级部门名称
    private String orgId;//所属部门id
    private String orgName;//所属部门名称

    //菜单
    private String title;
    private String index;
    private String icon;


    public TreeNodeEntity(String id, String pId, String name, boolean checked, boolean open, boolean isParent) {
        super();
        this.id = id;
        //this.tId=id;
        this.pId = pId;
        this.name = name;
        this.checked = checked;
        this.open = open;
        this.isParent = isParent;
    }

    public TreeNodeEntity(String id, String pId, String title, String index,String icon, boolean checked, boolean open, boolean isParent) {
        super();
        this.id = id;
        this.pId = pId;
        this.title = title;
        this.index = index;
        this.icon = icon;
        this.checked = checked;
        this.open = open;
        this.isParent = isParent;
    }

    public TreeNodeEntity(String id, String pId, String name) {
        super();
        this.id = id;
        this.pId = pId;
        this.name = name;
    }

}
