package doc.doccenterserver.domain.fileresource.exception;

/**
 * 异常枚举类
 *
 * @author Tangcancan
 */

public enum ExceptionEnum {

    // 400
    BAD_REQUEST("400", "请求数据格式不正确!"),
    UNAUTHORIZED("401", "未获取到登录令牌，认证失败!"),
    FORBIDDEN("403", "没有访问权限!"),
    REFRESH_TOKEN("10002", "刷新登录令牌失败，请重新登录！"),
    NOT_FOUND("404", "请求的资源找不到!"),
    // 500
    INTERNAL_SERVER_ERROR("500", "服务器内部错误!"),
    SERVICE_UNAVAILABLE("503", "服务器正忙，请稍后再试!"),
    // 未知异常
    UNKNOWN("10000", "未知异常!"),
    // 自定义
    IS_NOT_NULL("10001", "%s不能为空");

    /**
     * 错误码
     */
    private final String code;

    /**
     * 错误描述
     */
    private final String msg;

    ExceptionEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
