package doc.doccenterserver.domain.fileresource.system.model;

import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import com.baomidou.mybatisplus.annotation.TableId;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ch
 * @date 2023/4/11 22:05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "参数配置管理")
public class ParamEntity  extends BaseDto {
    @TableId
    private String pId;

    private String pKey;

    private String pValue;

    private String description;
}
