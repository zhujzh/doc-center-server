package doc.doccenterserver.domain.fileresource.system.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;

import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 19:23
 */
public interface LocalFileRepository {
    PageResult<List<FileEntity>> listfiles(FileEntity fileEntity);

    int reUpload(String fileId);

    Map<String, Object> showfiledetail(String fileId, String appName);

    String deletefiles(String fileIds);
}
