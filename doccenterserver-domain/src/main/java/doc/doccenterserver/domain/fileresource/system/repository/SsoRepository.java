package doc.doccenterserver.domain.fileresource.system.repository;


import doc.doccenterserver.domain.fileresource.sso.vo.SsoTokenVo;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wangxp
 * @description 认证 Repository
 * @date 2023/3/30 9:14
 */
public interface SsoRepository {


      SsoTokenVo getTokenByCode(String code);


      void saveRedisInfo(SsoTokenVo ssoVo, UserModel userModel);

      void loginOut(HttpServletRequest request);


}
