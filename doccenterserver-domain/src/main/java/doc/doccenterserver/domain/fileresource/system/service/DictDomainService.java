package doc.doccenterserver.domain.fileresource.system.service;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.Parameter;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.TreeNodeEntity;
import doc.doccenterserver.domain.fileresource.system.model.DictEntry;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import doc.doccenterserver.domain.fileresource.system.repository.DictRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;

/**
 * 查询字典
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Component("SYSTEMDictDS")
@DomainService(name = "查询字典")
public class DictDomainService {
    private static final String ICON = "pIcon01";

    @Resource
    private DictRepository dictDtoRepository;

    @ReturnValue
    @Method(name = "查询字典")
    public DictEntry selectDict(@Parameter(name = "pid", required = false) String pid) {
        return dictDtoRepository.selectDict(pid);
    }

    public List<TreeNodeEntity> findInitTree(String pid) {
        List<TreeNodeEntity> treeNodeList = new ArrayList<TreeNodeEntity>();
        //顶级节点
        DictEntry topMenu = dictDtoRepository.getByParentId(pid);
        if (topMenu != null) {
            if ("1".equals(topMenu.getId())) {
                addTreeNode(treeNodeList, topMenu, true);
            }
            List<DictEntry> dictList = dictDtoRepository.getInItTree(pid);
            if (dictList.size() > 0) {
                for (DictEntry res : dictList) {
                    addTreeNode(treeNodeList, res, true);
                }
            }
        }
        return treeNodeList;
    }

    private void addTreeNode(List<TreeNodeEntity> list, DictEntry dict, boolean isOpen) {
        Integer childrenNum = dictDtoRepository.findChildrenNumById(dict.getId());
        boolean hasChildren = (childrenNum > 0);
        TreeNodeEntity node = null;
        if (hasChildren) {
            node = new TreeNodeEntity(dict.getId(), dict.getPid(), dict.getDictName(), true, isOpen, hasChildren);
        } else {
            node = new TreeNodeEntity(dict.getId(), dict.getPid(), dict.getDictName(), true, isOpen, hasChildren);
        }
        node.setIconSkin(ICON);
        list.add(node);
    }

    public PageResult<List<DictEntry>> getListDict(DictEntry dictEntry) {
        return dictDtoRepository.getListDict(dictEntry);
    }

    public DictEntry getById(String id) {
        return dictDtoRepository.getById(id);
    }

    public DictEntry editDict(DictEntry dictEntry) {
        return dictDtoRepository.editDict(dictEntry);
    }

    public DictEntry deleteDict(String id) {
        return dictDtoRepository.deleteDict(id);
    }

    public DictEntry addDict(DictEntry dictEntry) {
        return dictDtoRepository.addDict(dictEntry);
    }

    public List<DictEntry> getByPId(String pid) {
        return dictDtoRepository.getByPId(pid);
    }

    public DictEntry getByPIdAndVal(String pid, String val) {
        return dictDtoRepository.getByPIdAndVal(pid,val);
    }
}
