package doc.doccenterserver.domain.fileresource.document.service;

import com.alibaba.bizworks.core.specification.Method;
import com.alibaba.bizworks.core.specification.ReturnValue;
import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.GroupEntity;
import doc.doccenterserver.domain.fileresource.document.repository.GroupRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("GroupDS")
@DomainService(name = "GroupDomainService")
public class GroupDomainService {

    @Resource
    private GroupRepository groupRepository;

    @ReturnValue
    @Method(name = "编辑初始化")
    public GroupEntity editInit(GroupEntity groupEntity) {
        return groupRepository.editInit(groupEntity);
    }

    @ReturnValue
    @Method(name = "保存操作")
    public GroupEntity save(GroupEntity groupEntity) {
        return groupRepository.save(groupEntity);
    }

    @ReturnValue
    @Method(name = "分页查询")
    public PageResult<List<GroupEntity>> getList(GroupEntity groupEntity) {
        return groupRepository.getList(groupEntity);
    }

    @ReturnValue
    @Method(name = "保存群组")
    public GroupEntity saveGroupMembers(GroupEntity groupEntity) {
        return groupRepository.saveGroupMembers(groupEntity);
    }

    @ReturnValue
    @Method(name = "批量删除")
    public GroupEntity delete(String groupIds) {
        return groupRepository.delete(groupIds);
    }

    @ReturnValue
    @Method(name = "根据id删除")
    public GroupEntity deleteGroupById(String groupId) {
        return groupRepository.deleteGroupById(groupId);
    }

    @ReturnValue
    @Method(name = "新增用户群组")
    public GroupEntity editUserGroup(HttpServletRequest request, GroupEntity groupEntity) {
        return groupRepository.editUserGroup(request, groupEntity);
    }

    @ReturnValue
    @Method(name = "新增角色群组")
    public GroupEntity insertRoleGroup(GroupEntity groupEntity) {
        return groupRepository.insertRoleGroup(groupEntity);
    }

    @ReturnValue
    @Method(name = "根据角色id获取")
    public GroupEntity selectRgByRoleId(GroupEntity groupEntity) {
        return groupRepository.selectRgByRoleId(groupEntity);
    }
}
