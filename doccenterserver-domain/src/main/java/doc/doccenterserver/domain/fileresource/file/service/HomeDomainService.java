package doc.doccenterserver.domain.fileresource.file.service;

import com.alibaba.bizworks.core.specification.ddd.DomainService;
import doc.doccenterserver.domain.fileresource.file.model.HomeEntity;
import doc.doccenterserver.domain.fileresource.file.repository.HomeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("HomeDS")
@DomainService(name = "首页全文检索服务")
public class HomeDomainService {

    @Resource
    private HomeRepository homeRepository;

    public HomeEntity getDocSearch(HttpServletRequest request, HomeEntity homeEntity) {
        return homeRepository.getDocSearch(request, homeEntity);
    }
}
