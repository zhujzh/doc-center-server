package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.InterfaceAccessLogEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
public interface InterfaceAccessLogRepository {
    PageResult<List<InterfaceAccessLogEntity>> getInterfaceAccessLogList(
            HttpServletRequest request, InterfaceAccessLogEntity interfaceAccessLogEntity);

    InterfaceAccessLogEntity getInterfaceAccessLog(String logId);

    InterfaceAccessLogEntity getInit(HttpServletRequest request);

    InterfaceAccessLogEntity insert(InterfaceAccessLogEntity interfaceAccessLogEntity);

}
