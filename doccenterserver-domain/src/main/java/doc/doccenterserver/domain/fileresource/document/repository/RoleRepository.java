package doc.doccenterserver.domain.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;
import doc.doccenterserver.domain.fileresource.document.model.RoleEntity;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;

import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
public interface RoleRepository {
    List<Map<String, Object>> querySelectedRoles(List<List<String>> roleMutilList);

    List<String> getRoleByUserId(String userId);

    RoleEntity editInit(String roleId);

    RoleEntity save(RoleEntity roleEntity);

    PageResult<List<RoleEntity>> getList(RoleEntity roleEntity);

    RoleEntity getById(String roleId);

    RoleEntity delete(String roleIds);

    List<ResEntity> getInitTree(String resId);

    RoleEntity getRes(String roleId);

    RoleEntity updateRes(RoleEntity roleDto);

    RoleEntity getUser(String roleId);

    RoleEntity updateUser(RoleEntity roleEntity);

    List<OrgEntity> getInitOrgTree(String orgId);
    List<OrgEntity> getInitOrg(String parentId,List<String> queryType);
    List<UserEntity> getUserByOrgIds(List<String> orgIds);
    List<UserEntity> getUserByUserName(String userName,List<String> orgIds);

    List<OrgEntity> getOrgListTreeByUnitIds(List<String> orgUnitIds);

    List<OrgEntity> getOrgByUserName(String name, List<String> bizIds);
}
