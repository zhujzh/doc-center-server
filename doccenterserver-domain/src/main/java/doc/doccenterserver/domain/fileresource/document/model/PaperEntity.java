package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "文档实体")
public class PaperEntity extends BaseDto {

    @Field(name = "文档id")
    private String paperId;

    @Field(name = "站点id＋频道树id")
    private String classId;

    @Field(name = "文档名称")
    private String paperName;

    private String paperText;

    @Field(name = "落款:组织信息")
    private String signOrgName;

    @Field(name = "落款:日期")
    private String signDate;

    @Field(name = "发布日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime issueDate;

    @Field(name = "发布用户ID")
    private String issueUserId;

    @Field(name = "发布标志")
    private Integer publicFlag;

    @Field(name = "是否公开")
    private String isPublic;

    @Field(name = "录入者用户id")
    private String inputUserId;

    @Field(name = "录入用户所属部门ID")
    private String inputDeptId;

    @Field(name = "录入用户所属公司ID")
    private String inputOrgId;

    @Field(name = "录入时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime inputTime;

    @Field(name = "失效标志")
    private Integer delFlag;

    @Field(name = "失效时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    @Field(name = "文档静态页面路径")
    private String pathUrl;

    @Field(name = "是否频道首页")
    private String isChannel;

    @Field(name = "字符统计")
    private Integer strCount;

    @Field(name = "文档类型")
    private Integer paperType;

    @Field(name = "点击的次数")
    private Integer clickCount;

    @Field(name = "文档排序号（用于置顶操作）")
    private Date paperOrder;

    @Field(name = "文档信息来源")
    private String appId;

    @Field(name = "文档信息来源详细地址")
    private String appUrl;

    @Field(name = "文档权限对象集合")
    private String authObjectNames;

    @Field(name = "图片新闻首页图片路径")
    private String picPath;

    @Field(name = "应用文档编号")
    private String sourceDocId;

    @Field(name = "否已经转化为PDF文件")
    private Integer isPdf;

    @Field(name = "转换后的PDF路径")
    private String pdfUrl;

    @Field(name = "归档组织ID")
    private String archiveOrgCode;

    @Field(name = "精简版文档正文")
    private String mobilePaperText;

    @Field(name = "文档描述")
    private String paperDesc;

    @Field(name = "备用字段1")
    private String reserved1;

    @Field(name = "备用字段2")
    private String reserved2;

    @Field(name = "备用字段3")
    private String reserved3;

    @Field(name = "备用字段4")
    private String reserved4;

    @Field(name = "备用字段5")
    private String reserved5;

    @Field(name = "备用字段6")
    private String reserved6;

    @Field(name = "备用字段7")
    private String reserved7;

    @Field(name = "备用字段8")
    private Integer reserved8;

    @Field(name = "备用字段9")
    private String reserved9;

    @Field(name = "备用字段10")
    private String reserved10;

    @Field(name = "备用字段11")
    private String reserved11;

    @Field(name = "备用字段12")
    private String reserved12;

    @Field(name = "备用字段13")
    private String reserved13;

    @Field(name = "备用字段14")
    private String reserved14;
    private String paperShowTitle;

    // 查询条件
    // ISSUE_DATE - 开始时间
    private String issueStartDate;

    // ISSUE_DATE - 结束时间
    private String issueEndDate;

    private String roleFlag;

    private Boolean istrue;

    private String html;

    // 置顶状态
    private Boolean topStatus;
    private Integer reserved;


    private String fileId;
    private Map<String, Object> channelPathName;
    private Map<String,Object> params;
    private List<String> channelPathName2;
    private String name;

    // addPaper() 新增参数↓
    private String fileApiInfo;

    // 附件信息
    private String fileArrJSON;

    // 图片信息
    private String imgArrJSON;

    private String authList;
    // 权限范围
    private  List<Map<String,Object>> authListMap;
    private String authObjectIds;
    private String authValue;

    private String address;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    // addPaper 新增参数↑


    // getAddPaper() 新增初始化参数↓
    // 判断是否是会议通知
    private Boolean ishytz;

    // 判断是否通知公告频道
    private Boolean istzgg;

    // 判断是否图片新闻频道
    private Boolean istpxw;

    private Boolean isParent;

    private UserEntity userEntity;

    // 当前发布文档的频道
    private ChannelEntity curchannel;

    // 当前发布文档的父类频道
    private List<ChannelEntity> channelList;
    // getAddPaper() 新增初始化参数↑

    private Boolean showNew;
    private Integer intervalTime;

    // 预览
    private String inputUserName;
    private String pdfFilePath;
    private List<AttachmentEntity> attachList;
    private List<AttachmentEntity> mainFile;
    private Integer filelength;
    private List<ImageEntity> imgList;
    // 预览

    private UserEntity curUser;

    private MeetingEntity meeting;

    //文档类型：网页文档
    public final static Integer PAPER_TYPE_HTML = 0;

    //文档类型：office文档
    public final static Integer PAPER_TYPE_OFFICE = 1;

    //发布状态：草稿
    public final static Integer PAPER_PUBLIC_FLAG_DRAFT = 0;
    //发布状态：已撤销
    public final static Integer PAPER_PUBLIC_FLAG_CANCEL = 1;
    //发布状态：已发布
    public final static Integer PAPER_PUBLIC_FLAG_ISSUE = 2;
    //发布状态：发布中
    public final static Integer PAPER_PUBLIC_FLAG_ISSUING = 3;
    //发布状态：审核中
    public final static Integer PAPER_PUBLIC_FLAG_AUDIT = 9;
    //发布状态：退回
    public final static Integer PAPER_PUBLIC_FLAG_BACK = -1;
    //发布状态：正在转换
    public final static Integer PAPER_PUBLIC_FLAG_CONVERTING = 99;
    /**
     * 附件文档
     */
    public final static Integer PAPER_TYPE_ATT = 1;


    private boolean hasChannelAuth;

    private ChannelEntity parentChannel;

    private List<String> authLists;
    private List<String> classIdList;

}
