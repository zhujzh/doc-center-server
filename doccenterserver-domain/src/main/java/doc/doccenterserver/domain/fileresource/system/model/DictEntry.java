package doc.doccenterserver.domain.fileresource.system.model;

import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import com.baomidou.mybatisplus.annotation.TableId;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 字典
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "字典")
public class DictEntry extends BaseDto {

    @TableId
    private String id;

    private String pid;

    private String dictName;

    private String dictValue;

    private Integer dictSort;

    private Integer dictStatus;
}
