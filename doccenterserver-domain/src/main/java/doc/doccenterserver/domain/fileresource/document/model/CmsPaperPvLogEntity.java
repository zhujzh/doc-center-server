package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "CmsPaperPvLog实体")
public class CmsPaperPvLogEntity {

    @Field(name = "PV_ID")
    private String pvId;

    @Field(name = "USER_CODE")
    private String userCode;

    @Field(name = "USER_CODE")
    private String userName;

    @Field(name = "BIZ_ID")
    private String bizId;

    @Field(name = "BIZ_NAME")
    private String bizName;

    @Field(name = "URL")
    private String url;

    @Field(name = "CREATE_TIME")
    private Date createTime;

}
