package doc.doccenterserver.domain.fileresource.document.model;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "InterfaceAccessLogEntity")
public class InterfaceAccessLogEntity extends BaseDto {

    @Field(name = "日志流水号")
    private String logId;

    @Field(name = "接口调用参数")
    private String params;

    @Field(name = "调用人appid")
    private String appid;

    @Field(name = "调用人密码")
    private String password;

    @Field(name = "接口调用时间")
    private Date callTime;

    @Field(name = "接口调用结果状态")
    private String resultStatus;

    @Field(name = "接口调用失败原因")
    private String failReason;

    @Field(name = "请求路径")
    private String requestUrl;

    @Field(name = "调用人ip")
    private String requestIp;

    @Field(name = "appname")
    private String appname;

    // 查询条件
    private String startTime;
    private String endTime;

    private List<AppEntity> appEntityList;
    private List<String> appNames;

    private String userId;
}
