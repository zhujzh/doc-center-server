package doc.doccenterserver.entry.fileresource.document.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import com.alibaba.fastjson2.JSON;
import doc.doccenterserver.app.fileresource.document.api.InterfaceAccessLogService;
import doc.doccenterserver.app.fileresource.document.api.TemplateService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.TemplateServiceI;
import doc.doccenterserver.client.fileresource.document.api.dto.TemplateDto;
import doc.doccenterserver.client.fileresource.document.api.req.TemplateRequest;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@RestController("TemplateCon")
public class TemplateController implements TemplateServiceI {

    @Resource
    private TemplateService templateService;

    @Resource
    private InterfaceAccessLogService interfaceAccessLogService;

    /**
     * 模板管理 - 列表查询
     *
     * @param request
     * @param templateRequest
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/q/template/getListTemplates/v1")
    public Response<PageResult<List<TemplateDto>>> getListTemplates(HttpServletRequest request, TemplateRequest templateRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(templateRequest));
        Response<PageResult<List<TemplateDto>>> response = Response.buildSuccess(templateService.getListTemplates(templateRequest));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 模板管理 - 根据模板id查询
     *
     * @param request
     * @param id
     * @return
     */
    @Override
    @GetMapping(path = "/mp/dc/q/template/getTemplate/v1")
    public Response<TemplateDto> getTemplate(HttpServletRequest request, Integer id) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(id));
        Response<TemplateDto> response = Response.buildSuccess(templateService.getTemplate(id));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 模板管理 - 新增Templet对象
     *
     * @param request
     * @param templateRequest
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/c/template/addTemplate/v1")
    public Response<TemplateDto> addTemplate(HttpServletRequest request, TemplateRequest templateRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(templateRequest));
        Response<TemplateDto> response = Response.buildSuccess(templateService.addTemplate(templateRequest));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 模板管理 - 修改Templet对象
     *
     * @param request
     * @param templateRequest
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/c/template/editTemplate/v1")
    public Response<TemplateDto> editTemplate(HttpServletRequest request, TemplateRequest templateRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(templateRequest));
        Response<TemplateDto> response = Response.buildSuccess(templateService.editTemplate(templateRequest));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 模板管理 - 批量删除Templet对象
     *
     * @param request
     * @param ids
     * @return
     */
    @Override
    @DeleteMapping(path = "/mp/dc/c/template/deleteTemplate/v1")
    public Response<TemplateDto> deleteTemplate(HttpServletRequest request, String ids) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(ids));
        Response<TemplateDto> response = Response.buildSuccess(templateService.deleteTemplate(ids));
        response.setMessage(ResponseEnum.DELETE_SUCCESS.getMsg());
        return response;
    }
}
