package doc.doccenterserver.entry.fileresource.document.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import com.alibaba.fastjson2.JSON;
import doc.doccenterserver.app.fileresource.document.api.ChannelService;
import doc.doccenterserver.app.fileresource.document.api.InterfaceAccessLogService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.ChannelServiceI;
import doc.doccenterserver.client.fileresource.document.api.dto.ChannelDto;
import doc.doccenterserver.client.fileresource.document.api.dto.TreeNode;
import doc.doccenterserver.client.fileresource.document.api.req.ChannelRequest;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import doc.doccenterserver.infrastructure.fileresource.security.model.LoginUser;
import doc.doccenterserver.infrastructure.fileresource.security.service.TokenService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 频道服务controller
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@RestController("ChannelCON")
public class ChannelServiceController implements ChannelServiceI {

    @Resource
    private ChannelService channelService;

    @Resource
    private TokenService tokenService;

    @Resource
    private InterfaceAccessLogService interfaceAccessLogService;

    @Override
    @GetMapping(path = "/mp/dc/q/channel/getEditChannel/v1")
    public Response<ChannelDto> getEditChannel(HttpServletRequest request, @RequestParam String id) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(id));
        Response<ChannelDto> response = Response.buildSuccess(channelService.getEditChannel(id));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    @Override
    @GetMapping(path = "/mp/dc/q/channel/getAddChannel/v1")
    public Response<ChannelDto> getAddChannel(HttpServletRequest request, String parentId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(parentId));
        Response<ChannelDto> response = Response.buildSuccess(channelService.getAddChannel(parentId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 频道管理-列表查询
     *
     * @param channelRequest
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/q/channel/page-list/v1")
    public Response<PageResult<List<ChannelDto>>> listChannels(
            HttpServletRequest request, ChannelRequest channelRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(channelRequest));
        Response<PageResult<List<ChannelDto>>> response = Response.buildSuccess(
                channelService.listChannels(channelRequest));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    @Override
    @GetMapping(path = "/mp/dc/q/channel/init-tree/v1")
    public Response<List<TreeNode>> initTree(HttpServletRequest request) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(""));
        List<TreeNode> treeList = channelService.findInItTree();

     //   LoginUser loginUser = tokenService.getLoginUser(request);
        Response<List<TreeNode>> response = Response.buildSuccess(treeList);
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    @Override
    @PostMapping(path = "/mp/dc/c/channel/addChannel/v1")
    public Response<ChannelDto> addChannel(HttpServletRequest request, ChannelRequest channelRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(channelRequest));
        Response<ChannelDto> response = Response.buildSuccess(channelService.addChannel(channelRequest));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    @Override
    @PostMapping(path = "/mp/dc/c/channel/editChannel/v1")
    public Response<ChannelDto> editChannel(HttpServletRequest request, ChannelRequest channelRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(channelRequest));
        Response<ChannelDto> response = Response.buildSuccess(channelService.editChannel(channelRequest));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    @Override
    @DeleteMapping(path = "/mp/dc/c/channel/deleteChannel/v1")
    public Response<ChannelDto> deleteChannel(HttpServletRequest request, String ids) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(ids));
        Response<ChannelDto> response = Response.buildSuccess(channelService.deleteChannel(ids));
        response.setMessage(ResponseEnum.DELETE_SUCCESS.getMsg());
        return response;
    }

    @Override
    @GetMapping(path = "/mp/dc/q/channel/getChannel/v1")
    public Response<ChannelDto> getChannel(HttpServletRequest request, String id) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(id));
        Response<ChannelDto> response = Response.buildSuccess(channelService.getChannel(id));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }
}
