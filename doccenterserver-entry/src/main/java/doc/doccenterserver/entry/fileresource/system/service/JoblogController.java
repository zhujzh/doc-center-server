package doc.doccenterserver.entry.fileresource.system.service;

import doc.doccenterserver.app.fileresource.system.api.JoblogService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.JobLogEntry;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/12 17:59
 */
@RestController
@RequestMapping(path = "/mp/dc")
public class JoblogController {

    @Autowired
    private JoblogService joblogService;

    /**
     * 分页显示JobLog列表
     */
    @PostMapping(value = "/q/joblog/listjobLogs/v1")
    public PageResult<List<JobLogEntry>> listjobLogs(@RequestBody JobLogEntry jobLogEntry) {
        return joblogService.listjobLogs(jobLogEntry);
    }

    @GetMapping(value = "/q/joblog/lookLogDetail/v1/{logId}")
    public Map<String, Object> lookLogDetail(@PathVariable String logId) {
        return joblogService.lookLogDetail(logId);
    }

    //    查看归档消息
    @GetMapping(value = "/q/joblog/viewarchmsg/v1")
    public Map<String, Object> viewarchmsg(@Param("logId")String logId,@Param("stepId") Integer stepId) {
        return joblogService.viewarchmsg(logId,stepId);
    }
    //    查看错误
    @GetMapping(value = "/q/joblog/viewerrmsg/v1")
    public Map<String, Object> viewerrmsg(@Param("logId")String logId,@Param("stepId") Integer stepId) {
        return joblogService.viewerrmsg(logId,stepId);
    }

    @ResponseBody
    @GetMapping("/c/joblog/retry/v1")
    public boolean retry(@Param("logId")String logId,@Param("stepId") Integer stepId) {
        return joblogService.retry(logId,stepId);
    }

    //重新入库
    @GetMapping(value = "/c/joblog/rearchive/v1")
    public Map<String, Object> rearchive(@Param("logId")String logId) {
        return joblogService.rearchive(logId);
    }

    /**
     * @方法名: handleFiles
     * @描述:获取文档中心，以便做上传和下载操作
     * @参数:@param itemId
     * @参数:@param appId
     * @参数:@param model
     * @参数:@return
     * @返回值：String
     */
    @GetMapping(value = "/c/joblog/handlefiles/v1")
    public Map<String, Object> handlefiles(@Param("sourcePaperId")String sourcePaperId, @Param("paperId")String paperId,
                                           @Param("appId")String appId, @Param("optType")String optType, HttpServletRequest request) {
        return joblogService.handlefiles(sourcePaperId,paperId,appId,optType,request);
    }

    /**
     * @方法名: handleIndex
     * @描述:对文档的索引进行创建或删除操作
     * * @参数:@param paperId 文档id
     * 	 * @参数:@param operation 操作类型 ：create 创建 retrieve 删除
     */
    @GetMapping(value = "/c/joblog/handleindex/v1")
    public Map<String, Object> handleIndex(@Param("logId")String logId, @Param("paperId")String paperId, @Param("operation")String operation,
                                           @Param("appId")String appId) {
        return joblogService.handleIndex(logId,paperId,operation,appId);
    }

    /**
     * sendMQ发送MQ消息(即统一消息)
     */
    @GetMapping(value = "/c/joblog/sendmq/v1")
    public Map<String, Object> sendmq(@Param("logId")String logId, @Param("stepId")Integer stepId) {
        return joblogService.sendmq(logId,stepId);
    }

}
