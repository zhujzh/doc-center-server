package doc.doccenterserver.entry.fileresource.sso;


import com.alibaba.bizworks.core.runtime.common.SingleResponse;
import doc.doccenterserver.app.fileresource.sso.SsoService;
import doc.doccenterserver.app.fileresource.sso.model.SsoDTO;
import doc.doccenterserver.infrastructure.fileresource.sso.annotation.Anonymous;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author wangxp
 * @description sso
 * @date 2023/3/27 14:58
 */
@RestController
public class SsoController {


    @Resource
    private SsoService ssoService;

    /**
     * @param code sso验证码
     * @description sso回调
     * @author wangxp
     * @date 2023/3/29 19:49
     */
    @GetMapping("/mp/dc/ssoRegister/login")
    @Anonymous
    public SingleResponse<SsoDTO> ssoCallback(@RequestParam("code") String code, @RequestParam("state") String requestUrl){
        // 保存 refresh_token 30天 refresh_token_access_token(value)
        try {
            SsoDTO ssoDTO =  ssoService.ssoCallback(code,requestUrl);
            return SingleResponse.buildSuccess(ssoDTO);
        }catch (Exception e){
            return SingleResponse.buildFailure("400",e.getMessage());
        }
    }

    /**
     * @param request 请求
     * @description  退出登录
     * @author wangxp
     * @date 2023/4/27 11:39
     */
    @GetMapping("/mp/dc/ssoRegister/loginOut/v1")
    @Anonymous
    public void loginOut(HttpServletRequest request){
        ssoService.loginOut(request);
    }

}
