package doc.doccenterserver.entry.fileresource.document.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.app.fileresource.document.api.PortalService;
import doc.doccenterserver.client.fileresource.document.api.dto.PortalDto;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@RestController("PortalCON")
public class PortalController {

    @Resource
    private PortalService portalService;

    @PostMapping(path = "/mp/dc/q/portal/getPortal/v1")
    public Response<List<PortalDto>> getPortal(@RequestBody PortalDto portalDto, HttpServletRequest request) {
        Response<List<PortalDto>> response = Response.buildSuccess(portalService.getPortal(portalDto, request));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

}
