package doc.doccenterserver.entry.fileresource.system.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.app.fileresource.system.api.ResService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.TreeNode;
import doc.doccenterserver.client.fileresource.system.api.dto.MenuDto;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

/**
 * @author Administrator
 */
@RestController
@RequestMapping(path = "/mp/dc")
public class ResController {
    @Resource
    private ResService resService;

    @GetMapping(path = "/q/res/getListRes/v1")
    public Response<PageResult<List<ResEntity>>> getListRes(ResEntity resEntity) {
        return Response.buildSuccess(resService.getListRes(resEntity));
    }

    /**
     * 资源管理-初始化树结构
     */
    @GetMapping("/q/res/init-tree/v1")
    public Response<List<TreeNode>> initTree(String parent){
        List<TreeNode> treeList = resService.findInItTree();
        return Response.buildSuccess(treeList);
    }

    @GetMapping("/q/res/getById/v1")
    public Response<ResEntity> getById(String id) {
        ResEntity resEntity = resService.getById(id);
        return Response.buildSuccess(resEntity);
    }

    @PostMapping("/c/res/addRes/v1")
    public Response<ResEntity> addRes(@RequestBody ResEntity resEntity) {
        resEntity.setResId(UUID.randomUUID().toString());
        return Response.buildSuccess(resService.addRes(resEntity));
    }

    @PostMapping("/c/res/editRes/v1")
    public Response<ResEntity> editRes(@RequestBody ResEntity resEntity) {
        return Response.buildSuccess(resService.editRes(resEntity));
    }

    @DeleteMapping("/c/res/deleteRes/v1")
    public Response<ResEntity> deleteRes(String id) {
        return Response.buildSuccess(resService.deleteRes(id));
    }

    //获取用户相关权限资源
    @GetMapping("/q/res/init-user-tree/v1")
    public Response<List<MenuDto>> initUserTree(HttpServletRequest request){
        List<MenuDto> treeList = resService.initUserTree(request);
        return Response.buildSuccess(treeList);
    }
}
