package doc.doccenterserver.entry.fileresource.system.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.app.fileresource.system.api.DictService;
import doc.doccenterserver.app.fileresource.system.model.SysTreeNode;
import doc.doccenterserver.domain.fileresource.system.model.DictEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典controller
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@RestController
@RequestMapping(path = "/mp/dc")
public class DictController {
    @Autowired
    private DictService dictService;

    /**
     * 字典管理-初始化树结构page-list
     */
    @GetMapping("/q/dict/init-tree/v1")
    public Response<List<SysTreeNode>> initTree(String pid) {
        List<SysTreeNode> treeList = dictService.findInItTree(pid);
        return Response.buildSuccess(treeList);
    }


    /**
     * 字典列表查询
     * @param dictEntry
     * @return
     */
    @PostMapping(path = "/q/dict/page-list/v1")
    public doc.doccenterserver.domain.fileresource.base.PageResult<List<DictEntry>> getListRes(@RequestBody DictEntry dictEntry) {
        return dictService.listDict(dictEntry);
    }

    @PostMapping("/q/dict/addDict/v1")
    public Response<DictEntry> addDict(@RequestBody DictEntry dictEntry) {
        try {
            return Response.buildSuccess(dictService.addDict(dictEntry));
        }catch (Exception e){
            return Response.buildFailure("500",e.getMessage());
        }
    }

    @GetMapping("/q/dict/getById/v1")
    public Response<DictEntry> getById(String id) {
        DictEntry dictEntry = dictService.getById(id);
        return Response.buildSuccess(dictEntry);
    }

    @GetMapping("/q/dict/getByPIdAndVal/v1")
    public Response<DictEntry> getByPIdAndVal(String pid,String val) {
        DictEntry dictEntry = dictService.getByPIdAndVal(pid,val);
        return Response.buildSuccess(dictEntry);
    }

    @GetMapping("/q/dict/getByPId/v1")
    public Response<List<DictEntry>> getByPId(String pid) {
        List<DictEntry> dictEntry = dictService.getByPId(pid);
        return Response.buildSuccess(dictEntry);
    }

    @PostMapping("/c/dict/editDict/v1")
    public Response<DictEntry> editDict(@RequestBody DictEntry dictEntry) {
        return Response.buildSuccess(dictService.editDict(dictEntry));
    }

    @DeleteMapping("/c/dict/deleteDict/v1")
    public Response<DictEntry> deleteDict(String id) {
        return Response.buildSuccess(dictService.deleteDict(id));
    }
}
