package doc.doccenterserver.entry.fileresource.file.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.app.fileresource.file.api.HomeService;
import doc.doccenterserver.client.fileresource.file.api.HomeServiceI;
import doc.doccenterserver.client.fileresource.file.api.dto.HomeDto;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


/**
 * @author Tangcancan
 */
@RestController("HomeServiceCON")
public class HomeServiceController implements HomeServiceI {

    @Resource
    private HomeService homeService;

    /**
     * @param homeDto
     * @return
     */
    @Override
    public Response<HomeDto> getDocSearch(HttpServletRequest request, HomeDto homeDto) {
        Response<HomeDto> response = Response.buildSuccess(homeService.getDocSearch(request, homeDto));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }
}
