package doc.doccenterserver.entry.fileresource.document.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import com.alibaba.fastjson2.JSON;
import doc.doccenterserver.app.fileresource.document.api.AppService;
import doc.doccenterserver.app.fileresource.document.api.InterfaceAccessLogService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.AppServiceI;
import doc.doccenterserver.client.fileresource.document.api.dto.AppDto;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author Tangcancan
 */
@RestController("AppServiceCON")
public class AppServiceController implements AppServiceI {

    @Resource
    private AppService appService;

    @Resource
    private InterfaceAccessLogService interfaceAccessLogService;

    @Override
    @PostMapping(path = "/mp/dc/q/app/getUseDetail/v1")
    public Response<PageResult<List<AppDto>>> getUseDetailList(HttpServletRequest request, @RequestBody AppDto appDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(appDto));
        Response<PageResult<List<AppDto>>> response = Response.buildSuccess(appService.getUseDetailList(request, appDto));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 编辑初始化
     *
     * @param appId
     * @return
     */
    @Override
    @GetMapping("/mp/dc/q/app/editInit/v1")
    public Response<AppDto> editInit(HttpServletRequest request, @RequestParam String appId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(appId));
        Response<AppDto> response = Response.buildSuccess(appService.editInit(appId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 保存（新增or编辑）
     *
     * @param appDto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/c/app/save/v1")
    public Response<AppDto> save(HttpServletRequest request, @RequestBody AppDto appDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(appDto));
        Response<AppDto> response = Response.buildSuccess(appService.save(appDto));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 分页查询
     *
     * @param appDto
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/q/app/getAppList/v1")
    public Response<PageResult<List<AppDto>>> getAppList(HttpServletRequest request, @RequestBody AppDto appDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(appDto));
        Response<PageResult<List<AppDto>>> response = Response.buildSuccess(appService.getAppList(appDto));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 删除
     *
     * @param appIds
     * @return
     */
    @Override
    @DeleteMapping("/mp/dc/c/app/delete/v1")
    public Response<AppDto> delete(HttpServletRequest request, @RequestParam String appIds) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(appIds));
        Response<AppDto> response = Response.buildSuccess(appService.delete(appIds));
        response.setMessage(ResponseEnum.DELETE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 详情
     *
     * @param appId
     * @return
     */
    @Override
    @GetMapping("/mp/dc/q/app/getAppDetail/v1")
    public Response<AppDto> getAppDetail(HttpServletRequest request, @RequestParam String appId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(appId));
        Response<AppDto> response = Response.buildSuccess(appService.getAppDetail(appId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }
}
