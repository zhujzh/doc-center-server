package doc.doccenterserver.entry.fileresource.system.service;

import doc.doccenterserver.app.fileresource.system.api.PaperManageService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 15:52
 */
@RestController
@RequestMapping(path = "/mp/dc")
public class PaperManageController {
    @Resource
    private PaperManageService paperManageService;
    /**
     *分页显示Paper列表
     */
    @PostMapping(path = "/q/paperManage/listpapers/v1")
    public PageResult<List<PaperEntity>> listpapers(@RequestBody PaperEntity paperEntity) {
        return paperManageService.listpapers(paperEntity);
    }

    @PostMapping(path = "/q/paperManage/topdoc/v1")
    public Map<String,Object> topdoc(@Param("paperId")String paperId, @Param("topStatus")Boolean topStatus) {
        return paperManageService.topdoc(paperId,topStatus);
    }

    @GetMapping(path = "/c/paperManage/editpaper/v1")
    public Map<String,Object> editpaper(@Param("paperId")String paperId, HttpServletRequest request) {
        return paperManageService.editpaper(paperId,request);
    }

    @PostMapping(path = "/c/paperManage/savePaper/v1")
    public Map<String,Object> savePaper(@RequestBody PaperEntity paperEntity, HttpServletRequest request) {
        return paperManageService.savePaper(paperEntity,request);
    }
}
