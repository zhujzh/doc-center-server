package doc.doccenterserver.entry.fileresource.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import doc.doccenterserver.app.fileresource.system.api.LocalFileService;
import doc.doccenterserver.client.fileresource.file.api.dto.FileDto;
import doc.doccenterserver.client.fileresource.file.api.dto.FileOldResponseDto;
import doc.doccenterserver.client.fileresource.file.api.req.FileOldUploadRequest;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 19:15
 */
@RestController
@RequestMapping(path = "/mp/dc")
public class LocalFileController {
    private String YUNPAN_FILE_DOWLOAD_URL = "aa";
    @Resource
    private LocalFileService localFileService;

    @PostMapping(path = "/q/localFile/listfiles/v1")
    public PageResult<List<FileEntity>> listfiles(@RequestBody FileEntity fileEntity) {
        return localFileService.listfiles(fileEntity);
    }

    /**
     * 重新上传至大数据平台
     */
    @PostMapping(path = "/reUpload")
    public int reUpload(String fileId) {
        return localFileService.reUpload(fileId);
    }

    @GetMapping(path="/q/localFile/showfiledetail/v1")
    public Map<String, Object>showfiledetail(@Param("fileId")String fileId, @Param("appName")String appName){
        return localFileService.showfiledetail(fileId,appName);
    }
    @GetMapping(path="/c/localFile/deletefiles/v1")
    public String deletefiles(@Param("fileIds")String fileIds){
        return localFileService.deletefiles(fileIds);
    }

    /**
     * 文件上传
     *
     * @param fileObj
     * @return Response
     */
    @PostMapping(path = "/c/localFile/upload/v1")
    FileOldResponseDto upload(@RequestParam("file") MultipartFile mFile,
                              @RequestParam Map<String, Object> fileObj) {
        FileOldResponseDto dto=new FileOldResponseDto();
        dto.setFlag(true);
        dto.setMessage("success");
        try {
            FileOldUploadRequest fileOldUploadDto = BeanUtil.fillBeanWithMap(fileObj, new FileOldUploadRequest(), false);
            String appId;
            if(ObjUtil.isNotNull(fileObj.get("appid")) && StrUtil.isNotBlank(fileObj.get("appid").toString())){
                appId= fileObj.get("appid").toString();
            }else if(ObjUtil.isNotNull(fileObj.get("appId"))&& StrUtil.isNotBlank(fileObj.get("appId").toString())){
                appId= fileObj.get("appId").toString();
            }else{
                appId="yunpan";
            }
            String labelName = fileOldUploadDto.getLabelName();
            fileOldUploadDto.setAppId(appId);
            FileDto fileDto = localFileService.upload(mFile,fileOldUploadDto);
            FileOldResponseDto.Domain domain=new FileOldResponseDto.Domain();
            if ("yunpan".equals(appId) || "ZYECM".equals(appId) || "ADMIN".equals(labelName)
                    || "JOB".equals(labelName)) {
                domain.setFileId(fileDto.getFileMd5());
                domain.setFileUrl(YUNPAN_FILE_DOWLOAD_URL + fileDto.getFileMd5());
                dto.setFileId(fileDto.getFileMd5());
            }else{
                domain.setFileId(fileDto.getFileId());
                domain.setFileUrl(fileDto.getFileUrl());
                dto.setFileId(fileDto.getFileId());
                dto.setFilePath(fileDto.getFilePath());
            }
            dto.setDomain(domain);
        }catch (Exception e){
            e.printStackTrace();
            dto.setMessage(e.getMessage());
            dto.setFlag(false);
        }
        return dto;
    }

}
