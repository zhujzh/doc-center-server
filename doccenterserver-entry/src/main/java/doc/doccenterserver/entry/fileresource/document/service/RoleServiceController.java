package doc.doccenterserver.entry.fileresource.document.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import com.alibaba.fastjson2.JSON;
import doc.doccenterserver.app.fileresource.document.api.InterfaceAccessLogService;
import doc.doccenterserver.app.fileresource.document.api.RoleService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.RoleServiceI;
import doc.doccenterserver.client.fileresource.document.api.dto.OrgTree;
import doc.doccenterserver.client.fileresource.document.api.dto.ResDto;
import doc.doccenterserver.client.fileresource.document.api.dto.RoleDto;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import doc.doccenterserver.entry.fileresource.document.req.GetOrgTreeRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */

@RestController("RoleServiceCON")
public class RoleServiceController implements RoleServiceI {

    @Resource
    private RoleService roleService;

    @Resource
    private InterfaceAccessLogService interfaceAccessLogService;

    /**
     * 编辑初始化
     *
     * @param roleId
     * @return
     */
    @Override
    @GetMapping("/mp/dc/q/role/editInit/v1")
    public Response<RoleDto> editInit(HttpServletRequest request, @RequestParam String roleId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(roleId));
        Response<RoleDto> response = Response.buildSuccess(roleService.editInit(roleId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 保存（新增or编辑）
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/c/role/save/v1")
    public Response<RoleDto> save(HttpServletRequest request, @RequestBody RoleDto dto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(dto));
        Response<RoleDto> response = Response.buildSuccess(roleService.save(dto));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 分页查询
     *
     * @param roleDto
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/q/role/getList/v1")
    public Response<PageResult<List<RoleDto>>> getList(HttpServletRequest request, @RequestBody RoleDto roleDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(roleDto));
        Response<PageResult<List<RoleDto>>> response = Response.buildSuccess(roleService.getList(roleDto));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    @Override
    @GetMapping("/mp/dc/q/role/getById/v1")
    public Response<RoleDto> getById(HttpServletRequest request, @RequestParam String roleId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(roleId));
        Response<RoleDto> response = Response.buildSuccess(roleService.getById(roleId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 删除
     *
     * @param roleIds
     * @return
     */
    @Override
    @DeleteMapping("/mp/dc/c/role/delete/v1")
    public Response<RoleDto> delete(HttpServletRequest request, @RequestParam String roleIds) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(roleIds));
        Response<RoleDto> response = Response.buildSuccess(roleService.delete(roleIds));
        response.setMessage(ResponseEnum.DELETE_SUCCESS.getMsg());
        return response;
    }

    /**
     * @param resId
     * @return
     */
    @GetMapping("/mp/dc/q/role/getInitTree/v1")
    public Response<List<ResDto>> getInitTree(HttpServletRequest request, @RequestParam String resId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(resId));
        Response<List<ResDto>> response = Response.buildSuccess(roleService.getInitTree(resId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 获取resId已有的权限资源
     *
     * @param roleId
     * @return
     */
    @GetMapping("/mp/dc/q/role/getRes/v1")
    public Response<RoleDto> getRes(HttpServletRequest request, @RequestParam String roleId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(roleId));
        Response<RoleDto> response = Response.buildSuccess(roleService.getRes(roleId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * @param orgId
     * @return
     */
    @GetMapping("/mp/dc/q/role/getInitOrgTree/v1")
    public Response<List<OrgTree>> getInitOrgTree(HttpServletRequest request, boolean isUser) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(""));
        Response<List<OrgTree>> response = Response.buildSuccess(roleService.getInitOrgTree(isUser));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }
    @PostMapping("/mp/dc/q/role/getInitOrg/v1")
    public Response<List<OrgTree>> getInitOrg(HttpServletRequest request, @RequestBody GetOrgTreeRequest getOrgTreeRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(getOrgTreeRequest));
        Response<List<OrgTree>> response = Response.buildSuccess(roleService.getInitOrg(getOrgTreeRequest.getParentId(),getOrgTreeRequest.getQueryType()));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }
    @GetMapping("/mp/dc/q/role/getInitOrgByUserName/v1")
    public Response<List<OrgTree>> getInitOrgByUserName(HttpServletRequest request, String userName) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(""));
        Response<List<OrgTree>> response = Response.buildSuccess(roleService.getInitOrgByUserName(userName));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }
    /**
     * 更新权限资源
     *
     * @param roleDto
     * @return
     */
    @PostMapping("/mp/dc/c/role/updateRes/v1")
    public Response<RoleDto> updateRes(HttpServletRequest request, @RequestBody RoleDto roleDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(roleDto));
        Response<RoleDto> response = Response.buildSuccess(roleService.updateRes(roleDto));
        response.setMessage(ResponseEnum.OPERATE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 获取roleId已有的用户
     *
     * @param roleId
     * @return
     */
    @GetMapping("/mp/dc/q/role/getUser/v1")
    public Response<RoleDto> getUser(HttpServletRequest request, @RequestParam String roleId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(roleId));
        Response<RoleDto> response = Response.buildSuccess(roleService.getUser(roleId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 更新绑定的用户
     *
     * @param roleDto
     * @return
     */
    @PostMapping("/mp/dc/c/role/updateUser/v1")
    public Response<RoleDto> updateUser(HttpServletRequest request, @RequestBody RoleDto roleDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(roleDto));
        Response<RoleDto> response = Response.buildSuccess(roleService.updateUser(roleDto));
        response.setMessage(ResponseEnum.OPERATE_SUCCESS.getMsg());
        return response;
    }
}
