package doc.doccenterserver.entry.fileresource.system.service;

import doc.doccenterserver.app.fileresource.system.api.JobDetailLogService;
import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/20 20:18
 */
@RestController
@RequestMapping(path = "/mp/dc/")
public class JobDetailLogController {

    @Resource
    private JobDetailLogService jobDetailLogService;

    /**
     *保存JobDetailLog对象到数据库，无id为新增操作，否则为修改操作
     */
    @RequestMapping(value = "/c/jobdetaillog/savejobdetaillog/v1")
    public Map<String, Object> savejobdetaillog(@RequestBody JobDetailLogEntity jobDetailLogEntity) {
        return jobDetailLogService.savejobdetaillog(jobDetailLogEntity);
    }

}
