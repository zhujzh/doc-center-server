package doc.doccenterserver.entry.fileresource.document.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.app.fileresource.document.api.InterfaceAccessLogService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.InterfaceAccessLogServiceI;
import doc.doccenterserver.client.fileresource.document.api.dto.InterfaceAccessLogDto;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@RestController("InterfaceAccessLogCon")
public class InterfaceAccessLogServiceController implements InterfaceAccessLogServiceI {

    @Resource
    private InterfaceAccessLogService interfaceAccessLogService;

    /**
     * 分页显示详细列表
     *
     * @param interfaceAc
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/q/interfaceAccess/getInterfaceAccessList/v1")
    public Response<PageResult<List<InterfaceAccessLogDto>>> getInterfaceAccessLogList(
            HttpServletRequest request, @RequestBody InterfaceAccessLogDto interfaceAc) {
        Response<PageResult<List<InterfaceAccessLogDto>>> response = Response.buildSuccess(
                interfaceAccessLogService.getInterfaceAccessLogList(request, interfaceAc));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 页面初始化
     *
     * @return
     */
    @Override
    @GetMapping(path = "/mp/dc/q/interfaceAccess/getInit/v1")
    public Response<InterfaceAccessLogDto> getInit(HttpServletRequest request) {
        Response<InterfaceAccessLogDto> response = Response.buildSuccess(interfaceAccessLogService.getInit(request));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 详情
     *
     * @param logId
     * @return
     */
    @Override
    @GetMapping(path = "/mp/dc/q/interfaceAccess/getInterfaceAccessLog/v1")
    public Response<InterfaceAccessLogDto> getInterfaceAccessLog(@RequestParam String logId) {
        Response<InterfaceAccessLogDto> response = Response.buildSuccess(
                interfaceAccessLogService.getInterfaceAccessLog(logId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }
}
