package doc.doccenterserver.entry.fileresource.document.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import com.alibaba.fastjson2.JSON;
import doc.doccenterserver.app.fileresource.document.api.AttachmentService;
import doc.doccenterserver.app.fileresource.document.api.InterfaceAccessLogService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.AttachmentServiceI;
import doc.doccenterserver.client.fileresource.document.api.dto.AttachmentDto;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@RestController("AttachmentServiceCON")
public class AttachmentServiceController implements AttachmentServiceI {
    
    @Resource
    private AttachmentService attachmentService;

    @Resource
    private InterfaceAccessLogService interfaceAccessLogService;
    
    /**
     * 编辑初始化
     *
     * @param attachmentDto
     * @return
     */
    @Override
    @GetMapping("/mp/dc/q/attachment/editInit/v1")
    public Response<AttachmentDto> editInit(HttpServletRequest request, @RequestBody AttachmentDto attachmentDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(attachmentDto));
        Response<AttachmentDto> response = Response.buildSuccess(attachmentService.editInit(attachmentDto));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 保存（新增or编辑）
     *
     * @param attachmentDto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/c/attachment/save/v1")
    public Response<AttachmentDto> save(HttpServletRequest request, @RequestBody AttachmentDto attachmentDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(attachmentDto));
        Response<AttachmentDto> response = Response.buildSuccess(attachmentService.save(attachmentDto));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 分页查询
     *
     * @param attachmentDto
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/q/attachment/getList/v1")
    public Response<PageResult<List<AttachmentDto>>> getList(
            HttpServletRequest request, @RequestBody AttachmentDto attachmentDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(attachmentDto));
        Response<PageResult<List<AttachmentDto>>> response = Response.buildSuccess(
                attachmentService.getList(attachmentDto));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 删除
     *
     * @param attachmentDto
     * @return
     */
    @Override
    @DeleteMapping("/mp/dc/c/attachment/delete/v1")
    public Response<AttachmentDto> delete(HttpServletRequest request, @RequestBody AttachmentDto attachmentDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(attachmentDto));
        Response<AttachmentDto> response = Response.buildSuccess(attachmentService.delete(attachmentDto));
        response.setMessage(ResponseEnum.DELETE_SUCCESS.getMsg());
        return response;
    }
}
