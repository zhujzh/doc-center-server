package doc.doccenterserver.entry.fileresource.system.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.app.fileresource.system.api.ParameterService;
import doc.doccenterserver.client.fileresource.system.api.ParameterServiceI;
import doc.doccenterserver.client.fileresource.system.api.dto.ParameterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统配置controller
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@RestController("SYSTEMParameterCON")
public class ParameterController implements ParameterServiceI {
    @Autowired
    private ParameterService parameterService;
    @Override
    public Response<List<ParameterDto>> getList() {
        return Response.buildSuccess(parameterService.getList());
    }
    @Override
    public Response<ParameterDto> getByKey(String key) {
        return Response.buildSuccess(parameterService.getByKey(key));
    }
}
