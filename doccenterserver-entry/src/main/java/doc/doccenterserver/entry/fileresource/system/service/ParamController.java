package doc.doccenterserver.entry.fileresource.system.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.app.fileresource.system.api.ParamService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

/**
 * @author ch
 * @date 2023/4/11 19:02
 */
@RestController
@RequestMapping(path = "/mp/dc")
public class ParamController {
    @Resource
    private ParamService paramService;

    @PostMapping(path = "/q/param/getListParam/v1")
    public PageResult<List<ParamEntity>> getListRes(@RequestBody ParamEntity paramEntity) {
        return paramService.getListParam(paramEntity);
    }

    @GetMapping("/q/param/getById/v1")
    public Response<ParamEntity> getById(String id) {
        ParamEntity paramEntity = paramService.getById(id);
        return Response.buildSuccess(paramEntity);
    }

    @PostMapping("/c/param/addParam/v1")
    public Response<ParamEntity> addParam(@RequestBody ParamEntity paramEntity) {
        paramEntity.setPId(UUID.randomUUID().toString());
        return Response.buildSuccess(paramService.addParam(paramEntity));
    }

    @PostMapping("/c/param/editParam/v1")
    public Response<ParamEntity> editParam(@RequestBody ParamEntity paramEntity) {
        return Response.buildSuccess(paramService.editParam(paramEntity));
    }

    @DeleteMapping("/c/param/deleteParam/v1")
    public Response<ParamEntity> deleteParam(String id) {
        return Response.buildSuccess(paramService.deleteParam(id));
    }
}
