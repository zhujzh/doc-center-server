package doc.doccenterserver.entry.fileresource.document.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import com.alibaba.fastjson2.JSON;
import doc.doccenterserver.app.fileresource.document.api.InterfaceAccessLogService;
import doc.doccenterserver.app.fileresource.document.api.PaperService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.PaperServiceI;
import doc.doccenterserver.client.fileresource.document.api.dto.PaperDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
@RestController("PaperServiceCON")
public class PaperServiceController implements PaperServiceI {

    @Resource
    private PaperService paperService;

    @Resource
    private InterfaceAccessLogService interfaceAccessLogService;

    /**
     * 文档管理，根据id，分页查询paper数据-列表查询
     *
     * @param paperRequest
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/q/paper/paper-list/v1")
    public Response<PageResult<List<PaperDto>>> listPapers(HttpServletRequest request, PaperRequest paperRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(paperRequest));
        Response<PageResult<List<PaperDto>>> response = Response.buildSuccess(paperService.listPapers(request, paperRequest));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 主页-查询初始化
     *
     * @param classId
     * @return
     */
    @Override
    @GetMapping(path = "/mp/dc/q/paper/getListPaper/v1")
    public Response<PaperDto> getListPaper(HttpServletRequest request, String classId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(classId));
        Response<PaperDto> response = Response.buildSuccess(paperService.getListPaper(request, classId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    @GetMapping(path = "/mp/dc/q/paper/getInit/v1")
    public Response<PaperDto> getInit(HttpServletRequest request, String classId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(classId));
        Response<PaperDto> response = Response.buildSuccess(paperService.getInit(classId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 查询文档内容 - 详细
     *
     * @param paperId
     * @return
     */
    @Override
    @GetMapping(path = "/mp/dc/q/paper/getPaperDetail/v1")
    public Response<PaperDto> getPaperDetail(String paperId, HttpServletRequest request) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(paperId));
        Response<PaperDto> response = Response.buildSuccess(paperService.getPaperDetail(paperId, request));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 新增初始化
     *
     * @param classId
     * @return
     */
    @Override
    @GetMapping(path = "/mp/dc/q/paper/getAddPaper/v1")
    public Response<PaperDto> getAddPaper(String classId, HttpServletRequest request) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(classId));
        Response<PaperDto> response = Response.buildSuccess(paperService.getAddPaper(classId, request));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 新增Paper对象
     *
     * @param paperRequest
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/c/paper/addPaper/v1")
    public Response<PaperDto> addPaper(HttpServletRequest request, PaperRequest paperRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(paperRequest));
        Response<PaperDto> response = Response.buildSuccess(paperService.addPaper(request, paperRequest));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 编辑初始化
     *
     * @param paperId
     * @return
     */
    @Override
    @GetMapping(path = "/mp/dc/q/paper/getEditPaper/v1")
    public Response<PaperDto> getEditPaper(String paperId, HttpServletRequest request) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(paperId));
        Response<PaperDto> response = Response.buildSuccess(paperService.getEditPaper(paperId, request));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 编辑Paper对象
     *
     * @param paperRequest
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/c/paper/editPaper/v1")
    public Response<PaperDto> editPaper(HttpServletRequest request, PaperRequest paperRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(paperRequest));
        Response<PaperDto> response = Response.buildSuccess(paperService.editPaper(request, paperRequest));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 置顶或取消置顶文档
     *
     * @param paperRequest
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/c/paper/topDoc/v1")
    public Response<PaperDto> topDoc(HttpServletRequest request, PaperRequest paperRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(paperRequest));
        Response<PaperDto> response = Response.buildSuccess(paperService.topDoc(paperRequest));
        response.setMessage(ResponseEnum.OPERATE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 置顶排序
     *
     * @param paperRequest
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/c/paper/topDocSort/v1")
    public Response<PaperDto> topDocSort(HttpServletRequest request, PaperRequest paperRequest) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(paperRequest));
        Response<PaperDto> response = Response.buildSuccess(paperService.topDocSort(paperRequest));
        response.setMessage(ResponseEnum.OPERATE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 预览文档
     *
     * @param paperRequest
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/q/paper/previewPaper/v1")
    public Response<PaperDto> previewPaper(PaperRequest paperRequest, HttpServletRequest request) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(paperRequest));
        Response<PaperDto> response = Response.buildSuccess(paperService.previewPaper(paperRequest, request));
        response.setMessage(ResponseEnum.OPERATE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 获取预览文件时的文档信息展示
     * @param paperId
     * @param request
     * @return
     */
    @GetMapping(path = "/mp/dc/q/paper/getPreviewInfo/v1")
    public Response<PaperDto> getPreviewInfo(String paperId, HttpServletRequest request) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(paperId));
        Response<PaperDto> response = Response.buildSuccess(paperService.getPreviewInfo(paperId, request));
        response.setMessage(ResponseEnum.OPERATE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 批量删除Paper对象
     *
     * @param ids
     * @return
     */
    @Override
    @DeleteMapping(path = "/mp/dc/c/paper/deletePaper/v1")
    public Response<PaperDto> deletePaper(HttpServletRequest request, String ids) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(ids));
        Response<PaperDto> response = Response.buildSuccess(paperService.deletePaper(ids));
        response.setMessage(ResponseEnum.DELETE_SUCCESS.getMsg());
        return response;
    }

    /**
     *  门户页面统一查询入口
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/c/paper/listfrontpapers/v1")
    public Response<PageResult<List<PaperDto>>> listfrontpapers(@RequestBody PaperRequest paperRequest, HttpServletRequest request){
        Response<PageResult<List<PaperDto>>> response = Response.buildSuccess(paperService.listfrontpapers(paperRequest,paperRequest.getClassId(),paperRequest.getCcId(),request));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     *  门户页面统一查询返回
     * @return
     */
    @Override
    @PostMapping(path = "/mp/dc/c/paper/papersQueryRet/v1")
    public Response<PaperDto> papersQueryRet(@RequestBody PaperRequest paperRequest, HttpServletRequest request){
        Response<PaperDto> response = Response.buildSuccess(paperService.papersQueryRet(paperRequest,paperRequest.getClassId(),paperRequest.getCcId(),request));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }
}
