package doc.doccenterserver.entry.fileresource.document.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import com.alibaba.fastjson2.JSON;
import doc.doccenterserver.app.fileresource.document.api.ImageService;
import doc.doccenterserver.app.fileresource.document.api.InterfaceAccessLogService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.ImageServiceI;
import doc.doccenterserver.client.fileresource.document.api.dto.ImageDto;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Tangcancan
 */
@RestController("ImageServiceCON")
public class ImageServiceIController implements ImageServiceI {

    @Resource
    private ImageService imageService;

    @Resource
    private InterfaceAccessLogService interfaceAccessLogService;

    /**
     * 新增初始化
     *
     * @return
     */
    @Override
    @GetMapping("/mp/dc/q/image/addInit/v1")
    public Response<ImageDto> addInit(HttpServletRequest request) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(""));
        Response<ImageDto> response = Response.buildSuccess(imageService.addInit());
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 编辑初始化
     *
     * @param imgId
     * @return
     */
    @Override
    @GetMapping("/mp/dc/q/image/editInit/v1")
    public Response<ImageDto> editInit(HttpServletRequest request, String imgId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(imgId));
        Response<ImageDto> response = Response.buildSuccess(imageService.editInit(imgId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 保存（新增or编辑）
     *
     * @param imgDto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/c/image/save/v1")
    public Response<ImageDto> save(HttpServletRequest request, ImageDto imgDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(imgDto));
        Response<ImageDto> response = Response.buildSuccess(imageService.save(imgDto));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 分页查询
     *
     * @param imgDto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/q/image/getList/v1")
    public Response<PageResult<List<ImageDto>>> getList(HttpServletRequest request, ImageDto imgDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(imgDto));
        Response<PageResult<List<ImageDto>>> response = Response.buildSuccess(imageService.getList(imgDto));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 删除
     *
     * @param imgIds
     * @return
     */
    @Override
    @DeleteMapping("/mp/dc/c/image/delete/v1")
    public Response<ImageDto> delete(HttpServletRequest request, String imgIds) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(imgIds));
        Response<ImageDto> response = Response.buildSuccess(imageService.delete(imgIds));
        response.setMessage(ResponseEnum.DELETE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 详情
     *
     * @param imgId
     * @return
     */
    @Override
    @GetMapping("/mp/dc/q/image/getDetail/v1")
    public Response<ImageDto> getDetail(HttpServletRequest request, String imgId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(imgId));
        Response<ImageDto> response = Response.buildSuccess(imageService.getDetail(imgId));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 下载
     *
     * @param imgId
     * @param request
     * @param response
     * @return
     */
    @GetMapping("/mp/dc/q/image/downloadFile/v1")
    public Response<ImageDto> downloadFile(String imgId, HttpServletRequest request, HttpServletResponse response) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(imgId));
        Response<ImageDto> response1 = Response.buildSuccess(imageService.downloadFile(imgId, request, response));
        response1.setMessage(ResponseEnum.OPERATE_SUCCESS.getMsg());
        return response1;
    }

    /**
     * 根据图片id或图片路径获取图片宽高大小裁剪参数等信息,进入裁剪页面
     *
     * @param imgDto
     * @return
     */
    @GetMapping("/mp/dc/q/image/showImageTailorPage/v1")
    public Response<ImageDto> showImageTailorPage(HttpServletRequest request, ImageDto imgDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(imgDto));
        Response<ImageDto> response = Response.buildSuccess(imageService.showImageTailorPage(imgDto));
        response.setMessage(ResponseEnum.OPERATE_SUCCESS.getMsg());
        return response;
    }

    @GetMapping("/mp/dc/q/image/getImageInfo/v1")
    public Response<ImageDto> getImageInfo(HttpServletRequest request, String imgId, HttpServletResponse response) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(imgId));
        Response<ImageDto> response1 = Response.buildSuccess(imageService.getImageInfo(imgId, response));
        response1.setMessage(ResponseEnum.OPERATE_SUCCESS.getMsg());
        return response1;
    }

    @GetMapping("/mp/dc/q/image/cutImage/v1")
    public Response<ImageDto> cutImage(HttpServletRequest request, ImageDto imgDto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(imgDto));
        Response<ImageDto> response = Response.buildSuccess(imageService.cutImage(imgDto));
        response.setMessage(ResponseEnum.OPERATE_SUCCESS.getMsg());
        return response;
    }
}
