package doc.doccenterserver.entry.fileresource.document.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import com.alibaba.fastjson2.JSON;
import doc.doccenterserver.app.fileresource.document.api.GroupService;
import doc.doccenterserver.app.fileresource.document.api.InterfaceAccessLogService;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.GroupServiceI;
import doc.doccenterserver.client.fileresource.document.api.dto.GroupDto;
import doc.doccenterserver.domain.fileresource.base.ResponseEnum;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */

@RestController("GroupServiceCON")
public class GroupServiceController implements GroupServiceI {

    @Resource
    private GroupService groupService;

    @Resource
    private InterfaceAccessLogService interfaceAccessLogService;

    /**
     * 编辑初始化
     *
     * @param dto
     * @return
     */
    @Override
    @GetMapping("/mp/dc/q/group/editInit/v1")
    public Response<GroupDto> editInit(HttpServletRequest request, GroupDto dto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(dto));
        Response<GroupDto> response = Response.buildSuccess(groupService.editInit(dto));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 保存（新增or编辑）
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/c/group/save/v1")
    public Response<GroupDto> save(HttpServletRequest request, GroupDto dto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(dto));
        Response<GroupDto> response = Response.buildSuccess(groupService.save(dto));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/q/group/getList/v1")
    public Response<PageResult<List<GroupDto>>> getList(HttpServletRequest request, GroupDto dto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(dto));
        Response<PageResult<List<GroupDto>>> response = Response.buildSuccess(groupService.getList(dto));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }

    /**
     * 更新组成员信息
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/c/group/saveGroupMembers/v1")
    public Response<GroupDto> saveGroupMembers(HttpServletRequest request, GroupDto dto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(dto));
        Response<GroupDto> response = Response.buildSuccess(groupService.saveGroupMembers(dto));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 删除
     *
     * @param groupIds
     * @return
     */
    @Override
    @DeleteMapping("/mp/dc/c/group/delete/v1")
    public Response<GroupDto> delete(HttpServletRequest request, String groupIds) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(groupIds));
        Response<GroupDto> response = Response.buildSuccess(groupService.delete(groupIds));
        response.setMessage(ResponseEnum.DELETE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 删除
     *
     * @param groupId
     * @return
     */
    @Override
    @DeleteMapping("/mp/dc/c/group/deleteGroupById/v1")
    public Response<GroupDto> deleteGroupById(HttpServletRequest request, String groupId) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(groupId));
        Response<GroupDto> response = Response.buildSuccess(groupService.deleteGroupById(groupId));
        response.setMessage(ResponseEnum.DELETE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 更新组成员信息
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/c/group/editUserGroup/v1")
    public Response<GroupDto> editUserGroup(HttpServletRequest request, GroupDto dto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(dto));
        Response<GroupDto> response = Response.buildSuccess(groupService.editUserGroup(request, dto));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 更新组成员信息
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/c/group/insertRoleGroup/v1")
    public Response<GroupDto> insertRoleGroup(HttpServletRequest request, GroupDto dto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(dto));
        Response<GroupDto> response = Response.buildSuccess(groupService.insertRoleGroup(dto));
        response.setMessage(ResponseEnum.SAVE_SUCCESS.getMsg());
        return response;
    }

    /**
     * 查询群组管理员列表
     *
     * @param dto
     * @return
     */
    @Override
    @PostMapping("/mp/dc/q/group/selectRgByRoleId/v1")
    public Response<GroupDto> selectRgByRoleId(HttpServletRequest request, GroupDto dto) {
        interfaceAccessLogService.insert(request, JSON.toJSONString(dto));
        Response<GroupDto> response = Response.buildSuccess(groupService.selectRgByRoleId(dto));
        response.setMessage(ResponseEnum.QUERY_SUCCESS.getMsg());
        return response;
    }
}
