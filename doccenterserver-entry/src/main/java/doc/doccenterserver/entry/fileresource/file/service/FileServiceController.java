package doc.doccenterserver.entry.fileresource.file.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.app.fileresource.file.api.FileService;
import doc.doccenterserver.client.fileresource.file.api.FileServiceI;
import doc.doccenterserver.client.fileresource.file.api.dto.FileDto;
import doc.doccenterserver.client.fileresource.file.api.dto.FileOldResponseDto;
import doc.doccenterserver.client.fileresource.file.api.req.FileOldUploadRequest;
import doc.doccenterserver.client.fileresource.file.api.req.FileRequest;
import doc.doccenterserver.infrastructure.fileresource.message.mqs.KafkaSend;
import doc.doccenterserver.infrastructure.fileresource.sso.annotation.Anonymous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * 文件服务controller
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@RestController("FILEFileServiceCON")
public class FileServiceController implements FileServiceI {
    @Autowired
    private FileService fileService;
    @Autowired
    private KafkaSend kafkaSend;
    private String YUNPAN_FILE_DOWLOAD_URL = "aa";
    @Override
    public Response<FileDto> getFileById(@RequestParam String fileId) {
		return Response.buildSuccess(fileService.getFileById(fileId));
	}
    @Override
    @Anonymous
    public FileOldResponseDto md5Check(FileRequest fileRequest, Map<String, Object> fileObj) {
        String appId =null;
        if(ObjUtil.isNotNull(fileObj.get("appid")) && StrUtil.isNotBlank(fileObj.get("appid").toString())){
            appId= fileObj.get("appid").toString();
        }else if(ObjUtil.isNotNull(fileObj.get("appId"))&& StrUtil.isNotBlank(fileObj.get("appId").toString())){
            appId= fileObj.get("appId").toString();
        }
        fileRequest.setAppId(appId);
        if(ObjUtil.isNotNull(fileObj.get("Password"))&& StrUtil.isNotBlank(fileObj.get("Password").toString())){
            fileRequest.setPassword(fileObj.get("Password").toString());
        }
        FileOldResponseDto dto=new FileOldResponseDto();
        dto.setFlag(true);
        dto.setMessage("success");
        try {
            String fileId=fileService.md5Check(fileRequest);
            FileOldResponseDto.Domain domain=new FileOldResponseDto.Domain();
            domain.setFileId(fileId);
            dto.setDomain(domain);
            dto.setFileId(fileId);
        }catch (Exception e){
            dto.setMessage(e.getMessage());
            dto.setFlag(false);
            e.printStackTrace();
        }
        return dto;
    }
    @Override
    @Anonymous
    public FileOldResponseDto upload(MultipartFile mFile,Map<String, Object> fileObj) {
        FileOldResponseDto dto=new FileOldResponseDto();
        dto.setFlag(true);
        dto.setMessage("success");
        try {
            FileOldUploadRequest fileOldUploadDto = BeanUtil.fillBeanWithMap(fileObj, new FileOldUploadRequest(), false);
            String appId;
            if(ObjUtil.isNotNull(fileObj.get("appid")) && StrUtil.isNotBlank(fileObj.get("appid").toString())){
                appId= fileObj.get("appid").toString();
            }else if(ObjUtil.isNotNull(fileObj.get("appId"))&& StrUtil.isNotBlank(fileObj.get("appId").toString())){
                appId= fileObj.get("appId").toString();
            }else{
                appId="yunpan";
            }
//            String labelName = fileOldUploadDto.getLabelName();
            fileOldUploadDto.setAppId(appId);
            FileDto fileDto = fileService.upload(mFile,fileOldUploadDto);
            FileOldResponseDto.Domain domain=new FileOldResponseDto.Domain();
            domain.setFileId(fileDto.getFileId());
            domain.setFileUrl(fileDto.getFileUrl());
            dto.setFileId(fileDto.getFileId());
            dto.setFilePath(fileDto.getFilePath());
            dto.setDomain(domain);
        }catch (Exception e){
            e.printStackTrace();
            dto.setMessage(e.getMessage());
            dto.setFlag(false);
        }
        return dto;
    }

    @Override
    @Anonymous
    public void download(String fileId, Map<String, Object> fileObj, HttpServletResponse response, HttpServletRequest request) throws UnsupportedEncodingException {
        request.setCharacterEncoding("UTF-8");
        try {
            FileOldUploadRequest fileOldUploadDto = BeanUtil.fillBeanWithMap(fileObj, new FileOldUploadRequest(), false);
            fileOldUploadDto.setFileId(fileId);
            fileService.download(fileOldUploadDto, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Response<FileDto> delete(String fileId, FileDto fileDto) {
        return null;
    }


    @PostMapping(path = "/mp/dc/q/file/sendMes/v1")
    @Anonymous
    public Response<Boolean> sendMes(@RequestBody FileRequest fileRequest) {
        kafkaSend.sendTest("doc-center-server-topic",fileRequest.getMd5());
        return Response.buildSuccess(true);
    }

}
