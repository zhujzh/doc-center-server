package doc.doccenterserver.entry.fileresource.document.req;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.StructureObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author wangxp
 * @description 获取组织用户树 请求
 * @date 2023/6/15 9:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "获取组织用户树请求参数", desc = "获取组织用户树请求参数")
public class GetOrgTreeRequest {


    @Field(name = "父级id")
    private String parentId;

    @Field(name = "查询数据类型") // ORG 组织 USER 用户
    private List<String> queryType;
}
