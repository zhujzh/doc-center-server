package doc.doccenterserver.entry.fileresource.system.service;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.app.fileresource.system.api.PvLogService;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.PvLogEntity;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

/**
 * @author zjz
 * @date 2023/4/11 19:02
 */
@RestController
@RequestMapping(path = "/mp/dc/pv-log")
public class PvlogController {
    @Resource
    private PvLogService pvLogService;
    @PostMapping(path = "/getListParam")
    public PageResult<List<PvLogEntity>> getListRes(@RequestBody PvLogEntity pvLogEntity) {
        return pvLogService.getListParam(pvLogEntity);
    }
    @PostMapping("/add")
    public Response<PvLogEntity> addRes(@RequestBody PvLogEntity pvLogEntity) {
        return Response.buildSuccess(pvLogService.add(pvLogEntity));
    }
}
