package doc.doccenterserver.client.fileresource.document.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.GroupDto;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */

public interface GroupServiceI {

    /**
     * 编辑初始化
     *
     * @param dto
     * @return
     */
    Response<GroupDto> editInit(HttpServletRequest request, @RequestBody GroupDto dto);

    /**
     * 保存（新增or编辑）
     *
     * @param dto
     * @return
     */
    Response<GroupDto> save(HttpServletRequest request, @RequestBody GroupDto dto);

    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    Response<PageResult<List<GroupDto>>> getList(HttpServletRequest request, @RequestBody GroupDto dto);

    /**
     * 更新组成员信息
     *
     * @param dto
     * @return
     */
    Response<GroupDto> saveGroupMembers(HttpServletRequest request, @RequestBody GroupDto dto);

    /**
     * 删除
     *
     * @param groupIds
     * @return
     */
    Response<GroupDto> delete(HttpServletRequest request, String groupIds);

    /**
     * 删除
     *
     * @param groupId
     * @return
     */
    Response<GroupDto> deleteGroupById(HttpServletRequest request, String groupId);

    /**
     * 更新组成员信息
     *
     * @param dto
     * @return
     */
    Response<GroupDto> editUserGroup(HttpServletRequest request, @RequestBody GroupDto dto);

    /**
     * 更新组成员信息
     *
     * @param dto
     * @return
     */
    Response<GroupDto> insertRoleGroup(HttpServletRequest request, @RequestBody GroupDto dto);

    /**
     * 查询群组管理员列表
     *
     * @param dto
     * @return
     */
    Response<GroupDto> selectRgByRoleId(HttpServletRequest request, @RequestBody GroupDto dto);

}
