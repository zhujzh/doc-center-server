package doc.doccenterserver.client.fileresource.document.api.req;

import com.alibaba.bizworks.core.specification.StructureObject;
import doc.doccenterserver.client.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "模板REQ", desc = "模板REQ")
public class TemplateRequest extends BaseDto {

    private long id;

    private String name;

    private Integer type;

    private Integer mode;

    private String descript;

    private String content;

    private String pathname;

}
