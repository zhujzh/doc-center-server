package doc.doccenterserver.client.fileresource.document.api;


import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.InterfaceAccessLogDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
public interface InterfaceAccessLogServiceI {

    /**
     * 分页显示详细列表
     *
     * @param interfaceAc
     * @return
     */
    Response<PageResult<List<InterfaceAccessLogDto>>> getInterfaceAccessLogList(
            HttpServletRequest request, @RequestBody InterfaceAccessLogDto interfaceAc);

    /**
     * 页面初始化
     *
     * @return
     */
    Response<InterfaceAccessLogDto> getInit(HttpServletRequest request);

    /**
     * 详情
     *
     * @param logId
     * @return
     */
    Response<InterfaceAccessLogDto> getInterfaceAccessLog(@RequestParam String logId);

}
