package doc.doccenterserver.client.fileresource.file.api.dto;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.StructureObject;
//import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文件
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "旧文档中心RES")
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileOldResponseDto {
    @Field(name = "校验状态。true：成功，false：失败")
    private Boolean flag;

    @Field(name = "提示信息")
    private String message;

    @Field(name = "文件id")
    private String fileId;

    @Field(name = "文件路径")
    private String filePath;
    @Field(name = "内容")
    private Domain domain;
    @Data
//    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Domain{
        @Field(name = "文件id")
        private String fileId;
        @Field(name = "文件路径")
        private String fileUrl;

    }
}
