package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.StructureObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@StructureObject(name = "OrgTree", desc = "OrgTree")
public class OrgTree {

    // orgId
    private String id;

    // orgName
    private String label;

    // orgParentId
    private String pid;

    private String deptName;
    private Boolean leaf;

    // 数据 类型
    private String dataType;

    // 子集
    private List<OrgTree> children;
}
