package doc.doccenterserver.client.fileresource.file.api.req;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.StructureObject;
import lombok.Data;

/**
 * 旧文档中心参数
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Data
@StructureObject(name = "旧文档中心REQ")
public class FileOldUploadRequest {
    @Field(name = "应用系统编码")
    private String appId;
    @Field(name = "文件校验码")
    private String md5;
    @Field(name = "文件块序号（默认从0开始）")
    private String chunk;
    @Field(name = "文件拆分快总数")
    private Integer chunks;
    @Field(name = "分类编码")
    private String classCode;
    @Field(name = "分类名称")
    private String className;
    @Field(name = "应用系统密码")
    private Integer password;
    @Field(name = "标签")
    private String labelName;
    @Field(name = "文件id")
    private String fileId;
    @Field(name = "是否下载")
    private String download;
    @Field(name = "文件名")
    private String fileName;

}
