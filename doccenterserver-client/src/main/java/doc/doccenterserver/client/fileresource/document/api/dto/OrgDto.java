package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.StructureObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "OrgDto", desc = "OrgDto")
public class OrgDto {

    private String orgId;

    private String orgName;

    private String orgFullName;

    private String orgFullPathName;

    private String orgFullPathId;

    private String orgParentId;

    private String orgType;

    private Integer orgLevel;

    private String orgAreaType;

    private Long orgSort;

    private String orgWorkPhone;

    private String orgWorkAddress;

    private String orgPrincipal;

    private String orgStatus;

    private Date orgCreateTime;

    private String remark;

    private String fundCode;

    private String fundName;

    private String companyId;

    private String deptId;

    private String deptName;

    private String companyName;

    private String orbBanchLeader;

    private List<OrgDto> children;

    // 上级 标识  0 true  1 false
    private Boolean orgLeaf;

    // 组织节点id
    private String orgUnitId;

    // 父级组织单元id
    private String parentOrgUnitId;

    // 组织树数据类型
    private String dataType;

}