package doc.doccenterserver.client.fileresource.document.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.AppDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */

public interface AppServiceI {

    Response<PageResult<List<AppDto>>> getUseDetailList(HttpServletRequest request, @RequestBody AppDto appDto);

    /**
     * 编辑初始化
     *
     * @param appId
     * @return
     */
    Response<AppDto> editInit(HttpServletRequest request, @RequestParam String appId);

    /**
     * 保存（新增or编辑）
     *
     * @param appDto
     * @return
     */
    Response<AppDto> save(HttpServletRequest request, @RequestBody AppDto appDto);

    /**
     * 分页查询
     *
     * @param appDto
     * @return
     */
    Response<PageResult<List<AppDto>>> getAppList(HttpServletRequest request, @RequestBody AppDto appDto);

    /**
     * 编辑初始化
     *
     * @param appIds
     * @return
     */
    Response<AppDto> delete(HttpServletRequest request, @RequestParam String appIds);

    /**
     * 详情
     *
     * @param appId
     * @return
     */
    Response<AppDto> getAppDetail(HttpServletRequest request, @RequestParam String appId);

}
