package doc.doccenterserver.client.fileresource.document.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.ChannelDto;
import doc.doccenterserver.client.fileresource.document.api.dto.TreeNode;
import doc.doccenterserver.client.fileresource.document.api.req.ChannelRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;


import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 频道管理
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
public interface ChannelServiceI {

    /**
     * 编辑-初始化
     *
     * @param id
     * @return
     */
    Response<ChannelDto> getEditChannel(HttpServletRequest request, @RequestParam String id);

    /**
     * 新增-初始化
     *
     * @param parentId
     * @return
     */
    Response<ChannelDto> getAddChannel(HttpServletRequest request, @RequestParam String parentId);

    /**
     * 频道管理-列表查询
     *
     * @param channelRequest
     * @return
     */
    Response<PageResult<List<ChannelDto>>> listChannels(HttpServletRequest request, @RequestBody ChannelRequest channelRequest);

    /**
     * 频道管理-初始化树结构
     *
     * @return
     */
    Response<List<TreeNode>> initTree(HttpServletRequest request);

    /**
     * 新增
     *
     * @param channelRequest
     * @return
     */
    Response<ChannelDto> addChannel(HttpServletRequest request, @RequestBody ChannelRequest channelRequest);

    /**
     * 编辑
     *
     * @param channelRequest
     * @return
     */
    Response<ChannelDto> editChannel(HttpServletRequest request, @RequestBody ChannelRequest channelRequest);

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    Response<ChannelDto> deleteChannel(HttpServletRequest request, @RequestParam(value = "ids", required = false) String ids);

    /**
     * 根据id获取频道信息
     *
     * @param id
     * @return
     */
    Response<ChannelDto> getChannel(HttpServletRequest request, @RequestParam(value = "id") String id);

}
