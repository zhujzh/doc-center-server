package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.StructureObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "UserDto", desc = "UserDto")
public class UserDto {
    private String userId;

    private String userCode;

    private String userName;

    private String userSex;

    private BigDecimal userAge;

    private String companyId;

    private String orgId;

    private String userMobile;

    private String userMail;

    private String userWorkAddress;

    private String userWorkPhone;

    private String userHomeAddree;

    private String userHomePhone;

    private String positionId;

    private String pluralityPositionId;

    private String titleId;

    private String pluralityTitleId;

    private String userType;

    private String userStatus;

    private BigDecimal userSort;

    private String userPwd;

    private Date userCreateTime;

    private Date userUpdateTime;

    private String userCreator;

    private String remark;

    private String deptId;

    private String companyName;

    private String deptName;

    private String orgName;
    
    private String allOrgId;
    
    private String allGroupId;
    
    private List<String> authList=new ArrayList<>();
    
    private String skinId;
    
}