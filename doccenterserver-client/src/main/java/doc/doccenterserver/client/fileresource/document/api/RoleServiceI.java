package doc.doccenterserver.client.fileresource.document.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.RoleDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
public interface RoleServiceI {

    /**
     * 编辑初始化
     *
     * @param roleId
     * @return
     */
    Response<RoleDto> editInit(HttpServletRequest request, String roleId);

    /**
     * 保存（新增or编辑）
     *
     * @param dto
     * @return
     */
    Response<RoleDto> save(HttpServletRequest request, @RequestBody RoleDto dto);

    /**
     * 分页查询
     *
     * @param dto
     * @return
     */
    Response<PageResult<List<RoleDto>>> getList(HttpServletRequest request, @RequestBody RoleDto dto);

    Response<RoleDto> getById(HttpServletRequest request, @RequestParam("roleId") String roleId);

    /**
     * 删除
     *
     * @param roleIds
     * @return
     */
    Response<RoleDto> delete(HttpServletRequest request, String roleIds);

}
