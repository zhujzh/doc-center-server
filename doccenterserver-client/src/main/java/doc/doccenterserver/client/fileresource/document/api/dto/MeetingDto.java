package doc.doccenterserver.client.fileresource.document.api.dto;


import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import doc.doccenterserver.client.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "ImageDto")
public class MeetingDto extends BaseDto {

    @Field(name = "文档编号")
    private String paperId;
    private String paperName;

    @Field(name = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    @Field(name = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;

    // <public：公告，notice:通知，meeting：会议通知>
    @Field(name = "业务类型")
    private String typeValue;

    @Field(name = "地址")
    private String address;
}
