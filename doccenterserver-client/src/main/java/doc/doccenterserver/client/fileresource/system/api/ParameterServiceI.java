package doc.doccenterserver.client.fileresource.system.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.system.api.dto.ParameterDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 系统配置服务
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@FeignClient(value = "ParameterServiceI", contextId = "ParameterServiceI-1678246494729")
@RequestMapping(path = "/SYSTEM/Parameter")
public interface ParameterServiceI {

    @GetMapping(path = "/getList/v1")
    Response<List<ParameterDto>> getList();

    @GetMapping(path = "/getByKey/v1")
    Response<ParameterDto> getByKey(@RequestParam String key);
}
