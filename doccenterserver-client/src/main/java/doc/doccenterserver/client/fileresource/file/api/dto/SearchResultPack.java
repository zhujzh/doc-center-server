package doc.doccenterserver.client.fileresource.file.api.dto;

import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DomainObject(isAggregateRoot = true, name = "全文检索结果类")
public class SearchResultPack {

    // 标题
    private String idxTitle;
    // 网页文档标识
    private String docType;
    // 摘要
    private String idxDesc;
    // 用户id
    private String authorId;
    // 用户名称
    private String authorName;
    private String objTag;
    private String objIcon;
    // 发布时间
    private String issueDate;
    private String objSize;
    // 网页文档没有评论功能
    private Integer plNum;

    private String menuid;
    // 判断文档是否被该用户收藏
    private Boolean isCollect;
    private String classId;
    private String className;
    private String appId;
    private Integer djNum;
    private String objName;
    private String paperId;
    private String deptName;

    private String fileId;
    private Integer xzNum;

    private String suffix;

    private Integer totalNum;
    private Integer loadedDataNum;
}
