package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.StructureObject;
import com.alibaba.bizworks.core.specification.ddd.DomainObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "User2roleDto", desc = "User2roleDto")
public class User2roleDto {

    @Field(name = "角色ID")
    private String roleId;

    @Field(name = "用户ID")
    private String userId;

    @Field(name = "备注")
    private String remark;

    @Field(name = "USER_CODE")
    private String userCode;

    @Field(name = "USER_NAME")
    private String userName;

    @Field(name = "ORG_ID")
    private String orgId;

    @Field(name = "ORG_NAME")
    private String orgName;

}
