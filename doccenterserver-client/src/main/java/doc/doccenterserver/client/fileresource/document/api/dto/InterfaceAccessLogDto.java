package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.StructureObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import doc.doccenterserver.client.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "InterfaceAccessLogDto", desc = "InterfaceAccessLogDto")
public class InterfaceAccessLogDto extends BaseDto {

    @Field(name = "日志流水号")
    private String logId;

    @Field(name = "接口调用参数")
    private String params;

    @Field(name = "调用人appid")
    private String appid;

    @Field(name = "调用人密码")
    private String password;

    @Field(name = "接口调用时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date callTime;

    @Field(name = "接口调用结果状态")
    private String resultStatus;

    @Field(name = "接口调用失败原因")
    private String failReason;

    @Field(name = "请求路径")
    private String requestUrl;

    @Field(name = "调用人ip")
    private String requestIp;

    @Field(name = "appname")
    private String appname;

    // 查询条件
    private String startTime;
    private String endTime;

    private List<AppDto> appDtoList;

    private String userId;
}
