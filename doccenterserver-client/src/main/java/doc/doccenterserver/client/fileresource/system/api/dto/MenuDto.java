package doc.doccenterserver.client.fileresource.system.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author ch
 * @date 2023/5/16 16:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuDto {

    private String id;
    private String pId;
    private Boolean open;
    private Boolean isParent;
    //菜单
    private String title;
    private String index;
    private String icon;

    private List<MenuDto> children;
}
