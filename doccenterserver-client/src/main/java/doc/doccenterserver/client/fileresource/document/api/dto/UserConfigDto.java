package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.StructureObject;
import doc.doccenterserver.client.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "UserConfigDto", desc = "UserConfigDto")
public class UserConfigDto extends BaseDto {

    @Field(name = "用户ID")
    private String userId;

    @Field(name = "页面缩放尺寸")
    private String paperScale;

    @Field(name = "主题皮肤ID")
    private String skinId;

}
