package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.StructureObject;
import doc.doccenterserver.client.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "PortalDto", desc = "PortalDto")
public class PortalDto extends BaseDto {

    // 类别
    private String msgType;

    // 频道id
    private String id;

    // 频道父id
    private String parentId;

    // 图片新闻
    private List<ImageDto> imageList;

    // 频道
    private List<ChannelDto> channelList;

    // 文档
    private List<PaperDto> paperList;

    // 会议
    private List<MeetingDto> meetingList;

    // 分页
    private BaseDto page;

    // 搜索条件
    private String content;

}
