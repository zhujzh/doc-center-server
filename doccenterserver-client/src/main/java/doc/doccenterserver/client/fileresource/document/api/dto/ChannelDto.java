package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.StructureObject;
import doc.doccenterserver.client.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * 文件
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "频道Dto", desc = "频道Dto")
public class ChannelDto extends BaseDto {
    @Field(name = "频道id")
    private String id;

    @Field(name = "频道名称")
    private String name;

    @Field(name = "上级频道id")
    private String parentId;

    @Field(name = "频道简称")
    private String code;

    @Field(name = "频道类型")
    private Integer grade;

    @Field(name = "路径")
    private String path;

    @Field(name = "频道描述")
    private String memo;

    @Field(name = "作废标志")
    private Integer deleteFlag;

    @Field(name = "主要概览模板编号")
    private Integer listTeplet;

    @Field(name = "细览模板编号")
    private Integer docTeplet;

    @Field(name = "所属公司id")
    private String orgId;

    @Field(name = "频道排序")
    private Integer sort;

    @Field(name = "频道其他概览模板ID")
    private String otherteplet;

    @Field(name = "频道发布流程定义ID")
    private String procDefId;

    @Field(name = "频道权限对象名称集合")
    private String authObjectNames;

    @Field(name = "预留文本1")
    private String reserved1;

    @Field(name = "预留文本2")
    private String reserved2;

    @Field(name = "预留文本3")
    private String reserved3;

    @Field(name = "预留文本4")
    private String reserved4;

    @Field(name = "预留文本5")
    private String reserved5;

    @Field(name = "预留文本6")
    private String reserved6;

    @Field(name = "预留文本7")
    private String reserved7;

    @Field(name = "预留文本8")
    private String reserved8;

    @Field(name = "预留文本9")
    private String reserved9;

    @Field(name = "预留文本10")
    private String reserved10;

    @Field(name = "预留文本11")
    private String reserved11;

    @Field(name = "预留文本12")
    private String reserved12;

    @Field(name = "预留文本13")
    private String reserved13;

    @Field(name = "预留文本14")
    private String reserved14;

    private List<String> authObjectIds;

    private List<Integer> authValue;

    private String orgName;

    private String cId;
    private String curId;

    private ChannelDto chan;

    // 概览模板
    private List<TemplateDto> overviewTemplate;
    // 细览模板
    private List<TemplateDto> viewTemplate;

    private List<Map<String,Object>> authObjectList;
    private List<Map<String,Object>> authObjectOrgList;

}
