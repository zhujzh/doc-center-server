package doc.doccenterserver.client.fileresource.file.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.file.api.dto.FileDto;
import doc.doccenterserver.client.fileresource.file.api.dto.FileOldResponseDto;
import doc.doccenterserver.client.fileresource.file.api.req.FileRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * 文件服务
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@FeignClient(value = "FileServiceI", contextId = "FileServiceI-167824649472911")
public interface FileServiceI {
    /**
     * 根据文件ID获取
     * @param fileId
     * @return Response
     */
    @GetMapping(path = "/mp/dc/q/file/getFileById/v1")
    Response<FileDto> getFileById(@RequestParam String fileId);
    /**
     * 文件MD5值校验
     * @param fileRequest
     * @return Response
     */
    @PostMapping(path = "/mp/dc/q/file/md5/check/v1")
    FileOldResponseDto md5Check(@RequestBody FileRequest fileRequest, @RequestParam Map<String, Object> fileObj);
    /**
     * 文件上传
     * @param fileObj
     * @return Response
     */
    @PostMapping(path = "/mp/dc/c/file/upload/v1")
    FileOldResponseDto upload(@RequestParam("file") MultipartFile mFile,
                             @RequestParam Map<String, Object> fileObj);
    /**
     * 文件下载
     * @param fileObj
     * @return Response
     */
    @GetMapping(path = "/mp/dc/c/file/download/{fileId}/v1")
    void download(@PathVariable String fileId, @RequestParam Map<String, Object> fileObj, HttpServletResponse response, HttpServletRequest request) throws UnsupportedEncodingException;
    /**
     * 删除文件
     * @param fileDto
     * @return Response
     */
    @GetMapping(path = "/mp/dc/c/file/delete/{fileId}/v1")
    Response<FileDto> delete(@PathVariable String fileId,@RequestBody FileDto fileDto);



}
