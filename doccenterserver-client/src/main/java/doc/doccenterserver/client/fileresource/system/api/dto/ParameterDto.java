package doc.doccenterserver.client.fileresource.system.api.dto;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.StructureObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统配置
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "系统配置")
public class ParameterDto {

    @Field(name = "主键")
    private String pId;

    @Field(name = "定义标识符")
    private String pKey;

    @Field(name = "值")
    private String pValue;

    @Field(name = "描述")
    private String description;
}
