package doc.doccenterserver.client.fileresource.document.api.dto;

import doc.doccenterserver.client.fileresource.base.BaseDto;
import lombok.Data;

/**
 * @author Tangcancan
 */
@Data
public class TemplateDto extends BaseDto {

    private long id;
    private String name;
    private Integer type;
    private String descript;
    private String content;
    private String pathname;
    private Integer mode;
}
