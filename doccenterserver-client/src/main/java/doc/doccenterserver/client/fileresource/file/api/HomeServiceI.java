package doc.doccenterserver.client.fileresource.file.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.file.api.dto.HomeDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 首页 文档检索查询
 *
 * @author Tangcancan
 */
@FeignClient(value = "HomeServiceI", contextId = "HomeServiceI-167824649472911")
@RequestMapping(path = "/mp/dc")
public interface HomeServiceI {

    /**
     * @param homeDto
     * @return
     */
    @PostMapping("/q/home/getDocSearch/v1")
    Response<HomeDto> getDocSearch(HttpServletRequest request, @RequestBody HomeDto homeDto);

}
