package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.StructureObject;
import doc.doccenterserver.client.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "RoleDto", desc = "RoleDto")
public class RoleDto extends BaseDto {

    @Field(name = "角色ID")
    private String roleId;

    @Field(name = "角色名称")
    private String roleName;

    @Field(name = "角色类型")
    private String roleType;

    @Field(name = "角色排序号")
    private Integer roleSort;

    @Field(name = "角色所属组织")
    private String roleOrgId;

    @Field(name = "角色所属系统")
    private String roleAppId;

    @Field(name = "角色状态")
    private String roleStatus;

    @Field(name = "角色创建时间")
    private Date roleCreateTime;

    @Field(name = "角色创建者")
    private String roleCreator;

    @Field(name = "备注")
    private String remark;

    private List<String> resList;
    private List<User2roleDto> userList;

}
