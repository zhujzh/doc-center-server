package doc.doccenterserver.client.fileresource.document.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.PaperDto;
import doc.doccenterserver.client.fileresource.document.api.req.PaperRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * front 文档管理
 *
 * @author Tangcancan
 */
public interface PaperServiceI {

    /**
     * 文档管理，根据id，分页查询paper数据-列表查询
     *
     * @param paperRequest
     * @return
     */
    Response<PageResult<List<PaperDto>>> listPapers(HttpServletRequest request, @RequestBody PaperRequest paperRequest);

    /**
     * 主页-查询初始化
     *
     * @param classId
     * @return
     */
    Response<PaperDto> getListPaper(HttpServletRequest request, @RequestParam String classId);

    /**
     * 查询文档内容 - 详细
     *
     * @param paperId
     * @return
     */
    Response<PaperDto> getPaperDetail(@RequestParam(value = "paperId") String paperId, HttpServletRequest request);

    /**
     * 新增初始化
     *
     * @param classId
     * @return
     */
    Response<PaperDto> getAddPaper(@RequestParam String classId, HttpServletRequest request);

    /**
     * 新增Paper对象
     *
     * @param paperRequest
     * @return
     */
    Response<PaperDto> addPaper(HttpServletRequest request, @RequestBody PaperRequest paperRequest);

    /**
     * 编辑初始化
     *
     * @param paperId
     * @param request
     * @return
     */
    Response<PaperDto> getEditPaper(@RequestParam String paperId, HttpServletRequest request);

    /**
     * 编辑Paper对象
     *
     * @param paperRequest
     * @return
     */
    Response<PaperDto> editPaper(HttpServletRequest request, @RequestBody PaperRequest paperRequest);

    /**
     * 置顶或取消置顶文档
     *
     * @param paperRequest
     * @return
     */
    Response<PaperDto> topDoc(HttpServletRequest request, @RequestBody PaperRequest paperRequest);

    /**
     * 置顶排序
     *
     * @param paperRequest
     * @return
     */
    Response<PaperDto> topDocSort(HttpServletRequest request, @RequestBody PaperRequest paperRequest);

    /**
     * 预览文档
     *
     * @param paperRequest
     * @return
     */
    Response<PaperDto> previewPaper(@RequestBody PaperRequest paperRequest, HttpServletRequest request);

    /**
     * 批量删除Paper对象
     *
     * @param ids
     * @return
     */
    Response<PaperDto> deletePaper(HttpServletRequest request, @RequestParam(value = "ids", required = false) String ids);

    /**
     * 门户页面统一查询入口
     * @return
     */
    Response<PageResult<List<PaperDto>>> listfrontpapers(PaperRequest paperRequest,HttpServletRequest request);

    /**
     * 门户页面统一查询入口
     * @return
     */
    Response<PaperDto> papersQueryRet(PaperRequest paperRequest,HttpServletRequest request);

}
