package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.Field;
import com.alibaba.bizworks.core.specification.StructureObject;
import doc.doccenterserver.client.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Tangcancan
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "GroupDto", desc = "GroupDto")
public class GroupDto extends BaseDto {

    @Field(name = "主键")
    private String groupId;

    @Field(name = "名称")
    private String groupName;

    @Field(name = "父组ID")
    private String groupParentId;

    @Field(name = "组排序号")
    private Integer groupSort;

    @Field(name = "组类型")
    private String groupType;

    @Field(name = "组所属用户id")
    private String groupUserid;

    private String userId;
    private String userName;
    private String groupParentName;
    private String userIds;
    private String groupIds;
    private Boolean isMany;

    private String role2group;

}
