package doc.doccenterserver.client.fileresource.document.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.AttachmentDto;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
public interface AttachmentServiceI {

    /**
     * 编辑初始化
     *
     * @param attachmentDto
     * @return
     */
    Response<AttachmentDto> editInit(HttpServletRequest request, @RequestBody AttachmentDto attachmentDto);

    /**
     * 保存（新增or编辑）
     *
     * @param attachmentDto
     * @return
     */
    Response<AttachmentDto> save(HttpServletRequest request, @RequestBody AttachmentDto attachmentDto);

    /**
     * 分页查询
     *
     * @param attachmentDto
     * @return
     */
    Response<PageResult<List<AttachmentDto>>> getList(HttpServletRequest request, @RequestBody AttachmentDto attachmentDto);

    /**
     * 删除
     *
     * @param attachmentDto
     * @return
     */
    Response<AttachmentDto> delete(HttpServletRequest request, @RequestBody AttachmentDto attachmentDto);

}
