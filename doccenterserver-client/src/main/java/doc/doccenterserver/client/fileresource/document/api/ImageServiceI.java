package doc.doccenterserver.client.fileresource.document.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.ImageDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
public interface ImageServiceI {

    /**
     * 新增初始化
     *
     * @return
     */
    Response<ImageDto> addInit(HttpServletRequest request);

    /**
     * 编辑初始化
     *
     * @param imgId
     * @return
     */
    Response<ImageDto> editInit(HttpServletRequest request, @RequestParam String imgId);

    /**
     * 保存（新增or编辑）
     *
     * @param imgDto
     * @return
     */
    Response<ImageDto> save(HttpServletRequest request, @RequestBody ImageDto imgDto);

    /**
     * 分页查询
     *
     * @param imgDto
     * @return
     */
    Response<PageResult<List<ImageDto>>> getList(HttpServletRequest request, @RequestBody ImageDto imgDto);

    /**
     * 删除
     *
     * @param imgIds
     * @return
     */
    Response<ImageDto> delete(HttpServletRequest request, @RequestParam String imgIds);

    /**
     * 详情
     *
     * @param imgId
     * @return
     */
    Response<ImageDto> getDetail(HttpServletRequest request, @RequestParam String imgId);

}
