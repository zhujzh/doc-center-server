package doc.doccenterserver.client.fileresource.system.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.system.api.dto.DictDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 字典服务
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@FeignClient(value = "DictServiceI", contextId = "DictServiceI-1678246494729")
@RequestMapping(path = "/SYSTEM/DICT")
public interface DictServiceI {

    /**
     * 获取字典集合
     *
     * @param pid
     * @return Response
     */
    @PostMapping(path = "/GetDictList/getDictList/v1")
    Response<DictDto> getDictList(@RequestBody(required = false) String pid);
}
