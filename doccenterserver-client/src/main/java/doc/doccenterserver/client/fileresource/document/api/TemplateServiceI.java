package doc.doccenterserver.client.fileresource.document.api;

import com.alibaba.bizworks.core.runtime.common.Response;
import doc.doccenterserver.client.fileresource.base.PageResult;
import doc.doccenterserver.client.fileresource.document.api.dto.TemplateDto;
import doc.doccenterserver.client.fileresource.document.api.req.TemplateRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Tangcancan
 */
public interface TemplateServiceI {

    /**
     * 模板管理 - 列表查询
     *
     * @param templateRequest
     * @return
     */

    Response<PageResult<List<TemplateDto>>> getListTemplates(HttpServletRequest request, @RequestBody TemplateRequest templateRequest);

    /**
     * 模板管理 - 根据模板id查询
     *
     * @param id
     * @return
     */
    Response<TemplateDto> getTemplate(HttpServletRequest request, @RequestParam(value = "id") Integer id);

    /**
     * 模板管理 - 新增Templet对象
     *
     * @param templateRequest
     * @return
     */
    Response<TemplateDto> addTemplate(HttpServletRequest request, @RequestBody TemplateRequest templateRequest);

    /**
     * 模板管理 - 修改Templet对象
     *
     * @param templateRequest
     * @return
     */
    Response<TemplateDto> editTemplate(HttpServletRequest request, @RequestBody TemplateRequest templateRequest);

    /**
     * 模板管理 - 批量删除Templet对象
     *
     * @param ids
     * @return
     */
    Response<TemplateDto> deleteTemplate(HttpServletRequest request, @RequestParam(value = "ids") String ids);


}
