package doc.doccenterserver.client.fileresource.document.api.dto;

import com.alibaba.bizworks.core.specification.StructureObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Tangcancan
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "ResDto", desc = "ResDto")
public class ResDto {

    private String resId;
    private Integer authYesNo;
    private String resIcon;
    private String resKey;
    private String resParentId;
    private Integer resSort;
    private String resType;
    private String resValue;
    private Boolean isParent;
    private List<ResDto> children;

}
