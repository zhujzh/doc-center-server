package doc.doccenterserver.client.fileresource.document.api.req;

import com.alibaba.bizworks.core.specification.StructureObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import doc.doccenterserver.client.fileresource.base.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StructureObject(name = "文档表REQ", desc = "文档表REQ")
public class PaperRequest extends BaseDto {

    private Integer delFlag;
    private String paperId;
    private String paperName;
    private String paperText;
    private String signOrgName;
    private String signDate;
    private String pathUrl;
    private Integer paperType;
    private String isPublic;
    private String address;
    private String orgId;
    private String picPath;
    private Integer reserved8;

    // 状态
    private Integer publicFlag;
    // ISSUE_DATE - 开始时间
    private String issueStartDate;
    // ISSUE_DATE - 结束时间
    private String issueEndDate;
    private String classId;
    private String ccId;
    // 置顶状态
    private Boolean topStatus;

    private String fileApiInfo;

    // 附件信息
    private String fileArrJSON;

    // 图片信息
    private String imgArrJSON;

    private String authList;
    private String authObjectIds;
    private String authValue;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime endTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime startTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime endDate;



}
