mybatis plus使用说明： 

1.开发包括两部分：  
&emsp;&emsp;数据实体类Entitiy开发,DDD架构建议放到infrastructure层的xxx.tunnel.dataobject，MVC架构放到src/main/java 下的 dao包下  
&emsp;&emsp;mybatis plus mapper接口开发,建议放到MVC架构放到src/main/java 下的 dao.*.mapper包下  
  
2.mybatis plus示例参考:  
DDD架构参考 starter模块 src/test/java目录 com.alibaba.bizworks.demo.mybatisplus包内容   
MVC架构参考 src/test/java目录 com.alibaba.bizworks.demo.mybatisplus包内容   
  
3.mybatis plus 常用注解说明：  
@TableName  
&emsp;&emsp;实体类名称和数据表名不一致时，通过它指定表名。  
  
 参数  | 描述  | 
 ---- | ----- | 
 value  | 表名  
 resultMap  | xml 中 resultMap 的 id       
 schema  | schema(@since 3.1.1)   
 keepGlobalPrefix  | 是否保持使用全局的 tablePrefix 的值(如果设置了全局 tablePrefix 且自行设置了 value 的值)(@since 3.1.1)
   
@TableId（type = IdType.值）  
&emsp;&emsp;实体类属性名称和数据表主键不是id时，通过它声明该属性为主键。  
  
 参数  | 描述  | 
 ---- | ----- | 
 AUTO  | 自增（1，2，3，…）数据库中需要设置主键自增
 NONE  | 该类型为未设置主键类型   
 INPUT  | 手动录入   
 ID_WORKER  | 默认主键类型，全局唯一ID，Long类型的主键
 UUID  | 自动生成uuid 插入   
 ID_WORKER_STR  | 字符串全局唯一ID  
  
@TableField    
&emsp;&emsp;实体类属性名称和数据表字段名不一致时，通过它指定数据表字段名称。 
  
 参数  | 描述  | 
 ---- | ----- | 
 value  | 字段值（驼峰命名方式，该值可无）
 update  | 预处理 set 字段自定义注入     
 condition  | 预处理 WHERE 实体条件自定义运算规则   
 el  | 详看注释说明
 exist  | 是否为数据库表字段（ 默认 true 存在，false 不存在 ）   
 strategy  | 字段验证 （ 默认 非 null 判断，查看com.baomidou.mybatisplus.enums.FieldStrategy    
 fill  | 字段填充标记 （ FieldFill, 配合自动填充使用 ）  
    