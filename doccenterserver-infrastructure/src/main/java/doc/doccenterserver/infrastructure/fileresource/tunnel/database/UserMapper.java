package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.UserDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 用户表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface UserMapper extends BaseMapper<UserDO> {
    List<Map<String, Object>> querySelectedUsers(List<String> userIdList);

}
