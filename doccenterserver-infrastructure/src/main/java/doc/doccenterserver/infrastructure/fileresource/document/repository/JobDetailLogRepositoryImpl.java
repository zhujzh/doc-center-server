package doc.doccenterserver.infrastructure.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;
import doc.doccenterserver.domain.fileresource.document.repository.JobDetailLogRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.JobDetailLogEntityToJobDetailLogDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.JobDetailLogMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("JobDetailLogDORepository")
public class JobDetailLogRepositoryImpl implements JobDetailLogRepository {

    @Resource
    private JobDetailLogMapper jobDetailLogMapper;

    @Override
    public Integer insertJobDetailLog(JobDetailLogEntity jobDetailLogEntity) {
        return jobDetailLogMapper.insert(JobDetailLogEntityToJobDetailLogDOConvertor.INSTANCE.entityToDO(jobDetailLogEntity));
    }
}
