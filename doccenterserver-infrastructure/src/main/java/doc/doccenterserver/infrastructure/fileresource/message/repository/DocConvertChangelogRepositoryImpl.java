package doc.doccenterserver.infrastructure.fileresource.message.repository;

import cn.hutool.json.JSONObject;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import doc.doccenterserver.domain.fileresource.document.model.DocConvertChangelogEntity;
import doc.doccenterserver.domain.fileresource.document.model.InterfaceAccessLogEntity;
import doc.doccenterserver.domain.fileresource.document.repository.InterfaceAccessLogRepository;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import doc.doccenterserver.domain.fileresource.file.repository.FileRepository;
import doc.doccenterserver.domain.fileresource.message.repository.DocConvertChangelogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobDetailRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.DocConvertChangelogEntityToDocConvertChangelogDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.file.utils.DocConvertUtil;
import doc.doccenterserver.infrastructure.fileresource.file.utils.MultipartFileUtil;
import doc.doccenterserver.infrastructure.fileresource.message.enums.*;
import doc.doccenterserver.infrastructure.fileresource.message.utils.IdentifierGenerator;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.*;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.rmi.ServerException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

/**
 * 文档转换
 */
@Slf4j
@Component("DocConvertChangelogDORepository")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DocConvertChangelogRepositoryImpl extends ServiceImpl<DocConvertChangelogMapper, DocConvertChangelogDO> implements DocConvertChangelogRepository {
    private final JSONObject parameterMap;
    private final JobDetailRepository jobDetailRepository;
    private final JobLogRepository jobLogRepository;
    private final AttachmentMapper attachmentMapper;
    private final PaperMapper paperMapper;
    private final DocPublishChangelogMapper docPublishChangelogMapper;
    private final FileRepository fileRepository;

    private final DocConvertChangelogMapper docConvertChangelogMapper;

    private final InterfaceAccessLogRepository interfaceAccessLogRepository;

    @Value("${xxl.job.admin.addresses}")
    private String adminAddresses;

    @Value("${xxl.job.executor.ip}")
    private String ip;

    @Override
    public void docConvert(){
        LambdaQueryWrapper<DocConvertChangelogDO> lqw=new LambdaQueryWrapper<>();
        lqw.lt(DocConvertChangelogDO::getRetryTimes,4);
        List<DocConvertChangelogDO> docConvertChangeloglist = getBaseMapper().selectList(lqw);
        for(DocConvertChangelogDO docConvertChangelog : docConvertChangeloglist){
            InterfaceAccessLogEntity interfaceAccessLogEntity = new InterfaceAccessLogEntity();
            interfaceAccessLogEntity.setLogId(IdentifierGenerator.createPrimaryKeySeq());
            interfaceAccessLogEntity.setParams(JSON.toJSONString(docConvertChangelog));
            interfaceAccessLogEntity.setCallTime(new Date());
            interfaceAccessLogEntity.setRequestUrl(adminAddresses);
            interfaceAccessLogEntity.setRequestIp(ip);
            interfaceAccessLogEntity.setAppname("文档中心扩展");
            try {
                this.updatePaper(docConvertChangelog);
                this.processData(docConvertChangelog);
                interfaceAccessLogEntity.setResultStatus("Y");
                interfaceAccessLogRepository.insert(interfaceAccessLogEntity);
            } catch (Exception e) {
                e.printStackTrace();
                docConvertChangelog.setRetryTimes(docConvertChangelog.getRetryTimes() + 1);
                jobLogRepository.updateError(docConvertChangelog.getLogId(), e.getMessage(), JobDetailLogStepIdEnum.STEP_ID_4.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_FAILD.getValue(), docConvertChangelog.getRetryTimes());
                interfaceAccessLogEntity.setResultStatus("N");
                interfaceAccessLogEntity.setFailReason(e.getMessage());
                interfaceAccessLogRepository.insert(interfaceAccessLogEntity);
            }
        }
    }
    /**
     * 修改文档转换中
     */
    public void updatePaper(DocConvertChangelogDO docConvertChangelog){
        String paperId = docConvertChangelog.getPaperId();
        String operation = docConvertChangelog.getOperation();
        if(OperationTypeEnum.CREATE.getValue().equals(operation)){
            PaperDO paper =new PaperDO();
            paper.setPaperId(paperId);
            paper.setPublicFlag(PublicFlagEnum.PAPER_PUBLIC_FLAG_CONVERTING.getValue());
            paperMapper.updateById(paper);
        }
    }
    public void processData(DocConvertChangelogDO docConvertChangelog) throws IOException {
        String paperId = docConvertChangelog.getPaperId();
        String operation = docConvertChangelog.getOperation();
        //根据应用ID、归档组织、应用文档编号，去统一消息暂存日志表是否存在
        if(OperationTypeEnum.CREATE.getValue().equals(operation) && Boolean.TRUE.equals(jobLogRepository.checkMqSaveExist(paperId))){
            jobLogRepository.clearDate4Repeated(docConvertChangelog.getLogId(),JobDetailLogStepIdEnum.STEP_ID_4.getValue());
            return;
        }
        this.fileConvert(paperId,operation);
        String logId = docConvertChangelog.getLogId();
        //更新任务日志从表《文档转换》任务状态为成功
        jobDetailRepository.updateStatus(logId,JobDetailLogStepIdEnum.STEP_ID_4.getValue(),JobDetailLogStepStatusEnum.STEP_STATUS_SUCCESS.getValue());
        //插入任务日志从表《文档发布》，任务状态为未完成
        jobDetailRepository.insert(logId,null,null,JobDetailLogStepIdEnum.STEP_ID_5.getValue(),JobDetailLogStepStatusEnum.STEP_STATUS_HANDLING.getValue());
        //删除文档转换任务changlog表
        this.removeById(logId);
        //写入文档发布任务change表
        DocPublishChangelogDO publishChangelog = new DocPublishChangelogDO(logId,new Date(),operation,paperId,0);
        docPublishChangelogMapper.insert(publishChangelog );
    }

    private void fileConvert(String paperId,String operation) throws IOException {
        PaperDO existsPaper =paperMapper.selectById(paperId);
        if(!PaperTypeEnum.FILE.getValue().equals(existsPaper.getPaperType())){
           return;
        }
        LambdaQueryWrapper<AttachmentDO> lwq=new LambdaQueryWrapper<>();
        lwq.eq(AttachmentDO::getPaperId,paperId);
        lwq.eq(AttachmentDO::getIsMain,IsMainEnum.Y.getValue());
        AttachmentDO oldAtt = attachmentMapper.selectList(lwq).stream().findAny().orElse(null);
        if(oldAtt==null){
            throw new ServerException("正文附件数据不存在");
        }
        if(OperationTypeEnum.CREATE.getValue().equals(operation)){
            //判断是否存在主附件，如果存在，转换PDF。判断PDF是否转换成功
            //转换并上传PDF文件
            String fileExt=oldAtt.getFileFormat();
            if(parameterMap.getStr("TRANSER_PDF_FORMAT").contains(fileExt) && StringUtils.isNoneBlank(oldAtt.getFileId())){
                LocalDate today = LocalDate.now();
                DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyMM");
                String timePath = today.format(fmt);
                String path="C:/appdata/content/data/Mon_" + timePath + "/attachments/convert/";
                FileEntity fileEntity= fileRepository.getFileById(oldAtt.getFileId());
                fileRepository.downloadFilePath(path,oldAtt.getFileName(),fileEntity.getBigdataFileId());
                File file = new File(path+oldAtt.getFileName());
                if(!file.exists() || !file.isFile()){
                    throw new ServerException("文件转换下载基线版文档中心失败:"+path+oldAtt.getFileName());
                }
                //转换pdf
                this.transferPDF(path+oldAtt.getFileName(), oldAtt);
                attachmentMapper.updateById(oldAtt);
            }
        }
    }
    /**
     * 正文文件转PDF
     */
    void transferPDF(String inputFilePath,AttachmentDO attachment) throws ServerException{
        File file = new File(inputFilePath);
        String attName=FilenameUtils.getBaseName(file.getName())+".pdf";
        String pdfFilePath = inputFilePath.substring(0, inputFilePath.lastIndexOf("/")+1)+ attName;
        //如果本身为PDF文件
        if(new File(pdfFilePath).exists()){
            attachment.setPdfFileMd5(attachment.getFileId());
            attachment.setPdfFileId(attachment.getFileId());
            FileUtils.deleteQuietly(new File(pdfFilePath));
            return;
        }
        //转换文件
        DocConvertUtil.convert(inputFilePath);
        //上传pdf文件并更新
        File pdfFile = new File(pdfFilePath);
        if (pdfFile.exists()) {
            try {
                String fileId=fileRepository.uploadFile(MultipartFileUtil.getMultipartFile(pdfFile));
                if(fileId!=null){
                    FileEntity fileDto = new FileEntity();
                    String ext = FilenameUtils.getExtension(attName);
                    fileDto.setFileId(IdentifierGenerator.createPrimaryKeySeq());
                    fileDto.setFileFormat(ext);
                    fileDto.setFileSize((double) pdfFile.length());
                    fileDto.setFilename(attName);
                    fileDto.setAppId(attachment.getAppId());
                    fileDto.setBigdataFileId(fileId);
                    fileDto.setCreateDate(new Date());
                    fileRepository.insertFile(fileDto);
                    attachment.setPdfFileMd5(fileDto.getFileId());
                    attachment.setPdfFileId(fileDto.getFileId());
                    FileUtils.deleteQuietly(new File(inputFilePath));
                    FileUtils.deleteQuietly(new File(pdfFilePath));
                }
            } catch (Exception e) {
                log.error("文件转换PDF文件上传基线版文档中心失败");
                throw new ServerException("文件转换PDF文件上传基线版文档中心失败");
            }
        } else {
            log.error("文件转换失败");
            throw new ServerException("文件转换失败");
        }
    }

    @Override
    public Integer insertDocConvertChangelog(DocConvertChangelogEntity docConvertChangelog) {
        return docConvertChangelogMapper.insert(DocConvertChangelogEntityToDocConvertChangelogDOConvertor.INSTANCE.entityToDO(docConvertChangelog));
    }

}
