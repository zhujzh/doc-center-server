package doc.doccenterserver.infrastructure.fileresource.es.search.elasticsearch;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.tobacco.mp.uc.client.api.org.dto.QueryParentOrgUnitsInOrgDTO;
import com.tobacco.mp.uc.kz.client.api.dto.ListPositionResponse;
import com.tobacco.mp.uc.kz.client.api.dto.ListUserPositionResponse;
import com.tobacco.mp.uc.kz.client.api.dto.OrgUnitResponse;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.document.model.GroupEntity;
import doc.doccenterserver.domain.fileresource.document.repository.GroupRepository;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcOrgUnitService;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcUserPositionService;
import doc.doccenterserver.infrastructure.fileresource.es.SearchDocument;
import doc.doccenterserver.infrastructure.fileresource.es.RawSearchResult;
import doc.doccenterserver.infrastructure.fileresource.es.SearchParam;
import doc.doccenterserver.infrastructure.fileresource.es.UpdateDocument;
import doc.doccenterserver.infrastructure.fileresource.es.bean.FilterParam;
import doc.doccenterserver.infrastructure.fileresource.es.bean.OrderParam;
import doc.doccenterserver.infrastructure.fileresource.es.bean.SearchDmlResult;
import doc.doccenterserver.infrastructure.fileresource.es.bean.SearchQuery;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.security.model.LoginUser;
import doc.doccenterserver.infrastructure.fileresource.security.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description: elastic search的搜索能力实现
 * @author: gj
 * @date 2023/4/11
 */
@Service
@Slf4j
public class ElasticSearchImpl implements SearchFacade {

    private static final String IDX_FIELD_DATE = "idx_date";
    private static final String IDX_FIELD_TITLE = "idx_title";
    private static final String IDX_FIELD_DESC = "idx_desc";
    private static final String IDX_FIELD_PARENT_ID = "idx_parent_id";
    private static final String IDX_FIELD_AUTH_VALUE = "idx_auth_value_string";
    private static final String PAPER_AUTH_PUBLIC = "public";

    @Autowired
    private ElasticInitializer elasticInitializer;
    @Autowired
    private TokenService tokenService;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    UcUserPositionService ucUserPositionService;

    @Autowired
    UcOrgUnitService ucOrgUnitService;

    public void testSearch() {
        SearchParam searchParam = new SearchParam();
        searchParam.setTitle("zjz测试文件");
//        searchParam.setContent("2011〕4号");
        String searchQueryString = searchParam.toString();
        RawSearchResult a = this.search(JSON.parseObject(searchQueryString, SearchQuery.class));
        System.out.println(JSONUtil.toJsonStr(a));
//        a.getList().stream().forEach(d->{
//            this.deleteDoc(d.getIdx_id());
//        });
    }


    @Override
    public RawSearchResult search(SearchQuery searchQuery) {
//        log.info("searchFacade.search + 开始查询 === ");
        RawSearchResult searchResult = new RawSearchResult();
        RestHighLevelClient highLevelClient = getHighLevelClient();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        //general filter
        for (FilterParam filterParam : searchQuery.getFilter()) {
            String filterName = filterParam.getName();
            String filterValueTmp = filterParam.getValue();
            String filterValue = filterValueTmp.replaceAll("\"", "");
            if (!StringUtils.isEmpty(filterName) && !StringUtils.isEmpty(filterValue)) {
                if (IDX_FIELD_DATE.equals(filterName)) { //date filter
                    String[] dateArr = filterValue.split(",");
                    SimpleDateFormat sdfs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        Date startDate = sdfs.parse(dateArr[0]);
                        Date endDate = sdfs.parse(dateArr[1]);
                        builder.must(QueryBuilders.rangeQuery(filterName).from(startDate.getTime()).to(endDate.getTime()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (IDX_FIELD_TITLE.equals(filterName) || IDX_FIELD_DESC.equals(filterName)) { //wild card filter
                    if (!isContainsChinese(filterValue)) {
                        builder.must(QueryBuilders.wildcardQuery(filterName, "*" + filterValue + "*"));
                    } else {
                        builder.must(QueryBuilders.matchQuery(filterName, filterValue));
                    }
                } else if (IDX_FIELD_PARENT_ID.equals(filterName)) {
                    filterValue = "p_" + filterValue;
                    builder.must(QueryBuilders.wildcardQuery(filterName, filterValue + "*"));
                } else {
                    builder.must(QueryBuilders.termQuery(filterName, filterValue));
                }
            }
        }
        //auth filter
        LoginUser loginUser = tokenService.getLoginUserV2();
        if (null == loginUser) throw new RuntimeException("get current login user failed, loginUser is null");
        UserModel curUser = tokenService.getLoginUserV2().getUserModel();
        if (ObjectUtils.isEmpty(curUser)) throw new RuntimeException("get user model failed, curUser is null");
        if (!ConstantSys.CZ_ADMIN.equals(curUser.getUserid())) {
            List<String> authList = getUserPaperAuth(curUser);
            if (CollectionUtil.isNotEmpty(authList)) {
                builder.must(QueryBuilders.termsQuery(IDX_FIELD_AUTH_VALUE, authList));
            }
        }
        searchSourceBuilder.query(builder);
        //order by
        if (searchQuery.getSort() != null && searchQuery.getSort().size() > 0) {
            for (OrderParam orderParam : searchQuery.getSort()) {
                String name = orderParam.getName();
                String order = orderParam.getOrder();
                if (null != name && null != order) {
                    searchSourceBuilder.sort(new FieldSortBuilder(name).
                            order("asc".equals(order) ? SortOrder.ASC : SortOrder.DESC));
                }
            }
        }
        //Request Highlighting
        String filterName = IDX_FIELD_DESC;
        for (FilterParam filterParam : searchQuery.getFilter()) {
            filterName = filterParam.getName();
            if (null != filterName) {
                if (IDX_FIELD_TITLE.equals(filterName) || IDX_FIELD_DESC.equals(filterName)) {
                    HighlightBuilder highlightBuilder = new HighlightBuilder();
                    HighlightBuilder.Field highlightUser = new HighlightBuilder.Field(filterName);
                    highlightBuilder.requireFieldMatch(false);
                    // Create a field highlighter for
                    highlightBuilder.preTags("<span style='color: red; font-style:italic'>");
                    highlightBuilder.postTags("</span>");
                    highlightBuilder.field(highlightUser);
                    searchSourceBuilder.highlighter(highlightBuilder);
                }
            }
        }
        searchSourceBuilder.from(searchQuery.getStart());
        searchSourceBuilder.size(searchQuery.getRows());
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        try {
//            log.info("Search requsearchDocuments = {ArrayList@18121}  size = 20est is {}.", JSONUtil.toJsonStr(searchRequest));
            SearchResponse searchResponse = highLevelClient.search(searchRequest, RequestOptions.DEFAULT);
//            log.info("Search response is {}.", JSONUtil.toJsonStr(searchResponse));
            List<SearchDocument> searchDocuments = parseSearchResponse(searchResponse, filterName);
            searchResult.setSeconds((int) searchResponse.getTook().getSeconds());
            searchResult.setTotalNum(searchResponse.getHits().getTotalHits().value);
            searchResult.setList(searchDocuments);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        log.info("searchFacade.search + 结束查询 === ");
        return searchResult;
    }


    @Override
    public SearchDmlResult create(SearchDocument searchDocument) {
        try {
            RestHighLevelClient highLevelClient = getHighLevelClient();
            IndexRequest indexRequest = new IndexRequest(indexNameGenerator(searchDocument)).id(searchDocument.getIdx_id());
            String jsonString = JSON.toJSONString(searchDocument);
            indexRequest.source(jsonString, XContentType.JSON);
            indexRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
            IndexResponse indexResponse = highLevelClient.index(indexRequest, RequestOptions.DEFAULT);
//            log.info("elastic search create response is {}.", indexResponse.toString());
            return SearchDmlResult.dmlSuccess();
        } catch (Exception e) {
            log.error("elastic search create failed, exception occurred.", e);
            return SearchDmlResult.dmlFailed(e.getMessage());
        }
    }

    @Override
    public SearchDmlResult delete(String index) {
        try {
            Assert.notNull(index, "索引不能为空");
            RestHighLevelClient highLevelClient = getHighLevelClient();
            DeleteRequest request = new DeleteRequest(index);
            request.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
            DeleteResponse indexResponse = highLevelClient.delete(request, RequestOptions.DEFAULT);
//            log.info("elastic search delete response is {}.", indexResponse.toString());
            return SearchDmlResult.dmlSuccess();
        } catch (Exception e) {
            log.error("elastic search delete failed, exception occurred.", e);
            return SearchDmlResult.dmlFailed(e.getMessage());
        }
    }

    @Override
    public SearchDmlResult deleteDoc(String ids) {
        try {
            Assert.notNull(ids, "文档id不能为空");
            RestHighLevelClient highLevelClient = getHighLevelClient();
            List idList = Arrays.asList(ids.split(","));
            idList.stream().forEach(item -> {
                try {
                    DeleteRequest deleteRequest = new DeleteRequest(item.toString().toLowerCase(), item.toString());
                    deleteRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
                    DeleteResponse deleteResponse = highLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
//                    System.out.println("response id: " + deleteResponse.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            return SearchDmlResult.dmlSuccess();
        } catch (Exception e) {
            log.error("elastic search delete failed, exception occurred.", e);
            return SearchDmlResult.dmlFailed(e.getMessage());
        }
    }

    @Override
    public SearchDmlResult updateDoc(UpdateDocument updateDocument) {
        try {
            RestHighLevelClient highLevelClient = getHighLevelClient();
            Map<String, Object> map = JSON.parseObject(JSON.toJSONString(updateDocument), Map.class);
            XContentBuilder builder = XContentFactory.jsonBuilder();
            builder.startObject();
            { // update information
                for (Map.Entry entry : map.entrySet()) {
                    String mapKey = (String) entry.getKey();
                    String mapValue = (String) entry.getValue();
                    if (null != mapValue && !"idx_id".equals(mapKey)) {
                        builder.field(mapKey, mapValue);
                    }
                }
            }
            builder.endObject();
            String docId = updateDocument.getIdx_id();
            UpdateRequest request = new UpdateRequest(docId.toLowerCase(), docId).doc(builder);
            request.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
            UpdateResponse updateResponse = highLevelClient.update(request, RequestOptions.DEFAULT);

//            log.info("Update response is {}.", updateResponse.toString());
            return SearchDmlResult.dmlSuccess();
        } catch (Exception e) {
            log.error("elastic search update failed, exception occurred.", e);
            return SearchDmlResult.dmlFailed(e.getMessage());
        }
    }


    //generate a new index name, use businessId as index by default
    //index name  must be lowercase
    public String indexNameGenerator(SearchDocument searchDocument) {
        return searchDocument.getIdx_id().toLowerCase();
    }

    /**
     * get elastic high level client
     *
     * @return
     */
    private RestHighLevelClient getHighLevelClient() {
        return elasticInitializer.getHighLevelClient();
    }


    //parse searchDocument from elastic search hits object
    public List<SearchDocument> parseSearchResponse(SearchResponse searchResponse, String filterName) {
        List<SearchDocument> parsedDocuments = new ArrayList<>();
        SearchHits searchHits = searchResponse.getHits();
        SearchHit[] hits = searchHits.getHits();
        for (SearchHit hit : hits) {
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
//            log.info("highlightFields is == " + JSONUtil.toJsonStr(highlightFields));
            HighlightField highlightField = highlightFields.get(filterName);
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            if (highlightField != null) {
                Text[] fragments = highlightField.fragments();
                StringBuilder newTitle = new StringBuilder();
                for (Text text : fragments) {
                    newTitle.append(text);
                }
                sourceAsMap.put(filterName, newTitle.toString());
            }
            String jsonString = JSON.toJSONString(sourceAsMap);
            SearchDocument dc = JSON.parseObject(jsonString, SearchDocument.class);
            if (null != dc) {
                parsedDocuments.add(dc);
            }
        }
        return parsedDocuments;
    }

    //check is some chinese is contained in str
    //true : contain
    public static boolean isContainsChinese(String str) {
        String regEx = "[\u4e00-\u9fa5]";
        Pattern pat = Pattern.compile(regEx);
        Matcher matcher = pat.matcher(str);
        boolean flg = false;
        if (matcher.find()) {
            flg = true;
        }
        return flg;
    }


    //build elastic search auth data
    public List<String> getUserPaperAuth(UserModel user) {
        List<String> authList = Lists.newArrayList();
        authList.add(PAPER_AUTH_PUBLIC);//公开
        authList.add("u_" + user.getUserid());//当前用户
        authList.add("g_" + user.getCompanyId());//当前用户所在公司id
        authList.add("g_" + user.getDeptId());//当前用户所在一级部门id
        authList.add("g_" + user.getOrgId());//当前用户所在部门id
        //get all dept info by current user
        Collection<ListUserPositionResponse> extraDeptList = ucUserPositionService.queryUserPositionByLoginId(user.getUserid());
        if (CollectionUtil.isNotEmpty(extraDeptList)) {
            for (ListUserPositionResponse userPosition : extraDeptList) {
                ListPositionResponse position = userPosition.getPosition();
                String unitId = position.getUnitId(); //this unitId is actually org bizCode
                if (StringUtils.isEmpty(unitId)) continue;
                OrgUnitResponse orgUnitResponse = ucOrgUnitService.queryUcOrgNodeByBizCode(unitId);
                Collection<QueryParentOrgUnitsInOrgDTO> orgList = ucOrgUnitService.queryOrgParentOrgUnits(orgUnitResponse.getId());
                if (CollectionUtil.isNotEmpty(orgList)) {
                    for (QueryParentOrgUnitsInOrgDTO orgDTO : orgList) {
                        //authList.add("g_" + orgDTO.getOrgUnitId());
                        authList.add("g_" + orgDTO.getBizCode());
                    }
                } else {
                    authList.add("g_" + unitId);
                }
            }
        }
        //custom group info
        List<GroupEntity> groupList = groupRepository.queryGroups(user.getUserid());
        if (CollectionUtil.isNotEmpty(groupList)) {
            for (GroupEntity group : groupList) {
                String groupId = group.getGroupId();
                if (StringUtils.isEmpty(groupId)) continue;
                authList.add("a_" + groupId);
            }
        }
        //deduplicate
        List<String> listWithoutDup = Lists.newArrayList(new HashSet<String>(authList));
        return listWithoutDup;
    }


}
