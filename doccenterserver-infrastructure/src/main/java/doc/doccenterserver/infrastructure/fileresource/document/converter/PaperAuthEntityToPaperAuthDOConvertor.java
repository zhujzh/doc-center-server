package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.PaperAuthEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PaperAuthDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface PaperAuthEntityToPaperAuthDOConvertor {

    PaperAuthEntityToPaperAuthDOConvertor INSTANCE = Mappers.getMapper(PaperAuthEntityToPaperAuthDOConvertor.class);

    @Mappings({})
    PaperAuthDO entityToDO(PaperAuthEntity paperAuthEntity);
    @Mappings({})
    PaperAuthEntity dOToEntity(PaperAuthDO paperAuthDO);
    @Mappings({})
    List<PaperAuthEntity> dOToEntityList(List<PaperAuthDO> paperAuthDOList);
    @Mappings({})
    List<PaperAuthDO> entityToDOList(List<PaperAuthEntity> paperAuthEntityList);
    
}
