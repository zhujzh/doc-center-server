package doc.doccenterserver.infrastructure.fileresource.message.utils.doc;

import cn.hutool.http.HttpUtil;
import cn.hutool.http.webservice.SoapClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.InputSource;
import sun.misc.BASE64Decoder;

import javax.net.ssl.SSLContext;
import javax.xml.namespace.QName;
import java.io.*;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

public class ECMWSTool_New {
    public ECMWSTool_New() {
    }

    public long processBase64(String code, String filePath) throws Exception {
        FileOutputStream out = null;
        long fileSize = 0L;
        BASE64Decoder decoder = new BASE64Decoder();

        try {
            byte[] bytes = decoder.decodeBuffer(code);
            fileSize = (long)bytes.length;
            out = new FileOutputStream(filePath);
            out.write(bytes);
            out.close();
        } finally {
            if (out != null) {
                out.close();
            }

        }

        return fileSize;
    }

    public long CZSTDD_receiveToPath(String att_id, String att_type, String filePath, String cz_stddfileservice_url) throws Exception {
        long fileSize = 0L;
        try {
            HashMap<String, Object> paramMap = new HashMap<>();
            paramMap.put("fileId", att_id);
            paramMap.put("type", att_type);
            paramMap.put("appcode", "CZSTDD");
            String result = HttpUtil.post(cz_stddfileservice_url,paramMap);
            if (result.length() > 2) {
                StringReader xmlReader = null;
                try {
                    xmlReader = new StringReader(result);
                    InputSource xmlSource = new InputSource(xmlReader);
                    SAXBuilder builder = new SAXBuilder();
                    Document doc = builder.build(xmlSource);
                    Element filecontentel = doc.getRootElement().getChild("filecontent");
                    String filecontent = filecontentel.getText();
                    if (filecontentel.getText().length() > 0) {
                        fileSize = this.processBase64(filecontent, filePath);
                    }
                } finally {
                    try {
                        if (xmlReader != null) {
                            xmlReader.close();
                        }
                    } catch (Exception var21) {
                        var21.printStackTrace();
                    }

                }
            }
        } catch (Exception var23) {
            var23.printStackTrace();
        }

        return fileSize;
    }

    public long CZCOP_receiveToPath(String att_id, String att_type, String filePath, String cz_copfileservice_url) throws Exception {
        long fileSize = 0L;

        try {
            HashMap<String, Object> paramMap = new HashMap<>();
            paramMap.put("fileId", att_id);
            paramMap.put("type", att_type);
            paramMap.put("appcode", "CZCOP");
            String result = HttpUtil.post(cz_copfileservice_url,paramMap);
            if (result.length() > 2) {
                StringReader xmlReader = null;
                try {
                    xmlReader = new StringReader(result);
                    InputSource xmlSource = new InputSource(xmlReader);
                    SAXBuilder builder = new SAXBuilder();
                    Document doc = builder.build(xmlSource);
                    Element filecontentel = doc.getRootElement().getChild("filecontent");
                    String filecontent = filecontentel.getText();
                    if (filecontentel.getText().length() > 0) {
                        fileSize = this.processBase64(filecontent, filePath);
                    }
                } finally {
                    try {
                        if (xmlReader != null) {
                            xmlReader.close();
                        }
                    } catch (Exception var21) {
                        var21.printStackTrace();
                    }

                }
            }
        } catch (Exception var23) {
            var23.printStackTrace();
        }

        return fileSize;
    }

    public long CZOA_receiveToPath(String att_id, String att_type, String filePath, String cz_oafileservice_url) throws Exception {
        long fileSize = 0L;

        try {
            String result = HttpUtil.get(cz_oafileservice_url.replace("{fileId}", att_id).replace("{type}", att_type));
            if (result.length() > 2) {
                StringReader xmlReader = null;
                try {
                    xmlReader = new StringReader(result);
                    InputSource xmlSource = new InputSource(xmlReader);
                    SAXBuilder builder = new SAXBuilder();
                    Document doc = builder.build(xmlSource);
                    Element filecontentel = doc.getRootElement().getChild("filecontent");
                    String filecontent = filecontentel.getText();
                    if (filecontentel.getText().length() > 0) {
                        fileSize = this.processBase64(filecontent, filePath);
                    }
                } finally {
                    try {
                        if (xmlReader != null) {
                            xmlReader.close();
                        }
                    } catch (Exception var21) {
                        var21.printStackTrace();
                    }

                }
            }
        } catch (Exception var23) {
            var23.printStackTrace();
        }

        return fileSize;
    }

    public long OA_receiveToPath(String att_id, String att_type, String filePath, String oafileservice_url) {
        long fileSize = 0L;

        try {
            OaReceiveForDocImplSoapBindingStub stub = new OaReceiveForDocImplSoapBindingStub(new URL(oafileservice_url), null);
            String filecontent = stub.receiveForDoc(att_id, att_type);
            if (filecontent.length() > 0) {
                fileSize = this.processBase64(filecontent, filePath);
            }
        } catch (Exception var9) {
            var9.printStackTrace();
        }

        return fileSize;
    }

    public static void main(String[] args) throws Exception {

        ECMWSTool_New ecmwsToolNew=new ECMWSTool_New();
        ecmwsToolNew.getAttFile("OA","459d7065-15925515a49-8a52a0d51510de2bef6158d80746c8c4",
                "1",
                "e:\\appdata\\content\\data\\Mon_2304\\attachments\\cmsUploadFile\\关于召开公司领导班子2016年度述职和民主评议会议的通知.doc",
                "http://oa.hngytobacco.com/rhasa/services/OaReceiveForDocImpl?wsdl");

        ecmwsToolNew.getAttFile("ZYOA","17dfb6cf02d418eae0629e74655a6db1",
                "1",
                "e:\\appdata\\content\\data\\Mon_2304\\attachments\\cmsUploadFile\\湖南中烟工业有限责任公司长沙卷烟厂关于2021年度资产处置预案执行情况的报告.doc",
                "http://10.158.3.119:9090/oa/sys/attdownload/sys_attdownload_main/sysAttdownloadMain.do?method=download&fdId=");

        ecmwsToolNew.getAttFile("ZYSTDD","84ca2f81-ff3b-40e0-8389-e04c0ee44549",
                "1",
                "e:\\appdata\\content\\data\\Mon_2304\\attachments\\cmsUploadFile\\ZH08档案管理办法230328.doc",
                "http://10.158.133.206:9080/stdd/attachment/downloadStddIdAttachment.do?stddid=");
    }


    public long CMS_receiveToPath(String att_id, String filePath, String phpcmsfileservice_url) {
        long fileSize = 0L;

        try {
            CreatewsdlBindingStub stub = new CreatewsdlBindingStub(new URL(phpcmsfileservice_url), null);
            String xml = stub.create(att_id);
            xml = new String(xml.getBytes("ISO-8859-1"), "UTF-8");
            String filecontent = "";
            ByteArrayInputStream is = null;

            try {
                SAXBuilder builder = new SAXBuilder();
                is = new ByteArrayInputStream(xml.getBytes("UTF-8"));
                xml = null;
                Document document = builder.build(is);
                Element node_fdata = ConfigUtil.findSingleNode(document.getRootElement(), "/root/fdata");
                is = null;
                builder = null;
                if (node_fdata != null) {
                    filecontent = node_fdata.getTextTrim();
                }

                document = null;
                node_fdata = null;
                if (filecontent.length() > 0) {
                    fileSize = this.processBase64(filecontent, filePath);
                }
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (Exception var19) {
                    var19.printStackTrace();
                }

            }
        } catch (Exception var21) {
            var21.printStackTrace();
        }

        return fileSize;
    }

    public long ECM_receiveToPath(String att_id, String filePath, String jt_ecmfileservice_url) throws Exception {
        long fileSize = 0L;
        String[] atts = att_id.split("_");
        AttDownloadPortBindingStub stub = new AttDownloadPortBindingStub(new URL(jt_ecmfileservice_url), null);
        String filecontent = stub.getAttachmentById(atts[1], atts[0], (String)null);
        if (filecontent.length() > 0) {
            fileSize = this.processBase64(filecontent, filePath);
        }

        return fileSize;
    }

    public long ZYSTDD_receiveToPath(String att_id, String filePath, String zy_stddfileservice_url) throws Exception {
        long fileSize = 0L;

        try {
            BufferedInputStream bis = null;
            BufferedOutputStream bos = null;
            CloseableHttpClient httpClient = createSSLInsecureClient();
            HttpGet getMethod = new HttpGet(zy_stddfileservice_url + att_id);
            HttpResponse httpresponse = httpClient.execute(getMethod);
            File file = new File(filePath);
            if (httpresponse.getStatusLine().getStatusCode() == 200) {
                bis = new BufferedInputStream(httpresponse.getEntity().getContent());
                bos = new BufferedOutputStream(new FileOutputStream(file));
                int len = 2048;
                byte[] b = new byte[len];

                while((len = bis.read(b)) != -1) {
                    bos.write(b, 0, len);
                }

                bos.flush();
                bis.close();
                fileSize = file.length();
            }
        } catch (Exception var14) {
            var14.printStackTrace();
        }

        return fileSize;
    }

    static CloseableHttpClient createSSLInsecureClient() {
        try {
            SSLContext sslContext = (new SSLContextBuilder()).loadTrustMaterial((KeyStore)null, new TrustStrategy() {
                public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                    return true;
                }
            }).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext);
            return HttpClients.custom().setSSLSocketFactory(sslsf).build();
        } catch (Exception var2) {
            var2.printStackTrace();
            return HttpClients.createDefault();
        }
    }

    public long getAttFile(String att_from, String att_id, String att_type, String filePath, String url) throws Exception {
        File file = new File(filePath);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        if ("OA".equals(att_from)) {
            //oa
            return this.OA_receiveToPath(att_id, att_type, filePath, url);
        } else if ("CZOA".equals(att_from)) {
            return this.CZOA_receiveToPath(att_id, att_type, filePath, url);
        } else if ("CZCOP".equals(att_from)) {
            return this.CZCOP_receiveToPath(att_id, att_type, filePath, url);
        } else if (!"CZSTDD".equals(att_from) && !"CZECM".equals(att_from)) {
            if ("ZYSTDD".equals(att_from)) {
                return this.ZYSTDD_receiveToPath(att_id, filePath, url);
            } else if ("CMS".equals(att_from)) {
                //cms
                return this.CMS_receiveToPath(att_id, filePath, url);
            } else {
                return !"WZECM".equals(att_from) && !"SPECM".equals(att_from) && !"ZYECM".equals(att_from) ? this.ZYSTDD_receiveToPath(att_id, filePath, url) : this.ECM_receiveToPath(att_id, filePath, url);
            }
        } else {
            return this.CZSTDD_receiveToPath(att_id, att_type, filePath, url);
        }
    }

}

