package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统配置参数
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_pub_parameter`")
@NoArgsConstructor
public class ParameterDO {

    @Field(name = "主键")
    @TableId(type = IdType.INPUT, value = "p_id")
    private String pId;

    @Field(name = "定义标识符")
    @TableField("p_key")
    private String pKey;

    @Field(name = "值")
    @TableField("p_value")
    private String pValue;

    @Field(name = "描述")
    @TableField("description")
    private String description;
}
