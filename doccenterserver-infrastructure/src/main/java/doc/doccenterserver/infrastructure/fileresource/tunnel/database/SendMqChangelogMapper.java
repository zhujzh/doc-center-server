package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.SendMqChangelogDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 发送统一消息任务表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface SendMqChangelogMapper extends BaseMapper<SendMqChangelogDO> {
}
