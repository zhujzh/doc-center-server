package doc.doccenterserver.infrastructure.fileresource.system.converter;

import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.UserDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;


/**
 * DictToDictDOConverter
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Mapper
public interface UserToUserDOConverter {

    UserToUserDOConverter INSTANCE = Mappers.getMapper(UserToUserDOConverter.class);

    @Mappings({  })
    UserDO userToUserDO(UserEntity user);

    @Mappings({})
    UserEntity userDOToUser(UserDO userDo);

}
