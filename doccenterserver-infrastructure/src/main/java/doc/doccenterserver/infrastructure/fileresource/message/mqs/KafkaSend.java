package doc.doccenterserver.infrastructure.fileresource.message.mqs;

import cn.hutool.core.util.XmlUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;


/**
 * @author wangxp
 * @date 2023/3/13 15:05
 */
@Slf4j
@Component()
@RequiredArgsConstructor
public class KafkaSend {
    private final KafkaTemplate<String, String> kafkaTemplate;
    /**
     * 下发消息
     */
    public void send(String topicName, String msg) {
        log.info("kafka待发送内容为：" + msg);
        if (JSONUtil.isTypeJSON(msg)) {
            JSONObject jsonObject= JSONUtil.parseObj(msg);
            msg=addMsgCenterHead(jsonObject,"JSON");
        } else if (isXML(msg)) {
            JSONObject jsonObject= JSONUtil.xmlToJson(msg);
            msg=addMsgCenterHead(jsonObject,"XML");
        }
        kafkaTemplate.send(topicName, msg);
        log.info("kafka成功发送消息给：" + topicName + "，内容为：" + msg);
    }

    public void sendTest(String topicName, String msg) {
        kafkaTemplate.send(topicName, msg);
        log.info("kafka成功发送消息给：" + topicName + "，内容为：" + msg);
    }
    public static String addMsgCenterHead(JSONObject jsonObject,String format){
        JSONObject root =jsonObject.isNull("root")?new JSONObject():jsonObject.getJSONObject("root");
        JSONObject head =root.isNull("head")?new JSONObject():root.getJSONObject("head");
        head.set("senderApp","dc-kz");
        head.set("senderAppKey","bb9a0fac-ffa5-4337-866b-93b24221d94e");
        head.set("msgType","application");
        head.set("accept","uc-kz-zb");
        head.set("format",format);
        root.set("head",head);
        jsonObject.set("root",root);
        return jsonObject.toString();
    }
    public static boolean isXML(String str) {
        try {
            XmlUtil.parseXml(str);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }


}



