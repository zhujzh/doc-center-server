package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import doc.doccenterserver.domain.fileresource.file.model.TPanObjEntity;
import doc.doccenterserver.domain.fileresource.pan.repository.TPanObjRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.TPanObjConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.TPanObjMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.TPanObjDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("TPanObjDORepository")
public class TPanObjRepositoryImpl implements TPanObjRepository {

    @Resource
    private TPanObjMapper tPanObjMapper;

    @Override
    public Double getTpanObjSize() {
        return tPanObjMapper.getTpanObjSize();
    }

    @Override
    public TPanObjEntity findTPanObjById(String objId) {
        return tPanObjMapper.findTPanObjById(objId);
    }

    @Override
    public List<TPanObjEntity> findChildObjsByParentId(TPanObjEntity temp) {
        LambdaQueryWrapper<TPanObjDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TPanObjDO::getParentObjId, temp.getParentObjId());
        List<TPanObjDO> tPanObjDOList = tPanObjMapper.selectList(wrapper);
        return TPanObjConvertor.INSTANCE.dOToEntity(tPanObjDOList);
    }
}
