package doc.doccenterserver.infrastructure.fileresource.message.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/** 
 * 采用MD5加密解密 
 * @author 李锋 
 * @datetime 2011-10-13 
 */  
public class MD5Util {  
	/*** 
     * 根据文件生成MD5
     */  
    public static String getMd5ByFile(File file)  {  
        String md5 = null;  
        FileInputStream fis = null;
        
        
        if(FileUtils.sizeOf(file)>1024 * 100 * 1024){
       	
        	try {
        		fis =new FileInputStream(file);
	        	byte[] buffer = new byte[8192];
	        	MessageDigest md5MD = MessageDigest.getInstance("MD5");
	            int len;
	            while((len = fis.read(buffer)) != -1){
	            	md5MD.update(buffer, 0, len);
	            }
	            IOUtils.closeQuietly(fis); 
	            md5 =DigestUtils.md5Hex(md5MD.digest());
        	 } catch (IOException e) {
     			// TODO Auto-generated catch block NoSuchAlgorithmException
     		}  catch (NoSuchAlgorithmException e) {
     			e.printStackTrace();
     		}
        }else{
       	 try {
           	 fis =new FileInputStream(file);
   			 md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));
   		} catch (Exception e) {
   			// TODO Auto-generated catch block
   		} 
            finally{
           	 IOUtils.closeQuietly(fis);
            } 
        	
        }
	    return md5;  
    }
  
    /*** 
     * MD5加码 生成32位md5码 
     */  
    public static String string2MD5(String inStr){  
        MessageDigest md5 = null;  
        try{  
            md5 = MessageDigest.getInstance("MD5");  
        }catch (Exception e){  
            System.out.println(e.toString());  
            e.printStackTrace();  
            return "";  
        }  
        char[] charArray = inStr.toCharArray();  
        byte[] byteArray = new byte[charArray.length];  
  
        for (int i = 0; i < charArray.length; i++) {
            byteArray[i] = (byte) charArray[i];
        }
        byte[] md5Bytes = md5.digest(byteArray);  
        StringBuffer hexValue = new StringBuffer();
        for (byte md5Byte : md5Bytes) {
            int val = ((int) md5Byte) & 0xff;
            if (val < 16) {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }  
        return hexValue.toString();  
  
    }  
  
    /** 
     * 加密解密算法 执行一次加密，两次解密 
     */   
    public static String convertMD5(String inStr){  
  
        char[] a = inStr.toCharArray();  
        for (int i = 0; i < a.length; i++){  
            a[i] = (char) (a[i] ^ 't');  
        }  
        String s = new String(a);  
        return s;  
  
    }  
  
    // 测试主函数   
    public static void main(String[] args) {
        String s = new String("E@E@AG");  
        System.out.println("原始：" + s);  
//        System.out.println("MD5后：" + string2MD5(s));  
        System.out.println("加密的：" + convertMD5(s));  
//        System.out.println("解密的：" + convertMD5(convertMD5(s)));  
  
    }  
} 