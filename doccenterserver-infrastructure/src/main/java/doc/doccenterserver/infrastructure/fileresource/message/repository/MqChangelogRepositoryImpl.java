package doc.doccenterserver.infrastructure.fileresource.message.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import doc.doccenterserver.domain.fileresource.message.repository.JobDetailRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.MqChangelogRepository;
import doc.doccenterserver.infrastructure.fileresource.message.config.MessageXmlConfig;
import doc.doccenterserver.infrastructure.fileresource.message.enums.JobDetailLogStepIdEnum;
import doc.doccenterserver.infrastructure.fileresource.message.enums.JobDetailLogStepStatusEnum;
import doc.doccenterserver.infrastructure.fileresource.message.enums.OperationTypeEnum;
import doc.doccenterserver.infrastructure.fileresource.message.utils.JdomXmlUtil;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.ArchiveChangelogMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.MqChangelogMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ArchiveChangelogDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.MqChangelogDO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jdom2.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 消息处理
 * repository实现类
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Slf4j
@Component("FILEMqChangelogDORepository")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MqChangelogRepositoryImpl extends ServiceImpl<MqChangelogMapper, MqChangelogDO> implements MqChangelogRepository {
    private final ArchiveChangelogMapper archiveChangelogMapper;
    private final JobDetailRepository jobDetailRepository;
    private final JobLogRepository jobLogRepository;

    @Override
    public void msgHandle() {
        LambdaQueryWrapper<MqChangelogDO> lqw = new LambdaQueryWrapper<>();
        lqw.lt(MqChangelogDO::getRetryTimes, 4);
        List<MqChangelogDO> mqchangeloglist = getBaseMapper().selectList(lqw);
        for (MqChangelogDO mqChangelog : mqchangeloglist) {
            try {
                this.processData(mqChangelog);
            } catch (Exception e) {
                mqChangelog.setRetryTimes(mqChangelog.getRetryTimes() + 1);
                jobLogRepository.updateError(mqChangelog.getLogId(), e.getMessage(), JobDetailLogStepIdEnum.STEP_ID_2.getValue(),
                        JobDetailLogStepStatusEnum.STEP_STATUS_FAILD.getValue(), mqChangelog.getRetryTimes());
            }
        }
    }
    /**
     * 消息处理
     */
    public void processData(MqChangelogDO mqChangelog) throws Exception {
        String content = mqChangelog.getContent();
        String archiveOrgCode = mqChangelog.getArchiveOrgCode();
        Document doc = JdomXmlUtil.xml2Document(content);
        String sourceAppId = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SOURCE_APP_ID);
        String sourcePaperId = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SOURCE_PAPER_ID);
        String operation = mqChangelog.getOperation();

        if ("CMS".equals(sourceAppId)) {
            sourcePaperId = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.CMS_SOURCE_PAPER_ID);
        }
        //根据应用ID、归档组织、应用文档编号，去统一消息暂存日志表是否存在
        if (OperationTypeEnum.CREATE.getValue().equals(operation) && Boolean.TRUE.equals(jobLogRepository.checkMqSaveExist(sourceAppId, sourcePaperId, archiveOrgCode))) {
            jobLogRepository.clearDate4Repeated(mqChangelog.getLogId(), JobDetailLogStepIdEnum.STEP_ID_2.getValue());
            return;
        }
        String logId = mqChangelog.getLogId();
        //写入消息归档change表
        ArchiveChangelogDO archiveChangelog = new ArchiveChangelogDO(logId, content, new Date(), operation, 0, sourceAppId, sourcePaperId);
        archiveChangelogMapper.insert(archiveChangelog);
        //删除消息入库changelog表
        this.removeById(logId);
        //更新任务日志从表《消息处理》任务状态为完成
        jobDetailRepository.updateStatus(logId, JobDetailLogStepIdEnum.STEP_ID_2.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_SUCCESS.getValue());
        //插入任务日志从表《消息归档》，任务状态为未完成
        jobDetailRepository.insert(logId, null, null, JobDetailLogStepIdEnum.STEP_ID_3.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_HANDLING.getValue());
    }

}
