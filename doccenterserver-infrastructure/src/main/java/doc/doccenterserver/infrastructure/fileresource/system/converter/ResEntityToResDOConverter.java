package doc.doccenterserver.infrastructure.fileresource.system.converter;

import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ResDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 9:29
 */
@Mapper
public interface ResEntityToResDOConverter {
    ResEntityToResDOConverter INSTANCE = Mappers.getMapper(ResEntityToResDOConverter.class);
    
    @Mappings({})
    ResEntity dOToEntity(ResDO resDO);
    @Mappings({})
    List<ResEntity> dOToEntityList(List<ResDO> resDOList);
    @Mappings({})
    ResDO entityToDO(ResEntity resEntity);
}
