package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ArchiveChangelogDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 消息归档Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface ArchiveChangelogMapper extends BaseMapper<ArchiveChangelogDO> {
}
