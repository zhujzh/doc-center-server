package doc.doccenterserver.infrastructure.fileresource.file.repository;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.document.model.PaperAuthEntity;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.document.repository.AttachmentRepository;
import doc.doccenterserver.domain.fileresource.document.repository.ChannelRepository;
import doc.doccenterserver.domain.fileresource.document.repository.PaperAuthRepository;
import doc.doccenterserver.domain.fileresource.document.repository.PaperRepository;
import doc.doccenterserver.domain.fileresource.document.repository.UserRepository;
import doc.doccenterserver.domain.fileresource.file.model.HomeEntity;
import doc.doccenterserver.domain.fileresource.file.model.SearchResultPack;
import doc.doccenterserver.domain.fileresource.file.model.TPanCollectFolderEntity;
import doc.doccenterserver.domain.fileresource.file.repository.HomeRepository;
import doc.doccenterserver.domain.fileresource.file.repository.TPanCollectFolderRepository;
import doc.doccenterserver.domain.fileresource.system.model.DictEntry;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.system.repository.DictRepository;
import doc.doccenterserver.infrastructure.fileresource.es.RawSearchResult;
import doc.doccenterserver.infrastructure.fileresource.es.SearchDocument;
import doc.doccenterserver.infrastructure.fileresource.es.SearchParam;
import doc.doccenterserver.infrastructure.fileresource.es.bean.SearchQuery;
import doc.doccenterserver.infrastructure.fileresource.es.search.elasticsearch.SearchFacade;
import doc.doccenterserver.infrastructure.fileresource.file.utils.FileUtil;
import doc.doccenterserver.infrastructure.fileresource.message.utils.DateUtil;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.security.model.LoginUser;
import doc.doccenterserver.infrastructure.fileresource.security.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("HomeRepository")
public class HomeRepositoryImpl implements HomeRepository {

    @Resource
    private SearchFacade searchFacade;

    @Resource
    private PaperRepository paperRepository;

    @Resource
    private PaperAuthRepository paperAuthRepository;

    @Resource
    private ChannelRepository channelRepository;

    @Resource
    private DictRepository dictRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private AttachmentRepository attachmentRepository;

    @Resource
    private TPanCollectFolderRepository tpanCollectFolderRepository;

    @Resource
    private TokenService tokenService;

    private static final String ECM_LOCATION = "CZECM";

    private static final String DOC_TYPE_DOCUMENT = "document";

    private static final String DOC_TYPE_HTML = "html";

    private static final String DOC_TYPE_YUNFILE = "yunFile";

    private static final String XXHGYS_ORG_PARENT_ID = "99";

    @Override
    public HomeEntity getDocSearch(HttpServletRequest request, HomeEntity homeEntity) {
        try {
            long startTime = System.currentTimeMillis();
            // 查询参数
            SearchParam searchParam = new SearchParam();
            // 搜索权限控制
            LoginUser loginUser = tokenService.getLoginUser(request);
            String userid = loginUser.getUserModel().getUserid();
            // String userid = ConstantSys.CZ_ADMIN;

            // 1.2.3这个三个只触发一个，前端传参控制
            // 1.文档管理分类
            if (StringUtil.notEmpty(homeEntity.getChannelId())) {
                searchParam.setObjType(homeEntity.getChannelId());
            }
            // 2.个人云分类
            if (StringUtil.notEmpty(homeEntity.getPersonType())) {
                searchParam.setObjType(homeEntity.getPersonType());
            }
            // 3.发布时间
            if (StringUtil.notEmpty(homeEntity.getIssueStartDate()) && StringUtil.notEmpty(homeEntity.getIssueEndDate())) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    searchParam.setStartDate(sdf.parse(homeEntity.getIssueStartDate()));
                    searchParam.setEndDate(sdf.parse(homeEntity.getIssueEndDate()));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }
            // 搜索内容
            if (StringUtil.notEmpty(homeEntity.getContent())) {
                // 搜索类型 - 0：按标题搜索  1：按内容搜索  为空：按标题、内容搜索
                if (StringUtil.notEmpty(homeEntity.getType())) {
                    if (HomeEntity.TYPE_ZERO.equals(homeEntity.getType())) {
                        searchParam.setTitle(homeEntity.getContent());
                    } else if (HomeEntity.TYPE_ONE.equals(homeEntity.getType())) {
                        searchParam.setContent(homeEntity.getContent());
                    }
                } else {
                    // searchParam.setText(homeEntity.getContent());
                    searchParam.setContent(homeEntity.getContent());
                }
            }
            // 文件类型
            // 只选择后缀查询时，按文件名查询，不管界面选择什么查询方式
            if (StringUtil.notEmpty(homeEntity.getObjsuffix())) {
                searchParam.setSuffix(homeEntity.getObjsuffix());
            }
            if (StringUtil.notEmpty(homeEntity.getAppId())) {
                searchParam.setAppId(homeEntity.getAppId());
            }
            // 分页
            searchParam.setRows(20);
            searchParam.setCurPage(homeEntity.getPageNum());
            // TODO
        /*if (!ConstantSys.CZ_ADMIN.equals(userid)) {
            searchParam.setAuthlist(user.getAuthList());
        }*/
            // 如果没有设置任何权限，说明当前人员为信息化供应商人员，不允许查询索引
//            if (searchParam.getAuthlist().size() == 0 && loginUser.getUserModel().getCompanyId().startsWith(XXHGYS_ORG_PARENT_ID) && !"czadmin".equals(userid)) {
//                searchParam = new SearchParam();
//            }

            List<Map<String, String>> sortColumns = Lists.newArrayList();
            Map<String, String> sortColumn = Maps.newHashMap();
            //按索引创建时间降序排列
            sortColumn.put("name", "idx_date");
            sortColumn.put("order", "desc");
            sortColumns.add(sortColumn);
            searchParam.setSorts(sortColumns);
            String searchQueryString = searchParam.toString();
            log.info("查询参数 === " + searchQueryString);
            RawSearchResult searchResult = searchFacade.search(JSON.parseObject(searchQueryString, SearchQuery.class));
            List<SearchDocument> documentList = searchResult.getList();
            log.info("查询结果 === " + documentList.size());
            // 处理文档结果集
            List<SearchResultPack> docList = handleSearchResultV2(documentList, userid);
            long endTime = System.currentTimeMillis();
            homeEntity.setTimeConsuming((endTime - startTime) / 1000D);
            // 总记录数
            homeEntity.setTotalNum(documentList.size() > 0 ? new Long(searchResult.getTotalNum()).intValue() : 0);
            // 已加载数据总数
            // item.setLoadedDataNum(docList.size() + (curPage - 1) * pageSize);
            homeEntity.setLoadedDataNum(documentList.size());
            homeEntity.setDocList(docList);
            return homeEntity;
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    private List<SearchResultPack> handleSearchResult(List<SearchDocument> docList, String userId) {
        List<SearchResultPack> resultList = Lists.newArrayList();
        if (docList == null || docList.size() == 0) {
            return resultList;
        }
        for (SearchDocument doc : docList) {
            String idxDesc = doc.getIdx_desc();
            if (StringUtils.isBlank(idxDesc) || "null".equals(idxDesc)) {
                doc.setIdx_desc("");
            }
            String id = doc.getId();
            if (StringUtils.isBlank(id)) {
                continue;
            }
            String appId = doc.getIdx_app_id();
            //内容来自文档中心
            if (ECM_LOCATION.equals(appId)) {
                String[] idArr = id.split("_");
                String paperId = idArr[0];
                // 权限判断
                // 管理员查看全部，其他用户根据用户权限过滤
                Boolean isFlag = true;
                if (!ConstantSys.CZ_ADMIN.equals(userId)) {
                    isFlag = paperAuthRepository.getPaperAuth(userId, paperId);
                }
                if (isFlag) {
                    PaperEntity paper = paperRepository.findPaperById(paperId);
                    AttachmentEntity attachment = attachmentRepository.getAttachmentByPaperId(paperId);
                    if (paper != null) {
                        SearchResultPack resultPack;
                        if (0 == paper.getPaperType()) {
                            // 网页文档
                            resultPack = handleHtmlDocument(doc, paper, userId);
                            resultPack.setSuffix(DOC_TYPE_HTML);
                        } else {
                            // office正文文档
                            resultPack = handleOfficeDocument(doc, paper, userId);
                            resultPack.setSuffix(attachment.getFileFormat());
                        }
                        resultList.add(resultPack);
                    }
                }
            } else {
                // 来自网盘
                resultList.add(handleYunFile(doc));
            }
        }
        return resultList;
    }

    //对 handleSearchResult 进行了优化， by gj, 2023/6/13
    private List<SearchResultPack> handleSearchResultV2(List<SearchDocument> docList, String userId) {
        List<SearchResultPack> resultList = Lists.newArrayList();
        if (docList == null || docList.size() == 0) {
            return resultList;
        }
        List<String> paperIdList = docList.stream().map(SearchDocument::getId).collect(Collectors.toList());
        List<PaperEntity> paperEntityList = paperRepository.getByIds(paperIdList);
        List<AttachmentEntity> attachmentList = attachmentRepository.getAttachmentsByPaperIdList(paperIdList);
        for (SearchDocument doc : docList) {
            if (StringUtils.isEmpty(doc.getIdx_desc())) {
                doc.setIdx_desc("");
            }
            String id = doc.getId();
            if (StringUtils.isBlank(id)) {
                continue;
            }
            List<PaperEntity> paperFilterList = paperEntityList.stream().filter(i -> doc.getId().equals(i.getPaperId())).collect(Collectors.toList());
            List<AttachmentEntity> attachmentFilterList = attachmentList.stream().filter(i -> doc.getId().equals(i.getPaperId())).collect(Collectors.toList());
            PaperEntity paperEntity = CollectionUtil.isEmpty(paperFilterList) ? null : paperEntityList.get(0);
            AttachmentEntity attachment = CollectionUtil.isEmpty(attachmentFilterList) ? null : attachmentFilterList.get(0);
            SearchResultPack resultPack = doHandleDocument(doc, paperEntity, attachment);
            resultList.add(resultPack);
        }
        return resultList;
    }


    private SearchResultPack doHandleDocument(SearchDocument document, PaperEntity paper, AttachmentEntity attachment) {
        SearchResultPack item = new SearchResultPack();
        String paperId = document.getId();//文档id
        item.setFileId(document.getId());
        //item.setDocType(DOC_TYPE_DOCUMENT);//正文文档标识
        item.setObjTag(document.getIdx_parent_id());//文件大类
        item.setIdxTitle(document.getIdx_title());//标题,用户列表视图
        item.setIdxDesc(document.getIdx_desc());//摘要,用户列表视图
        item.setAuthorId(document.getIdx_user_id());//用户id
        item.setAuthorName(document.getIdx_user_name());//用户名称
        LocalDateTime issueDate = document.getIdx_date();//发布时间
        if (issueDate != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            item.setIssueDate(issueDate.format(formatter));
        }
        item.setAppId(ECM_LOCATION);
        item.setPaperId(paperId);
        item.setSuffix(document.getIdx_suffix());
        //paper handle
        if (null != paper) {
            DictEntry dict = new DictEntry();
            dict.setId(paper.getPaperId());
            dict.setPid("DOC_SEARCH_TYPE");
//            DictEntry dictEntry = dictRepository.getDictByIdAndPid(dict);
//            if (dictEntry != null) {
//                item.setMenuid(dict.getDictValue());
//            }
            // 判断文档是否被该用户收藏
//            item.setIsCollect(isCollect(paper.getPaperId(), userId));
//            item.setClassId(paper.getClassId());
//            item.setAppId(ECM_LOCATION);
            // 分类名称
            item.setClassName(paper.getName());
            // 点击数
            item.setDjNum(paper.getClickCount());
            // 文档名称
            item.setObjName(paper.getPaperName());
//            UserEntity user = userRepository.getUserInfo(paper.getIssueUserId());
//            if (user != null) {
//                item.setDeptName(user.getDeptName());
//            }
//            ChannelEntity channelEntity = channelRepository.getById(paper.getClassId());
//            item.setMenuid(channelEntity.getName());
        } else {
            item.setDjNum(0);//点击数
            item.setObjIcon("not-found");
        }
        //attachment handle
        if (null != attachment) {
            item.setFileId(attachment.getFileId());//大数据平台对应文件id
            if (!ObjectUtils.isEmpty(attachment.getFileSize())) {
                item.setObjSize(FileUtil.formatObjSize(attachment.getFileSize()));
            } else {
                item.setObjSize("0KB");
            }
            item.setXzNum(attachment.getDownloadCount());//正文文档下载数
            item.setObjName(attachment.getFileName());
            item.setPlNum(0);
            String objIcon = attachment.getFileName().substring(attachment.getFileName().lastIndexOf(".") + 1);
            if (StringUtils.isBlank(objIcon)) {
                objIcon = "default";
            }
            item.setObjIcon(objIcon);//后缀
        } else {
            item.setObjSize("0KB");
            item.setObjIcon("not-found");
            item.setXzNum(0);
            item.setDjNum(0);
            item.setPlNum(0);
        }
        return item;
    }

    private SearchResultPack handleHtmlDocument(SearchDocument document, PaperEntity paper, String userId) {
        SearchResultPack item = new SearchResultPack();
        // int nameIndex = document.getIdx_title().lastIndexOf(".");
        // if (nameIndex > 0) {
        // 标题
        item.setIdxTitle(document.getIdx_title());
        // } else {
        //    item.setIdxTitle(document.getIdx_title());
        // }
        item.setFileId(document.getId());
        // 网页文档标识
        item.setDocType(DOC_TYPE_HTML);
        // 摘要
        item.setIdxDesc(document.getIdx_desc());
        // 用户id
        item.setAuthorId(document.getIdx_user_id());
        // 用户名称
        item.setAuthorName(document.getIdx_user_name());
        item.setObjTag(DOC_TYPE_HTML);
        item.setObjIcon(DOC_TYPE_HTML);
        LocalDateTime issueDate = document.getIdx_date();//发布时间
        if (issueDate != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            item.setIssueDate(issueDate.format(formatter));
        }
        item.setObjSize("--");
        // 网页文档没有评论功能
        item.setPlNum(0);
        if (null != paper) {
            DictEntry dict = new DictEntry();
            dict.setId(paper.getPaperId());
            dict.setPid("DOC_SEARCH_TYPE");
            DictEntry dictEntry = dictRepository.getDictByIdAndPid(dict);
            if (dictEntry != null) {
                item.setMenuid(dict.getDictValue());
            }
            // 判断文档是否被该用户收藏
            item.setIsCollect(isCollect(paper.getPaperId(), userId));
            item.setClassId(paper.getClassId());
            item.setAppId(ECM_LOCATION);
            // 分类名称
            item.setClassName(paper.getName());
            // 点击数
            item.setDjNum(paper.getClickCount());
            // 文档名称
            item.setObjName(paper.getPaperName());
            item.setPaperId(paper.getPaperId());
            UserEntity user = userRepository.getUserInfo(paper.getIssueUserId());
            if (user != null) {
                item.setDeptName(user.getDeptName());
            }
            ChannelEntity channelEntity = channelRepository.getById(paper.getClassId());
            item.setMenuid(channelEntity.getName());
        } else {
            item.setPaperId("");
            item.setDjNum(0);//点击数
            item.setObjIcon("not-found");
        }
        return item;
    }

    private SearchResultPack handleOfficeDocument(SearchDocument document, PaperEntity paper, String userId) {
        SearchResultPack item = new SearchResultPack();
        String[] idArr = document.getId().split("_");
        String paperId = idArr[0];//文档id
        item.setFileId(document.getId());
        item.setDocType(DOC_TYPE_DOCUMENT);//正文文档标识
        item.setObjTag(DOC_TYPE_DOCUMENT);
        item.setIdxTitle(document.getIdx_title());//标题,用户列表视图
        item.setIdxDesc(document.getIdx_desc());//摘要,用户列表视图
        item.setAuthorId(document.getIdx_user_id());//用户id
        item.setAuthorName(document.getIdx_user_name());//用户名称
        LocalDateTime issueDate = document.getIdx_date();//发布时间
        if (issueDate != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            item.setIssueDate(issueDate.format(formatter));
        }
        item.setObjIcon("default");//后缀
        if (null != paper) {
            DictEntry dict = new DictEntry();
            dict.setId(paper.getPaperId());
            dict.setPid("DOC_SEARCH_TYPE");
            DictEntry dictEntry = dictRepository.getDictByIdAndPid(dict);
            if (dictEntry != null) {
                item.setMenuid(dict.getDictValue());
            }
            item.setIsCollect(isCollect(paper.getPaperId(), userId));//判断文档是否被该用户收藏
            item.setClassId(paper.getClassId());
            item.setClassName(paper.getName());
            item.setAppId(ECM_LOCATION);
            item.setDjNum(paper.getClickCount());//点击数
            item.setObjName(paper.getPaperName());//文档名称
            item.setPaperId(paperId);

            UserEntity user = userRepository.getUserInfo(paper.getIssueUserId());
            if (user != null) {
                item.setDeptName(user.getDeptName());
            }
            ChannelEntity channelEntity = channelRepository.getById(paper.getClassId());
            item.setMenuid(channelEntity.getName());
        } else {
            item.setDjNum(0);//点击数
            item.setPaperId("");
        }
        // 查询主附件
        AttachmentEntity attachmentEntity = new AttachmentEntity();
        attachmentEntity.setPaperId(paperId);
        attachmentEntity.setIsMain("1");
        List<AttachmentEntity> attachmentList = attachmentRepository.getAttachmentList(attachmentEntity);

        AttachmentEntity attachment = null;
        if (attachmentList.size() > 0) {
            attachment = attachmentList.get(0);
        }
        if (null != attachment) {
            item.setFileId(attachment.getFileId());//大数据平台对应文件id
            item.setObjSize(FileUtil.formatObjSize(attachment.getFileSize()));
            item.setXzNum(attachment.getDownloadCount());//正文文档下载数
            item.setObjName(attachment.getFileName());
            item.setPlNum(0);
            String objIcon = attachment.getFileName().substring(attachment.getFileName().lastIndexOf(".") + 1);
            if (StringUtils.isBlank(objIcon)) {
                objIcon = "default";
            }
            item.setObjIcon(objIcon);//后缀
        } else {
            item.setObjSize("0KB");
            item.setObjIcon("not-found");
            item.setXzNum(0);
            item.setDjNum(0);
            item.setPlNum(0);
        }
        return item;
    }

    private SearchResultPack handleYunFile(SearchDocument document) {
        SearchResultPack item = new SearchResultPack();
        item.setDocType(DOC_TYPE_YUNFILE);//网盘文件标识
        item.setIdxTitle(document.getIdx_title());//标题，用于列表视图
        item.setIdxDesc(document.getIdx_desc());//摘要，用于列表视图
        item.setAuthorId(document.getIdx_user_id());//用户id
        item.setAuthorName(document.getIdx_user_name());//用户名称
        item.setObjTag(document.getIdx_parent_id());//文件大类
        LocalDateTime issueDate = document.getIdx_date();//发布时间
        if (issueDate != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            item.setIssueDate(issueDate.format(formatter));
        }
        //默认值
        item.setObjSize("0KB");
        item.setDjNum(0);//点击数
        item.setXzNum(0);//下载数
        item.setObjIcon("not-found");
        String objId = document.getIdx_id();
        if (null == objId) {
            return item;
        }
        // TODO 以下待处理
        /*TPanObjEntry panObj = tPanObjEntryMapper.findTPanObjById(objId);
        if (null != panObj) {
            item.put("classId", panObj.get("classId"));//主键
            item.put("appId", "yunpan");//主键
            item.put("className", panObj.get("className"));//主键
            item.put("objId", panObj.getObjId());//主键
            item.put("objSize", FileUtil.formatObjSize(panObj.getObjSize()));
            item.put("objName", panObj.getObjName());
            item.put("parentObjId", panObj.getParentObjId());
            String objIcon = panObj.getObjIcon();
            item.put("isCollect", isCollect(panObj.getObjId()));//判断文档是否被该用户收藏
            if (StringUtils.isBlank(objIcon)) objIcon = "default";
            item.set("objIcon", objIcon);
            List<Map<String, Object>> sharelist = tPanShareEntryMapper.findObjDpxnumGroupBySid(panObj.getObjId());
            if (sharelist != null && sharelist.size() > 0) {
                Map<String, Object> list_num = sharelist.get(0);
                item.put("djNum", list_num.get("djnum"));//点击数
                //item.put("plNum",list_num.get("plnum"));//评论数
                item.put("xzNum", list_num.get("xznum"));//下载数
            }
            UserEntry user = UserEntryMapper.selectByPrimaryKey(panObj.getUserId());
            if (user != null) {
                item.set("deptName", user.getDeptName());
            }
        }

        TPanFileEntry objFile = tPanFileEntryMapper.findTPanFileByObjId(objId);
        if (null != objFile) {
            item.set("fileId", objFile.getFileId());
            item.set("pdfFileId", objFile.getPdfFileId());
            //判断是否有缩略图
            String fileType = objFile.getFileType();//获取文件后缀名

            Map<String, String> fileApiMap = OrderedPropertyPlaceholderConfigurer.getJsonMapContextProperty("FILE_API_INFO");

            String supportedViewTypes = JSONObject.parseObject(fileApiMap.get("fileTag")).getString("image");
            if (supportedViewTypes.contains(fileType)) {
                item.set("objThumbPath", THUMB_URL + "/images/" + objFile.getFileId() + ".jpg_260x190.jpg");
            }
        }*/
        return item;
    }

    private boolean isCollect(String objId, String userId) {
        // 默认文档没有被该用户收藏
        TPanCollectFolderEntity tpanCol = new TPanCollectFolderEntity();
        tpanCol.setObjId(objId);
        tpanCol.setUserId(userId);
        Integer num = tpanCollectFolderRepository.getCountByObjId(tpanCol);
        //判断文档是否
        return num > 0;
    }

}
