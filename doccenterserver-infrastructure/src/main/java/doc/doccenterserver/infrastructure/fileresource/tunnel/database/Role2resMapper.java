package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.Role2resDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色-资源关联Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface Role2resMapper extends BaseMapper<Role2resDO> {
}
