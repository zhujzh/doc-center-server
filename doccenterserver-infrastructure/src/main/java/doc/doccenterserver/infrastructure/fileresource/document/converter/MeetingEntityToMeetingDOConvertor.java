package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.MeetingEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.MeetingDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface MeetingEntityToMeetingDOConvertor {

    MeetingEntityToMeetingDOConvertor INSTANCE = Mappers.getMapper(MeetingEntityToMeetingDOConvertor.class);

    @Mappings({})
    MeetingDO entityToDO(MeetingEntity meetingEntity);

    @Mappings({})
    MeetingEntity dOToEntity(MeetingDO meetingDO);

    @Mappings({})
    List<MeetingEntity> dOToEntityList(List<MeetingDO> meetingDOList);

    @Mappings({})
    List<MeetingDO> entityToDOList(List<MeetingEntity> meetingEntityList);
}
