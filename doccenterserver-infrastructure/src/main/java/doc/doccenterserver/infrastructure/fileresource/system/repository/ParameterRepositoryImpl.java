package doc.doccenterserver.infrastructure.fileresource.system.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import doc.doccenterserver.domain.fileresource.system.model.ParameterEntry;
import doc.doccenterserver.domain.fileresource.system.repository.ParameterRepository;
import doc.doccenterserver.infrastructure.fileresource.system.converter.ParameterEntryToParameterDOConverter;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.ParameterMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ParameterDO;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 字典
 * repository实现类
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Component("SYSTEMParameteDtoDORepository")
public class ParameterRepositoryImpl implements ParameterRepository {
    @Resource
    private ParameterMapper parameterMapper;
    @Override
    public List<ParameterEntry> selectList() {
        List<ParameterDO> doList= parameterMapper.selectList(new QueryWrapper<>());
        return ParameterEntryToParameterDOConverter.INSTANCE.parameterDOToParameterEntryList(doList);
    }

    @Override
    public ParameterEntry getByKey(String key) {
        LambdaQueryWrapper<ParameterDO> lqw=new LambdaQueryWrapper<>();
        lqw.eq(ParameterDO::getPKey,key);
        ParameterDO parameterDO= parameterMapper.selectList(lqw).stream().findAny().orElse(null);
        return ParameterEntryToParameterDOConverter.INSTANCE.parameterDOToParameterEntry(parameterDO);
    }
}
