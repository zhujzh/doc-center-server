package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */

@Data
@TableName("`t_pan_collect_folder`")
@NoArgsConstructor
public class TPanCollectFolderDO {

    @Field(name = "collect_id")
    @TableId(type = IdType.INPUT, value = "collect_id")
    private String collectId;

    @Field(name = "folder_name")
    @TableField("folder_name")
    private String folderName;

    @Field(name = "user_id")
    @TableField("user_id")
    private String userId;

    @Field(name = "parent_collect_id")
    @TableField("parent_collect_id")
    private String parentCollectId;

    @Field(name = "create_time")
    @TableField("create_time")
    private Date createTime;

    @Field(name = "obj_id")
    @TableField("obj_id")
    private String objId;

    @Field(name = "doc_type")
    @TableField("doc_type")
    private String docType;

    @Field(name = "obj_name")
    @TableField("obj_name")
    private String objName;
}
