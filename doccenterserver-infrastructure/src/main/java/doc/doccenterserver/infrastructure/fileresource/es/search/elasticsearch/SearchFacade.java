package doc.doccenterserver.infrastructure.fileresource.es.search.elasticsearch;

import doc.doccenterserver.infrastructure.fileresource.es.SearchDocument;
import doc.doccenterserver.infrastructure.fileresource.es.RawSearchResult;
import doc.doccenterserver.infrastructure.fileresource.es.UpdateDocument;
import doc.doccenterserver.infrastructure.fileresource.es.bean.SearchDmlResult;
import doc.doccenterserver.infrastructure.fileresource.es.bean.SearchQuery;
import org.springframework.stereotype.Service;


/**
 * @Description: 执行搜索操作，该接口仅定义与搜索相关的方法，具体实现逻辑需要在实现类中体现
 * @author: gj
 * @date 2023/4/11
 */
@Service
public interface SearchFacade {
    void testSearch();
    /**
     * 查找索引
     *
     * @param searchParam 查询参数对象
     * @return SearchResult
     */
    RawSearchResult search(SearchQuery searchQuery);


    /**
     * 添加索引
     *
     * @param searchDocument 索引对象
     * @return SearchDmlResult
     */
    SearchDmlResult create(SearchDocument searchDocument);

    /**
     * 删除单个索引
     *
     * @param index 索引名称
     * @return SearchDmlResult
     */
    SearchDmlResult delete(String index);


    /**
     * 根据文档业务id(idx_id)删除文档
     *
     * @param ids 多个文档，使用英文逗号分隔
     * @return SearchDmlResult
     */
    SearchDmlResult deleteDoc(String ids);


    /**
     * 更新索引中的文档
     *
     * @param updateDocument 需要更新的文档对象
     * @return SearchDmlResult
     */
    SearchDmlResult updateDoc(UpdateDocument updateDocument);

}
