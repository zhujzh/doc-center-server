package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.DocPublishChangelogDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 待文档发布任务处理Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface DocPublishChangelogMapper extends BaseMapper<DocPublishChangelogDO> {
}
