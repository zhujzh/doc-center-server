package doc.doccenterserver.infrastructure.fileresource.message.enums;

public enum JobDetailLogStepIdEnum {
    STEP_ID_1(1, "MQ消息入库"),
    STEP_ID_2(2, "MQ消息处理"),
    STEP_ID_3(3, "消息归档"),
    STEP_ID_4(4, "文档转换"),
    STEP_ID_5(5, "文档发布"),
    STEP_ID_6(6, "创建索引"),
    STEP_ID_7(7, "发送ESB消息"),
    STEP_ID_8(8, "消息异动"),
    STEP_ID_9(9, "网盘文档转换"),
    STEP_ID_10(10, "网盘创建索引"),
    STEP_ID_11(11, "网盘索引删除")
    ;
    private final Integer value;
    private final String name;
    JobDetailLogStepIdEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }
    public Integer getValue() {
        return this.value;
    }

}
