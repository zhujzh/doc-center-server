package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自定义组
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_group`")
@NoArgsConstructor
public class GroupDO {

    @Field(name = "主键")
    @TableId(type = IdType.INPUT, value = "group_id")
    private String groupId;

    @Field(name = "名称")
    @TableField("group_name")
    private String groupName;

    @Field(name = "父组ID")
    @TableField("group_parent_id")
    private String groupParentId;

    @Field(name = "组排序号")
    @TableField("group_sort")
    private Integer groupSort;

    @Field(name = "组类型")
    @TableField("group_type")
    private String groupType;

    @Field(name = "组所属用户id")
    @TableField("group_userid")
    private String groupUserid;
}
