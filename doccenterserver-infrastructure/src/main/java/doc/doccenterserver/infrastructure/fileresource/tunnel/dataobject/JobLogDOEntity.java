package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author Administrator
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("`t_cms_job_log`")
public class JobLogDOEntity extends JobLogDO {

    @TableField(exist = false)
    private String stepStatus;

    @TableField(exist = false)
    private String stepId;

    @TableField(exist = false)
    private String stepName;

    @TableField(exist = false)
    private String xmlContent;

    @TableField(exist = false)
    private String orderByColumns;

    @TableField(exist = false)
    private String createStartDate;

    @TableField(exist = false)
    private String createEndDate;

}
