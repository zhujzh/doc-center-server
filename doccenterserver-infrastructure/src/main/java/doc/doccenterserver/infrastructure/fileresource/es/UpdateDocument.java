package doc.doccenterserver.infrastructure.fileresource.es;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Description: 文档更新对象
 * @author: gj
 * @date 2023/4/13
 */
@Data
public class UpdateDocument {

    private String idx_id; //业务主键Id
    private String idx_title;//标题
    private String idx_desc;//业务描述
    private List<String> idx_auth_value;//授权值
    private String idx_attachment_id;//附件ID,多个附件id以”,”分割
    private String idx_suffix;//文件名后缀
    private String idx_text;//文件文本内容（用于全文索引）

}
