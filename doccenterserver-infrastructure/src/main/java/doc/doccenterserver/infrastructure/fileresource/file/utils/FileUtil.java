package doc.doccenterserver.infrastructure.fileresource.file.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Tangcancan
 */
@Slf4j
public class FileUtil {

    public static String formatObjSize(Double objSize) {
        // 格式化文件大小
        int objSizeFlag = 0;
        do {
            objSize = objSize / 1024;
            objSizeFlag++;
        } while (objSize > 99);
        String[] sFlag = {"B", "KB", "MB", "GB", "TB", "PB", "EB"};
        return String.format("%.1f", objSize) + sFlag[objSizeFlag];
    }

}
