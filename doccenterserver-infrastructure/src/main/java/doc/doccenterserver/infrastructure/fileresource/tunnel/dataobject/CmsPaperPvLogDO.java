package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Tangcancan
 */
@Data
@TableName("`t_cms_paper_pv_log`")
@NoArgsConstructor
public class CmsPaperPvLogDO {

    @Field(name = "PV_ID")
    @TableField("PV_ID")
    private String pvId;

    @Field(name = "USER_CODE")
    @TableField("USER_CODE")
    private String userCode;

    @Field(name = "USER_CODE")
    @TableField("USER_NAME")
    private String userName;

    @Field(name = "BIZ_ID")
    @TableField("BIZ_ID")
    private String bizId;

    @Field(name = "BIZ_NAME")
    @TableField("BIZ_NAME")
    private String bizName;

    @Field(name = "URL")
    @TableField("URL")
    private String url;

    @Field(name = "CREATE_TIME")
    @TableField("CREATE_TIME")
    private Date createTime;

}
