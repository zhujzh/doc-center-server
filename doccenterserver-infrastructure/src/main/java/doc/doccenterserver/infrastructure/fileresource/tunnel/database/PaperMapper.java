package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PaperDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 文档表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface PaperMapper extends BaseMapper<PaperDO> {

    PaperEntity findPaperById(String paperId);

    Integer updatePaperClickCount(String paperId);

    IPage<PaperDO>  findPaperRefListByParams(Map<String, Object> params);

    IPage<PaperDO> findPaperList(IPage<PaperDO> page, @Param("paper") PaperEntity paper, @Param(Constants.WRAPPER) Wrapper<PaperDO> queryWrapper);


    Double findsumpaperfilesize(String appId);

    Double sumtpanfilesize(String appId);

    IPage<PaperDO> findPaperRefByIdList(Map<String, Object> paper);

    IPage<PaperDO> findPaperListByCzadmin(Page<PaperDO> page,@Param(Constants.WRAPPER)Map<String, Object> paper);

    IPage<PaperDO> findPaperByIdList(Page<PaperDO> page,@Param(Constants.WRAPPER)Map<String, Object> paper);
}
