//Powered By Webit Team, Since 2012 - 2014

package doc.doccenterserver.infrastructure.fileresource.es;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 *
 * @author 高星
 */

@SuppressWarnings({ "unused"})
@Data
public class SearchPaperAllEntry {

    private String id; //ID <>

    private String appId; //APP_ID <>

    private String authValue; //AUTH_VALUE <>

    private String bussinessKey; //BUSSINESS_KEY <>

    private String content; //CONTENT <>

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date createTime; //CREATE_TIME <>

    private String parentId; //PARENT_ID <>

    private String status; //STATUS <>

    private String title; //TITLE <>

    private String userId; //USER_ID <>

    private String userName; //USER_NAME <>

}
