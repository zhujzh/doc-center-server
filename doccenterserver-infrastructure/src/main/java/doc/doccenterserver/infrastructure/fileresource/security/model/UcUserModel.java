package doc.doccenterserver.infrastructure.fileresource.security.model;

import com.alibaba.bizworks.core.specification.Field;
import lombok.Data;

/**
 * @author wangxp
 * @description 用户中心 用户对象
 * @date 2023/6/12 19:06
 */
@Data
public class UcUserModel {

    @Field(
            value = "部门名称",
            name = "deptName"
    )
    private String deptName;
    @Field(
            value = "英文名",
            name = "staffEnName"
    )
    private String staffEnName;
    @Field(
            value = "修改人",
            name = "staffUpdator"
    )
    private String staffUpdator;
    @Field(
            value = "staffAccountType",
            name = "staffAccountType"
    )
    private String staffAccountType;
    @Field(
            value = "公司名称",
            name = "companyName"
    )
    private String companyName;
    @Field(
            value = "操作人",
            name = "optUserName"
    )
    private String optUserName;
    @Field(
            value = "所属城市",
            name = "cityId"
    )
    private String cityId;
    @Field(
            value = "人资编号",
            name = "positionHrId"
    )
    private String positionHrId;
    @Field(
            value = "退休时间",
            name = "staffRetiredTime"
    )
    private String staffRetiredTime;
    @Field(
            value = "员工子类",
            name = "staffSubType"
    )
    private String staffSubType;
    @Field(
            value = "是否返回",
            name = "isBack"
    )
    private String isBack;
    @Field(
            value = "零售户名称",
            name = "retailName"
    )
    private String retailName;
    @Field(
            value = "员工编制类型",
            name = "staffWorkType"
    )
    private String staffWorkType;
    @Field(
            value = "短位工号",
            name = "staffShortNum"
    )
    private String staffShortNum;
    @Field(
            value = "输入事件Id",
            name = "inputEventId"
    )
    private String inputEventId;
    @Field(
            value = "人资Id",
            name = "staffHrId"
    )
    private String staffHrId;
    @Field(
            value = "前后台类型",
            name = "staffPortType"
    )
    private String staffPortType;
    @Field(
            value = "是否投资公司",
            name = "isTzgs"
    )
    private String isTzgs;
    @Field(
            value = "办公电话",
            name = "staffWorkPhone"
    )
    private String staffWorkPhone;
    @Field(
            value = "更新时间",
            name = "staffLastupdateTime"
    )
    private String staffLastupdateTime;
    @Field(
            value = "根据组织查询",
            name = "queryOrgId"
    )
    private String queryOrgId;
    @Field(
            value = "最高专业技术资格等级",
            name = "staffSpecialtyLevel"
    )
    private String staffSpecialtyLevel;
    @Field(
            value = "员工编号",
            name = "staffCode"
    )
    private String staffCode;
    @Field(
            value = "组织机构名称",
            name = "orgName"
    )
    private String orgName;
    @Field(
            value = "父级组织Id",
            name = "orgParentId"
    )
    private String orgParentId;
    @Field(
            value = "部门Id",
            name = "deptId"
    )
    private String deptId;
    @Field(
            value = "按员工查询",
            name = "queryStaff"
    )
    private String queryStaff;
    @Field(
            value = "微信号",
            name = "staffWechart"
    )
    private String staffWechart;
    @Field(
            value = "全拼",
            name = "staffSpells"
    )
    private String staffSpells;
    @Field(
            value = "性别",
            name = "staffSex"
    )
    private String staffSex;
    @Field(
            value = "是否是主身份",
            name = "mainIdentityStatus"
    )
    private String mainIdentityStatus;
    @Field(
            value = "所属省份",
            name = "provinceId"
    )
    private String provinceId;
    @Field(
            value = "借调外单位名称",
            name = "staffSecondName"
    )
    private String staffSecondName;
    @Field(
            value = "入职时间",
            name = "staffHiredate"
    )
    private String staffHiredate;
    @Field(
            value = "公司Id",
            name = "companyId"
    )
    private String companyId;
    @Field(
            value = "年龄",
            name = "staffAge"
    )
    private String staffAge;
    @Field(
            value = "手机号码",
            name = "staffMobile"
    )
    private String staffMobile;
    @Field(
            value = "最高职业资格信息名称",
            name = "staffProfession"
    )
    private String staffProfession;
    @Field(
            value = "身份证号",
            name = "staffPaperworkNum"
    )
    private String staffPaperworkNum;
    @Field(
            value = "人员兼职状态",
            name = "status"
    )
    private String status;
    @Field(
            value = "家庭电话",
            name = "staffHomePhone"
    )
    private String staffHomePhone;
    @Field(
            value = "服务编码",
            name = "serviceCode"
    )
    private String serviceCode;
    @Field(
            value = "最高职业资格信息名称等级",
            name = "staffProfessionLevel"
    )
    private String staffProfessionLevel;
    @Field(
            value = "民族",
            name = "staffNation"
    )
    private String staffNation;
    @Field(
            value = "排序",
            name = "staffSort"
    )
    private String staffSort;
    @Field(
            value = "操作Id",
            name = "optUserId"
    )
    private String optUserId;
    @Field(
            value = "员工号",
            name = "staffNum"
    )
    private String staffNum;
    @Field(
            value = "桌面端显示的职位名称",
            name = "dmdPositionName"
    )
    private String dmdPositionName;
    @Field(
            value = "输入日志Id",
            name = "inputLogId"
    )
    private String inputLogId;
    @Field(
            value = "职级排序",
            name = "staffRankSort"
    )
    private String staffRankSort;
    @Field(
            value = "组织机构UUID",
            name = "orgId"
    )
    private String orgId;
    @Field(
            value = "岗位名称",
            name = "positionName"
    )
    private String positionName;
    @Field(
            value = "备用手机",
            name = "staffMobileBak"
    )
    private String staffMobileBak;
    @Field(
            value = "岗位角色",
            name = "staffPositionRole"
    )
    private String staffPositionRole;
    @Field(
            value = "传真",
            name = "staffFax"
    )
    private String staffFax;
    @Field(
            value = "发送方",
            name = "sendSwitch"
    )
    private String sendSwitch;
    @Field(
            value = "员工姓名的简拼",
            name = "staffShortSpells"
    )
    private String staffShortSpells;
    @Field(
            value = "办公地址",
            name = "staffWorkAddress"
    )
    private String staffWorkAddress;
    @Field(
            value = "是否已经创建标识",
            name = "isCreate"
    )
    private String isCreate;
    @Field(
            value = "姓名",
            name = "staffName"
    )
    private String staffName;
    @Field(
            value = "开始时间",
            name = "startTime"
    )
    private String startTime;
    @Field(
            value = "籍贯",
            name = "staffNativePlace"
    )
    private String staffNativePlace;
    @Field(
            value = "人员生日",
            name = "staffBirthday"
    )
    private String staffBirthday;
    @Field(
            value = "所学专业",
            name = "staffSpecialty"
    )
    private String staffSpecialty;
    @Field(
            value = "等级名称",
            name = "gradeName"
    )
    private String gradeName;
    @Field(
            value = "最高学位",
            name = "staffDegree"
    )
    private String staffDegree;
    @Field(
            value = "等级ID",
            name = "gradeId"
    )
    private String gradeId;
    @Field(
            value = "创建时间",
            name = "staffCreateTime"
    )
    private String staffCreateTime;
    @Field(
            value = "当前员工的职位",
            name = "currentPositionId"
    )
    private String currentPositionId;
    @Field(
            value = "员工状态",
            name = "staffStatus"
    )
    private String staffStatus;
    @Field(
            value = "只查询该组织下主身份的员工",
            name = "mainIdentityStaff"
    )
    private String mainIdentityStaff;
    @Field(
            value = "是否是当前身份",
            name = "currentIdentityStatus"
    )
    private String currentIdentityStatus;
    @Field(
            value = "创建人",
            name = "staffCreator"
    )
    private String staffCreator;
    @Field(
            value = "员工类型",
            name = "staffType"
    )
    private String staffType;
    @Field(
            value = "用户ID",
            name = "userId"
    )
    private String userId;
    @Field(
            value = "职位别名",
            name = "dutyName"
    )
    private String dutyName;
    @Field(
            value = "区编号",
            name = "areaId"
    )
    private String areaId;
    @Field(
            value = "当前员工的职位",
            name = "positionId"
    )
    private String positionId;
    @Field(
            value = "短号码",
            name = "staffShortFax"
    )
    private String staffShortFax;
    @Field(
            value = "家庭住址",
            name = "staffHomeAddress"
    )
    private String staffHomeAddress;
    @Field(
            value = "护照号",
            name = "staffPassport"
    )
    private String staffPassport;
    @Field(
            value = "政治面貌",
            name = "staffPoliticalStatus"
    )
    private String staffPoliticalStatus;
    @Field(
            value = "学历",
            name = "staffEducation"
    )
    private String staffEducation;
    @Field(
            value = "职级",
            name = "staffRank"
    )
    private String staffRank;
    @Field(
            value = "结束时间",
            name = "endTime"
    )
    private String endTime;
    @Field(
            value = "员工岗位状态",
            name = "staffJobStatus"
    )
    private String staffJobStatus;
    @Field(
            value = "员工编码",
            name = "staffId"
    )
    private String staffId;

    @Field(
            value = "基线版员工id",
            name = "employeeId"
    )
    private String employeeId;
    @Field(
            value = "基线版用户id",
            name = "ucUserId"
    )
    private String ucUserId;
    @Field(
            value = "基线版用户类型",
            name = "userType"
    )
    private String userType;
}
