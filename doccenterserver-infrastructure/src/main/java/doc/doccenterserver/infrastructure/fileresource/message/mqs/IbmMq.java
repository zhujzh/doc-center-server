package doc.doccenterserver.infrastructure.fileresource.message.mqs;


import cn.hutool.json.JSONObject;
import com.ibm.mq.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

//----------------------------------------------   
// 非集团的布署点（如吴忠等）MQ信息类（用于读取归档信息）
//----------------------------------------------   

public class IbmMq {
	
	private static Logger log = LoggerFactory.getLogger(IbmMq.class);
	private static String mqErrorStartTime = "9999-12-30 00:00:00";
    private static JSONObject MQ_CONFIG_MAP;

    /**
     * 构造器
     */
    public IbmMq(){
    }
    public IbmMq(JSONObject object){
        MQ_CONFIG_MAP = object;
    }
    /**
     * MQ发送方法
     */
    @SuppressWarnings("finally")
	public static Boolean send(String content) {
    	Boolean result = false;
    	MQQueue q = null;
    	MQQueueManager qMgr = null;
        // MQ发送   
        try {   
            // 建立MQ客户端使用上下文  
            MQEnvironment.hostname = MQ_CONFIG_MAP.getStr("mqHostName"); // 服务器ip地址
            MQEnvironment.port = Integer.parseInt(MQ_CONFIG_MAP.getStr("mqPort")); // 服务器MQ服器端口
            MQEnvironment.CCSID = Integer.parseInt(MQ_CONFIG_MAP.getStr("mqCCSID")); // 服务器MQ服务使用的编码
            log.debug("CCSID:"+MQEnvironment.CCSID);
            MQEnvironment.userID = MQ_CONFIG_MAP.getStr("mqUserName"); // MQ服务用户名
            MQEnvironment.password = MQ_CONFIG_MAP.getStr("mqPassword");
            MQEnvironment.channel = MQ_CONFIG_MAP.getStr("mqChannel"); // 服务器连接通道名

            // 链接队列管理器   
            qMgr = new MQQueueManager(MQ_CONFIG_MAP.getStr("mqQManager"));
            log.debug(qMgr.name);

            int openOptions = MQC.MQOO_OUTPUT | MQC.MQOO_FAIL_IF_QUIESCING;  
            log.debug("openOptions="+openOptions);
            // 打开MQ队列   
            q = qMgr.accessQueue(MQ_CONFIG_MAP.getStr("mqLocalInQueue"), openOptions);
            //FileInputStream fins = new FileInputStream(new File(strExtraSendXmlFileName));   
            //InputStream fins = ClassLoader.getSystemResourceAsStream(strExtraSendXmlFileName);   
           // byte[] data = new byte[fins.available()]; 
            byte[] data=content.getBytes("UTF-8");
            //log.debug("data="+new String(data));
            //fins.read(data);   
          //  fins.close();   
            MQMessage msg = new MQMessage();   
            msg.write(data);   
            // 放入消息   
            q.put(msg);   
            // 关闭队列   
            q.close();   
            // 关闭队列管理器   
            qMgr.disconnect(); 
            
            result = true;
        } catch (Exception e) {   
//        	insertMonitorErrLog(e);
        	log.error("MqUtil.send...........",e);
        } finally {
			try {
				if (q != null) {
					q.close();
				}
				if (qMgr != null) {
					qMgr.close();
					qMgr.disconnect();
				}
			} catch (Exception e) {
			}  
			return result;
        }  
    }   
  
    public String recieve() {
    	String content = "";
    	MQQueue q = null;
    	MQQueueManager qMgr = null;
        // MQ接收   
        try {   
            // 建立MQ客户端使用上下文  
            MQEnvironment.hostname = MQ_CONFIG_MAP.getStr("mqHostName"); // 服务器ip地址
            MQEnvironment.port = Integer.parseInt(MQ_CONFIG_MAP.getStr("mqPort")); // 服务器MQ服器端口
            MQEnvironment.CCSID = Integer.parseInt(MQ_CONFIG_MAP.getStr("mqCCSID")); // 服务器MQ服务使用的编码
            MQEnvironment.userID = MQ_CONFIG_MAP.getStr("mqUserName"); // MQ服务用户名
            MQEnvironment.password = MQ_CONFIG_MAP.getStr("mqPassword");
            MQEnvironment.channel = MQ_CONFIG_MAP.getStr("mqChannel"); // 服务器连接通道名

            // 链接队列管理器   
            qMgr = new MQQueueManager(MQ_CONFIG_MAP.getStr("mqQManager"));
            log.debug(qMgr.name);
            log.debug("qMgr.name:"+qMgr.name);
            int openOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_FAIL_IF_QUIESCING;   
            log.debug("RopenOptions="+openOptions);
            // 打开MQ队列   
            q = qMgr.accessQueue(MQ_CONFIG_MAP.getStr("mqLocalOutQueue"), openOptions);
            log.debug("q.name:"+q.name);
            MQGetMessageOptions mgo = new MQGetMessageOptions();   
            mgo.options |= MQC.MQGMO_NO_WAIT;   
  
            MQMessage msg = new MQMessage();   
            //log.debug("fetchOneMsg(q)="+fetchOneMsg(q));
            if ((msg = fetchOneMsg(q)) != null) {   
                byte[] xmlData = new byte[msg.getDataLength()];   
                msg.readFully(xmlData);   
                content = new String(xmlData, "UTF-8");  
//            	content = msg.readUTF();
//            	content = (String)msg.readObject();
                log.debug("====="+content);
            }   
        } catch (Exception e) {  
//        	insertMonitorErrLog(e);
        	log.error("MqUtil.recieve...........",e);
        }finally {
			try {
				if (q != null) {
					q.close();
				}
				if (qMgr != null) {
					qMgr.close();
					qMgr.disconnect();
				}
			} catch (Exception e) {
				e.printStackTrace();   
			}   
        }
        
        return content;
    }   
  
    /**  
     * 从队列中取出一个消息  
     * @param q  队列名称  
     * @return  
     * @throws Exception  
     */  
    private MQMessage fetchOneMsg(MQQueue q) throws Exception {
        MQGetMessageOptions mgo = new MQGetMessageOptions();   
        mgo.options |= MQC.MQGMO_NO_WAIT;   
        MQMessage msg = new MQMessage();   
        try {
            // 获取消息   
            q.get(msg, mgo);   
        } catch (MQException e) {   
            return null;   
        }   
        return msg;   
    }

    /**
     * 将异常信息插入到监控日志表中
     * @param e
     */
//    private void insertMonitorErrLog(Exception e){
//    	//挂起一小时，一小时后则再次向数据库中插入此类型的错误信息
//    	if(dateDiff() > 1){
//	    	//解析异常信息
//	    	ByteArrayOutputStream baos = new ByteArrayOutputStream();   
//	        PrintStream ps = new PrintStream(baos);   
//	    	e.printStackTrace(ps);
//	    	String logErrmsg = baos.toString();
//	    	if(ps != null)
//	    		ps.close();
//	    	if(baos != null){
//	    		try{
//	    			baos.close();
//	    		}catch(Exception exe){}
//	    	}
//	    	//插入日志表
//	    	Monitor monitor = new Monitor();
//	    	monitor.insertMonitorLog(monitor.getInterfaceLogNextID(), "1", "0", logErrmsg);
//	    	//挂 起一小时不向数据库插入此类错误信息
//			Date currentTime = new Date();
//			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//	    	mqErrorStartTime = formatter.format(currentTime);
//    	}
//    }

    /**
     * 比较时间方法，返回相差多少小时
     */
    public long dateDiff() {
    	long result = 0;
    	//按照传入的格式生成一个simpledateformate对象
    	SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	long nd = 1000*24*60*60;//一天的毫秒数
    	long nh = 1000*60*60;//一小时的毫秒数
//    	long nm = 1000*60;//一分钟的毫秒数
//    	long ns = 1000;//一秒钟的毫秒数
    	long diff;
    	try {
    		//获得两个时间的毫秒时间差异
    		diff = (new Date()).getTime() - sd.parse(mqErrorStartTime).getTime();
    		long day = diff/nd;//计算差多少天
    		long hour = diff%nd/nh;//计算差多少小时
//    		long min = diff%nd%nh/nm;//计算差多少分钟
//    		long sec = diff%nd%nh%nm/ns;//计算差多少秒
    		//输出结果
//    		log.debug("时间相差："+day+"天"+hour+"小时"+min+"分钟"+sec+"秒。");
    		result = day * 24 + hour; 
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    	return result;
    }
    
    /**
     * 将信息发送到指定烟厂的MQ中
     * targetMq：wz、cz、cd等烟厂标识
     */
    public void jt_send_targetmq_receive(String targetOrgMqSufix, String content) {
    	MQQueue q = null;
    	MQQueueManager qMgr = null;
        // MQ发送   
        try {   
            // 建立MQ客户端使用上下文  
            MQEnvironment.hostname = MQ_CONFIG_MAP.getStr(targetOrgMqSufix + "_read_mqHostName"); // 服务器ip地址
            MQEnvironment.port = Integer.parseInt(MQ_CONFIG_MAP.getStr(targetOrgMqSufix + "_read_mqPort")); // 服务器MQ服器端口
            MQEnvironment.CCSID = Integer.parseInt(MQ_CONFIG_MAP.getStr(targetOrgMqSufix + "_read_mqCCSID")); // 服务器MQ服务使用的编码
            MQEnvironment.userID = MQ_CONFIG_MAP.getStr(targetOrgMqSufix + "_read_mqUserName"); // MQ服务用户名
            MQEnvironment.password = MQ_CONFIG_MAP.getStr(targetOrgMqSufix + "_read_mqPassword");
            MQEnvironment.channel = MQ_CONFIG_MAP.getStr(targetOrgMqSufix + "_read_mqChannel"); // 服务器连接通道名

            // 链接队列管理器   
            qMgr = new MQQueueManager(MQ_CONFIG_MAP.getStr(targetOrgMqSufix + "_read_mqQManager"));
            //System.out.println(qMgr.name);

            int openOptions = MQC.MQOO_OUTPUT | MQC.MQOO_FAIL_IF_QUIESCING;  
            //System.out.println("openOptions="+openOptions);
            // 打开MQ队列   
            q = qMgr.accessQueue(MQ_CONFIG_MAP.getStr(targetOrgMqSufix + "_read_mqLocalOutQueue"), openOptions);
            //FileInputStream fins = new FileInputStream(new File(strExtraSendXmlFileName));   
            //InputStream fins = ClassLoader.getSystemResourceAsStream(strExtraSendXmlFileName);   
           // byte[] data = new byte[fins.available()]; 
            byte[] data=content.getBytes("UTF-8");
            //System.out.println("data="+new String(data));
            //fins.read(data);   
          //  fins.close();   
            MQMessage msg = new MQMessage();   
            msg.write(data);   
            // 放入消息   
            q.put(msg);   
            // 关闭队列   
            q.close();   
            // 关闭队列管理器   
            qMgr.disconnect();   
        } catch (Exception e) {  
        	log.error("MqUtil.jt_send_"+targetOrgMqSufix+"_receive...........",e);
        } finally {
			try {
				if (q != null) {
					q.close();
				}
				if (qMgr != null) {
					qMgr.close();
					qMgr.disconnect();
				}
			} catch (Exception e) {
			}   
        }  
    }


}   