package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import doc.doccenterserver.domain.fileresource.document.model.Role2resEntity;
import doc.doccenterserver.domain.fileresource.document.repository.Role2resRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.Role2resConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.Role2resMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.Role2resDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */

@Slf4j
@Component("Role2ResDORepository")
public class Role2resRepositoryImpl implements Role2resRepository {

    @Resource
    private Role2resMapper role2resMapper;

    @Override
    public List<Role2resEntity> getRes(String roleId) {
        LambdaQueryWrapper<Role2resDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Role2resDO::getRoleId, roleId);
        wrapper.ne(Role2resDO::getResId, "1");
        List<Role2resDO> role2resDOList = role2resMapper.selectList(wrapper);
        return Role2resConvertor.INSTANCE.dOToEntity(role2resDOList);
    }

    @Override
    public Integer delete(String roleId) {
        LambdaQueryWrapper<Role2resDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Role2resDO::getRoleId, roleId);
        return role2resMapper.delete(wrapper);
    }

    @Override
    public Integer insert(Role2resEntity role2resEntity) {
        Role2resDO role2resDO = Role2resConvertor.INSTANCE.entityToDO(role2resEntity);
        return role2resMapper.insert(role2resDO);
    }
}
