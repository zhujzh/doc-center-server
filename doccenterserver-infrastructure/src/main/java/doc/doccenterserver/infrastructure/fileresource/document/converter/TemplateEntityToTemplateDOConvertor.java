package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.TemplateEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.TempletDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface TemplateEntityToTemplateDOConvertor {

    TemplateEntityToTemplateDOConvertor INSTANCE = Mappers.getMapper(TemplateEntityToTemplateDOConvertor.class);

    @Mappings({})
    TempletDO entityToDO(TemplateEntity templateEntity);

    @Mappings({})
    TemplateEntity dOToEntity(TempletDO templetDO);

    @Mappings({})
    List<TemplateEntity> dOToEntityList(List<TempletDO> templetDOList);

    @Mappings({})
    List<TempletDO> entityToDOList(List<TemplateEntity> templateEntityList);
}
