package doc.doccenterserver.infrastructure.fileresource.message.job;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.xxl.job.core.handler.annotation.XxlJob;
import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import doc.doccenterserver.infrastructure.fileresource.message.mqs.IbmMq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 消息入库
 */
@Component
@Slf4j
public class MsgInJob {
    @Autowired
    private JobLogRepository jobLogRepository;
    @Autowired
    private JSONObject parameterMap;

    @XxlJob("msgIn")
    public void msgIn() throws Exception {
        log.info("调用了");
        JSONObject obj= JSONUtil.parseObj(parameterMap.getStr("MQ_CONFIG"));
        IbmMq ibmMq=new IbmMq(obj);
        String mqmsg = ibmMq.recieve();
        jobLogRepository.msgIn(mqmsg);
    }
}
