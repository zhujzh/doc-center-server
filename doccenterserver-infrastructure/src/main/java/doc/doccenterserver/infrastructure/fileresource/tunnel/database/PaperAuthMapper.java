package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PaperAuthDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文档浏览权限设置表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface PaperAuthMapper extends BaseMapper<PaperAuthDO> {
}
