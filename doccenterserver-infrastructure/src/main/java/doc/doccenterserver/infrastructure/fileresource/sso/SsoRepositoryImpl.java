package doc.doccenterserver.infrastructure.fileresource.sso;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import doc.doccenterserver.domain.fileresource.sso.vo.SsoTokenVo;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.domain.fileresource.system.repository.SsoRepository;
import doc.doccenterserver.infrastructure.fileresource.security.model.SsoModel;
import doc.doccenterserver.infrastructure.fileresource.security.service.TokenService;
import doc.doccenterserver.infrastructure.fileresource.sso.util.SsoUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author wangxp
 * @description sso 用户 认证实现
 * @date 2023/3/30 9:16
 */
@Component("ssoRepositoryImpl")
public class SsoRepositoryImpl implements SsoRepository {


    @Resource
    private TokenService tokenService;


    /**
     * @param code 验证码
     * @return SsoVo 认证信息
     * @description 根据验证码 获取认证信息
     * @author wangxp
     * @date 2023/3/30 9:33
     */
    @Override
    public SsoTokenVo getTokenByCode(String code) {
        SsoModel ssoModel = SsoUtils.getToken(code);
        if(ObjectUtil.isNotEmpty(ssoModel)){
            return BeanUtil.copyProperties(ssoModel, SsoTokenVo.class);
        }
        return null;
    }

    /**
     * @param ssoVo 认证信息
     * @param userModel 用户信息
     * @description 认证信息存储redis
     * @author wangxp
     * @date 2023/3/30 9:33
     */
    @Override
    public void saveRedisInfo(SsoTokenVo ssoVo, UserModel userModel) {
        tokenService.saveCache(BeanUtil.copyProperties(ssoVo,SsoModel.class),userModel);
    }

    @Override
    public void loginOut(HttpServletRequest request) {
        tokenService.loginOut(request);
    }

}
