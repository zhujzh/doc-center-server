package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.document.model.ChannelAuthEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.document.repository.ChannelAuthRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.ChannelAuthEntityToChannelAuthDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.ChannelAuthMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ChannelAuthDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("ChannelAuthDORepository")
public class ChannelAuthRepositoryImpl implements ChannelAuthRepository {

    @Resource
    private ChannelAuthMapper channelAuthMapper;

    @Override
    public List<ChannelAuthEntity> getChannelAuthLists(String id) {
        LambdaQueryWrapper<ChannelAuthDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ChannelAuthDO::getChannelId, id);
        List<ChannelAuthDO> channelAuthDOList = channelAuthMapper.selectList(wrapper);
        return ChannelAuthEntityToChannelAuthDOConvertor.INSTANCE.dOToEntityList(channelAuthDOList);
    }

    @Override
    public Integer deleteChannelAuthById(String id) {
        LambdaQueryWrapper<ChannelAuthDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ChannelAuthDO::getChannelId, id);
        return channelAuthMapper.delete(wrapper);
    }

    @Override
    public ChannelAuthEntity addChannelAuth(ChannelAuthEntity channelAuthEntity) {
        // 纯数字，是组织-ORG，否则为用户-USER
        channelAuthEntity.setObjectType(StringUtils.isNumeric(channelAuthEntity.getAuthObject()) ? ConstantSys.ORG : ConstantSys.USER);
        channelAuthMapper.insert(ChannelAuthEntityToChannelAuthDOConvertor.INSTANCE.entityToDO(channelAuthEntity));
        return channelAuthEntity;
    }

    @Override
    public List<ChannelAuthEntity> isChannelAuthList(UserEntity userEntity) {
        LambdaQueryWrapper<ChannelAuthDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ChannelAuthDO::getChannelId, userEntity.getChannelId());
        wrapper.and(wrap -> wrap.eq(ChannelAuthDO::getAuthObject, userEntity.getUserId()).or()
                .eq(ChannelAuthDO::getAuthObject, userEntity.getCompanyId()).or()
                .eq(ChannelAuthDO::getAuthObject, userEntity.getDeptId()).or()
                .eq(ChannelAuthDO::getAuthObject, userEntity.getOrgId()));
        List<ChannelAuthDO> channelAuthDOList = channelAuthMapper.selectList(wrapper);
        return ChannelAuthEntityToChannelAuthDOConvertor.INSTANCE.dOToEntityList(channelAuthDOList);
    }
}
