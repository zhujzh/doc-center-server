package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_pub_role`")
@NoArgsConstructor
public class RoleDO {

    @Field(name = "角色ID")
    @TableId(type = IdType.INPUT, value = "role_id")
    private String roleId;

    @Field(name = "角色名称")
    @TableField("role_name")
    private String roleName;

    @Field(name = "角色类型")
    @TableField("role_type")
    private String roleType;

    @Field(name = "角色排序号")
    @TableField("role_sort")
    private Integer roleSort;

    @Field(name = "角色所属组织")
    @TableField("role_org_id")
    private String roleOrgId;

    @Field(name = "角色所属系统")
    @TableField("role_app_id")
    private String roleAppId;

    @Field(name = "角色状态")
    @TableField("role_status")
    private String roleStatus;

    @Field(name = "角色创建时间")
    @TableField("role_create_time")
    private Date roleCreateTime;

    @Field(name = "角色创建者")
    @TableField("role_creator")
    private String roleCreator;

    @Field(name = "备注")
    @TableField("remark")
    private String remark;
}
