package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.AttachmentDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface AttachmentEntityToAttachmentDOConvertor {

    AttachmentEntityToAttachmentDOConvertor INSTANCE = Mappers.getMapper(AttachmentEntityToAttachmentDOConvertor.class);

    @Mappings({})
    AttachmentDO entityToDO(AttachmentEntity attachmentEntity);

    @Mappings({})
    AttachmentEntity dOToEntity(AttachmentDO attachmentDO);

    @Mappings({})
    List<AttachmentEntity> dOToEntityList(List<AttachmentDO> attachmentDOList);

    @Mappings({})
    List<AttachmentDO> entityToDOList(List<AttachmentEntity> attachmentEntityList);

}
