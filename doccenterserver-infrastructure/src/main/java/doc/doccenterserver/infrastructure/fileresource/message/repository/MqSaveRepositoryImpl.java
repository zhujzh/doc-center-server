package doc.doccenterserver.infrastructure.fileresource.message.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.MqSaveRepository;
import doc.doccenterserver.infrastructure.fileresource.message.enums.*;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.*;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.rmi.ServerException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 异常消息
 * repository实现类
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Slf4j
@Component("FILEMqSaveDORepository")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MqSaveRepositoryImpl extends ServiceImpl<MqSaveMapper, MqSaveDO> implements MqSaveRepository {
    private final JobLogMapper jobLogMapper;
    private final JobDetailLogMapper jobDetailLogMapper;
    private final MqChangelogMapper mqChangelogMapper;
    private final DocConvertChangelogMapper docConvertChangelogMapper;
    private final JobLogRepository jobLogRepository;

    @Override
    public void msgErr() {
        List<MqSaveDO> mqSaveDOList = getBaseMapper().selectList(null);
        for (MqSaveDO mqSave : mqSaveDOList) {
            try {
                this.processData(mqSave);
            } catch (Exception e) {
                jobLogRepository.updateError(mqSave.getLogId(), e.getMessage(), JobDetailLogStepIdEnum.STEP_ID_1.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_FAILD.getValue(), 0);
            }
        }
    }

    public void processData(MqSaveDO mqSave) throws ServerException {
        String sourceApp = mqSave.getSourceApp();
        String sourcePaperId = mqSave.getSourcePaperId();
        String archiveOrgCode = mqSave.getArchiveOrgCode();
        //2.更新应用ID、应用文档编号相同，但任务日志表主键不等于步骤1中的主键的 日志从表记录为忽略
        //3.删除应用ID、应用文档编号相同，但任务日志表主键不等于步骤1中的主键的记录
        //4.根据应用ID、应用文档编号、主日志状态为未完成，查找任务主日志表,判断是否存在
        //找出所有 相关 主日志表中 状态为 排队的数据
        LambdaQueryWrapper<JobLogDO> lqw = new LambdaQueryWrapper<>();
        lqw.eq(JobLogDO::getSourceAppId, sourceApp);
        lqw.eq(JobLogDO::getSourcePaperId, sourcePaperId);
        lqw.eq(JobLogDO::getArchiveOrgCode, archiveOrgCode);
        List<JobLogDO> joblist = jobLogMapper.selectList(lqw);
        //如果存在处理中 的数据库，退出等待处理完成
        List<JobLogDO> jobInglist = joblist.stream().filter(jobLog -> JobLogStatusEnum.STATUS_N.getValue().equals(jobLog.getStatus())).collect(Collectors.toList());
        if (!jobInglist.isEmpty()) {
            return;
        }
        //如果是创建，找到暂存表中第二条，判断是否是删除，如果不是，就抛出异常，如果是的话，就将第二天进行执行处理
        if (OperationTypeEnum.CREATE.getValue().equals(mqSave.getOperation())) {
            mqSave = this.create(mqSave, jobInglist);
        } else if (OperationTypeEnum.RETRIEVE.getValue().equals(mqSave.getOperation())) {
            //清除之前的所有排队信息
            this.retrieve(mqSave, jobInglist);
        } else {
            throw new ServerException("operation error !operation:" + mqSave.getOperation());
        }
        // N:更新任务日志主表为未完成、插入对应步骤的 changelog表、删除暂存日志表
        //删除暂存日志表'
        String logId = mqSave.getLogId();
        this.removeById(logId);
        //更新主任务日志主表为未完成
        jobLogRepository.updateStatus(logId, JobLogStatusEnum.STATUS_N.getValue());
        //执行任务
        JobDetailLogDO jobDetailLogDO = jobDetailLogMapper.selectList(new LambdaQueryWrapper<JobDetailLogDO>()
                        .eq(JobDetailLogDO::getLogId, logId)
                        .orderByDesc(JobDetailLogDO::getCreateTime))
                .stream().findAny().orElse(null);
        if (jobDetailLogDO == null) {
            return;
        }
        //消息入库changelog表
        if (JobDetailLogStepIdEnum.STEP_ID_1.getValue().equals(jobDetailLogDO.getStepId())) {
            MqChangelogDO mqChangelog = new MqChangelogDO(logId, mqSave.getContent(), new Date(), mqSave.getOperation(), 0, mqSave.getArchiveOrgCode());
            mqChangelogMapper.insert(mqChangelog);
            //写入文档转换任务change表
        } else if (JobDetailLogStepIdEnum.STEP_ID_4.getValue().equals(jobDetailLogDO.getStepId())) {
            DocConvertChangelogDO docConvertChangelog = new DocConvertChangelogDO(logId, new Date(), mqSave.getOperation(), mqSave.getSourcePaperId(), 0);
            docConvertChangelogMapper.insert(docConvertChangelog);
        }
    }

    public MqSaveDO create(MqSaveDO mqSave, List<JobLogDO> joblist) {
        String logId = mqSave.getLogId();
        String sourceApp = mqSave.getSourceApp();
        String sourcePaperId = mqSave.getSourcePaperId();
        String archiveOrgCode = mqSave.getArchiveOrgCode();
        LambdaQueryWrapper<MqSaveDO> lqwmq = new LambdaQueryWrapper<>();
        lqwmq.eq(MqSaveDO::getSourceApp, sourceApp);
        lqwmq.eq(MqSaveDO::getSourcePaperId, sourcePaperId);
        lqwmq.eq(MqSaveDO::getArchiveOrgCode, archiveOrgCode);
        lqwmq.orderByDesc(MqSaveDO::getCreateTime);
        List<MqSaveDO> mqsavelist = getBaseMapper().selectList(lqwmq);
        //如果暂存表中包含数据超过1条（大于等于2条），判断第二条是否是删除，如果不是，说明归档有问题，重复归档新增需要报错
        if (mqsavelist.isEmpty() || mqsavelist.size() == 1) {
            return mqSave;
        }
        MqSaveDO finalMqSave;
        if (OperationTypeEnum.CREATE.getValue().equals(mqSave.getOperation())) {
            finalMqSave = mqsavelist.get(1);
            List<JobLogDO> jobAwaitlist = joblist.stream()
                    .filter(jobLog -> JobLogStatusEnum.STATUS_Q.getValue().equals(jobLog.getStatus())
                            && !jobLog.getLogId().equals(logId)
                            && !jobLog.getLogId().equals(finalMqSave.getLogId()))
                    .collect(Collectors.toList());
            this.removeList(jobAwaitlist);
        } else {
            finalMqSave = mqsavelist.get(0);
            List<JobLogDO> jobAwaitlist = joblist.stream()
                    .filter(jobLog -> JobLogStatusEnum.STATUS_Q.getValue().equals(jobLog.getStatus())
                            && !jobLog.getLogId().equals(finalMqSave.getLogId()))
                    .collect(Collectors.toList());
            this.removeList(jobAwaitlist);
        }
        return finalMqSave;
    }

    public void retrieve(MqSaveDO mqSave, List<JobLogDO> joblist) {
        String logId = mqSave.getLogId();
        //清除之前的所有排队信息
        List<JobLogDO> jobAwaitlist = joblist.stream()
                .filter(jobLog -> JobLogStatusEnum.STATUS_Q.getValue().equals(jobLog.getStatus())
                        && !jobLog.getLogId().equals(logId))
                .collect(Collectors.toList());
        this.removeList(jobAwaitlist);
    }

    public void removeList(List<JobLogDO> jobAwaitlist) {
        for (JobLogDO jobLog : jobAwaitlist) {
            //删除暂存日志表
            this.removeById(jobLog.getLogId());
            //更新主表 为忽略
            jobLogRepository.updateStatus(jobLog.getLogId(), JobLogStatusEnum.STATUS_IGNOR.getValue());
        }
    }
}
