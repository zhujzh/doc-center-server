package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 频道操作权限表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`T_CMS_D_CHANNEL_AUTH`")
@NoArgsConstructor
public class ChannelAuthDO {

    @Field(name = "频道编号")
    @TableField("channel_id")
    private String channelId;

    @Field(name = "授权对象")
    @TableField("auth_object")
    private String authObject;

    @Field(name = "对象类型")
    @TableField("object_type")
    private String objectType;

    @Field(name = "相应的权限值")
    @TableField("auth_value")
    private Integer authValue;
}
