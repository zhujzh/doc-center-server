package doc.doccenterserver.infrastructure.fileresource.message.utils;

import cn.hutool.core.util.StrUtil;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.MeetingDO;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


public class JdomXmlUtil {
	private static Logger log = LoggerFactory.getLogger(JdomXmlUtil.class);
	//预处理 修正 xml
	
	//预处理 修正 xml
	public static String precorrectxml(String mqXml){
		if(mqXml == null || "".equals(mqXml)){
        	return "";
        }else{
	    	//插入日志表（MQ服务正常）
	    	mqXml = mqXml.replaceAll("< pe>","</type>");
	    	
			if(mqXml.indexOf("<context><!") == -1){
	            mqXml = mqXml.replaceAll("<context>","<context><![CDATA[");
	            mqXml = mqXml.replaceAll("</context>","]]></context>");
			}		            
            
            if(mqXml.indexOf("<category><!") == -1){
	            mqXml = mqXml.replaceAll("<category>","<category><![CDATA[");
	            mqXml = mqXml.replaceAll("</category>","]]></category>");
            }
            
            if(mqXml.indexOf("<subject><!") == -1){
	            mqXml = mqXml.replaceAll("<subject>","<subject><![CDATA[");
	            mqXml = mqXml.replaceAll("</subject>","]]></subject>");
            }
            if(mqXml.indexOf("<name><!") == -1){
	            mqXml = mqXml.replaceAll("<name>","<name><![CDATA[");
	            mqXml = mqXml.replaceAll("</name>","]]></name>");
            }

            if(mqXml.indexOf("<absurl><!") == -1){
	            mqXml = mqXml.replaceAll("<absurl>","<absurl><![CDATA[");
	            mqXml = mqXml.replaceAll("</absurl>","]]></absurl>");
            }
            
            if(mqXml.indexOf("&amp;#8226;") != -1){
	            mqXml = mqXml.replaceAll("&amp;#8226;","•");
            }
            if(mqXml.indexOf("&amp;") != -1){
	            mqXml = mqXml.replaceAll("&amp;","&");
            }
            if(mqXml.indexOf("&lt;") != -1){
	            mqXml = mqXml.replaceAll("&lt;","<");
            }
            if(mqXml.indexOf("&gt;") != -1){
	            mqXml = mqXml.replaceAll("&gt;",">");
            }
//            if(mqXml.indexOf("&nbsp;") != -1){
//	            mqXml = mqXml.replaceAll("&nbsp;"," ");
//            }
//            if(mqXml.indexOf("&nbsp") != -1){
//	            mqXml = mqXml.replaceAll("&nbsp"," ");
//            }
            if(mqXml.indexOf("&quot;") != -1){
	            mqXml = mqXml.replaceAll("&quot;","\"");
            }
        }
        return mqXml;
	}
	
	//辅助方法：查找指定PATH的XML节点信息
	public static Element getSingleNode(Element root, String nodeName) throws JDOMException{
		Element e = (Element)XPath.selectSingleNode(root,nodeName);
		return e;
	}
	
	public static Element getElementFromDocument(Document doc, String nodeName) throws JDOMException{
		return (Element)XPath.selectSingleNode(doc.getRootElement(),nodeName);
	}
	
	public static Element getElementFromElement(Element element, String nodeName) throws JDOMException{
		return (Element)XPath.selectSingleNode(element,nodeName);
	}
	
	//辅助方法：查询指定PATH下的XML节点列表信息
	public static List getElementListFromDocument(Document doc, String nodeName) throws JDOMException{
		 return XPath.selectNodes(doc.getRootElement(),nodeName);
	}
		
		
	//辅助方法：查找指定PATH的XML节点文本内容
	public static String getTextvalueFromDocument(Document doc, String nodeName) throws JDOMException{
		if(null == doc){
			return null;
		}
		Element e = (Element)XPath.selectSingleNode(doc.getRootElement(),nodeName);
		if(null != e){
			return e.getTextTrim();
		}
		return null;
	}
	
	
	
	public static Document xml2Document(String xmlstr) throws Exception{
		SAXBuilder builder = new SAXBuilder();
		return builder.build(new StringReader(precorrectxml(xmlstr)));
	}
	 
    public static String txt2String(File file){
        StringBuilder result = new StringBuilder();
        try{
            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                result.append(System.lineSeparator()+s);
            }
            br.close();    
        }catch(Exception e){
            e.printStackTrace();
        }
        return result.toString();
    }
    
	 public  static void main(String[] args){
//		 String xmlstr = "<root><head><servicecode>OATOEKP</servicecode><sender>OA</sender><serialnumber>1464569749166</serialnumber><bizid>0003</bizid><SENDQ>BZ.OA.SEND</SENDQ></head><kmdoc><ip>10.158.129.29</ip><from>OA</from><id>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c8c4</id><category>[收文]</category><subject>国家烟草专卖局办公室关于转发电子商务统计报表制度的通知</subject><creator>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</creator><created>2016-05-30 08:55:46</created><docauthor>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</docauthor><keywords/><useyears>0</useyears><description/><context/><printcopy>0</printcopy><readers>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=jintl0124,cn=employees,dc=hngytobacco,dc=com;uid=yangzhm0819,cn=employees,dc=hngytobacco,dc=com;uid=liujf1103,cn=employees,dc=hngytobacco,dc=com;uid=lil0801,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</readers><authors/><attcopy/><copyusers/><attprint/><printusers/><attdownload/><dwldusers/><attachments><attachment><name>国家烟草专卖局办公室关于转发电子商务统计报表制度的通知.doc</name><access>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c8c4</access><type>1</type></attachment></attachments><operation>create</operation><publish>1</publish><toporgcode>cn=20430001,cn=43,cn=0,dc=hngytobacco,dc=com</toporgcode><archiveorgcode>20430001</archiveorgcode><remark>（不公开）</remark><meetingstarttime/><meetingendtime/><formname>收文</formname><permision>u:zhoujl0617;u:jintl0124;u:yangzhm0819;u:liujf1103;u:lil0801;u:zhoujl0617;u:zhoujl0617;u:zhoujl0617</permision><resourceid>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c8c4</resourceid><channelid>101080106</channelid><changlogid>1464569749166</changlogid><addition><element id="channelname">收文</element></addition><extension1/></kmdoc></root>";
//		 String mqmsg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root><head><servicecode>OATOEKP</servicecode><sender>OA</sender><serialnumber>1464569749166</serialnumber><bizid>0003</bizid><SENDQ>BZ.OA.SEND</SENDQ></head><kmdoc><ip>10.158.129.29</ip><from>OA</from><id>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c8c4</id><category><![CDATA[[收文]]><![CDATA[]]]></category><subject><![CDATA[国家烟草专卖局办公室关于转发电子商务统计报表制度的通知]]></subject><creator>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</creator><created>2016-05-30 08:55:46</created><docauthor>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</docauthor><keywords/><useyears>0</useyears><description/><context/><printcopy>0</printcopy><readers>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=jintl0124,cn=employees,dc=hngytobacco,dc=com;uid=yangzhm0819,cn=employees,dc=hngytobacco,dc=com;uid=liujf1103,cn=employees,dc=hngytobacco,dc=com;uid=lil0801,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</readers><authors/><attcopy/><copyusers/><attprint/><printusers/><attdownload/><dwldusers/><attachments><attachment><name><![CDATA[国家烟草专卖局办公室关于转发电子商务统计报表制度的通知.doc]]></name><access>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c8c4</access><type>1</type></attachment></attachments><operation>create</operation><publish>1</publish><toporgcode>cn=20430001,cn=43,cn=0,dc=hngytobacco,dc=com</toporgcode><archiveorgcode>20430001</archiveorgcode><remark>（不公开）</remark><meetingstarttime/><meetingendtime/><formname>收文</formname><permision>u:zhoujl0617;u:jintl0124;u:yangzhm0819;u:liujf1103;u:lil0801;u:zhoujl0617;u:zhoujl0617;u:zhoujl0617</permision><resourceid>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c8c4</resourceid><channelid>101080106</channelid><changlogid>1464569749166</changlogid><addition><element id=\"channelname\">收文</element></addition><extension1/></kmdoc></root>";
//		 String mqmsg = "<root><head><servicecode>OATOEKP</servicecode><sender>OA</sender><serialnumber>1464569749166</serialnumber><bizid>0003</bizid><SENDQ>BZ.OA.SEND</SENDQ></head><kmdoc><ip>10.158.129.29</ip><from>OA</from><id>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c8c4</id><category><![CDATA[[收文]]><![CDATA[]]]></category><subject><![CDATA[国家烟草专卖局办公室关于转发电子商务统计报表制度的通知]]></subject><creator>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</creator><created>2016-05-30 08:55:46</created><docauthor>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</docauthor><keywords/><useyears>0</useyears><description/><context/><printcopy>0</printcopy><readers>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=jintl0124,cn=employees,dc=hngytobacco,dc=com;uid=yangzhm0819,cn=employees,dc=hngytobacco,dc=com;uid=liujf1103,cn=employees,dc=hngytobacco,dc=com;uid=lil0801,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</readers><authors/><attcopy/><copyusers/><attprint/><printusers/><attdownload/><dwldusers/><attachments><attachment><name><![CDATA[国家烟草专卖局办公室关于转发电子商务统计报表制度的通知.doc]]></name><access>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c8c4</access><type>1</type></attachment></attachments><operation>create</operation><publish>1</publish><toporgcode>cn=20430001,cn=43,cn=0,dc=hngytobacco,dc=com</toporgcode><archiveorgcode>20430001</archiveorgcode><remark>（不公开）</remark><meetingstarttime/><meetingendtime/><formname>收文</formname><permision>u:zhoujl0617;u:jintl0124;u:yangzhm0819;u:liujf1103;u:lil0801;u:zhoujl0617;u:zhoujl0617;u:zhoujl0617</permision><resourceid>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c8c4</resourceid><channelid>101080106</channelid><changlogid>1464569749166</changlogid><addition><element id=\"channelname\">收文</element></addition><extension1/></kmdoc></root>";
//		 String mqmsg = "<root><head><servicecode>OATOEKP</servicecode><sender>OA</sender><serialnumber>1469068626196</serialnumber><bizid>0003</bizid><SENDQ>BZ.OA.SEND</SENDQ></head><kmdoc><ip>10.158.129.29</ip><from>OA</from><id>bf32c0f-15605fb3fc4-8a52a0d51510de2bef6158d80746c8c4</id><category><![CDATA[[收文]]]></category><subject><![CDATA[国家烟草专卖局办公室关于落实《中华人民共和国境内卷烟包装标识的规定》电视电话会议的通知]]></subject><creator>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</creator><created>2016-07-21 10:37:03</created><docauthor>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</docauthor><keywords/><useyears>0</useyears><description/><context/><printcopy>0</printcopy><readers>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=jintl0124,cn=employees,dc=hngytobacco,dc=com;uid=lup0223,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=huangxsh0110,cn=employees,dc=hngytobacco,dc=com;uid=huangxsh0110,cn=employees,dc=hngytobacco,dc=com;uid=huangxsh0110,cn=employees,dc=hngytobacco,dc=com;cn=43,cn=0,dc=hngytobacco,dc=com</readers><authors/><attcopy/><copyusers/><attprint/><printusers/><attdownload/><dwldusers/><attachments><attachment><name><![CDATA[国家烟草专卖局办公室关于落实《中华人民共和国境内卷烟包装标识的规定》电视电话会议的通知.doc]]></name><access>bf32c0f-15605fb3fc4-8a52a0d51510de2bef6158d80746c8c4</access><type>1</type></attachment></attachments><operation>create</operation><publish>1</publish><toporgcode>cn=20430001,cn=43,cn=0,dc=hngytobacco,dc=com</toporgcode><archiveorgcode>20430001</archiveorgcode><remark>（可公开）</remark><meetingstarttime/><meetingendtime/><formname>收文</formname><permision>all</permision><resourceid>bf32c0f-15605fb3fc4-8a52a0d51510de2bef6158d80746c8c4</resourceid><channelid>101080106</channelid><changlogid>1469068626196</changlogid><addition><element id=\"channelname\">收文</element></addition><extension1/></kmdoc></root>";
//		 String mqmsg = "<root><head><servicecode>ZYSTDDTOZYECM</servicecode> <sender>ZYSTDD</sender><serialnumber>1479885808055</serialnumber><bizid>0003</bizid><SENDQ>BZ.ZYCOP.SEND</SENDQ></head><kmdoc><ip>10.158.133.206</ip><from>ZYSTDD</from><id>ZYSTDD5cabd349-1eae-4622-9111-ae0626c7989b2</id><operation>create</operation><absurl></absurl><category><![CDATA[4]]></category><subject><![CDATA[监视与测量装置管理办法细则]]></subject><creator>huangjy1101</creator><created>2016-11-23 15:23:28</created><docauthor>huangjy1101</docauthor><keywords/><useyears>0</useyears><description/><context></context><publish>1</publish><printcopy>0</printcopy><readers></readers><authors/><attcopy/><copyusers/><attprint/><printusers/><attdownload/><dwldusers/><attachments><attachment><name><![CDATA[GJ01监视与测量装置管理办法细则161123.doc]]></name><access><![CDATA[5cabd349-1eae-4622-9111-ae0626c7989b]]></access><type>1</type></attachment></attachments><toporgcode>2043000153</toporgcode><archiveorgcode>2043000153</archiveorgcode><remark></remark><meetingstarttime></meetingstarttime><meetingendtime></meetingendtime><formname></formname><extension1></extension1><extension2></extension2><permision>g:2043000153</permision><resourceid>ZYSTDD5cabd349-1eae-4622-9111-ae0626c7989b1</resourceid><channelid>1040401</channelid><changlogid>1479885808055</changlogid><addition></addition> </kmdoc></root> ";
//		 String mqmsg ="<root><head><servicecode>OATOEKP</servicecode><sender>OA</sender><serialnumber>1481162674030</serialnumber><bizid>0003</bizid><SENDQ>BZ.OA.SEND</SENDQ></head><kmdoc><ip>10.158.129.29</ip><from>OA</from><id>68d57dbf-158dbedc5e5-8a52a0d51510de2bef6158d80746c8c4</id><category>[会议通知]</category><subject>关于召开公司第四次政工例会的通知</subject><creator>uid=zhounzh0922,cn=employees,dc=hngytobacco,dc=com</creator><created>2016-12-08 10:04:32</created><docauthor>uid=zhounzh0922,cn=employees,dc=hngytobacco,dc=com</docauthor><keywords/><useyears>0</useyears><description/><context/><printcopy>0</printcopy><readers>uid=zhounzh0922,cn=employees,dc=hngytobacco,dc=com;uid=ouyhy1210,cn=employees,dc=hngytobacco,dc=com;uid=zhaoy0905,cn=employees,dc=hngytobacco,dc=com;uid=ouyhy1210,cn=employees,dc=hngytobacco,dc=com;uid=zhaoy0905,cn=employees,dc=hngytobacco,dc=com;uid=zhaoy0905,cn=employees,dc=hngytobacco,dc=com;uid=zhounzh0922,cn=employees,dc=hngytobacco,dc=com;uid=zhaoy0905,cn=employees,dc=hngytobacco,dc=com;uid=zhaoy0905,cn=employees,dc=hngytobacco,dc=com;cn=43,cn=0,dc=hngytobacco,dc=com</readers><authors/><attcopy/><copyusers/><attprint/><printusers/><attdownload/><dwldusers/><attachments><attachment><name>关于召开公司第四次政工例会的通知.doc</name><access>68d57dbf-158dbedc5e5-8a52a0d51510de2bef6158d80746c8c4</access><type>1</type></attachment></attachments><operation>create</operation><publish>1</publish><toporgcode>cn=20430001,cn=43,cn=0,dc=hngytobacco,dc=com</toporgcode><archiveorgcode>20430001</archiveorgcode><remark>是</remark><meetingstarttime>2016-12-14 08:40</meetingstarttime><meetingendtime>2016-12-14 17:00</meetingendtime><formname>会议通知</formname><permision>all</permision><resourceid>68d57dbf-158dbedc5e5-8a52a0d51510de2bef6158d80746c8c4</resourceid><channelid>1010103</channelid><changlogid>1481162674030</changlogid><addition><element id=\"channelname\">会议通知</element></addition><extension1><![CDATA[<meetingstarttime>2016-12-14 08:40</meetingstarttime><meetingendtime>2016-12-14 17:00</meetingendtime>]]></extension1></kmdoc></root>";
		 String mqmsg = txt2String(	new File("D:/errlog.txt"));
		 try {
			 
			 SAXBuilder builder = new SAXBuilder();
			 StringReader read = new StringReader(precorrectxml(mqmsg));
			Document doc = builder.build(read);
//			List<Element> ddd = getElementListFromDocument(doc,"/root/kmdoc/attachments/attachment");
//			for(Element e :ddd){
//				Element el = getElementFromElement(e,"access");
//				System.out.println("3:"+el.getTextTrim());
//			}
//			Element atts = getElementFromDocument(doc,"/root/kmdoc/attachments");
//			
//			System.out.println(getTextvalueFromDocument(doc,"/root/kmdoc/attachments/attachment/access"));
//			
//			 List<Element> ddsd = atts.getChildren();
//			 for(Element e :ddsd){
//				 System.out.println("2"+e.getTextTrim());
//				 
//			}
			getMeetingEntry(doc);
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	 }
	 
	public	static MeetingDO getMeetingEntry(Document doc) throws Exception{
		MeetingDO meeting = null;
			//会议起始时间
			String meetingstarttime = JdomXmlUtil.getTextvalueFromDocument(doc,"/root/kmdoc/meetingstarttime");
			String meetingendtime = JdomXmlUtil.getTextvalueFromDocument(doc,"/root/kmdoc/meetingendtime");
			if(StrUtil.isNotBlank(meetingstarttime) && StrUtil.isNotBlank(meetingendtime)){
				String[] meetingstarttimes = meetingstarttime.split(":");
				if(meetingstarttimes.length == 1){
					meetingstarttime += " 00:00:00";
				}
				else if(meetingstarttimes.length == 2){
					meetingstarttime += ":00";
				}
				String[] meetingendtimes = meetingendtime.split(":");
				if(meetingendtimes.length == 1){
					meetingendtime += " 00:00:00";
				}
				else if(meetingendtimes.length == 2){
					meetingendtime += ":00";
				}
				meeting = new MeetingDO();
				LocalDateTime startTime= LocalDateTime.parse(meetingstarttime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
				LocalDateTime endTime= LocalDateTime.parse(meetingendtime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

				meeting.setStartTime(startTime);
				meeting.setEndTime(endTime);
				String meetingplace = JdomXmlUtil.getTextvalueFromDocument(doc,"/root/kmdoc/meetingplace");
				meeting.setAddress(meetingplace);
			}	
			return meeting;
		}
}
