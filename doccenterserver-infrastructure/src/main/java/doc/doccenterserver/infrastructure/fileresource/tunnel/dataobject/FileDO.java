package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文件存储表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_file`")
@NoArgsConstructor
public class FileDO {

    @Field(name = "主键")
    @TableId("file_id")
    private String fileId;

    @Field(name = "文件名称")
    @TableField("filename")
    private String filename;

    @Field(name = "文件保存路径")
    @TableField("file_path")
    private String filePath;

    @Field(name = "文件大小")
    @TableField("file_size")
    private Double fileSize;

    @Field(name = "文件后缀")
    @TableField("file_format")
    private String fileFormat;

    @Field(name = "文件MD5值")
    @TableField("file_md5")
    private String fileMd5;

    @Field(name = "应用ID")
    @TableField("app_id")
    private String appId;

    @Field(name = "上传时间")
    @TableField("create_date")
    private Date createDate;

    @Field(name = "修改时间")
    @TableField("update_date")
    private Date updateDate;

    @Field(name = "删除标记")
    @TableField("delete_flag")
    private String deleteFlag;

    @Field(name = "删除时间")
    @TableField("delete_date")
    private Date deleteDate;

    @Field(name = "bigdataFileId")
    @TableField("bigdata_file_id")
    private String bigdataFileId;

    @Field(name = "LABEL_NAME")
    @TableField("label_name")
    private String labelName;

    @Field(name = "IS_UPLOAD")
    @TableField("is_upload")
    private String isUpload;
}
