package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.domain.fileresource.document.model.User2roleEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.User2roleDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 用户角色关联表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface User2roleMapper extends BaseMapper<User2roleDO> {
    List<User2roleEntity> findAccountInfoListByUserId(String userId);

}
