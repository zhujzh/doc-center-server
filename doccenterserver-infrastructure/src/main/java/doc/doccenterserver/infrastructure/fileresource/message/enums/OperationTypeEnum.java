package doc.doccenterserver.infrastructure.fileresource.message.enums;

public enum OperationTypeEnum {
    CREATE("create", "创建"),
    UPDATE("update", "更新，网盘独有操作"),
    RETRIEVE("retrieve", "删除")
    ;
    private final String value;
    private final String name;
    OperationTypeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }
    public String getValue() {
        return this.value;
    }

}
