package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.TPanCollectFolderDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Tangcancan
 */

@Mapper
public interface TPanCollectFolderMapper extends BaseMapper<TPanCollectFolderDO> {
}
