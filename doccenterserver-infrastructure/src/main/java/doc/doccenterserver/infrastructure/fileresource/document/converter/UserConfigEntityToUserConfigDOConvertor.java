package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.UserConfigEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.UserConfigDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface UserConfigEntityToUserConfigDOConvertor {

    UserConfigEntityToUserConfigDOConvertor INSTANCE = Mappers.getMapper(UserConfigEntityToUserConfigDOConvertor.class);

    @Mappings({})
    UserConfigDO entityToDO(UserConfigEntity userConfigEntity);

    @Mappings({})
    UserConfigEntity dOToEntity(UserConfigDO userConfigDO);

    @Mappings({})
    List<UserConfigEntity> dOToEntityList(List<UserConfigDO> userConfigDOList);

    @Mappings({})
    List<UserConfigDO> entityToDOList(List<UserConfigEntity> userConfigEntityList);

}
