package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.UserDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface UserEntityToUserDOConvertor {

    UserEntityToUserDOConvertor INSTANCE = Mappers.getMapper(UserEntityToUserDOConvertor.class);

    @Mappings({})
    UserDO entityToDO(UserEntity userEntity);

    @Mappings({})
    UserEntity dOToEntity(UserDO userDO);

    @Mappings({})
    List<UserEntity> dOToEntityList(List<UserDO> userDOList);

    @Mappings({})
    List<UserDO> entityToDOList(List<UserEntity> userEntityList);

}
