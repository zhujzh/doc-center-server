package doc.doccenterserver.infrastructure.fileresource.es.bean;

import lombok.Data;

@Data
public class OrderParam {
	private String name;
	private String order;
}
