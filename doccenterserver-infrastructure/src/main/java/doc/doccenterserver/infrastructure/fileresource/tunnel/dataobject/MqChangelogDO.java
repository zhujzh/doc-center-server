package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 消息入库
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_mq_changelog`")
@NoArgsConstructor
public class MqChangelogDO {

    @Field(name = "日志流水号")
    @TableId(type = IdType.INPUT, value = "log_id")
    private String logId;

    @Field(name = "信息的XML内容")
    @TableField("content")
    private String content;

    @Field(name = "源应用文档编号")
    @TableField("source_paper_id")
    private String sourcePaperId;

    @Field(name = "MQ信息的发送方")
    @TableField("source_app")
    private String sourceApp;

    @Field(name = "信息归档操作类型")
    @TableField("operation")
    private String operation;

    @Field(name = "错误重试次数")
    @TableField("retry_times")
    private Integer retryTimes;

    @Field(name = "信息产生时间")
    @TableField("create_time")
    private Date createTime;

    @Field(name = "归档组织ID")
    @TableField("archive_org_code")
    private String archiveOrgCode;
    public MqChangelogDO(String logId, String content, Date createTime,
                         String operation, Integer retryTimes,String archiveOrgCode) {
        super();
        this.setLogId ( logId);
        this.setContent ( content);
        this.setCreateTime ( createTime);
        this.setOperation ( operation);
        this.setRetryTimes ( retryTimes);
        this.setArchiveOrgCode( archiveOrgCode);
    }
}
