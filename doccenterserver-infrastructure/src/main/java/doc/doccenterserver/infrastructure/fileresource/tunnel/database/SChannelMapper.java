package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.SChannelDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 搜索 - 频道分类表、用于在搜索首页展现分类Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface SChannelMapper extends BaseMapper<SChannelDO> {
}
