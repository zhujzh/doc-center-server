package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.GroupMemberEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.GroupMemberDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;


/**
 * @author Tangcancan
 */
@Mapper
public interface GroupMemberConvertor {

    GroupMemberConvertor INSTANCE = Mappers.getMapper(GroupMemberConvertor.class);

    @Mappings({})
    GroupMemberDO entityToDO(GroupMemberEntity parameter);

    @Mappings({})
    GroupMemberEntity dOToEntity(GroupMemberDO parameter);

    @Mappings({})
    List<GroupMemberEntity> dOToEntity(List<GroupMemberDO> parameter);

    @Mappings({})
    List<GroupMemberDO> entityToDO(List<GroupMemberEntity> parameter);
}
