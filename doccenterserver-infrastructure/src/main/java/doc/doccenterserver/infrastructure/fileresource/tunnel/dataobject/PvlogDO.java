package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 操作日志
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_pub_pvlog`")
@NoArgsConstructor
public class PvlogDO {

    @Field(name = "主键")
    @TableId(type = IdType.INPUT, value = "pv_id")
    private String pvId;

    @Field(name = "当前操作的用户id")
    @TableField("user_id")
    private String userId;

    @Field(name = "当前操作的用户名称")
    @TableField("user_name")
    private String userName;

    @Field(name = "当前操作用户的部门id")
    @TableField("org_id")
    private String orgId;

    @Field(name = "当前操作用户的部门名称")
    @TableField("org_name")
    private String orgName;

    @Field(name = "当前操作用户所操作的菜单id")
    @TableField("menu_id")
    private String menuId;

    @Field(name = "操作时间")
    @TableField("visit_time")
    private Date visitTime;

    @Field(name = "当前用户所使用的浏览器类型")
    @TableField("browse")
    private String browse;

    @Field(name = "操作类型")
    @TableField("operate_type")
    private String operateType;
}
