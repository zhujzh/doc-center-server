package doc.doccenterserver.infrastructure.fileresource.message.enums;

/**
 * 发布状态
 */
public enum PublicFlagEnum {
    PAPER_PUBLIC_FLAG_DRAFT(0, "草稿"),
    PAPER_PUBLIC_FLAG_CANCEL(1, "已撤销"),
    PAPER_PUBLIC_FLAG_ISSUE(2, "已发布"),
    PAPER_PUBLIC_FLAG_ISSUING(3, "发布中"),
    PAPER_PUBLIC_FLAG_AUDIT(9, "审核中"),
    PAPER_PUBLIC_FLAG_BACK(-1, "退回"),
    PAPER_PUBLIC_FLAG_CONVERTING(99, "正在转换"),
    ;
    private final Integer value;
    private final String name;
    PublicFlagEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }
    public Integer getValue() {
        return this.value;
    }

}
