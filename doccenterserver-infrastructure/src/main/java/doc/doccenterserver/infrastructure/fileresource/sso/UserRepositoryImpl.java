package doc.doccenterserver.infrastructure.fileresource.sso;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import doc.doccenterserver.domain.fileresource.exception.BusinessException;
import doc.doccenterserver.domain.fileresource.exception.ExceptionEnum;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.domain.fileresource.system.repository.UserRepository;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcUserPositionService;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcUserService;
import doc.doccenterserver.infrastructure.fileresource.security.model.UcUserModel;
import doc.doccenterserver.infrastructure.fileresource.security.properties.SsoTokenProperties;
import doc.doccenterserver.infrastructure.fileresource.security.properties.UcCenterProperties;
import doc.doccenterserver.infrastructure.fileresource.system.converter.UserToUserDOConverter;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.UserMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.UserDO;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;


/**
 * @author wangxp
 * @description 用户 实现
 * @date 2023/3/29 20:50
 */
@Component("userRepositoryImpl")
public class UserRepositoryImpl implements UserRepository {


    @Resource
    private SsoTokenProperties ssoTokenProperties;
    @Resource
    private UcUserService ucUserService;

    /**
     * @param accessToken token
     * @return UserModel
     * @description 通过token 获取用户信息
     * @author wangxp
     * @date 2023/3/29 20:51
     */
    @Override
    public UserModel getUserInfoByToken(String accessToken) {
        Map<String, Object> formMap = new HashMap<>();
        formMap.put("access_token",accessToken);
        HttpResponse response = HttpRequest.post(ssoTokenProperties.getSsoApiUrl() + "api/userinfo")
                .form(formMap)
                .execute();
        UserModel userModel = null;
        if(response.isOk()){
            JSONObject obj = JSONUtil.parseObj(response.body());
            if("0".equals(obj.getStr("code"))){
                userModel = JSONUtil.toBean(obj.getJSONObject("data"), UserModel.class);
            }
        }
        return userModel;
    }

    @Override
    public UserEntity getUserInfo(String userId){
       return ucUserService.getUserInfoByUserCode(userId);
    }


}
