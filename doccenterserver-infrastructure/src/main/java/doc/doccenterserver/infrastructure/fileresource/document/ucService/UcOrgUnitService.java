package doc.doccenterserver.infrastructure.fileresource.document.ucService;

import com.tobacco.mp.uc.client.api.org.dto.OrgNodeUnitDTO;
import com.tobacco.mp.uc.client.api.org.dto.QueryParentOrgUnitsInOrgDTO;
import com.tobacco.mp.uc.kz.client.api.dto.ListOrgRootOrgUnitsResponse;
import com.tobacco.mp.uc.kz.client.api.dto.OrgUnitResponse;
import com.tobacco.mp.uc.kz.client.api.dto.QueryChildrenOrgUnitsInOrgResponse;

import java.util.Collection;
import java.util.List;

/**
 * @author wangxp
 * @description 用户中心扩展-组织
 * @date 2023/6/13 16:49
 */
public interface UcOrgUnitService {

    /**
     * @param parentUnitId 上级id
     * @return Collection<QueryChildrenOrgUnitsInOrgResponse>
     * @description 根据 上级组织单元id 查询组织列表
     * @author wangxp
     * @date 2023/6/13 16:51
     */
    Collection<QueryChildrenOrgUnitsInOrgResponse> getOrgByParentId(String parentUnitId);

    /**
     * @param bizCode 组织id
     * @return OrgUnitResponse
     * @description 根据组织id查询 组织单元
     * @author wangxp
     * @date 2023/6/13 16:56
     */
    OrgUnitResponse queryUcOrgNodeByBizCode(String bizCode);



    /**
     * @return   Collection<ListOrgRootOrgUnitsResponse>
     * @description  查询根组织节点
     * @author wangxp
     * @date 2023/6/13 17:01
     */
    Collection<ListOrgRootOrgUnitsResponse>  queryOrgRootOrgUnits();


    /**
     * @param bizCodes 组织编码 列表
     * @return 根据 组织编码集合查询 组织单元
     * @description 根据 组织编码集合查询 组织单元
     * @author wangxp
     * @date 2023/6/13 17:21
     */
    Collection<OrgNodeUnitDTO>  queryOrgUnitByBizCode(List<String> bizCodes);


    /**
     * @param orgUnitIds 组织单元id
     * @return Collection<OrgNodeUnitDTO>
     * @description 根据组织单元id 查询所有上级 （包含本身）
     * @author wangxp
     * @date 2023/6/14 9:16
     */
    Collection<QueryParentOrgUnitsInOrgDTO>  queryOrgParentOrgUnits(String orgUnitIds);
}
