package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 组织表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_pub_org`")
@NoArgsConstructor
public class OrgDO {

    @Field(name = "ORG_ID")
    @TableId(type = IdType.INPUT, value = "org_id")
    private String orgId;

    @Field(name = "ORG_NAME")
    @TableField("org_name")
    private String orgName;

    @Field(name = "ORG_FULL_NAME")
    @TableField("org_full_name")
    private String orgFullName;

    @Field(name = "ORG_FULL_PATH_NAME")
    @TableField("org_full_path_name")
    private String orgFullPathName;

    @Field(name = "ORG_FULL_PATH_ID")
    @TableField("org_full_path_id")
    private String orgFullPathId;

    @Field(name = "ORG_PARENT_ID")
    @TableField("org_parent_id")
    private String orgParentId;

    @Field(name = "ORG_TYPE")
    @TableField("org_type")
    private String orgType;

    @Field(name = "ORG_LEVEL")
    @TableField("org_level")
    private Integer orgLevel;

    @Field(name = "ORG_AREA_TYPE")
    @TableField("org_area_type")
    private String orgAreaType;

    @Field(name = "ORG_SORT")
    @TableField("org_sort")
    private Integer orgSort;

    @Field(name = "ORG_WORK_PHONE")
    @TableField("org_work_phone")
    private String orgWorkPhone;

    @Field(name = "ORG_WORK_ADDRESS")
    @TableField("org_work_address")
    private String orgWorkAddress;

    @Field(name = "ORG_PRINCIPAL")
    @TableField("org_principal")
    private String orgPrincipal;

    @Field(name = "ORG_STATUS")
    @TableField("org_status")
    private String orgStatus;

    @Field(name = "ORG_CREATE_TIME")
    @TableField("org_create_time")
    private Date orgCreateTime;

    @Field(name = "REMARK")
    @TableField("remark")
    private String remark;

    @Field(name = "FUND_CODE")
    @TableField("fund_code")
    private String fundCode;

    @Field(name = "FUND_NAME")
    @TableField("fund_name")
    private String fundName;

    @Field(name = "COMPANY_ID")
    @TableField("company_id")
    private String companyId;

    @Field(name = "DEPT_ID")
    @TableField("dept_id")
    private String deptId;

    @Field(name = "DEPT_NAME")
    @TableField("dept_name")
    private String deptName;

    @Field(name = "COMPANY_NAME")
    @TableField("company_name")
    private String companyName;

    @Field(name = "ORG_BRANCH_LEADER")
    @TableField("org_branch_leader")
    private String orgBranchLeader;
}
