package doc.doccenterserver.infrastructure.fileresource.thread;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 线程池
 * </p>
 */
@Configuration
public class ThreadPoolConfig {

    @Bean
    public ExecutorService getThreadPool() {
        return new ThreadPoolExecutor(50, 100, 60L, TimeUnit.MINUTES
                , new ArrayBlockingQueue(100));
    }

}
