package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 文档表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_d_paper`")
@NoArgsConstructor
public class PaperDO {

    @Field(name = "文档id")
    @TableId(type = IdType.INPUT, value = "paper_id")
    private String paperId;

    @Field(name = "站点id＋频道树id")
    @TableField("class_id")
    private String classId;

    @Field(name = "文档名称")
    @TableField("paper_name")
    private String paperName;

    @Field(name = "文档正文")
    @TableField("paper_text")
    private String paperText;

    @Field(name = "落款:组织信息")
    @TableField("sign_org_name")
    private String signOrgName;

    @Field(name = "落款:日期")
    @TableField("sign_date")
    private String signDate;

    @Field(name = "发布日期")
    @TableField("issue_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime issueDate;

    @Field(name = "发布用户ID")
    @TableField("issue_user_id")
    private String issueUserId;

    @Field(name = "发布标志")
    @TableField("public_flag")
    private Integer publicFlag;

    @Field(name = "是否公开")
    @TableField("is_public")
    private String isPublic;

    @Field(name = "录入者用户id")
    @TableField("input_user_id")
    private String inputUserId;

    @Field(name = "录入用户所属部门ID")
    @TableField("input_dept_id")
    private String inputDeptId;

    @Field(name = "录入用户所属公司ID")
    @TableField("input_org_id")
    private String inputOrgId;

    @Field(name = "录入时间")
    @TableField("input_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime inputTime;

    @Field(name = "失效标志")
    @TableField("del_flag")
    private Integer delFlag;

    @Field(name = "失效时间")
    @TableField("end_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    @Field(name = "文档静态页面路径")
    @TableField("path_url")
    private String pathUrl;

    @Field(name = "是否频道首页")
    @TableField("is_channel")
    private String isChannel;

    @Field(name = "字符统计")
    @TableField("str_count")
    private Integer strCount;

    @Field(name = "文档类型")
    @TableField("paper_type")
    private Integer paperType;

    @Field(name = "点击的次数")
    @TableField("click_count")
    private Integer clickCount;

    @Field(name = "文档排序号（用于置顶操作）")
    @TableField("paper_order")
    private Date paperOrder;

    @Field(name = "文档信息来源")
    @TableField("app_id")
    private String appId;

    @Field(name = "文档信息来源详细地址")
    @TableField("app_url")
    private String appUrl;

    @Field(name = "文档权限对象集合")
    @TableField("auth_object_names")
    private String authObjectNames;

    @Field(name = "图片新闻首页图片路径")
    @TableField("pic_path")
    private String picPath;

    @Field(name = "应用文档编号")
    @TableField("source_doc_id")
    private String sourceDocId;

    @Field(name = "否已经转化为PDF文件")
    @TableField("is_pdf")
    private Integer isPdf;

    @Field(name = "转换后的PDF路径")
    @TableField("pdf_url")
    private String pdfUrl;

    @Field(name = "归档组织ID")
    @TableField("archive_org_code")
    private String archiveOrgCode;

    @Field(name = "精简版文档正文")
    @TableField("mobile_paper_text")
    private String mobilePaperText;

    @Field(name = "文档描述")
    @TableField("paper_desc")
    private String paperDesc;

    @Field(name = "备用字段1")
    @TableField("reserved_1")
    private String reserved1;

    @Field(name = "备用字段2")
    @TableField("reserved_2")
    private String reserved2;

    @Field(name = "备用字段3")
    @TableField("reserved_3")
    private String reserved3;

    @Field(name = "备用字段4")
    @TableField("reserved_4")
    private String reserved4;

    @Field(name = "备用字段5")
    @TableField("reserved_5")
    private String reserved5;

    @Field(name = "备用字段6")
    @TableField("reserved_6")
    private String reserved6;

    @Field(name = "备用字段7")
    @TableField("reserved_7")
    private String reserved7;

    @Field(name = "备用字段8")
    @TableField("reserved_8")
    private Integer reserved8;

    @Field(name = "备用字段9")
    @TableField("reserved_9")
    private String reserved9;

    @Field(name = "备用字段10")
    @TableField("reserved_10")
    private String reserved10;

    @Field(name = "备用字段11")
    @TableField("reserved_11")
    private String reserved11;

    @Field(name = "备用字段12")
    @TableField("reserved_12")
    private String reserved12;

    @Field(name = "备用字段13")
    @TableField("reserved_13")
    private String reserved13;

    @Field(name = "备用字段14")
    @TableField("reserved_14")
    private String reserved14;

    @TableField(exist = false)
    private String userId;
    @TableField(exist = false)
    private String companyId;
    @TableField(exist = false)
    private String deptId;
    @TableField(exist = false)
    private String orgId;
}
