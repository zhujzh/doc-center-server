package doc.doccenterserver.infrastructure.fileresource.system.repository;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.PvLogEntity;
import doc.doccenterserver.domain.fileresource.system.repository.PvLogRepository;
import doc.doccenterserver.infrastructure.fileresource.system.converter.PvLogEntityToPvLogDOConverter;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.PvlogMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.ResMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PvlogDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ResDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author ch
 * @date 2023/4/11 22:12
 */
@Slf4j
@Component("PvLogDORepository")
public class PvLogRepositoryImpl implements PvLogRepository {

    @Resource
    private PvlogMapper pvlogMapper;
    @Resource
    private ResMapper resMapper;
    @Override
    public PageResult<List<PvLogEntity>> getListParam(PvLogEntity pvLogEntity) {
        LambdaQueryWrapper<PvlogDO> wrapper = new LambdaQueryWrapper<>();
        if(StrUtil.isNotBlank(pvLogEntity.getUserName())){
            wrapper.like(PvlogDO::getUserName,pvLogEntity.getUserName().trim());
        }
        if(StrUtil.isNotBlank(pvLogEntity.getIssueStartDate())){
            wrapper.gt(PvlogDO::getVisitTime,pvLogEntity.getIssueStartDate());
        }
        if(StrUtil.isNotBlank(pvLogEntity.getIssueEndDate())){
            wrapper.lt(PvlogDO::getVisitTime,pvLogEntity.getIssueEndDate());
        }
        wrapper.orderByDesc(PvlogDO::getVisitTime);
        Page<PvlogDO> page = new Page<>(pvLogEntity.getPageNum(), pvLogEntity.getPageSize());
        IPage<PvlogDO> paramDoPage = pvlogMapper.selectPage(page, wrapper);
        List<PvlogDO> paramDoPageList = paramDoPage.getRecords();
        List<String> menuIdList = paramDoPageList.stream().map(PvlogDO::getMenuId).collect(Collectors.toList());
        List<PvLogEntity> logEntityList= PvLogEntityToPvLogDOConverter.INSTANCE.dOToEntityList(paramDoPageList);
        if (!menuIdList.isEmpty()){
            List<ResDO> resDOList = resMapper.selectList(new LambdaQueryWrapper<ResDO>().in(ResDO::getResId,menuIdList));
            logEntityList.forEach(log->{
                ResDO resDO=resDOList.stream().filter(res ->res.getResId().equals(log.getMenuId())).findAny().orElse(null);
                log.setResKey(resDO==null?null:resDO.getResKey());
            });
        }
        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(page.getCurrent());
        baseDto.setPageSize(page.getSize());
        baseDto.setTotal(page.getTotal());
        baseDto.setTotalPage(page.getPages());
        return new PageResult(logEntityList,baseDto);

    }

    @Override
    public PvLogEntity add(PvLogEntity pvLogEntity) {
        pvLogEntity.setPvId(UUID.randomUUID().toString());
        pvLogEntity.setVisitTime(new Date());
        ResDO resDO= resMapper.selectOne(new LambdaQueryWrapper<ResDO>().eq(ResDO::getResValue,pvLogEntity.getMenuValue()));
        pvLogEntity.setMenuId(resDO==null?"":resDO.getResId());
        pvlogMapper.insert(PvLogEntityToPvLogDOConverter.INSTANCE.entityToDO(pvLogEntity));
        return pvLogEntity;
    }

}
