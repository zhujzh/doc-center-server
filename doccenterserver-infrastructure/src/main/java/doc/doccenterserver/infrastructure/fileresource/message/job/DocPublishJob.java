package doc.doccenterserver.infrastructure.fileresource.message.job;

import com.xxl.job.core.handler.annotation.XxlJob;
import doc.doccenterserver.domain.fileresource.message.repository.DocIndexChangelogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.DocPublishChangelogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文档发布
 */
@Component
@Slf4j
public class DocPublishJob {
    @Autowired
    private DocPublishChangelogRepository docPublishChangelogRepository;
    @XxlJob("docPublish")
    public void  docPublish(){
        docPublishChangelogRepository.docPublish();
        log.info("调用了");
    }
}
