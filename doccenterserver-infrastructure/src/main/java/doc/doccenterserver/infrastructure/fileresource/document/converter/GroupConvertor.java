package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.GroupEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.GroupDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface GroupConvertor {

    GroupConvertor INSTANCE = Mappers.getMapper(GroupConvertor.class);

    @Mappings({})
    GroupDO entityToDO(GroupEntity parameter);

    @Mappings({})
    GroupEntity dOToEntity(GroupDO parameter);

    @Mappings({})
    List<GroupEntity> dOToEntity(List<GroupDO> parameter);

    @Mappings({})
    List<GroupDO> entityToDO(List<GroupEntity> parameter);

}
