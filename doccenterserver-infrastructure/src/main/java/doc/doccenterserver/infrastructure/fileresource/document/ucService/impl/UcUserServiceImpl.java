package doc.doccenterserver.infrastructure.fileresource.document.ucService.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.bizworks.core.runtime.common.MultiResponse;
import com.tobacco.mp.uc.kz.client.api.EmployeeExtServiceAPI;
import com.tobacco.mp.uc.kz.client.api.dto.PageQueryEmployeeExtResponse;
import com.tobacco.mp.uc.kz.client.api.req.PageQueryEmployeeExtRequest;
import com.tobacco.mp.uc.kz.client.api.req.QueryEmployeeExtRequest;
import doc.doccenterserver.domain.fileresource.exception.BusinessException;
import doc.doccenterserver.domain.fileresource.exception.ExceptionEnum;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcUserService;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.constant.Constants;
import doc.doccenterserver.infrastructure.fileresource.security.model.UcUserModel;
import doc.doccenterserver.infrastructure.fileresource.security.properties.UcCenterProperties;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author wangxp
 * @description 用户中心扩展- 用户
 * @date 2023/6/13 16:42
 */
@Service
public class UcUserServiceImpl implements UcUserService {

    @Resource
    private UcCenterProperties ucCenterProperties;

    @Resource
    private EmployeeExtServiceAPI employeeExtServiceAPI;

    //TODO 筛选启用
    @Override
    public Collection<PageQueryEmployeeExtResponse> queryUserListByOrgIds(String userName, List<String> orgUnitIds, Integer QueryMode) {
        QueryEmployeeExtRequest queryEmployeeExtRequest =new QueryEmployeeExtRequest();
        queryEmployeeExtRequest.setOrgBizCode(ucCenterProperties.getOrgBizCode());
        queryEmployeeExtRequest.setOrgUnitIds(orgUnitIds);
        queryEmployeeExtRequest.setQueryMode(QueryMode);
        if(StrUtil.isNotEmpty(userName)){
            QueryEmployeeExtRequest.PageQueryEmployeeFilterCondition pageQueryEmployeeFilterCondition = new QueryEmployeeExtRequest.PageQueryEmployeeFilterCondition();
            pageQueryEmployeeFilterCondition.setSearchTerm(userName);
            queryEmployeeExtRequest.setFilterCondition(pageQueryEmployeeFilterCondition);
        }
        MultiResponse<PageQueryEmployeeExtResponse> userRes = employeeExtServiceAPI.queryEmployeeExt(queryEmployeeExtRequest);
        if(ObjectUtil.isNotEmpty(userRes.getData()) && CollUtil.isNotEmpty(userRes.getData().getItems())){
            return userRes.getData().getItems();
        }
        return null;
    }



    @Override
    public Collection<PageQueryEmployeeExtResponse> pageUserListByOrgIds(String userName, List<String> orgUnitIds, Integer QueryMode) {
        PageQueryEmployeeExtRequest pageQueryEmployeeExtRequest = new PageQueryEmployeeExtRequest();
        pageQueryEmployeeExtRequest.setOrgUnitIds(orgUnitIds);
        pageQueryEmployeeExtRequest.setBizOrgCode(ucCenterProperties.getOrgBizCode());
        pageQueryEmployeeExtRequest.setPageSize(20);
        pageQueryEmployeeExtRequest.setPageNumber(1);
        pageQueryEmployeeExtRequest.setQueryMode(2);
        if(StrUtil.isNotEmpty(userName)){
            PageQueryEmployeeExtRequest.PageQueryEmployeeFilterCondition employeeFilterCondition = new PageQueryEmployeeExtRequest.PageQueryEmployeeFilterCondition();
            employeeFilterCondition.setSearchTerm(userName);
            pageQueryEmployeeExtRequest.setFilterCondition(employeeFilterCondition);
        }
        MultiResponse<PageQueryEmployeeExtResponse> response = employeeExtServiceAPI.pageQueryEmployeeExt(pageQueryEmployeeExtRequest);
        if(ObjectUtil.isNotEmpty(response.getData()) && CollUtil.isNotEmpty(response.getData().getItems())){
            return response.getData().getItems().stream() // 筛选启用
                    .filter(i->ObjectUtil.isNotEmpty(i.getExtendProps())
                            && ObjectUtil.isNotEmpty(i.getExtendProps().get("staffStatus"))
                            && Constants.USER_START.equals(String.valueOf(i.getExtendProps().get("staffStatus")))
                    )
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public UserEntity getUserInfoByUserCode(String userCode) {
        HttpResponse response = HttpRequest.get(ucCenterProperties.getHostName() + "/mp/uc/staff/"+ userCode)
                .execute();
        UserEntity userEntity = null;
        if(response.isOk()) {
            JSONObject obj = JSONUtil.parseObj(response.body());
            if ("200".equals(obj.getStr("resultCode")) && StrUtil.isNotEmpty(obj.getStr("result"))) {
                userEntity = BuildUser(obj.getJSONObject("result"));
            }
        }
        return userEntity;
    }

    @Override
    public List<UserEntity> getUsersByIds(List<String> ids) {
        Map<String,Object> map = new HashMap<>();
        map.put("bizCodes",ids);
        HttpResponse response = HttpRequest.post(ucCenterProperties.getHostName() + "/mp/uc/staff/getStaffByStaffIds")
                .body(JSONUtil.toJsonStr(map))
                .execute();
        List<UserEntity> userEntitys = new ArrayList<>();
        if(response.isOk()) {
            JSONObject obj = JSONUtil.parseObj(response.body());
            System.out.println(JSONUtil.toJsonStr(obj));
            if ("200".equals(obj.getStr("resultCode")) && StrUtil.isNotEmpty(obj.getStr("result"))) {
                JSONArray users = JSONUtil.parseArray(obj.getStr("result"));
                for (Object item:users){
                    userEntitys.add(BuildUser(JSONUtil.parseObj(item)));
                }
            }
        }
        return userEntitys;
    }

    /**
     * @param result 用户中心返回 用户数据
     * @return UserEntity
     * @description 组装用户中心 返回的 用户数据
     * @author wangxp
     * @date 2023/6/12 19:11
     */
    private UserEntity BuildUser(JSONObject result) {
        UcUserModel userModel = JSONUtil.toBean(result,UcUserModel.class);
        if(StrUtil.isEmpty(userModel.getEmployeeId())){
            throw new BusinessException(ExceptionEnum.UNAUTHORIZED.getCode(), "未找到员工信息，认证失败!");
        }
        return UserEntity.builder()
                .userId(userModel.getUserId())
                .userCode(userModel.getUserId())
                .companyId(userModel.getCompanyId())
                .companyName(userModel.getCompanyName())
                .orgId(userModel.getOrgId())
                .deptId(userModel.getDeptId())
                .deptName(userModel.getDeptName())
                .orgName(userModel.getOrgName())
                .userMobile(userModel.getStaffMobile())
                .positionId(userModel.getPositionId())
                .userName(userModel.getStaffName())
                .userSort(StrUtil.isNotEmpty(userModel.getStaffSort())?Integer.valueOf(userModel.getStaffSort()):null)
                .userStatus(userModel.getStatus())
                .userType(userModel.getStaffType())
                .employeeId(userModel.getEmployeeId())
                .build();
    }
}
