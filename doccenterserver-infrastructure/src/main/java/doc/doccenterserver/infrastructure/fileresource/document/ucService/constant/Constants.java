package doc.doccenterserver.infrastructure.fileresource.document.ucService.constant;

/**
 * @author wangxp
 * @description 用户中心常量
 * @date 2023/6/13 18:50
 */
public class Constants {

    // 组织启用状态
    public static final String ORG_START = "1";


    // 组织禁用状态
    public static final String ORG_DISABLED = "0";



    // 用户禁用状态
    public static final String USER_DISABLED = "JY";


    // 用户启用状态
    public static final String USER_START = "QY";


    // 查询类型 组织
    public static final String QUERY_ORG = "ORG";


    // 查询类型 用户
    public static final String QUERY_USER = "USER";
}
