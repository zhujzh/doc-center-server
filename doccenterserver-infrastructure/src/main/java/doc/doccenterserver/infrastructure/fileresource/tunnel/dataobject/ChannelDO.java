package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 频道表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`T_CMS_D_CHANNEL`")
@NoArgsConstructor
public class ChannelDO {

    @Field(name = "频道id")
    @TableField("id")
    private String id;

    @Field(name = "频道名称")
    @TableField("name")
    private String name;

    @Field(name = "上级频道id")
    @TableField("parent_id")
    private String parentId;

    @Field(name = "频道简称")
    @TableField("code")
    private String code;

    @Field(name = "频道类型")
    @TableField("grade")
    private Integer grade;

    @Field(name = "路径")
    @TableField("path")
    private String path;

    @Field(name = "频道描述")
    @TableField("memo")
    private String memo;

    @Field(name = "作废标志")
    @TableField("delete_flag")
    private Integer deleteFlag;

    @Field(name = "主要概览模板编号")
    @TableField("list_teplet")
    private Integer listTeplet;

    @Field(name = "细览模板编号")
    @TableField("doc_teplet")
    private String docTeplet;

    @Field(name = "所属部门id")
    @TableField("dept_id")
    private String deptId;

    @Field(name = "所属公司id")
    @TableField("org_id")
    private String orgId;

    @Field(name = "频道排序")
    @TableField("sort")
    private Integer sort;

    @Field(name = "频道其他概览模板ID")
    @TableField("otherteplet")
    private String otherteplet;

    @Field(name = "频道发布流程定义ID")
    @TableField("proc_def_id")
    private String procDefId;

    @Field(name = "频道权限对象名称集合")
    @TableField("auth_object_names")
    private String authObjectNames;

    @Field(name = "预留文本1")
    @TableField("RESERVED_1")
    private String reserved1;

    @Field(name = "预留文本2")
    @TableField("RESERVED_2")
    private String reserved2;

    @Field(name = "预留文本3")
    @TableField("RESERVED_3")
    private String reserved3;

    @Field(name = "预留文本4")
    @TableField("RESERVED_4")
    private String reserved4;

    @Field(name = "预留文本5")
    @TableField("RESERVED_5")
    private String reserved5;

    @Field(name = "预留文本6")
    @TableField("RESERVED_6")
    private String reserved6;

    @Field(name = "预留文本7")
    @TableField("RESERVED_7")
    private String reserved7;

    @Field(name = "预留文本8")
    @TableField("RESERVED_8")
    private String reserved8;

    @Field(name = "预留文本9")
    @TableField("RESERVED_9")
    private String reserved9;

    @Field(name = "预留文本10")
    @TableField("RESERVED_10")
    private String reserved10;

    @Field(name = "预留文本11")
    @TableField("RESERVED_11")
    private String reserved11;

    @Field(name = "预留文本12")
    @TableField("RESERVED_12")
    private String reserved12;

    @Field(name = "预留文本13")
    @TableField("RESERVED_13")
    private String reserved13;

    @Field(name = "预留文本14")
    @TableField("RESERVED_14")
    private String reserved14;
}
