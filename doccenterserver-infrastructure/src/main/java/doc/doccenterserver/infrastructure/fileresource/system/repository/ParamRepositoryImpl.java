package doc.doccenterserver.infrastructure.fileresource.system.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.domain.fileresource.system.repository.ParamRepository;
import doc.doccenterserver.infrastructure.fileresource.system.converter.ParamEntityToParamDOConverter;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.ParamMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ParameterDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 22:12
 */
@Slf4j
@Component("ParamDORepository")
public class ParamRepositoryImpl implements ParamRepository {

    @Resource
    private ParamMapper paramMapper;

    @Override
    public PageResult<List<ParamEntity>> getListParam(ParamEntity paramEntity) {
        QueryWrapper<ParameterDO> wrapper = new QueryWrapper<>();
            wrapper.like("DESCRIPTION", paramEntity.getDescription());
            wrapper.like("P_KEY", paramEntity.getPKey());
        Page<ParameterDO> page = new Page<>(paramEntity.getPageNum(), paramEntity.getPageSize());
        IPage<ParameterDO> paramDoPage = paramMapper.selectPage(page, wrapper);
        List<ParameterDO> paramDoPageList = paramDoPage.getRecords();

        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(page.getCurrent());
        baseDto.setPageSize(page.getSize());
        baseDto.setTotal(page.getTotal());
        baseDto.setTotalPage(page.getPages());
        return new PageResult(ParamEntityToParamDOConverter.INSTANCE.dOToEntityList(paramDoPageList),baseDto);

    }

    @Override
    public ParamEntity addParam(ParamEntity paramEntity) {
        ParameterDO paramDO = ParamEntityToParamDOConverter.INSTANCE.entityToDO(paramEntity);
        paramMapper.insert(paramDO);
        return ParamEntityToParamDOConverter.INSTANCE.dOToEntity(paramDO);
    }

    @Override
    public ParamEntity editParam(ParamEntity paramEntity) {
        ParameterDO paramDO = ParamEntityToParamDOConverter.INSTANCE.entityToDO(paramEntity);
        paramMapper.updateById(paramDO);
        return ParamEntityToParamDOConverter.INSTANCE.dOToEntity(paramDO);
    }

    @Override
    public ParamEntity deleteParam(String id) {
        paramMapper.deleteById(id);
        return null;
    }

    @Override
    public ParamEntity getById(String id) {
        QueryWrapper<ParameterDO> wrapper = new QueryWrapper<>();
        wrapper.eq("P_ID",id);
        ParameterDO parameterDO = paramMapper.selectOne(wrapper);
        return ParamEntityToParamDOConverter.INSTANCE.dOToEntity(parameterDO);
    }
}
