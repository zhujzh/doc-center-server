package doc.doccenterserver.infrastructure.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.UserConfigEntity;
import doc.doccenterserver.domain.fileresource.document.repository.UserConfigRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.UserConfigEntityToUserConfigDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.document.converter.UserEntityToUserDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.UserConfigMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.UserConfigDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("UserConfigDORepository")
public class UserConfigRepositoryImpl implements UserConfigRepository {

    @Resource
    private UserConfigMapper userConfigMapper;

    @Override
    public UserConfigEntity getUserConfigById(String userId) {
        UserConfigDO userConfigDO = userConfigMapper.selectById(userId);
        return UserConfigEntityToUserConfigDOConvertor.INSTANCE.dOToEntity(userConfigDO);
    }
}
