package doc.doccenterserver.infrastructure.fileresource.es;

import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.ibm.mq.MQEnvironment;
import doc.doccenterserver.infrastructure.fileresource.es.bean.SearchDmlResult;
import doc.doccenterserver.infrastructure.fileresource.es.bean.SearchQuery;
import doc.doccenterserver.infrastructure.fileresource.es.search.elasticsearch.SearchFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.*;

/**
 * 文档检索工具类，包含针对index的基本增删改查等操作
 */
@Slf4j
@Configuration
public class SearchUtil {

    @Autowired
    private SearchFacade searchFacade;


    /**
     * 查找索引
     *
     * @param searchParam 查询参数对象
     * @return
     */
    public SearchResult search(SearchParam searchParam) {
        String searchQueryString = searchParam.toString();
        RawSearchResult result = searchFacade.search(JSON.parseObject(searchQueryString, SearchQuery.class));
        return transferResult(searchParam, result);
    }


    public SearchResult transferResult(SearchParam searchParam, RawSearchResult rawSearchResult) {
        SearchResult searchResult = new SearchResult();
        try {
            searchResult.setNumFound(rawSearchResult.getTotalNum());
            searchResult.setqTime(new Double(rawSearchResult.getSeconds()));
            long start = searchParam.getStart();
            searchResult.setPageSize(searchParam.getRows());
            long currentPage = UtilPage.getCurrentPage(start, searchParam.getRows());
            searchResult.setCurrentPage(currentPage);
            searchResult.setEntities(rawSearchResult.getList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return searchResult;
    }


    /**
     * 添加索引
     *
     * @param searchDocument 索引对象
     * @return
     */
    public Map<String, Object> create(SearchDocument searchDocument) {
        Map<String, Object> retMap = Maps.newHashMap();
        retMap.put("success", false);
        try {
            List<String> authlist = searchDocument.getIdx_auth_value();
            //searchDocument.remove("idx_auth_value");
            String strauth = Joiner.on(",").skipNulls().join(authlist);
            searchDocument.setIdx_auth_value_string(strauth);
            //searchDocument.setIdx_auth_value(strauth);
            String solrString = JSON.toJSONString(searchDocument);
            log.error("create=========begain===========" + solrString);
            SearchDmlResult result = searchFacade.create(searchDocument);
            if (result != null) {
                if (result.isSuccess()) {
                    retMap.put("success", true);
                } else {
                    retMap.put("message", result.getMessage());
                }
            }
        } catch (Exception ex) {
            log.error("create=============", ex);
            retMap.put("message", ex.getMessage());
        }

        return retMap;
    }


    /**
     * 删除索引，多个索引用逗号隔开（大数据平台只支持删除单个索引）
     *
     * @param ids
     * @return
     */
    public Map<String, Object> delete(String ids) {
        Map<String, Object> retMap = Maps.newHashMap();
        retMap.put("success", false);
        try {
            SearchDmlResult result = searchFacade.deleteDoc(ids);
            if (result != null) {
                if (result.isSuccess()) {
                    retMap.put("success", true);
                } else {
                    retMap.put("message", result.getMessage());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            retMap.put("message", ex.getMessage());
        }
        return retMap;
    }

    /**
     * 更新索引
     *
     * @param updateDocument
     * @return
     */
    public Map<String, Object> update(UpdateDocument updateDocument) {
        Map<String, Object> retMap = Maps.newHashMap();
        retMap.put("success", false);
        try {
            SearchDmlResult result = searchFacade.updateDoc(updateDocument);
            if (result != null) {
                if (result.isSuccess()) {
                    retMap.put("success", true);
                } else {
                    retMap.put("message", result.getMessage());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            retMap.put("message", ex.getMessage());
        }
        return retMap;
    }

}
