package doc.doccenterserver.infrastructure.fileresource.sso.annotation;

import java.lang.annotation.*;

/**
 * @author wangxp
 * @description 匿名访问不鉴权注解
 * @date 2023/3/30 14:01
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Anonymous {
}
