package doc.doccenterserver.infrastructure.fileresource.es.bean;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Description: 对索引执行DML（insert, update, remove）操作后返回的结果
 * @author: gj
 * @date 2023/4/11
 */
@Data
@Accessors(chain = true)
public class SearchDmlResult {

    /**
     * 操作结果 true/false
     */
    private boolean success;

    /**
     * 与操作相关的消息
     */
    private String message;

    /**
     * 操作成功
     * @return SearchDmlResult
     */
    public static SearchDmlResult dmlSuccess(){
        SearchDmlResult result = new SearchDmlResult();
        result.setSuccess(true).setMessage("success");
        return result;
    }

    /**
     * 操作异常
     * @return SearchDmlResult
     */
    public static SearchDmlResult dmlFailed(String errorMsg){
        SearchDmlResult result = new SearchDmlResult();
        result.setSuccess(false).setMessage(errorMsg);
        return result;
    }
}
