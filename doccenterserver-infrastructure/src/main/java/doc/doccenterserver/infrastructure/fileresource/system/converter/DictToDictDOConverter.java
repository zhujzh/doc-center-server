package doc.doccenterserver.infrastructure.fileresource.system.converter;

import doc.doccenterserver.domain.fileresource.system.model.DictEntry;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.DictDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * DictToDictDOConverter
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Mapper
public interface DictToDictDOConverter {

    DictToDictDOConverter INSTANCE = Mappers.getMapper(DictToDictDOConverter.class);
    /**
     * 从Dict转换到DictDO
     *
     * @param dict
     * @return DictDO
     */
    @Mappings({  })
    DictDO dictToDictDO(DictEntry dict);

    /**
     * 从DictDO转换到Dict
     *
     * @param dictDO
     * @return Dict
     */
    @Mappings({ })
    DictEntry dictDOToDict(DictDO dictDO);

    @Mappings({ })
    List<DictEntry> dOToEntityList(List<DictDO> dictDOList);
}
