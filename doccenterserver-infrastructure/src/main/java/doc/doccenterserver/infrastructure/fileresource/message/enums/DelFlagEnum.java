package doc.doccenterserver.infrastructure.fileresource.message.enums;

/**
 * 失效标志
 */
public enum DelFlagEnum {
    N(0, "未失效"),
    Y(1, "失效"),
    ;
    private final Integer value;
    private final String name;
    DelFlagEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }
    public Integer getValue() {
        return this.value;
    }

}
