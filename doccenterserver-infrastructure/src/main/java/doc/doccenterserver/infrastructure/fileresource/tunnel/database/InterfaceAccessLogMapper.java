package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.InterfaceAccessLogDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 接口调用日志表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface InterfaceAccessLogMapper extends BaseMapper<InterfaceAccessLogDO> {
}
