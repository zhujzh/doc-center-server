package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ChannelDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 频道表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface ChannelMapper extends BaseMapper<ChannelDO> {

    String getLatestChannelId(@Param("id") String parentId);

    List<ChannelDO> queryParentChannels(@Param("id") String parentId);

    ChannelDO findChannelById(@Param("id") String classid);

    List<ChannelDO> queryChildChannles(@Param("id") String classId);

    List<String> isChannelAuthList(Map<String, Object> channel);

    List<ChannelDO> queryParentChannles(@Param("id")String classId);
}
