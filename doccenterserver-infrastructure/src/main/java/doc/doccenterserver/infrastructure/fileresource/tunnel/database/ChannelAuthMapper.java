package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ChannelAuthDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 频道操作权限表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface ChannelAuthMapper extends BaseMapper<ChannelAuthDO> {
}
