package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.GroupEntity;
import doc.doccenterserver.domain.fileresource.document.model.GroupMemberEntity;
import doc.doccenterserver.domain.fileresource.document.repository.GroupMemberRepository;
import doc.doccenterserver.domain.fileresource.document.repository.GroupRepository;
import doc.doccenterserver.domain.fileresource.document.repository.UserRepository;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.infrastructure.fileresource.document.converter.GroupConvertor;
import doc.doccenterserver.infrastructure.fileresource.message.utils.IdentifierGenerator;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.security.model.LoginUser;
import doc.doccenterserver.infrastructure.fileresource.security.service.TokenService;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.GroupMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.Role2groupMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.GroupDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.Role2groupDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("GroupDORepository")
public class GroupRepositoryImpl implements GroupRepository {

    @Resource
    private GroupMapper groupMapper;

    @Resource
    private GroupMemberRepository groupMemberRepository;

    @Resource
    private Role2groupMapper role2groupMapper;

    @Resource
    private UserRepository userRepository;

    @Resource
    private TokenService tokenService;

    @Override
    public List<Map<String, Object>> querySelectedGroups(List<List<String>> groupMutilList) {
        return groupMapper.querySelectedGroups(groupMutilList);
    }

    @Override
    public GroupEntity editInit(GroupEntity groupEntity) {
        GroupDO groupDO = GroupConvertor.INSTANCE.entityToDO(groupEntity);
        return GroupConvertor.INSTANCE.dOToEntity(groupMapper.selectById(groupDO.getGroupId()));
    }

    @Override
    public GroupEntity save(GroupEntity groupEntity) {
        GroupDO groupDO = GroupConvertor.INSTANCE.entityToDO(groupEntity);
        if (StringUtil.notEmpty(groupDO.getGroupId())) {
            groupDO.setGroupId(IdentifierGenerator.createPrimaryKeySeq());
            groupMapper.insert(groupDO);
        } else {
            groupMapper.updateById(groupDO);
        }
        return GroupConvertor.INSTANCE.dOToEntity(groupDO);
    }

    @Override
    public PageResult<List<GroupEntity>> getList(GroupEntity groupEntity) {
        LambdaQueryWrapper<GroupDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtil.notEmpty(groupEntity.getGroupName()), GroupDO::getGroupName, groupEntity.getGroupName());
        wrapper.eq(StringUtil.notEmpty(groupEntity.getGroupType()), GroupDO::getGroupType, groupEntity.getGroupType());
        wrapper.isNotNull(GroupDO::getGroupParentId);
        wrapper.ne(GroupDO::getGroupParentId, "0");
        Page<GroupDO> page = new Page<>(groupEntity.getPageNum(), groupEntity.getPageSize());
        IPage<GroupDO> pages = groupMapper.selectPage(page, wrapper);
        List<GroupDO> groupDOList = pages.getRecords();
        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(pages.getCurrent());
        baseDto.setPageSize(pages.getSize());
        baseDto.setTotal(pages.getTotal());
        baseDto.setTotalPage(pages.getPages());
        List<GroupEntity> groupEntityList = GroupConvertor.INSTANCE.dOToEntity(groupDOList);

        List<String> groupUserId = new ArrayList<>();
        groupEntityList.forEach(e -> groupUserId.add(e.getGroupUserid()));
        List<UserEntity> userEntityList = userRepository.getUserListByGroupId(groupUserId);

        groupEntityList.forEach(e -> {
            if (ConstantSys.COMMON_GROUP_CODE.equals(e.getGroupParentId())) {
                e.setGroupParentName(ConstantSys.COMMON_GROUP_NAME);
            } else if (ConstantSys.PRIVATE_GROUP_CODE.equals(e.getGroupParentId())) {
                e.setGroupParentName(ConstantSys.PRIVATE_GROUP_NAME);
            } else {
                groupEntityList.forEach(item -> {
                    if (e.getGroupParentId().equals(item.getGroupId())) {
                        e.setGroupParentName(item.getGroupName());
                    }
                });
            }
            for (UserEntity userEntity : userEntityList) {
                if (e.getGroupUserid().equals(userEntity.getUserId())) {
                    e.setUserName(userEntity.getUserName());
                    break;
                }
            }
        });
        return new PageResult<>(groupEntityList, baseDto);
    }

    @Override
    public GroupEntity saveGroupMembers(GroupEntity groupEntity) {
        String groupId = groupEntity.getGroupId();
        // 先删除当前组现有成员
        groupMemberRepository.deleteGroupMemberByGroupId(groupId);
        List<String> userIdList = StringUtil.strToList(groupEntity.getUserIds(), ConstantSys.SEPARATE_SEMICOLON);
        for (int i = 0; i < userIdList.size(); i++) {
            // 添加新成员
            GroupMemberEntity member = new GroupMemberEntity();
            member.setGroupId(groupId);
            member.setMemberSort((i + 1) * 10);
            String uniquemember = userIdList.get(i);
            if (StringUtils.isNumeric(uniquemember)) {
                member.setUniquememberType(ConstantSys.ORG);
            } else {
                member.setUniquememberType(ConstantSys.USER);
            }
            member.setUniquemember(uniquemember);

            if (StringUtil.isEmpty(member.getGroupMemberid())) {
                member.setGroupMemberid(IdentifierGenerator.createPrimaryKeySeq());
                groupMemberRepository.insertGroupMember(member);
            } else {
                groupMemberRepository.updateGroupMember(member);
            }
        }
        return groupEntity;
    }

    @Override
    public GroupEntity delete(String groupIds) {
        List<String> idList = StringUtil.strToList(groupIds, ConstantSys.SEPARATE_SEMICOLON);
        groupMapper.deleteBatchIds(idList);
        return new GroupEntity();
    }

    @Override
    public GroupEntity deleteGroupById(String groupId) {
        groupMemberRepository.deleteGroupMemberByGroupId(groupId);
        LambdaQueryWrapper<GroupDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(GroupDO::getGroupId, groupId);
        groupMapper.delete(queryWrapper);
        return new GroupEntity();
    }

    @Override
    public GroupEntity editUserGroup(HttpServletRequest request, GroupEntity groupEntity) {
        LoginUser loginUser = tokenService.getLoginUser(request);
        UserModel user = loginUser.getUserModel();
        String userId = user.getUserid();

        String groupName = groupEntity.getGroupName();
        GroupDO groupDO = groupMapper.selectById(groupEntity.getGroupId());
        if (null == groupDO) {
            Integer maxSort = groupMapper.selectGroupMaxSortNum(userId);
            groupDO = new GroupDO();
            groupDO.setGroupId(IdentifierGenerator.createPrimaryKeySeq());
            groupDO.setGroupName(groupName);
            groupDO.setGroupSort(maxSort + 10);
            groupDO.setGroupType("private");
            groupDO.setGroupUserid(userId);
            groupMapper.insert(groupDO);
        } else {
            groupDO.setGroupName(groupName);
            groupDO.setGroupSort(groupDO.getGroupSort());
            groupMapper.updateById(groupDO);
        }
        return GroupConvertor.INSTANCE.dOToEntity(groupDO);
    }

    @Override
    public GroupEntity insertRoleGroup(GroupEntity groupEntity) {
        String groupId = groupEntity.getGroupId();
        String userId = groupEntity.getUserId();
        if (StringUtil.notEmpty(groupId) && !"-1".equals(groupId)
                && StringUtil.notEmpty(userId) && !"-1".equals(userId)) {
            if (!groupEntity.getIsMany()) {
                // groupService.deleteRgByroleId(groupId);
                LambdaQueryWrapper<Role2groupDO> queryWrapper = new LambdaQueryWrapper<>();
                queryWrapper.eq(Role2groupDO::getGroupId, groupId);
                role2groupMapper.delete(queryWrapper);
            }
            Role2groupDO role2groupDO = new Role2groupDO();
            role2groupDO.setRoleId(userId);
            role2groupDO.setGroupId(groupId);
            role2groupMapper.insert(role2groupDO);
        }
        return groupEntity;
    }

    @Override
    public GroupEntity selectRgByRoleId(GroupEntity groupEntity) {
        String groupId = groupEntity.getGroupId();
        StringBuilder result = new StringBuilder();
        if (StringUtil.notEmpty(groupId) && !"-1".equals(groupId)) {
            LambdaQueryWrapper<Role2groupDO> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(Role2groupDO::getGroupId, groupId);
            List<Role2groupDO> role2groupDOList = role2groupMapper.selectList(queryWrapper);

            for (int i = 0; i < role2groupDOList.size(); i++) {
                if (i == role2groupDOList.size() - 1) {
                    result.append(role2groupDOList.get(i).toString());
                } else {
                    result.append(role2groupDOList.get(i).toString()).append(";");
                }
            }
        }
        groupEntity.setRole2group(result.toString());
        return groupEntity;
    }

    @Override
    public List<GroupEntity> queryGroups(String groupUserId) {
        LambdaQueryWrapper<GroupDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(GroupDO::getGroupUserid, groupUserId);
        List<GroupDO> list = groupMapper.selectList(queryWrapper);
        return GroupConvertor.INSTANCE.dOToEntity(list);
    }
}
