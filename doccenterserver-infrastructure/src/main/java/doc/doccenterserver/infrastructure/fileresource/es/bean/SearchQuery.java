package doc.doccenterserver.infrastructure.fileresource.es.bean;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

@Data
public class SearchQuery {

	private List<FilterParam> filter = Lists.newArrayList();
	private List<OrderParam> sort = Lists.newArrayList();
	private int start = 0;
	private int rows = 10;
}
