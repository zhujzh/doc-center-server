package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 应用系统
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_app`")
@NoArgsConstructor
public class AppDO {

    @Field(name = "应用ID")
    @TableId(type = IdType.INPUT, value = "app_id")
    private String appId;

    @Field(name = "应用名称")
    @TableField("app_name")
    private String appName;

    @Field(name = "应用承建厂商")
    @TableField("app_belong")
    private String appBelong;

    @Field(name = "应用运维接口人")
    @TableField("user_name")
    private String userName;

    @Field(name = "应用运维接口人联系方式")
    @TableField("user_mobtel")
    private String userMobtel;

    @Field(name = "应用系统磁盘配额")
    @TableField("quota")
    private Double quota;

    @Field(name = "应用系统权限")
    @TableField("auth_value")
    private String authValue;

    @Field(name = "应用系统状态")
    @TableField("status")
    private String status;

    @Field(name = "应用密码")
    @TableField("app_password")
    private String appPassword;

    @Field(name = "USER_ID")
    @TableField("user_id")
    private String userId;

    @Field(name = "是否启用文件服务")
    @TableField("fileservice_status")
    private String fileserviceStatus;
}
