package doc.doccenterserver.infrastructure.fileresource.message.enums;

public enum IsMainEnum {
    Y("1", "是主附件"),
    N("0", "不是主附件"),
    ;
    private final String value;
    private final String name;
    IsMainEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }
    public String getValue() {
        return this.value;
    }

}
