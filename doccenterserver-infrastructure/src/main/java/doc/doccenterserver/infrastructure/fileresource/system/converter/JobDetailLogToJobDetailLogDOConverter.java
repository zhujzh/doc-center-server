package doc.doccenterserver.infrastructure.fileresource.system.converter;

import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;
import doc.doccenterserver.domain.fileresource.system.model.JobLogEntry;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobDetailLogDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobLogDOEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author ch
 * @date 2023/4/12 19:16
 */
@Mapper
public interface JobDetailLogToJobDetailLogDOConverter {
    JobDetailLogToJobDetailLogDOConverter INSTANCE = Mappers.getMapper(JobDetailLogToJobDetailLogDOConverter.class);
    @Mappings({  })
    JobDetailLogDO entityToDO(JobDetailLogEntity jobDetailLogEntity);
}
