package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.ChannelAuthEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ChannelAuthDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface ChannelAuthEntityToChannelAuthDOConvertor {

    ChannelAuthEntityToChannelAuthDOConvertor INSTANCE = Mappers.getMapper(ChannelAuthEntityToChannelAuthDOConvertor.class);

    @Mappings({})
    ChannelAuthDO entityToDO(ChannelAuthEntity channelAuthEntity);

    @Mappings({})
    ChannelAuthEntity dOToEntity(ChannelAuthDO channelAuthDO);

    @Mappings({})
    List<ChannelAuthEntity> dOToEntityList(List<ChannelAuthDO> channelAuthDOList);

    @Mappings({})
    List<ChannelAuthDO> entityToDOList(List<ChannelAuthEntity> channelAuthEntityList);
}
