package doc.doccenterserver.infrastructure.fileresource.tunnel.database;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.domain.fileresource.file.model.TPanObjEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.TPanObjDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Tangcancan
 */
@Mapper
public interface TPanObjMapper extends BaseMapper<TPanObjDO> {

    Double getTpanObjSize();

    TPanObjEntity findTPanObjById(String objId);

}
