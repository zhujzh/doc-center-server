package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文档浏览权限设置表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_d_paper_auth`")
@NoArgsConstructor
public class PaperAuthDO {

    @Field(name = "文档id")
    @TableField("paper_id")
    private String paperId;

    @Field(name = "对象主键")
    @TableField("auth_object")
    private String authObject;

    @Field(name = "对象类型")
    @TableField("object_type")
    private String objectType;

    @Field(name = "相应的权限值")
    @TableField("auth_value")
    private Integer authValue;

    public PaperAuthDO(String authObject, Integer authValue,
                          String objectType, String paperId) {
        super();
        this.setAuthObject(authObject);
        this.setAuthValue (authValue);
        this.setObjectType (objectType);
        this.setPaperId (paperId);
    }
}
