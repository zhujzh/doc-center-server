package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 频道模板主表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`T_CMS_D_TEMPLET`")
@NoArgsConstructor
public class TempletDO {

    @Field(name = "模板id")
    @TableField("id")
    @TableId(type = IdType.INPUT)
    private Integer id;

    @Field(name = "模板名称")
    @TableField("name")
    private String name;

    @Field(name = "类型")
    @TableField("type")
    private Integer type;

    @Field(name = "描述")
    @TableField("descript")
    private String descript;

    @Field(name = "模板内容")
    @TableField("content")
    private String content;

    @Field(name = "生成文件名称")
    @TableField("pathname")
    private String pathname;
}
