package doc.doccenterserver.infrastructure.fileresource.security.model;

import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import lombok.Data;

/**
 * @author wangxp
 * @description 登录用户
 * @date 2023/3/29 9:20
 */
@Data
public class LoginUser {

    // 用户信息
    private UserModel userModel;


    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */
    private Long expireTime;


}
