package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色-资源关联
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_pub_role2res`")
@NoArgsConstructor
public class Role2resDO {

    @Field(name = "角色ID")
    @TableField("role_id")
    private String roleId;

    @Field(name = "资源ID")
    @TableField("res_id")
    private String resId;
}
