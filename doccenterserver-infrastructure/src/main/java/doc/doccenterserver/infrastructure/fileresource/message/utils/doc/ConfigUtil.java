package doc.doccenterserver.infrastructure.fileresource.message.utils.doc;


import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.xpath.XPath;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ConfigUtil {
    public static String webSystemRootPath = "";
    static Properties properties;

    public ConfigUtil() {
    }

    public static String getKey(String key) {
        return getKey(key, (String)null);
    }

    public static String getKey(String key, String defaul) {
        if (properties == null) {
            InputStream is = null;

            try {
                properties = new Properties();
                is = ConfigUtil.class.getResourceAsStream("/config.properties");
                properties.load(is);
                System.getProperties().putAll(properties);
            } catch (IOException var12) {
                var12.printStackTrace();
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (Exception var11) {
                    var11.printStackTrace();
                }

            }
        }

        String value = properties.getProperty(key, defaul);
        if (value == null) {
            value = "";
        }

        return value;
    }

    public static List getFileType(String key) {
        List list = new ArrayList();
        String _value = getKey(key, (String)null);
        String[] _typeArr = _value.split("\\|");

        for(int i = 0; i < _typeArr.length; ++i) {
            if (_typeArr[i].trim().length() > 0) {
                list.add(_typeArr[i].trim().toLowerCase());
            }
        }

        return list;
    }

    public static Element findSingleNode(Element root, String nodeName) throws JDOMException {
        Element e = null;
        e = (Element)XPath.selectSingleNode(root, nodeName);
        return e;
    }

    public static List<Element> queryChildNodes(Element root, String nodeName) throws JDOMException {
        return (List<Element>) XPath.selectNodes(root, nodeName);
    }
}

