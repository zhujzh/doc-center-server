package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.FileDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件存储表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface FileMapper extends BaseMapper<FileDO> {
}
