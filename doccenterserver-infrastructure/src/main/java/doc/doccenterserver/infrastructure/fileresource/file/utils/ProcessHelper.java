/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License") +  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package doc.doccenterserver.infrastructure.fileresource.file.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class ProcessHelper {
    public static final Logger log = LoggerFactory.getLogger(ProcessHelper.class);

    public static ProcessResult executeScriptWindows(String process, String[] argv) {
        try {
            String[] cmd = new String[argv.length + 2];
            cmd[0] = "cmd.exe";
            cmd[1] = "/C";
            System.arraycopy(argv, 0, cmd, 2, argv.length);
            Map<String, String> env = new HashMap<String, String>();
            return executeScript(process, env, cmd);
        } catch (Exception t) {
            log.error("executeScriptWindows", t);
            return new ProcessResult(process, t.getMessage(), t);
        }
    }

    public static ProcessResult executeScript(String process, String  ... argv) {
        Map<String, String> env = new HashMap<String, String>();
        return executeScript(process,  env,argv);
    }

    public static ProcessResult executeScript(String process,
                                             Map<? extends String, ? extends String> env,  String... argv) {
        ProcessResult returnMap = new ProcessResult();
        returnMap.setProcess(process);
        if (log.isDebugEnabled()) {
            log.debug("process: " + process);
            log.debug("args: " + Arrays.toString(argv));
        }

        try {
            returnMap.setCommand(Arrays.toString(argv));
            returnMap.setOut("");

            // By using the process Builder we have access to modify the
            // environment variables
            // that is handy to set variables to run it inside eclipse
           final  ProcessBuilder pb = new ProcessBuilder(argv);

            if(env!=null && !env.isEmpty()) {
                pb.environment().putAll(env);
            }

            final Process proc = pb.start();
            // 20-minute timeout for command execution
            // FFMPEG conversion of Recordings may take a real long time until
            // its finished
            long timeout = 60000 * 20;

            StreamWatcher errorWatcher = new StreamWatcher(proc, true);
            Worker worker = new Worker(proc);
            StreamWatcher inputWatcher = new StreamWatcher(proc, false);

            errorWatcher.start();
            inputWatcher.start();
            worker.start();
            try {
                worker.join(timeout);
                if (worker.exitCode != null) {
                    returnMap.setExitValue("" + worker.exitCode);
                    returnMap.setOut(inputWatcher.output);
                } else {
                    returnMap.setException("timeOut");
                    returnMap.setOut("{success:'N'}");
                    returnMap.setExitValue("-1");

                    throw new TimeoutException();
                }
            } catch (InterruptedException ex) {
                worker.interrupt();
                errorWatcher.interrupt();
                inputWatcher.interrupt();
                Thread.currentThread().interrupt();
                returnMap.setOut("{success:'N',message:'"+ex.getMessage()+"'}");
                returnMap.setExitValue("-1");

                throw ex;
            } finally {
                proc.destroy();
            }

        } catch (TimeoutException e) {
            // Timeout exception is processed above
            log.error("executeScript", e);
            returnMap.setOut("{success:'N',message:'"+e.getMessage()+"'}");
            returnMap.setExitValue("-1");
        } catch (Throwable t) {
            // Any other exception is shown in debug window
            log.error("executeScript", t);
            returnMap.setOut("{success:'N',message:'"+t.getMessage()+"'}");
            returnMap.setExitValue("-1");
        }

        return returnMap;
    }

    private static class Worker extends Thread {
        private final Process process;
        private Integer exitCode;

        private Worker(Process process) {
            this.process = process;
        }

        @Override
        public void run() {
            try {
                exitCode = process.waitFor();
            } catch (InterruptedException ignore) {
                return;
            }
        }
    }

    private static class StreamWatcher extends Thread {
        private final InputStream is;
        private final BufferedReader br;
        public String output;

        private StreamWatcher(Process process, boolean isError) throws UnsupportedEncodingException {
            is = isError ? process.getErrorStream() : process.getInputStream();
            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        }

        @Override
        public void run() {
            try {
                String line = br.readLine();
                while (line != null) {
                    output=line;
                    line = br.readLine();
                }
            } catch (Exception ex) {
            	log.error("StreamWatcher============run=========",ex);
                return;
            }
        }
    }

}