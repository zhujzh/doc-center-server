package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.CmsPaperPvLogEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.CmsPaperPvLogDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface CmsPaperPvLogEntityToCmsPaperPvLogDOConvertor {

    CmsPaperPvLogEntityToCmsPaperPvLogDOConvertor INSTANCE = Mappers.getMapper(CmsPaperPvLogEntityToCmsPaperPvLogDOConvertor.class);

    @Mappings({})
    CmsPaperPvLogDO entityToDO(CmsPaperPvLogEntity cmsPaperPvLogEntity);

    @Mappings({})
    CmsPaperPvLogEntity dOToEntity(CmsPaperPvLogDO cmsPaperPvLogDO);

    @Mappings({})
    List<CmsPaperPvLogEntity> dOToEntityList(List<CmsPaperPvLogDO> cmsPaperPvLogDOList);

    @Mappings({})
    List<CmsPaperPvLogDO> entityToDOList(List<CmsPaperPvLogEntity> cmsPaperPvLogEntityList);

}
