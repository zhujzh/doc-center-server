package doc.doccenterserver.infrastructure.fileresource.system.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.system.model.DictEntry;
import doc.doccenterserver.domain.fileresource.system.repository.DictRepository;
import doc.doccenterserver.infrastructure.fileresource.system.converter.DictToDictDOConverter;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.DictMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.DictDO;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 字典
 * repository实现类
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Component("SYSTEMDictDtoDORepository")
public class DictRepositoryImpl implements DictRepository {
    @Resource
    private DictMapper dictMapper;
    @Override
    public DictEntry selectDict(String pid) {
        DictDO dictDO=new DictDO();
        dictDO.setId(pid);
        DictDO d= dictMapper.selectOne(new LambdaQueryWrapper<>(dictDO));

        return DictToDictDOConverter.INSTANCE.dictDOToDict(d);
    }

    @Override
    public DictEntry getByParentId(String pid) {
        DictDO dictDO = dictMapper.getByParentId(pid);
        return DictToDictDOConverter.INSTANCE.dictDOToDict(dictDO);
    }

    @Override
    public List<DictEntry> getInItTree(String pid) {
        List<DictDO> dictDOList = dictMapper.getInItTree(pid);
        return DictToDictDOConverter.INSTANCE.dOToEntityList(dictDOList);
    }

    @Override
    public Integer findChildrenNumById(String id) {
        return dictMapper.findChildrenNumById(id);
    }

    @Override
    public PageResult<List<DictEntry>> getListDict(DictEntry dictEntry) {
        QueryWrapper<DictDO> wrapper = new QueryWrapper<>();
//        wrapper.eq("DICT_NAME", dictEntry.getDictName());
        wrapper.eq("PID", dictEntry.getPid());
        Page<DictDO> page = new Page<>(dictEntry.getPageNum(), dictEntry.getPageSize());
        IPage<DictDO> doctDoPage = dictMapper.selectPage(page, wrapper);
        List<DictDO> dictDoPageList = doctDoPage.getRecords();

        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(page.getCurrent());
        baseDto.setPageSize(page.getSize());
        baseDto.setTotal(page.getTotal());
        baseDto.setTotalPage(page.getPages());
        return new PageResult(DictToDictDOConverter.INSTANCE.dOToEntityList(dictDoPageList),baseDto);
    }

    @Override
    public DictEntry getById(String id) {
        QueryWrapper<DictDO> wrapper = new QueryWrapper<>();
        wrapper.eq("ID",id);
        DictDO dictDO = dictMapper.selectOne(wrapper);
        return DictToDictDOConverter.INSTANCE.dictDOToDict(dictDO);
    }

    @Override
    public DictEntry editDict(DictEntry dictEntry) {
        DictDO dictDO = DictToDictDOConverter.INSTANCE.dictToDictDO(dictEntry);
        dictMapper.updateById(dictDO);
        return DictToDictDOConverter.INSTANCE.dictDOToDict(dictDO);
    }

    @Override
    public DictEntry deleteDict(String id) {
        dictMapper.deleteById(id);
        return null;
    }

    @Override
    public DictEntry addDict(DictEntry dictEntry) {
        DictDO dictDO = DictToDictDOConverter.INSTANCE.dictToDictDO(dictEntry);
        dictMapper.insert(dictDO);
        return DictToDictDOConverter.INSTANCE.dictDOToDict(dictDO);
    }

    @Override
    public List<DictEntry> getByPId(String pid) {
        QueryWrapper<DictDO> wrapper = new QueryWrapper<>();
        wrapper.eq("PID",pid);
        List<DictDO> dictDOList = dictMapper.selectList(wrapper);
        return DictToDictDOConverter.INSTANCE.dOToEntityList(dictDOList);
    }

    @Override
    public DictEntry getByPIdAndVal(String pid, String val) {
        QueryWrapper<DictDO> wrapper = new QueryWrapper<>();
        wrapper.eq("PID",pid);
        wrapper.eq("DICT_VALUE",val);
        DictDO dictDO = dictMapper.selectOne(wrapper);
        return DictToDictDOConverter.INSTANCE.dictDOToDict(dictDO);
    }

    @Override
    public DictEntry getDictByIdAndPid(DictEntry dict) {
        QueryWrapper<DictDO> wrapper = new QueryWrapper<>();
        wrapper.eq("PID",dict.getPid());
        wrapper.eq("ID",dict.getId());
        return DictToDictDOConverter.INSTANCE.dictDOToDict(dictMapper.selectOne(wrapper));
    }
}
