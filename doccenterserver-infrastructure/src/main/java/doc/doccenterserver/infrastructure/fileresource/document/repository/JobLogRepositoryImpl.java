package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import doc.doccenterserver.domain.fileresource.document.model.JobLogEntity;
import doc.doccenterserver.domain.fileresource.document.repository.JobLogRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.JobLogEntityToJobLogDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.JobLogMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobLogDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("JobLogDORepository")
public class JobLogRepositoryImpl implements JobLogRepository {

    @Resource
    private JobLogMapper jobLogMapper;

    @Override
    public Integer findJobLogCountByPaperId(String paperId) {
        LambdaQueryWrapper<JobLogDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(JobLogDO::getPaperId, paperId);
        return jobLogMapper.selectCount(wrapper);
    }

    @Override
    public Integer insertJobLog(JobLogEntity jobLog) {
        return jobLogMapper.insert(JobLogEntityToJobLogDOConvertor.INSTANCE.entityToDO(jobLog));
    }

    @Override
    public List<JobLogEntity> getJobLogList(JobLogEntity jobLogEntity) {
        LambdaQueryWrapper<JobLogDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(JobLogDO::getSourceAppId, jobLogEntity.getSourceAppId());
        wrapper.eq(JobLogDO::getSourcePaperId, jobLogEntity.getSourcePaperId());
        wrapper.eq(JobLogDO::getArchiveOrgCode, jobLogEntity.getArchiveOrgCode());
        wrapper.eq(JobLogDO::getStatus, jobLogEntity.getStatus());
        List<JobLogDO> jobLogDOList = jobLogMapper.selectList(wrapper);
        return JobLogEntityToJobLogDOConvertor.INSTANCE.dOToEntityList(jobLogDOList);
    }
}
