package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ChannelDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * DictToDictDOConverter
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Mapper
public interface ChannelEntityToChannelDOConverter {

    ChannelEntityToChannelDOConverter INSTANCE = Mappers.getMapper(ChannelEntityToChannelDOConverter.class);
    @Mappings({})
    ChannelDO entityToDO(ChannelEntity channelEntity);
    @Mappings({})
    ChannelEntity dOToEntity(ChannelDO channelDO);
    @Mappings({})
    List<ChannelEntity> dOToEntityList(List<ChannelDO> channelDOList);
    @Mappings({})
    List<ChannelDO> entityToDOList(List<ChannelEntity> channelEntityList);
}
