package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobDetailLogDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface JobDetailLogEntityToJobDetailLogDOConvertor {

    JobDetailLogEntityToJobDetailLogDOConvertor INSTANCE = Mappers.getMapper(JobDetailLogEntityToJobDetailLogDOConvertor.class);

    @Mappings({})
    JobDetailLogDO entityToDO(JobDetailLogEntity jobDetailLogEntity);

    @Mappings({})
    JobDetailLogEntity dOToEntity(JobDetailLogDO jobDetailLogDO);

    @Mappings({})
    List<JobDetailLogEntity> dOToEntityList(List<JobDetailLogDO> jobDetailLogDOList);

    @Mappings({})
    List<JobDetailLogDO> entityToDOList(List<JobDetailLogEntity> jobDetailLogEntityList);

}
