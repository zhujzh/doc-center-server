package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import doc.doccenterserver.domain.fileresource.document.model.MqSaveEntity;
import doc.doccenterserver.domain.fileresource.document.repository.MqSaveRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.MqSaveEntityToMqSaveDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.MqSaveMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.MqSaveDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("MqSaveDORepository")
public class MqSaveRepositoryImpl implements MqSaveRepository {

    @Resource
    private MqSaveMapper mqSaveMapper;

    @Override
    public List<MqSaveEntity> getMqSaveList(MqSaveEntity mqSaveEntity) {
        LambdaQueryWrapper<MqSaveDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(MqSaveDO::getSourceApp, mqSaveEntity.getSourceApp());
        wrapper.eq(MqSaveDO::getSourcePaperId, mqSaveEntity.getSourcePaperId());
        wrapper.eq(MqSaveDO::getArchiveOrgCode, mqSaveEntity.getArchiveOrgCode());
        List<MqSaveDO> mqSaveDOList = mqSaveMapper.selectList(wrapper);
        return MqSaveEntityToMqSaveDOConvertor.INSTANCE.dOToEntityList(mqSaveDOList);
    }

    @Override
    public Integer insertMqSave(MqSaveEntity mqSave) {
        return mqSaveMapper.insert(MqSaveEntityToMqSaveDOConvertor.INSTANCE.entityToDO(mqSave));
    }
}
