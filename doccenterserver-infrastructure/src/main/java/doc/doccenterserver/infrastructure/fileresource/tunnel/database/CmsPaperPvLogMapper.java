package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.CmsPaperPvLogDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Tangcancan
 */
@Mapper
public interface CmsPaperPvLogMapper extends BaseMapper<CmsPaperPvLogDO> {

}
