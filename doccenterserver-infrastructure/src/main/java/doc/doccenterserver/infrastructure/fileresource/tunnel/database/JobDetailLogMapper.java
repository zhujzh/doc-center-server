package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.domain.fileresource.system.model.JobDetailLogEntry;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobDetailLogDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 任务日志详细表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface JobDetailLogMapper extends BaseMapper<JobDetailLogDO> {

    List<JobDetailLogDO> findJobDetailLogListByParams(@Param("params")Map<String, String> params);
}
