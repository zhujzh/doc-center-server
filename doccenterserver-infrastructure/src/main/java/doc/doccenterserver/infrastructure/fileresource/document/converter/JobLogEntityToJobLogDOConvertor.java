package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.JobLogEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobLogDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface JobLogEntityToJobLogDOConvertor {

    JobLogEntityToJobLogDOConvertor INSTANCE = Mappers.getMapper(JobLogEntityToJobLogDOConvertor.class);

    @Mappings({})
    JobLogDO entityToDO(JobLogEntity jobLogEntity);

    @Mappings({})
    JobLogEntity dOToEntity(JobLogDO jobLogDO);

    @Mappings({})
    List<JobLogEntity> dOToEntityList(List<JobLogDO> jobLogDOList);

    @Mappings({})
    List<JobLogDO> entityToDOList(List<JobLogEntity> jobLogEntityList);

}
