package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户自定义配置
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_d_user_config`")
@NoArgsConstructor
public class UserConfigDO {

    @Field(name = "用户ID")
    @TableId(type = IdType.INPUT, value = "user_id")
    private String userId;

    @Field(name = "页面缩放尺寸")
    @TableField("paper_scale")
    private String paperScale;

    @Field(name = "主题皮肤ID")
    @TableField("skin_id")
    private String skinId;
}
