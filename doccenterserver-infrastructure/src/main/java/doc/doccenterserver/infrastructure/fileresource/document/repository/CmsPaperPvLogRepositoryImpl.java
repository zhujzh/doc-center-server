package doc.doccenterserver.infrastructure.fileresource.document.repository;

import doc.doccenterserver.domain.fileresource.document.model.CmsPaperPvLogEntity;
import doc.doccenterserver.domain.fileresource.document.repository.CmsPaperPvLogRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.CmsPaperPvLogEntityToCmsPaperPvLogDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.message.utils.IdentifierGenerator;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.CmsPaperPvLogMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("CmsPaperPvLogDORepository")
public class CmsPaperPvLogRepositoryImpl implements CmsPaperPvLogRepository {

    @Resource
    private CmsPaperPvLogMapper cmsPaperPvLogMapper;

    @Override
    public Integer saveCmsPaperPvLog(CmsPaperPvLogEntity cmsPaperPvLogEntity) {
        if (StringUtils.isBlank(cmsPaperPvLogEntity.getPvId())) {
            cmsPaperPvLogEntity.setPvId(IdentifierGenerator.createPrimaryKeySeq());
            return cmsPaperPvLogMapper.insert(CmsPaperPvLogEntityToCmsPaperPvLogDOConvertor.INSTANCE.entityToDO(cmsPaperPvLogEntity));
        } else {
            return cmsPaperPvLogMapper.updateById(CmsPaperPvLogEntityToCmsPaperPvLogDOConvertor.INSTANCE.entityToDO(cmsPaperPvLogEntity));
        }

    }
}
