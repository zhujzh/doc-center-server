package doc.doccenterserver.infrastructure.fileresource.system.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.tobacco.mp.dc.client.api.FileServiceAPI;
import com.tobacco.mp.dc.client.api.basefile.req.DeleteFileRequest;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.system.repository.LocalFileRepository;
import doc.doccenterserver.infrastructure.fileresource.file.converter.FileEntityToFileDOConverter;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.AppMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.FileMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.AppDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.FileDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author ch
 * @date 2023/4/23 19:25
 */
@Slf4j
@Component("LocalFileDORepository")
public class LocalFileRepositoryImpl implements LocalFileRepository {
    @Resource
    private FileMapper fileMapper;

    @Resource
    private AppMapper appMapper;

    @Resource
    private FileServiceAPI fileServiceAPI;

    @Override
    public PageResult<List<FileEntity>> listfiles(FileEntity fileEntity) {
        LambdaQueryWrapper<FileDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.ge(StringUtil.notEmpty(fileEntity.getUploadStartDate()),FileDO::getCreateDate,fileEntity.getUploadStartDate()+" 00:00:00");
        wrapper.le(StringUtil.notEmpty(fileEntity.getUploadEndDate()), FileDO::getCreateDate, fileEntity.getUploadEndDate()+" 23:59:59");
        wrapper.like(StringUtil.notEmpty(fileEntity.getFilename()),FileDO::getFilename,fileEntity.getFilename());
        wrapper.like(StringUtil.notEmpty(fileEntity.getFileMd5()),FileDO::getFileMd5,fileEntity.getFileMd5());
        wrapper.like(StringUtil.notEmpty(fileEntity.getFileId()),FileDO::getFileId,fileEntity.getFileId());
        wrapper.eq(StringUtil.notEmpty(fileEntity.getAppId()),FileDO::getAppId,fileEntity.getAppId());
        wrapper.like(StringUtil.notEmpty(fileEntity.getLabelName()),FileDO::getLabelName,fileEntity.getLabelName());
        wrapper.orderByDesc(FileDO::getCreateDate);
        if(StringUtil.notEmpty(fileEntity.getAppName())){
           List<AppDO> appList= appMapper.selectList(new LambdaQueryWrapper<AppDO>().like(AppDO::getAppName,fileEntity.getAppName()));
           List<String> appIdList = appList.stream().map(AppDO::getAppId).collect(Collectors.toList());
           if(appIdList.isEmpty()){
               return new PageResult(new ArrayList<>(),new BaseDto());
           }
           wrapper.in(!appIdList.isEmpty(),FileDO::getAppId,appIdList);
        }
        Page<FileDO> page = new Page<>(fileEntity.getPageNum(), fileEntity.getPageSize());
        IPage<FileDO> fileDOPage = fileMapper.selectPage(page, wrapper);
        List<FileDO> fileDOPageList = fileDOPage.getRecords();

        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(page.getCurrent());
        baseDto.setPageSize(page.getSize());
        baseDto.setTotal(page.getTotal());
        baseDto.setTotalPage(page.getPages());

        List<FileEntity> paperEntityList = FileEntityToFileDOConverter.INSTANCE.fileDOToFileEntityList(fileDOPageList);
        for (FileEntity file : paperEntityList) {
            if (file.getAppId()==null){
                continue;
            }
            AppDO app= appMapper.selectById(file.getAppId());
            if(app!=null){
                file.setAppName(app.getAppName());
            }
            if(file.getFileSize() != null){
                file.setFileSizeStr(formatObjSize(file.getFileSize()));
            }
        }
        return new PageResult(paperEntityList,baseDto);
    }

    @Override
    public int reUpload(String fileId) {
        int total = 0;
        if (fileId.equals("reUploadAll")){

        }else{
            FileDO file = fileMapper.selectById(fileId);
            File uploadFile = new File(file.getFilePath());
        }
        return 0;
    }

    @Override
    public Map<String, Object> showfiledetail(String fileId, String appName) {
        Map<String,Object> map = Maps.newHashMap();
        FileDO fileDo =fileMapper.selectById(fileId);
        AppDO app= appMapper.selectById(fileDo.getAppId());
        FileEntity fileEntity =  FileEntityToFileDOConverter.INSTANCE.fileDOToFileEntity(fileDo);
        fileEntity.setAppName(app.getAppName());
        fileEntity.setFileSizeStr(formatObjSize(fileDo.getFileSize()));
        map.put("file", fileEntity);
        return map;
    }

    @Override
    public String deletefiles(String fileIds) {
        List<String> idList = StringUtil.strToList(fileIds, ";");
        int num = 0;
        for(String fileId :idList){
            try {
                num = fileMapper.deleteById(fileId);
                // 同时删除大数据平台数据
                try {
                    DeleteFileRequest request = new DeleteFileRequest();
                    request.setFileId(fileId);
                    fileServiceAPI.deleteFile(request);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (num > 0) {
            return String.valueOf(num);
        } else {
            return "0";
        }
    }

    public static String formatObjSize(Double objSize) {
        // 格式化文件大小
        int objSizeFlag = 0;
        do {
            objSize = objSize / 1024;
            objSizeFlag++;
        } while (objSize > 99);
        String[] sFlag = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
        String objs = String.format("%.1f", objSize) + sFlag[objSizeFlag];
        return objs;
    }
}
