package doc.doccenterserver.infrastructure.fileresource.document.repository;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import doc.doccenterserver.domain.fileresource.document.model.ChannelAuthEntity;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.document.model.CmsPaperPvLogEntity;
import doc.doccenterserver.domain.fileresource.document.model.DocConvertChangelogEntity;
import doc.doccenterserver.domain.fileresource.document.model.GroupMemberEntity;
import doc.doccenterserver.domain.fileresource.document.model.ImageEntity;
import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;
import doc.doccenterserver.domain.fileresource.document.model.JobLogEntity;
import doc.doccenterserver.domain.fileresource.document.model.MeetingEntity;
import doc.doccenterserver.domain.fileresource.document.model.MqSaveEntity;
import doc.doccenterserver.domain.fileresource.document.model.PaperAuthEntity;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.document.model.User2roleEntity;
import doc.doccenterserver.domain.fileresource.document.repository.ChannelAuthRepository;
import doc.doccenterserver.domain.fileresource.message.repository.DocConvertChangelogRepository;
import doc.doccenterserver.domain.fileresource.system.model.ParameterEntry;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.document.repository.AttachmentRepository;
import doc.doccenterserver.domain.fileresource.document.repository.ChannelRepository;
import doc.doccenterserver.domain.fileresource.document.repository.CmsPaperPvLogRepository;
import doc.doccenterserver.domain.fileresource.document.repository.GroupMemberRepository;
import doc.doccenterserver.domain.fileresource.document.repository.ImageRepository;
import doc.doccenterserver.domain.fileresource.document.repository.JobDetailLogRepository;
import doc.doccenterserver.domain.fileresource.document.repository.JobLogRepository;
import doc.doccenterserver.domain.fileresource.document.repository.MeetingRepository;
import doc.doccenterserver.domain.fileresource.document.repository.MqSaveRepository;
import doc.doccenterserver.domain.fileresource.document.repository.PaperAuthRepository;
import doc.doccenterserver.domain.fileresource.document.repository.PaperRepository;
import doc.doccenterserver.domain.fileresource.document.repository.User2RoleRepository;
import doc.doccenterserver.domain.fileresource.document.repository.UserRepository;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.domain.fileresource.system.repository.ParameterRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.PaperEntityToPaperDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.message.utils.DateUtil;
import doc.doccenterserver.infrastructure.fileresource.message.utils.IdentifierGenerator;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.security.model.LoginUser;
import doc.doccenterserver.infrastructure.fileresource.security.service.TokenService;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.PaperMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PaperDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("PaperDORepository")
public class PaperRepositoryImpl implements PaperRepository {

    @Resource
    private PaperMapper paperMapper;

    @Resource
    private PaperAuthRepository paperAuthRepository;

    @Resource
    private ImageRepository imageRepository;

    @Resource
    private AttachmentRepository attachmentRepository;

    @Resource
    private JobLogRepository jobLogRepository;

    @Resource
    private JobDetailLogRepository jobDetailLogRepository;

    @Resource
    private ChannelRepository channelRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private User2RoleRepository user2RoleRepository;

    @Resource
    private MeetingRepository meetingRepository;

    @Resource
    private GroupMemberRepository groupMemberRepository;

    @Resource
    private CmsPaperPvLogRepository cmsPaperPvLogRepository;

    @Resource
    private MqSaveRepository mqSaveRepository;

    @Resource
    private ChannelAuthRepository channelAuthRepository;

    @Resource
    private DocConvertChangelogRepository docConvertChangelogRepository;

    @Resource
    private TokenService tokenService;

    @Resource
    private ParameterRepository parameterRepository;

    // 引用频道id集合，英文逗号分隔
    private static String REF_PAPER_CHANNELS = "";
    private static final String OBJ_TYPE_U="u";//用户权限
    private static final String OBJ_TYPE_G="g";//组织权限
    private static final String OBJ_TYPE_A="a";//自定义组权限
    @PostConstruct
    public void init() {
        // 初始化引用频道
        REF_PAPER_CHANNELS = channelRepository.queryRefChannels();
    }

    private static final String BIG_DATA_DOWNLOAD_URL = "https://www.rzdata.net/doc-cloud-file/file/download/";

    private static final String DOMAIN_PDF_FILE_PATH = "https://www.rzdata.net/doc-cloud-file/file/download/";

    private static final String DOMAIN_FILE_API_INFO = "{\"downloadURL\":\"https://www.rzdata.net/doc-cloud-file/file/download/\",\"md5URL\":\"https://www.rzdata.net/doc-cloud-file/file/md5/check/\",\"uploadURL\":\"https://www.rzdata.net/doc-cloud-file/file/upload\",\"updateURL\":\"https://www.rzdata.net/doc-cloud-file/file/get\",\"deleteURL\":\"https://www.rzdata.net/doc-cloud-file/file/delete\",\"fileTag\":{\"image\":\"jpg,jpeg,bmp,png,gif\",\"document\":\"doc,docx,xls,xlsx,ppt,pptx,txt,pdf\",\"video\":\"avi,wma,rmvb,rm,swf,mp4,mid,3gp\",\"music\":\"mp3,wma,wav,mod,aac,ape,ogg,m4a\"},\"supportedViewTypes\":\"doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,bmp,png,gif\"}";

    private static final String FILE_API_INFO = "{\"downloadURL\":\"http://10.158.134.38:9081/doc-cloud-file/file/download/\",\"md5URL\":\"http://10.158.134.38:9081/doc-cloud-file/file/md5/check/\",\"uploadURL\":\"http://10.158.134.38:9081/doc-cloud-file/file/upload\",\"updateURL\":\"http://10.158.134.38:9081/doc-cloud-file/file/update\",\"deleteURL\":\"http://10.158.134.38:9081/doc-cloud-file/file/del/\",\"fileTag\":{\"image\":\"jpg,jpeg,bmp,png,gif\",\"document\":\"doc,docx,xls,xlsx,ppt,pptx,txt,pdf\",\"video\":\"avi,wma,rmvb,rm,swf,mp4,mid,3gp\",\"music\":\"mp3,wma,wav,mod,aac,ape,ogg,m4a\"},\"supportedViewTypes\":\"doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,bmp,png,gif\"}";

    // 语畅租户
    private static final String YUN_STORAGE_SPACCE = "ZYECM";

    // 企业网盘系统
    private static final String DOC_CLOUD_FRONT_URL = "https://www.rzdata.net/doc-cloud-front/";

    // 文档中心_集团
    private static final String ECM_LOCATION_NAME = "文档中心_集团";

    @Override
    public PageResult<List<PaperEntity>> listPapers(HttpServletRequest request, PaperEntity paperEntity) {
        LambdaQueryWrapper<PaperDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PaperDO:: getDelFlag, paperEntity.getDelFlag());
        wrapper.eq(PaperDO:: getClassId, paperEntity.getClassId());
        wrapper.eq(StringUtil.notEmpty(String.valueOf(paperEntity.getPublicFlag())), PaperDO:: getPublicFlag, paperEntity.getPublicFlag());
        wrapper.like(StringUtil.notEmpty(paperEntity.getPaperName()), PaperDO:: getPaperName, paperEntity.getPaperName());
        wrapper.ge(StringUtil.notEmpty(paperEntity.getIssueStartDate()), PaperDO:: getIssueDate, paperEntity.getIssueStartDate());
        wrapper.le(StringUtil.notEmpty(paperEntity.getIssueEndDate()), PaperDO:: getIssueDate, paperEntity.getIssueEndDate());
        wrapper.orderByDesc(PaperDO:: getReserved8, PaperDO:: getPaperOrder, PaperDO:: getIssueDate, PaperDO:: getInputTime);
        // 查询一般频道 or 查询引用频道
        Page<PaperDO> page = new Page<>(paperEntity.getPageNum(), paperEntity.getPageSize());
        //根据权限查询
        LoginUser loginUser = tokenService.getLoginUser(request);
        UserModel userModel = loginUser.getUserModel();
        if (null != userModel && !ConstantSys.CZ_ADMIN.equals(userModel.getUserid())) {
            List<String> authLists=Arrays.asList(userModel.getUserid(),userModel.getCompanyId(),userModel.getDeptId(),userModel.getOrgId());
            paperEntity.setAuthLists(authLists);
        }
        IPage<PaperDO> pages = paperMapper.findPaperList(page,paperEntity,wrapper);
        List<PaperDO> paperDOList = pages.getRecords();
        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(pages.getCurrent());
        baseDto.setPageSize(pages.getSize());
        baseDto.setTotal(pages.getTotal());
        baseDto.setTotalPage(pages.getPages());
        List<PaperEntity> paperEntityList = PaperEntityToPaperDOConvertor.INSTANCE.dOToEntityList(paperDOList);
        // 根据PAPER_ORDER，添加置顶状态标识topStatus，true：已置顶 false：未置顶
        for (PaperEntity paper : paperEntityList) {
            Date date = paper.getPaperOrder();
            paper.setTopStatus(null != date && !"1900".equals(DateUtil.getDateFormat(date, "yyyy")));
            paper.setReserved(paper.getReserved8());
        }
        return new PageResult<>(paperEntityList, baseDto);
    }
    @Override
    public List<PaperEntity> getPortalList(HttpServletRequest request, PaperEntity paperEntity) {
        List<PaperEntity> list=this.listPapers(request,paperEntity).getData();
        list.forEach(e -> {
            // 需要显示new的数据（设置几天内的文档显示new）
            // 获取三天前的日期
            LocalDateTime threeDaysAgo = LocalDateTime.now().minusDays(3);
            LocalDateTime myDateTime =  e.getIssueDate();
            if (e.getIssueDate() != null) {
                // 判断文档发布日期是否在三天前的日期之前
                boolean flag = myDateTime.isBefore(threeDaysAgo);
                if (!flag) {
                    e.setShowNew(true);
                }
            }
            // 标题处理
            String paperName = e.getPaperName();
            if (paperName.startsWith("湖南中烟投资管理有限公司")) {
                paperName = paperName.substring(10);
            }
            if (paperName.startsWith("中共湖南中烟工业有限责任公司")) {
                paperName = paperName.substring(14);
            }
            if (paperName.startsWith("湖南中烟工业有限责任公司")) {
                paperName = paperName.substring(12);
            }
            // 把页面需要展示的文档标题处理后放入临时字段paperShowTitle中
            e.setPaperName(paperName);
        });
        return list;
    }

    @Override
    public List<PaperEntity> listPaper(HttpServletRequest request, PaperEntity paperEntity, String[] split) {
        LambdaQueryWrapper<PaperDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PaperDO:: getDelFlag, paperEntity.getDelFlag());
        wrapper.likeRight(PaperDO:: getClassId, paperEntity.getClassId());
        // wrapper.in(CollectionUtil.isNotEmpty(list), PaperDO:: getClassId, list);
        wrapper.eq(StringUtil.notEmpty(String.valueOf(paperEntity.getPublicFlag())), PaperDO:: getPublicFlag, paperEntity.getPublicFlag());
        wrapper.like(StringUtil.notEmpty(paperEntity.getPaperName()), PaperDO:: getPaperName, paperEntity.getPaperName());
        // 获取截止日期之前的数据（针对于公告、通知、会议通知数据）
        wrapper.and(e -> e.gt(StringUtil.notEmpty(String.valueOf(paperEntity.getEndTime())), PaperDO:: getEndTime, paperEntity.getEndTime()).or().isNull(PaperDO:: getEndTime));
        wrapper.orderByDesc(PaperDO:: getReserved8, PaperDO:: getPaperOrder, PaperDO:: getIssueDate, PaperDO:: getInputTime);
        // 查询一般频道 or 查询引用频道
        Page<PaperDO> page = new Page<>(paperEntity.getPageNum(), paperEntity.getPageSize());
        IPage<PaperDO> pages = new Page<>();
        Map<String, Object> params = new HashMap<>();
        if (!REF_PAPER_CHANNELS.contains("," + paperEntity.getClassId() + ",")) {
            pages = paperMapper.selectPage(page, wrapper);
        } else {
            params.put("classId", paperEntity.getClassId());
            params.put("delFlag", paperEntity.getDelFlag());
            LoginUser loginUser = tokenService.getLoginUser(request);
            if (loginUser != null) {
                UserModel userModel = loginUser.getUserModel();
                if (userModel != null && userModel.getCompanyId() != null) {
                    if (params.containsKey("classId")) {
                        String classId = params.get("classId").toString();
                        // 将当前的频道id作为引用频道id查询，查询之后复原
                        params.remove("classId");
                        params.put("channelNid", classId);
                    }
                    pages = paperMapper.findPaperRefListByParams(params);
                    if (params.containsKey("classId")) {
                        params.put("classId", params.get("classId").toString());
                    }
                }
            }
        }
        // 处理查询出来的数据标题
        List<PaperDO> paperDOList = pages.getRecords();
        List<PaperEntity> paperEntityList = PaperEntityToPaperDOConvertor.INSTANCE.dOToEntityList(paperDOList);
        paperEntityList.forEach(e -> {
            // 需要显示new的数据（设置几天内的文档显示new）
            // 获取三天前的日期
            LocalDateTime threeDaysAgo = LocalDateTime.now().minusDays(3);
            LocalDateTime myDateTime =  e.getIssueDate();
            if (e.getIssueDate() != null) {
                // 判断文档发布日期是否在三天前的日期之前
                boolean flag = myDateTime.isBefore(threeDaysAgo);
                if (!flag) {
                    e.setShowNew(true);
                }
            }
            // 标题处理
            String paperName = e.getPaperName();
            if (paperName.startsWith("湖南中烟投资管理有限公司")) {
                paperName = paperName.substring(10);
            }
            if (paperName.startsWith("中共湖南中烟工业有限责任公司")) {
                paperName = paperName.substring(14);
            }
            if (paperName.startsWith("湖南中烟工业有限责任公司")) {
                paperName = paperName.substring(12);
            }
            // 把页面需要展示的文档标题处理后放入临时字段paperShowTitle中
            e.setPaperName(paperName);
        });
        return paperEntityList;
    }

    @Override
    public PaperEntity getListPaper(HttpServletRequest request, String classId) {
        Map<String, Object> map = new HashMap<>();
        PaperEntity paperEntity = new PaperEntity();

        UserModel userModel = tokenService.getLoginUser(request).getUserModel();
        UserEntity userEntity = new UserEntity();
        if (null != userModel) {
            userEntity.setUserId(userModel.getUserid());
            userEntity.setDeptId(userModel.getDeptId());
            userEntity.setOrgId(userModel.getOrgId());
            userEntity.setCompanyId(userModel.getCompanyId());
        }
        // 待修改
        List<User2roleEntity> user2roleEntityList = user2RoleRepository.findAccountInfoListByUserId(userEntity.getUserId());
        if (CollectionUtil.size(user2roleEntityList) > 0 && CollectionUtil.isNotEmpty(user2roleEntityList)) {
            paperEntity.setRoleFlag("Y");
        } else {
            paperEntity.setRoleFlag("N");
        }
        // 查询用户是否存在文档管理频道权限(用来判断在页面上是否展示文档管理菜单)
        boolean channelAuthFlag = isChannelAuthList(userEntity);
        paperEntity.setIstrue(channelAuthFlag);
        paperEntity.setUserEntity(userEntity);
        paperEntity.setClassId(classId);
        // 应用云盘的标识
        paperEntity.setHtml("paperindex");

        // 初始化 channelPathName
        if (StringUtil.notEmpty(classId)) {
            map.put("channelPathName", channelRepository.initChannelPath(classId));
        } else {
            map.put("channelPathName", "公司总部");
        }
        paperEntity.setChannelPathName(map);

        return paperEntity;
    }

    private boolean isChannelAuthList(UserEntity userEntity) {
        boolean isChannel = false;
        if (ConstantSys.CZ_ADMIN.equals(userEntity.getUserId())) {
            isChannel = true;
        } else {
            // 判断用户是否有文档管理的任一频道权限 ，如果有则返回true
            List<ChannelAuthEntity> channelAuthEntityList = channelAuthRepository.isChannelAuthList(userEntity);
            if (CollectionUtil.size(channelAuthEntityList) > 0 && CollectionUtil.isNotEmpty(channelAuthEntityList)) {
                isChannel = true;
            }
        }
        return isChannel;
    }

    /**
     * 处理门户首页以及二级页面展示的数据标题
     *
     * @param paperList 需要处理的数据
     * @param iscportal 是否是获取数据到cportal展示
     */
    private void processPaperList(List<PaperEntity> paperList, Map<String, Object> params, boolean iscportal) {

        // 需要显示new的数据（设置几天内的文档显示new）
        int shownew = 3;
        if (params.containsKey("shownew") && null != params.get("shownew") && !"".equals(params.get("shownew"))) {
            shownew = Integer.parseInt(params.get("shownew").toString());
        }
        if (params.containsKey("iscportal") && null != params.get("iscportal") && !"".equals(params.get("iscportal"))) {
            iscportal = Boolean.parseBoolean(params.get("iscportal").toString());
        }
        String showpicStr = "【图】";
        if (params.containsKey("hidepic") && null != params.get("hidepic") && !"".equals(params.get("hidepic"))) {
            showpicStr = "";
        }
        String showatt = "&nbsp;<img src='" + DOC_CLOUD_FRONT_URL + "/resources/tecm/images/icon-new.png' style='width: 28px;height: 18px;' border=0/>";
        if(!paperList.isEmpty()){
            for (PaperEntity paper : paperList) {
                boolean ispicdoc = null != paper.getPicPath() && !"".equals(paper.getPicPath());
                String papertitle = paper.getPaperName();
                if (papertitle == null || "".equals(papertitle)) {
                    paper.setPaperName("");
                    continue;
                }
                Integer attCount = attachmentRepository.findAttCountByPaperId(paper.getPaperId());
                if ("1".equals(paper.getPaperType().toString())) {
                    if (attCount <= 1) {
                        showatt = "";
                    }
                }
                if (ispicdoc || attCount == 0) {
                    showatt = "";
                }
                if (!ispicdoc || null == paper.getPicPath() || "".equals(paper.getPicPath())) {
                    showpicStr = "";
                }

                /*
                 * 针对投资公司做的特殊处理
                 */

                if (papertitle.startsWith("湖南中烟投资管理有限公司")) {
                    papertitle = papertitle.substring(10);
                }
                if (papertitle.startsWith("中共湖南中烟工业有限责任公司")) {
                    papertitle = papertitle.substring(14);
                }
                if (papertitle.startsWith("湖南中烟工业有限责任公司")) {
                    papertitle = papertitle.substring(12);
                }

                if (!"".equals(showpicStr)) {
                    papertitle += "<font color='#3479c6'>" + showpicStr + "</font>";
                }
                if (!"".equals(showatt)) {
                    paper.setShowNew(true);
                }
                if (!iscportal) {
                    LocalDateTime threeDaysAgo = LocalDateTime.now().minusDays(shownew);// 获取三天前的日期
                    LocalDateTime myDateTime =  paper.getIssueDate();
                    if (paper.getIssueDate() != null) {
                        // 判断文档发布日期是否在三天前的日期之前
                        boolean flag = myDateTime.isBefore(threeDaysAgo);// 判断文档发布日期是否在三天前的日期之前
                        if (!flag) {
                            paper.setShowNew(true);
                        }
                    }
                } else {
                    LocalDateTime now = LocalDateTime.now();
                    LocalDateTime myDateTime = paper.getIssueDate();
                    int intervalTime = (int) ChronoUnit.DAYS.between(myDateTime, now);
                    paper.setIntervalTime(intervalTime);
                }
                // 把页面需要展示的文档标题处理后放入临时字段paperShowTitle中
                paper.setPaperShowTitle(papertitle);
            }
        }
    }

    @Override
    public PaperEntity getPaperDetail(String paperId, HttpServletRequest request) {

        UserModel curUser = tokenService.getLoginUser(request).getUserModel();
        PaperEntity paperEntity = paperMapper.findPaperById(paperId);
        if (paperEntity != null) {
            // 增加点击数
            paperMapper.updatePaperClickCount(paperEntity.getPaperId());
            CmsPaperPvLogEntity cmsPaperPvLogEntity = new CmsPaperPvLogEntity();
            cmsPaperPvLogEntity.setCreateTime(new Date());
            cmsPaperPvLogEntity.setBizId(paperEntity.getPaperId());
            cmsPaperPvLogEntity.setBizName(paperEntity.getPaperName());
            cmsPaperPvLogEntity.setUserCode(curUser.getUserid());
            cmsPaperPvLogEntity.setUserName(curUser.getName());
            cmsPaperPvLogEntity.setUrl(request.getRequestURL().toString());
            cmsPaperPvLogRepository.saveCmsPaperPvLog(cmsPaperPvLogEntity);
        }
        // 权限
        boolean res = false;
        if (paperEntity != null && "N".equals(paperEntity.getIsPublic()) && !ConstantSys.CZ_ADMIN.equals(curUser.getUserid())) {
            List<PaperAuthEntity> paperAuthList = paperAuthRepository.getPaperAuthList(paperId);
            for (PaperAuthEntity paperAuth : paperAuthList) {
                if (ConstantSys.OBJ_TYPE_A.equals(paperAuth.getObjectType())) {
                    // 判断是否有自定义组的权限
                    String groupId = paperAuth.getAuthObject();
                    List<GroupMemberEntity> groupMemberList = groupMemberRepository.getGroupAuth(groupId);
                    for (GroupMemberEntity groupMember : groupMemberList) {
                        if (groupMember.getUniquemember().equals(curUser.getCompanyId())
                                || groupMember.getUniquemember().equals(curUser.getDeptId())
                                || groupMember.getUniquemember().equals(curUser.getUserid())) {
                            res = true;
                            break;
                        }
                    }
                } else {
                    if (paperAuth.getAuthObject().equals(curUser.getCompanyId())
                            || paperAuth.getAuthObject().equals(curUser.getDeptId())
                            || paperAuth.getAuthObject().equals(curUser.getOrgId())
                            || paperEntity.getIssueUserId().equals(curUser.getUserid())
                            || paperAuth.getAuthObject().equals(curUser.getUserid())) {
                        res = true;
                    }
                }
            }
        } else if (paperEntity != null && "Y".equals(paperEntity.getIsPublic())) {
            res = true;
        }
        if (!res) {
            return new PaperEntity();
        }

        String inputUserId = paperEntity.getInputUserId();
        if (StringUtil.notEmpty(inputUserId)) {
            UserEntity user = userRepository.getUserInfo(inputUserId);
            if (user != null) {
                paperEntity.setInputUserName(user.getUserName());
            }
        }
        // 查找普通附件
        AttachmentEntity attachmentEntity = new AttachmentEntity();
        attachmentEntity.setPaperId(paperEntity.getPaperId());
        attachmentEntity.setIsMain(ConstantSys.IS_MAIN_ZERO);
        List<AttachmentEntity> attachList = attachmentRepository.getAttachmentList(attachmentEntity);
        paperEntity.setAttachList(attachList);
        // 网页
        if (0 == paperEntity.getPaperType()) {
            // imgList
            ImageEntity imageEntity = new ImageEntity();
            imageEntity.setPaperId(paperId);
            List<ImageEntity> imgList = imageRepository.getImageList(imageEntity);
            paperEntity.setImgList(imgList);
        } else if (1 == paperEntity.getPaperType()) {
            // office
            attachmentEntity.setIsMain(ConstantSys.IS_MAIN_ONE);
            List<AttachmentEntity> mainFiles = attachmentRepository.getAttachmentList(attachmentEntity);
            paperEntity.setMainFile(mainFiles);
        }
        return paperEntity;
    }

    @Override
    public PaperEntity getAddPaper(String classId, HttpServletRequest request) {
        PaperEntity paperEntity = new PaperEntity();
        String fileApi = DOMAIN_FILE_API_INFO;
        if (request.getHeader("iv-user") == null) {
            fileApi = FILE_API_INFO;
        }
        ChannelEntity channelEntity = channelRepository.getById(classId);
        // 当前发布文档的频道
        paperEntity.setCurchannel(channelEntity);
        paperEntity.setClassId(classId);
        paperEntity.setFileApiInfo(fileApi);
        ParameterEntry video = parameterRepository.getByKey("vedio_channel");
        String vedioChannel = video.getPValue();
        if (vedioChannel.contains(classId)) {
            // 如果是视频学习环节
            // return "ecm/jsp/paper/editvediopaper";
        }
        // 判断是否是会议通知
        this.setPaperType(paperEntity);
        return paperEntity;
    }

    /**
     * 设置是否为会议通知、通知公告、图片新闻
     * @param paperEntity
     */
    @Override
    public void setPaperType(PaperEntity paperEntity){
        ParameterEntry meet = parameterRepository.getByKey(ConstantSys.HYTZ_CHANNEL);
        String hytzChannel = meet.getPValue();
        paperEntity.setIshytz(false);
        if (hytzChannel.contains(paperEntity.getClassId())) {
            // 如果是会议通知环节
            paperEntity.setIshytz(true);
        }
        // 判断是否通知公告频道
        ParameterEntry notice = parameterRepository.getByKey(ConstantSys.TZGG_CHANNEL);
        String tzggChannel = notice.getPValue();
        paperEntity.setIstzgg(false);
        if (tzggChannel.contains(paperEntity.getClassId())) {
            // 如果是通知公告频道
            paperEntity.setIstzgg(true);
        }
        // 判断是否图片新闻频道
        ParameterEntry picNew = parameterRepository.getByKey(ConstantSys.TPXW_CHANNEL);
        String picChannel = picNew.getPValue();
        paperEntity.setIstpxw(false);
        if (picChannel.contains(paperEntity.getClassId())) {
            // 如果是图片频道
            paperEntity.setIstpxw(true);
        }
    }

    @Override
    public PaperEntity addPaper(HttpServletRequest request, PaperEntity paperEntity) {
        String isPublic=paperEntity.getIsPublic();
        paperEntity.setAppId(ConstantSys.ECM_LOCATION);
        String paperId = paperEntity.getPaperId();
        Integer publicFlag = paperEntity.getPublicFlag();
        LoginUser loginUser = tokenService.getLoginUser(request);
        UserModel curUser = loginUser.getUserModel();
        // 文档发布，记录发布时间
        if (ConstantSys.ISSUE_FLAG.equals(publicFlag)) {
            paperEntity.setIssueDate(LocalDateTime.now());
            paperEntity.setIssueUserId(curUser.getUserid());
        }
        // changelog表记录数
        Integer joblogNum = 0;
        if (StringUtil.notEmpty(paperId)) {
            joblogNum = jobLogRepository.findJobLogCountByPaperId(paperId);
        }
        PaperDO paperDO;
        if("C".equals(paperEntity.getIsPublic())){
            paperEntity.setIsPublic("N");
        }
        if (StringUtil.isEmpty(paperId)) {
            // 初始化文档参数
            paperEntity.setInputUserId(curUser.getUserid());
            paperEntity.setInputDeptId(curUser.getDeptId());
            paperEntity.setInputOrgId(curUser.getOrgId());
            paperEntity.setInputTime(LocalDateTime.now());
            paperEntity.setPaperId(IdentifierGenerator.createPrimaryKeySeq());
            // 新增文档默认PAPER_ORDER的值
            Date orderDate = DateUtil.parseDateFormat("1900", "yyyy");
            paperEntity.setPaperOrder(orderDate);
            paperEntity.setArchiveOrgCode(paperEntity.getInputOrgId());
            paperEntity.setPublicFlag(PaperEntity.PAPER_PUBLIC_FLAG_DRAFT);
            // 状态为发布的新增文档操作，文档状态改为转换中
            if (PaperEntity.PAPER_PUBLIC_FLAG_ISSUE.equals(publicFlag)) {
                paperEntity.setPublicFlag(PaperEntity.PAPER_PUBLIC_FLAG_CONVERTING);
            }
            paperEntity.setDelFlag(ConstantSys.DELETE_FLAG_ZERO);
            paperEntity.setReserved8(0);
            paperEntity.setClickCount(0);
            paperDO = PaperEntityToPaperDOConvertor.INSTANCE.entityToDO(paperEntity);
            paperMapper.insert(paperDO);
        } else {
            // 状态为发布的修改文档操作，如果文档转换changelog有记录，文档状态设为已撤销，否则为转换中
            if (PaperEntity.PAPER_PUBLIC_FLAG_ISSUE.equals(publicFlag)) {
                // 文档从未发布过，设状态为转换中
                if (joblogNum == 0) {
                    paperEntity.setPublicFlag(PaperEntity.PAPER_PUBLIC_FLAG_CONVERTING);
                    // 文档非首次发布，状态先设为发布中
                } else {
                    paperEntity.setPublicFlag(PaperEntity.PAPER_PUBLIC_FLAG_ISSUING);
                }
            }
            paperDO = PaperEntityToPaperDOConvertor.INSTANCE.entityToDO(paperEntity);
            paperMapper.updateById(paperDO);
        }

        // 处理附件信息
        // 1.删除原有附件
        attachmentRepository.deleteAttachmentByPaperId(paperId);
        // 2.插入新附件
        // 附件信息
        String fileArrJSON = paperEntity.getFileArrJSON();
        if (StringUtils.isNotBlank(fileArrJSON)) {
            List<AttachmentEntity> fileList = JSONObject.parseArray(fileArrJSON, AttachmentEntity.class);
            for (int i = 0; i < fileList.size(); i++) {
                AttachmentEntity attachmentEntity = fileList.get(i);
                attachmentEntity.setAttaId(IdentifierGenerator.createPrimaryKeySeq());
                // 解决乱码
                // attachmentEntity.setFileName(URLDecoder.decode(attachmentEntity.getFileName(),"utf-8"));
                // attachmentEntity.setFileDesc(URLDecoder.decode(attachmentEntity.getFileDesc(),"utf-8"));
                // 自动生成排序号
                attachmentEntity.setOrderBy((i + 1) * 10);
                // 绑定文档id
                attachmentEntity.setPaperId(paperDO.getPaperId());
                // 新插入附件都未转化成pdf
                attachmentEntity.setIsPdf(0);
                attachmentEntity.setAppId(YUN_STORAGE_SPACCE);
                attachmentEntity.setFileMd5(attachmentEntity.getFileId());
                attachmentRepository.insertAttachment(attachmentEntity);
            }
        }

        // 处理图片信息
        // 1.删除原有图片
        imageRepository.deleteImagesByPaperId(paperId);
        // 2.插入新图片
        // 图片信息
        String imgArrJSON = paperEntity.getImgArrJSON();
        if (StringUtils.isNotBlank(imgArrJSON)) {
            List<ImageEntity> fileList = JSONObject.parseArray(imgArrJSON, ImageEntity.class);
            for (int i = 0; i < fileList.size(); i++) {
                ImageEntity imageEntity = fileList.get(i);
                imageEntity.setAppId(YUN_STORAGE_SPACCE);
                imageEntity.setImgId(IdentifierGenerator.createPrimaryKeySeq());
                imageEntity.setFileMd5(imageEntity.getFileId());
                // 解决乱码
                // imageEntity.setImgName(URLDecoder.decode(imageEntity.getImgName(),"utf-8"));
                /*
                 * if(StringUtil.notEmpty(imageEntity.getImgDesc())){
                 * imageEntity.setImgDesc(URLDecoder.decode(imageEntity.getImgDesc(),"utf-8")); }
                 */
                // 自动生成排序号
                imageEntity.setOrderBy((i + 1) * 10);
                // 绑定文档id
                imageEntity.setPaperId(paperDO.getPaperId());
                imageRepository.insertImage(imageEntity);
            }
        }

        // 处理操作权限，先删除原来的权限
        paperAuthRepository.deletePaperAuthById(paperId);
        // 再插入最新的权限
        if ("C".equals(isPublic)) {
            List<String> orgId = new ArrayList<>();
            orgId.add(curUser.getOrgId());
            List<UserEntity> userEntityList = userRepository.getUserListByOrgIds(orgId);
            for (UserEntity userEntity : userEntityList) {
                PaperAuthEntity paperAuthEntity = new PaperAuthEntity();
                paperAuthEntity.setPaperId(paperDO.getPaperId());
                paperAuthEntity.setAuthObject(userEntity.getUserCode());
                paperAuthEntity.setAuthValue(1);
                paperAuthEntity.setObjectType("u");
                paperAuthRepository.insertPaperAuth(paperAuthEntity);
            }
        } else if ("N".equals(isPublic)) {
            String authObjectIds = paperEntity.getAuthObjectIds();
            if (StringUtil.notEmpty(authObjectIds)) {
                String ids = paperEntity.getAuthObjectIds();
                // 获取权限对象
                List<String> authObjectArr = Lists.newArrayList();
                if (StringUtil.notEmpty(ids)) {
                    authObjectArr = Lists.newArrayList(Splitter.on(",").trimResults().omitEmptyStrings().split(ids));
                }
                for (String authObject : authObjectArr) {
                    PaperAuthEntity paperAuthEntity = new PaperAuthEntity();
                    if ("".equals(authObject)) {
                        continue;
                    }
                    paperAuthEntity.setPaperId(paperDO.getPaperId());
                    paperAuthEntity.setAuthObject(authObject);
                    paperAuthEntity.setAuthValue(1);
                    if (authObject.contains("^ORG")) {
                        // 组织
                        paperAuthEntity.setAuthObject(authObject.substring(0, authObject.length() - 4));
                        paperAuthEntity.setObjectType("g");
                    } else if (authObject.contains("^GROUP")) {
                        // a代表自定义组
                        paperAuthEntity.setAuthObject(authObject.substring(0, authObject.length() - 6));
                        paperAuthEntity.setObjectType("a");
                    } else if (authObjectIds.contains("^USER")) {
                        // u代表用户
                        paperAuthEntity.setAuthObject(authObject.substring(0, authObject.length() - 5));
                        paperAuthEntity.setObjectType("u");
                    } else {
                        paperAuthEntity.setAuthObject(authObject);
                        paperAuthEntity.setObjectType("g");
                    }
                    paperAuthRepository.insertPaperAuth(paperAuthEntity);
                }
            }
        }

        // 操作类别
        String operation = null;

        // 根据changlog表判断文档是首次发布还是重新发布，前者直接插创建日志，后者先插撤销，后插新增日志
        if (ConstantSys.ISSUE_FLAG.equals(publicFlag)) {
            if (joblogNum > 0) {
                // 不是第一次发布，直接先插入一条删除日志
                addJobLog(paperEntity, JobLogEntity.OPERATION_RETRIEVE);
            }
            // 文档发布，设置日志状态为新建
            operation = JobLogEntity.OPERATION_CREATE;
        } else if (ConstantSys.CANCEL_FLAG.equals(publicFlag)) {
            // 撤销操作，直接插入撤销日志
            operation = JobLogEntity.OPERATION_RETRIEVE;
        }
        // 根据前面的状态插入对应类型日志
        if (operation != null) {
            addJobLog(paperEntity, operation);
        }

        if (null != paperEntity.getAddress() && !"".equals(paperEntity.getAddress())) {
            MeetingEntity meeting = new MeetingEntity();
            meeting.setStartTime(paperEntity.getStartDate());
            meeting.setEndTime(paperEntity.getEndDate());
            meeting.setAddress(paperEntity.getAddress());
            meeting.setPaperId(paperDO.getPaperId());
            meeting.setTypeValue("meeting");
            meetingRepository.saveMeeting(meeting);
        }
        return PaperEntityToPaperDOConvertor.INSTANCE.dOToEntity(paperDO);
    }

    /**
     * 插入日志到任务日志主表和明细表，在插入xxx_changlog表后执行此插入
     *
     * @param paper
     * @param operation
     * @throws Exception
     */
    private void addJobLog(PaperEntity paper, String operation) {
        // 根据应用ID ,归档组织和应用文档编号，去统一消息暂存日志表是否存在
        if (checkMqSaveExist(paper.getPaperId(), paper.getArchiveOrgCode())) {
            // 存在：插入暂存日志表，插入任务日志主表（任务异常）、从表《文档转换》任务未完成
            save4existjob(paper, JSONObject.toJSONString(paper), operation);
        } else {
            // 根据应用ID、应用文档编号、主日志状态为处理中，查找任务主日志表,判断是否存在
            if (checkJobLogExist(paper.getPaperId(), paper.getArchiveOrgCode())) {
                // 存在：插入暂存日志表，插入任务日志主表（任务异常）、从表《文档转换》任务未完成
                save4existjob(paper, JSONObject.toJSONString(paper), operation);
            } else {
                // 插入任务日志主表、从表(《文档转换》任务未完成)、文档转换changelog表
                save4normejob(paper, JSONObject.toJSONString(paper), operation);
            }
        }
    }

    /**
     * 根据应用ID和应用文档编号，去统一消息暂存日志表是否存在
     *
     * @param sourcePaperId
     * @param archiveOrgCode
     * @return
     */
    private Boolean checkMqSaveExist(String sourcePaperId, String archiveOrgCode) {
        MqSaveEntity mqSaveEntity = new MqSaveEntity();
        mqSaveEntity.setSourceApp(ConstantSys.ECM_LOCATION);
        mqSaveEntity.setSourcePaperId(sourcePaperId);
        mqSaveEntity.setArchiveOrgCode(archiveOrgCode);
        List<MqSaveEntity> result = mqSaveRepository.getMqSaveList(mqSaveEntity);
        return null != result && !result.isEmpty();
    }

    private Boolean checkJobLogExist(String sourcePaperId, String archiveOrgCode) {
        JobLogEntity jobLogEntity = new JobLogEntity();
        jobLogEntity.setSourceAppId(ConstantSys.ECM_LOCATION);
        jobLogEntity.setSourcePaperId(sourcePaperId);
        jobLogEntity.setArchiveOrgCode(archiveOrgCode);
        jobLogEntity.setStatus(JobLogEntity.STATUS_N);
        List<JobLogEntity> result = jobLogRepository.getJobLogList(jobLogEntity);
        return null != result && !result.isEmpty();
    }

    /**
     * 插入暂存日志表，插入任务日志主表（任务异常）、从表《文档转换》任务未完成
     *
     * @param paper
     * @param mqmsg
     * @param operation
     */
    private void save4existjob(PaperEntity paper, String mqmsg, String operation) {
        String logId = IdentifierGenerator.createPrimaryKeySeq();
        // 插入任务日志主表
        JobLogEntity jobLog = new JobLogEntity(logId, mqmsg, paper.getPaperName(), operation, ConstantSys.ECM_LOCATION, ECM_LOCATION_NAME,
                paper.getPaperId(), JobLogEntity.STATUS_Q, paper.getArchiveOrgCode());
        jobLog.setPaperId(paper.getPaperId());
        jobLogRepository.insertJobLog(jobLog);
        // 插入暂存日志表
        MqSaveEntity mqSave = new MqSaveEntity(logId, mqmsg, new Date(), operation, ConstantSys.ECM_LOCATION, paper.getPaperId(),
                paper.getArchiveOrgCode());
        mqSaveRepository.insertMqSave(mqSave);
        // 从表《文档转换》任务未完成
        JobDetailLogEntity jobDetailLogEntity = new JobDetailLogEntity(logId, mqmsg, new Date(), null, null,
                JobDetailLogEntity.STEP_ID_4, JobDetailLogEntity.STEP_STATUS_HANDLING, new Date());
        jobDetailLogRepository.insertJobDetailLog(jobDetailLogEntity);
    }

    /**
     * 插入任务日志主表、从表(《文档转换》任务未完成)、文档转换changelog表
     *
     * @param paper
     * @param mqmsg
     * @param operation
     * @throws Exception
     */
    private void save4normejob(PaperEntity paper, String mqmsg, String operation) {
        String logId = IdentifierGenerator.createPrimaryKeySeq();
        // 插入任务日志主表
        JobLogEntity jobLog = new JobLogEntity(logId, mqmsg, paper.getPaperName(), operation, ConstantSys.ECM_LOCATION, ECM_LOCATION_NAME,
                paper.getPaperId(), JobLogEntity.STATUS_N, paper.getArchiveOrgCode());
        jobLog.setPaperId(paper.getPaperId());
        jobLogRepository.insertJobLog(jobLog);
        // 从表(《文档转换》任务未完成
        JobDetailLogEntity jobDetailLogEntity = new JobDetailLogEntity(logId, mqmsg, new Date(), null, null,
                JobDetailLogEntity.STEP_ID_4, JobDetailLogEntity.STEP_STATUS_HANDLING, new Date());
        jobDetailLogRepository.insertJobDetailLog(jobDetailLogEntity);
        // 插入文档转换changelog表
        DocConvertChangelogEntity docConvertChangelog = new DocConvertChangelogEntity(logId, new Date(), operation,
                paper.getPaperId(), 0);
        docConvertChangelogRepository.insertDocConvertChangelog(docConvertChangelog);
    }

    @Override
    public PaperEntity getEditPaper(String paperId, HttpServletRequest request) {
        PaperEntity paperEntity = paperMapper.findPaperById(paperId);

        String downloadPath = DOMAIN_PDF_FILE_PATH;
        String fileApi = DOMAIN_FILE_API_INFO;
        if (request.getHeader("iv-user") == null) {
            downloadPath = BIG_DATA_DOWNLOAD_URL;
            fileApi = FILE_API_INFO;
        }
        // StringUtil.handleObjWithClob(paperEntity);
        paperEntity.setPdfFilePath(downloadPath);

        // 查找普通附件
        AttachmentEntity attachmentEntity = new AttachmentEntity();
        attachmentEntity.setPaperId(paperEntity.getPaperId());
        attachmentEntity.setIsMain(ConstantSys.IS_MAIN_ZERO);
        List<AttachmentEntity> attachList = attachmentRepository.getAttachmentList(attachmentEntity);
        paperEntity.setAttachList(attachList);

        if (paperEntity.getPaperType().equals(1)) {
            // 如果是正文文档，查找正文附件
            attachmentEntity.setIsMain(ConstantSys.IS_MAIN_ONE);
            List<AttachmentEntity> mainFile = attachmentRepository.getAttachmentList(attachmentEntity);
            if (CollectionUtil.isNotEmpty(mainFile) && mainFile.size() > 0) {
                paperEntity.setMainFile(mainFile);
            }
        } else if (paperEntity.getPaperType().equals(3)) {
            // 视频文档
            // 视频文档路径
            attachmentEntity.setIsMain(ConstantSys.IS_MAIN_ONE);
            List<AttachmentEntity> mainFile = attachmentRepository.getAttachmentList(attachmentEntity);
            if (CollectionUtil.isNotEmpty(mainFile) && mainFile.size() > 0) {
                paperEntity.setMainFile(mainFile);
            }
            // 视频文件图片信息以及正文内容都需要查询
            ImageEntity imageEntity = new ImageEntity();
            imageEntity.setPaperId(paperEntity.getPaperId());
            List<ImageEntity> imgList = imageRepository.getImageList(imageEntity);
            if (CollectionUtil.isNotEmpty(imgList) && imgList.size() > 0) {
                paperEntity.setImgList(imgList);
            }
            // 处理正文内容
            paperEntity.setPaperText(StringUtil.textEncode(paperEntity.getPaperText()));
        } else {
            // 查找图片信息
            ImageEntity imageEntity = new ImageEntity();
            imageEntity.setPaperId(paperEntity.getPaperId());
            List<ImageEntity> imgList = imageRepository.getImageList(imageEntity);
            if (CollectionUtil.isNotEmpty(imgList) && imgList.size() > 0) {
                paperEntity.setImgList(imgList);
            }
            // 处理正文内容
            String paperText = paperEntity.getPaperText();
            paperEntity.setPaperText(StringUtil.textEncode(paperText));
        }
        // 初始化权限信息
        StringBuilder authObjectIds = new StringBuilder();
        List<PaperAuthEntity> paperAuthList = paperAuthRepository.getPaperAuthList(paperId);
        for (int i = 0; i < paperAuthList.size(); i++) {
            PaperAuthEntity paperAuthEntity = paperAuthList.get(i);
            String objectType = paperAuthEntity.getObjectType();
            String authObject = paperAuthEntity.getAuthObject();
            if (OBJ_TYPE_A.equals(objectType)) {
                authObject += "^GROUP";
            } else if (OBJ_TYPE_G.equals(objectType)) {
                authObject += "^ORG";
            } else if (OBJ_TYPE_U.equals(objectType)) {
                authObject += "^USER";
            }
            if (i < paperAuthList.size() - 1) {
                authObjectIds.append(authObject).append(",");
            } else {
                authObjectIds.append(authObject);
                paperEntity.setAuthValue(String.valueOf(paperAuthEntity.getAuthValue()));
            }
        }

        String authObjIds = authObjectIds.toString();
        paperEntity.setAuthObjectIds(authObjIds);
        MeetingEntity meeting = meetingRepository.getMeetingById(paperId);
        paperEntity.setMeeting(meeting);
        paperEntity.setFileApiInfo(fileApi);

        if (paperEntity.getPaperType().equals(3)) {
            // return "ecm/jsp/paper/editvediopaper";
        }
        // 判断是否是会议通知
        this.setPaperType(paperEntity);

        if(!paperAuthList.isEmpty()){
            List<Map<String,Object>> authList=userRepository.querySelectedData(authObjIds);
            paperEntity.setAuthListMap(authList);
        }


        return paperEntity;
    }

    @Override
    public PaperEntity topDoc(PaperEntity paperEntity) {
        PaperDO paperDO = PaperEntityToPaperDOConvertor.INSTANCE.entityToDO(paperEntity);
        if (paperEntity.getTopStatus()) {
            // 已置顶，则是取消置顶操作
            paperDO.setPaperOrder(DateUtil.parseDateFormat("1900", "yyyy"));
            paperDO.setReserved8(0);
        } else {
            // 未置顶，则是置顶操作
            paperDO.setPaperOrder(new Date());
        }
        paperMapper.updateById(paperDO);
        return PaperEntityToPaperDOConvertor.INSTANCE.dOToEntity(paperDO);
    }

    @Override
    public PaperEntity topDocSort(PaperEntity paperEntity) {
        PaperDO paperDO = PaperEntityToPaperDOConvertor.INSTANCE.entityToDO(paperEntity);
        paperMapper.updateById(paperDO);
        return PaperEntityToPaperDOConvertor.INSTANCE.dOToEntity(paperDO);
    }

    @Override
    public PaperEntity previewPaper(PaperEntity paperEntity, HttpServletRequest request) {
        String paperId = paperEntity.getPaperId();
        paperEntity.setClickCount(0);
        UserModel curUser = tokenService.getLoginUser(request).getUserModel();
        UserEntity user = new UserEntity();
        if (StringUtil.notEmpty(paperId)) {
            PaperEntity oldPaper = paperMapper.findPaperById(paperId);
            user = userRepository.getUserInfo(oldPaper.getInputUserId());
            paperEntity.setIssueDate(oldPaper.getIssueDate());
            paperEntity.setClickCount(oldPaper.getClickCount());
        }
        paperEntity.setInputUserName(curUser.getName());
        if (request.getHeader("iv-user") == null) {
            paperEntity.setPdfFilePath(BIG_DATA_DOWNLOAD_URL);
        } else {
            paperEntity.setPdfFilePath(DOMAIN_PDF_FILE_PATH);
        }
        // 附件信息
        String fileArrJson = paperEntity.getFileArrJSON();
        if (StringUtils.isNotBlank(fileArrJson)) {
            List<AttachmentEntity> fileList = JSONObject.parseArray(fileArrJson, AttachmentEntity.class);
            paperEntity.setAttachList(fileList);
            paperEntity.setFilelength(fileList.size());
        }
        // 图片信息
        String imgArrJson = paperEntity.getImgArrJSON();
        if (StringUtils.isNotBlank(imgArrJson)) {
            List<ImageEntity> fileList = JSONObject.parseArray(imgArrJson, ImageEntity.class);
            paperEntity.setImgList(fileList);

        }
        paperEntity.setCurUser(user);
        return paperEntity;
    }

    @Override
    public PaperEntity deletePaper(String ids) {
        if (StringUtil.notEmpty(ids)) {
            List<String> idList = StringUtil.strToList(ids, ",");
            // 删除前获取其发布状态，文档已发布则删除后要记录日志，否则直接删除
            List<PaperDO> paperDOList = paperMapper.selectBatchIds(idList);
            for (PaperDO paperDO : paperDOList) {
                paperDO.setDelFlag(ConstantSys.DELETE_FLAG_ONE);
                paperMapper.updateById(paperDO);
                // 在日志表中插入一条待处理的删除日志，会在后台服务中删除
                if (PaperEntity.PAPER_PUBLIC_FLAG_ISSUE.equals(paperDO.getPublicFlag())) {
                    addJobLog(PaperEntityToPaperDOConvertor.INSTANCE.dOToEntity(paperDO), JobLogEntity.OPERATION_RETRIEVE);
                }
            }
        }
        return new PaperEntity();
    }

    @Override
    public Double findsumpaperfilesize(String appId) {
        return paperMapper.findsumpaperfilesize(appId);
    }

    @Override
    public Double sumtpanfilesize(String appId) {
        return paperMapper.sumtpanfilesize(appId);
    }

    @Override
    public PaperEntity findPaperById(String paperId) {
        return paperMapper.findPaperById(paperId);
    }

    @Override
    public PageResult<List<PaperEntity>> listfrontpapers(PaperEntity paperEntity, String classid, String ccid, HttpServletRequest request) {
        Map<String,Object> params = Maps.newHashMap();
        Page<PaperDO> page = new Page<>(paperEntity.getPageNum(), paperEntity.getPageSize());
        IPage<PaperDO> pages = new Page<>();
        List<PaperDO> result = new ArrayList<>();
        BaseDto baseDto = new BaseDto();
        try{
            //临时注释
            UserModel curUser = tokenService.getLoginUser(request).getUserModel();

            //当前选中的频道id，如果子频道id有值，选中的是子频道，否则是父频道
            String classId=classid;
            if(StringUtils.isNotBlank(ccid)){
                classId=ccid;
            }else{
                params.put("showall", "1");
            }

            params.put("classId",classId);
            params.put("delFlag", 0);
            params.put("paperName",paperEntity.getPaperName());
            params.put("orderByColumns","T1.RESERVED_8 DESC,T1.PAPER_ORDER DESC,T1.ISSUE_DATE DESC,T1.INPUT_TIME DESC");
            //查询引用频道
            if (!REF_PAPER_CHANNELS.contains("," + paperEntity.getClassId() + ",")) {
                pages = this.findPaperByIdList(page,params,curUser);
            }else{
                //查询引用频道
                params.put("refClassId",classId);
                pages = this.findPaperRefPageListByParams(page,params,paperEntity.getPageNum(), paperEntity.getPageSize(),curUser);
            }

            baseDto.setPageNum(pages.getCurrent());
            baseDto.setPageSize(pages.getSize());
            baseDto.setTotal(pages.getTotal());
            baseDto.setTotalPage(pages.getPages());
            result= pages.getRecords();
        }catch (Exception e){
            e.printStackTrace();
            log.error("首页获取文档列表失败（listfrontpapers）",e);
        }
        List<PaperEntity> pe = PaperEntityToPaperDOConvertor.INSTANCE.dOToEntityList(result);
        // 处理查询出来的数据标题
        processPaperList(pe, params, false);
        return new PageResult(pe,baseDto);
    }

    @Override
    public PaperEntity papersQueryRet(PaperEntity paperEntity, String classid, String ccid, HttpServletRequest request) {
        try{
            Map<String,Object> params = Maps.newHashMap();
            List<ChannelEntity> channelList =channelRepository.queryChildChannlesById(classid);//子频道列表
            ChannelEntity parentChannel=channelRepository.findChannelById(classid);//父频道对象
            paperEntity.setParentChannel(parentChannel);
            paperEntity.setChannelList(channelList);


            UserModel curUser = tokenService.getLoginUser(request).getUserModel();
            String classId=classid;
            boolean hasChannelAuth = false;//默认没有发布权限
            if(StringUtils.isNotBlank(ccid)){
                classId=ccid;
                //判断用户是否有发布权限，true：有，false：没有(只有在子频道下判断)
                hasChannelAuth =channelRepository.isChannelAuthList(classId,curUser);
            }else{
                params.put("showall", "1");
            }
            paperEntity.setHasChannelAuth(hasChannelAuth);

            ChannelEntity curChannel=channelRepository.findChannelById(classId);
            paperEntity.setCurchannel(curChannel);

            params.put("classId",classId);
            params.put("paperName",paperEntity.getPaperName());


            paperEntity.setChannelPathName2(channelRepository.initChannelPathName(paperEntity.getClassId()));
            paperEntity.setParams(params);

        }catch (Exception e){
            e.printStackTrace();
            log.error("统一查询返回参数（papersQueryRet）",e);
        }
        return paperEntity;
    }

    @Override
    public PaperEntity getInit(String classId) {
        PaperEntity paperEntity = new PaperEntity();
        paperEntity.setClassId(classId);
        paperEntity.setIsParent(channelRepository.isHasChildrenChannel(classId));
        return paperEntity;
    }

    @Override
    public PaperEntity getById(String paperId) {
        LambdaQueryWrapper<PaperDO> wrapper = new LambdaQueryWrapper();
        wrapper.eq(PaperDO::getPaperId, paperId);
        PaperDO paperDO = paperMapper.selectOne(wrapper);
        return PaperEntityToPaperDOConvertor.INSTANCE.dOToEntity(paperDO);
    }

    @Override
    public List<PaperEntity> getByIds(List<String> paperIds) {
        LambdaQueryWrapper<PaperDO> wrapper = new LambdaQueryWrapper();
        wrapper.in(PaperDO::getPaperId, paperIds);
        List<PaperDO> paperDOList = paperMapper.selectList(wrapper);
        return PaperEntityToPaperDOConvertor.INSTANCE.dOToEntityList(paperDOList);
    }

    private IPage<PaperDO> findPaperRefPageListByParams(Page<PaperDO> page, Map<String, Object> paper, Long pageNum, Long pageSize, UserModel curUser) {
        IPage<PaperDO> result = new Page<>();
        if (null != curUser.getCompanyId() ) {
            if(paper.containsKey("classId")){
                String classId = paper.get("classId").toString();
                // 将当前的频道id作为引用频道id查询，查询之后复原
                paper.remove("classId");
                paper.put("channelNid", classId);
            }
            result = paperMapper.findPaperRefListByParams(paper);
            if(paper.containsKey("classId")){
                paper.put("classId", paper.get("classId").toString());
            }
            List<PaperEntity> paperE= PaperEntityToPaperDOConvertor.INSTANCE.dOToEntityList(result.getRecords());
            // 根据PAPER_ORDER，添加置顶状态标识topStatus，true：已置顶 false：未置顶
            for (PaperEntity item : paperE) {
                Date date = item.getPaperOrder();
                if (null == date || "1900".equals(DateUtil.getDateFormat(date, "yyyy"))) {
                    // 未置顶
                    item.setTopStatus(false);
                } else {
                    // 已置顶
                    item.setTopStatus(true);
                }
            }
            List<PaperDO> paDto = PaperEntityToPaperDOConvertor.INSTANCE.entityToDOList(paperE);
            result.setRecords(paDto);
        }
        return result;
    }

    private IPage<PaperDO> findPaperByIdList(Page<PaperDO> page,Map<String, Object> paper, UserModel curUser) {
        IPage<PaperDO> result = new Page<>();
        List<PaperEntity> pe = new ArrayList<>();
        if (null != curUser.getCompanyId() && !curUser.getCompanyId().startsWith("99")) {
            if (paper.containsKey("refClassId")) {// 查询引用频道数据
                result = paperMapper.findPaperRefByIdList(paper);
            } else {// 查询一般频道数据
                paper.put("userId", curUser.getUserid());
                paper.put("companyId", curUser.getCompanyId());
                paper.put("deptId", curUser.getDeptId());
                paper.put("orgId", curUser.getOrgId());
                paper.put("delFlag", "0");
                result = paperMapper.findPaperByIdList(page,paper);
            }
            pe = PaperEntityToPaperDOConvertor.INSTANCE.dOToEntityList(result.getRecords());
        } else if (ConstantSys.CZ_ADMIN.equals(curUser.getUserid())) {
            paper.put("publicFlag", "2");
            paper.put("delFlag", "0");
            result = paperMapper.findPaperListByCzadmin(page,paper);
        }
        List<PaperDO> paDto = PaperEntityToPaperDOConvertor.INSTANCE.entityToDOList(pe);
        result.setRecords(paDto);
        return result;
    }
}
