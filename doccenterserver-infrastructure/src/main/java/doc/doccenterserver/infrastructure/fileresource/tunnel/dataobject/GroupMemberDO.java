package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自定义组成员
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_group_member`")
@NoArgsConstructor
public class GroupMemberDO {

    @Field(name = "主键")
    @TableField("group_memberid")
    private String groupMemberid;

    @Field(name = "排序号")
    @TableField("member_sort")
    private Integer memberSort;

    @Field(name = "组id")
    @TableField("group_id")
    private String groupId;

    @Field(name = "成员ID")
    @TableField("uniquemember")
    private String uniquemember;

    @Field(name = "成员类型")
    @TableField("uniquemember_type")
    private String uniquememberType;
}
