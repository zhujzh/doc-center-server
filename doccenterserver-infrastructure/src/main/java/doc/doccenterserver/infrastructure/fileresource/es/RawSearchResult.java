package doc.doccenterserver.infrastructure.fileresource.es;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * 搜索集合
 *
 * @author bluesky
 */
@Data
public class RawSearchResult {

    private String code = "200";
    private boolean flag = true;
    private String message = "success";
    private long totalNum;
    private int seconds;
    private List<SearchDocument> list = Lists.newArrayList();
}
