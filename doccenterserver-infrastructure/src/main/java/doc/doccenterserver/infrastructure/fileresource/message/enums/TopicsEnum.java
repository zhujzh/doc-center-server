package doc.doccenterserver.infrastructure.fileresource.message.enums;

public enum TopicsEnum {
    ZB("doc-center-server-send-zb-test-topic", "总部"),
    SP("doc-center-server-send-sp-test-topic", "四平"),
    CZ("doc-center-server-send-cz-test-topic", "郴州"),
    WZ("doc-center-server-send-wz-test-topic", "吴忠"),
    ;
    private final String value;
    private final String name;
    
    TopicsEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }
    public String getValue() {
        return this.value;
    }


}
