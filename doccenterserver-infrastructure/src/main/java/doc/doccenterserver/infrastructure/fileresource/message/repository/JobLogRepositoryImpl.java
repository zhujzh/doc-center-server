package doc.doccenterserver.infrastructure.fileresource.message.repository;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.document.repository.ChannelRepository;
import doc.doccenterserver.domain.fileresource.message.model.JobDetailLogEntity;
import doc.doccenterserver.domain.fileresource.message.repository.JobDetailRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import doc.doccenterserver.infrastructure.fileresource.message.config.MessageXmlConfig;
import doc.doccenterserver.infrastructure.fileresource.message.enums.*;
import doc.doccenterserver.infrastructure.fileresource.message.mqs.KafkaSend;
import doc.doccenterserver.infrastructure.fileresource.message.utils.IdentifierGenerator;
import doc.doccenterserver.infrastructure.fileresource.message.utils.JdomXmlUtil;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.*;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.rmi.ServerException;
import java.time.LocalDateTime;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.List;


/**
 * 任务主表
 */
@Slf4j
@Component("FILEJobLogDORepository")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class JobLogRepositoryImpl extends ServiceImpl<JobLogMapper, JobLogDO> implements JobLogRepository {
    private final ChannelRepository channelRepository;
    private final JSONObject parameterMap;
    private final JobDetailRepository jobDetailRepository;
    private final MqSaveMapper mqSaveMapper;
    private final MqChangelogMapper mqChangelogMapper;
    private final ArchiveChangelogMapper archiveChangelogMapper;
    private final DocConvertChangelogMapper docConvertChangelogMapper;
    private final DocPublishChangelogMapper docPublishChangelogMapper;
    private final DocIndexChangelogMapper docIndexChangelogMapper;
    private final SendMqChangelogMapper sendMqChangelogMapper;
    private final JobLogMapper jobLogMapper;
    private final JobDetailLogMapper jobDetailLogMapper;
    private final KafkaSend kafkaSend;


    @Override
    public ChannelEntity cmsCl(Document doc) throws ServerException, JDOMException {
        ChannelEntity channel;
        String category = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.CATEGORY);
        String sourceApp = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SENDER);
        if(category==null || StringUtils.isBlank(category.trim())){
            return null;
        }
        String classId = channelRepository.getChannelId(category, null, sourceApp);
        if (null == classId) {
            throw new ServerException("CMS文档特殊处理，如果是删除或新增发布信息，并且归属栏目是在指定烟厂栏目下，需要马上转发到其目的烟厂：没有找到指定频道..............." + category);
        }
        channel = channelRepository.getById(classId);
        if (channel==null || channel.getOrgId()==null){
            throw new ServerException("CMS文档特殊处理，如果是删除或新增发布信息，并且归属栏目是在指定烟厂栏目下，需要马上转发到其目的烟厂:没有找到指定的频道信息...............channelId=" + classId);
        }
        return channel;
    }

    @Override
    public boolean msgIn(String mqmsg) throws Exception {
        boolean result = false;
        //预处理xml
        String newmqmsg = JdomXmlUtil.precorrectxml(mqmsg);
        Document doc = JdomXmlUtil.xml2Document(newmqmsg);
        String sourceApp = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SENDER);
        String sourcePaperId = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SOURCE_PAPER_ID);
        String logname = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SUBJECT);
        String operation = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.OPERATION);
        String sourceAppId = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SENDER);
        String sourceAppName = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SENDER);
        String archiveOrgCode = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.ARCHIVE_ORG_CODE);
        //CMS文档特殊处理
        if ("CMS".equals(sourceApp)) {
            ChannelEntity channel = this.cmsCl(doc);
            if (channel!=null) {
                archiveOrgCode = channel.getOrgId();
            }
            sourcePaperId = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.CMS_SOURCE_PAPER_ID);
        }
        String logId = IdentifierGenerator.createPrimaryKeySeq();
        JobLogDO joblog = new JobLogDO(logId, mqmsg, logname, operation, sourceAppId, sourceAppName, sourcePaperId, JobLogStatusEnum.STATUS_Q.getValue(), archiveOrgCode);
        MqSaveDO mqSave = new MqSaveDO(logId, mqmsg, new Date(), operation, sourceAppId, sourcePaperId, archiveOrgCode);

        //从表(《消息入库》任务完成
        jobDetailRepository.insert(logId, mqmsg, "成功", JobDetailLogStepIdEnum.STEP_ID_1.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_SUCCESS.getValue());

        //根据应用ID ,归档组织和应用文档编号，去统一消息暂存日志表是否存在
        if (Boolean.FALSE.equals(checkMqSaveExist(sourceApp, sourcePaperId, archiveOrgCode)) && Boolean.FALSE.equals(checkJobLogExist(sourceApp, sourcePaperId, archiveOrgCode))) {
            joblog = new JobLogDO(logId, mqmsg, logname, operation, sourceAppId, sourceAppName, sourcePaperId, JobLogStatusEnum.STATUS_N.getValue(), archiveOrgCode);
            //插入消息入库changelog表
            MqChangelogDO mqChangelog = new MqChangelogDO(logId, mqmsg, new Date(), operation, 0, archiveOrgCode);
            mqChangelogMapper.insert(mqChangelog);
            //从表(《消息处理》任务未完成
            jobDetailRepository.insert(logId, mqmsg, null, JobDetailLogStepIdEnum.STEP_ID_2.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_HANDLING.getValue());
        } else {
            mqSaveMapper.insert(mqSave);
        }
        joblog.setCreateTime(LocalDateTime.now());
        this.save(joblog);
        //将集团消息进行复制分发到对应厂部文档中心MQ接收队列进行归档
        this.distributionOrgMsg(sourceApp, archiveOrgCode, mqmsg);
        return result;
    }


    @Override
    public void clearDate4Repeated(String logId, Integer stepId) throws ServerException {
        if (JobDetailLogStepIdEnum.STEP_ID_2.getValue().equals(stepId)) {
            mqChangelogMapper.deleteById(logId);
        } else if (JobDetailLogStepIdEnum.STEP_ID_3.getValue().equals(stepId)) {
            archiveChangelogMapper.deleteById(logId);
        } else if (JobDetailLogStepIdEnum.STEP_ID_4.getValue().equals(stepId)) {
            docConvertChangelogMapper.deleteById(logId);
        } else if (JobDetailLogStepIdEnum.STEP_ID_5.getValue().equals(stepId)) {
            docPublishChangelogMapper.deleteById(logId);
        } else if (JobDetailLogStepIdEnum.STEP_ID_6.getValue().equals(stepId)) {
            docIndexChangelogMapper.deleteById(logId);
        } else if (JobDetailLogStepIdEnum.STEP_ID_7.getValue().equals(stepId)) {
            sendMqChangelogMapper.deleteById(logId);
        }
        //更新任务日志从表任务状态为会略
        JobDetailLogEntity jobDetailLogEntity=new JobDetailLogEntity(logId,null,null,null,null,stepId,JobDetailLogStepStatusEnum.STEP_STATUS_IGNOR.getValue(),new Date());
        jobDetailRepository.updateByStepId(jobDetailLogEntity);
        //更新任务日志表 状态为异常
        JobLogDO jobLog = new JobLogDO();
        jobLog.setLogId(logId);
        jobLog.setStatus(JobLogStatusEnum.STATUS_IGNOR.getValue());
        getBaseMapper().updateById(jobLog);
    }


    /**
     * 消息入库下发集团
     */
    private void distributionOrgMsg(String sourceApp, String archiveOrgCode, String msg) {
        if (SourceAppEnum.OA.getValue().equals(sourceApp) || SourceAppEnum.ZYOA.getValue().equals(sourceApp)) {
            if (ArchiveOrgCodeEnum.SP.getValue().equals(archiveOrgCode)) {
                kafkaSend.send(TopicsEnum.SP.getValue(),msg);
            }
            if (SourceAppEnum.ZYOA.getValue().equals(sourceApp) && ArchiveOrgCodeEnum.CZ.getValue().equals(archiveOrgCode)) {
                kafkaSend.send(TopicsEnum.CZ.getValue(),msg);
            }
        } else if (SourceAppEnum.ZYSTDD.getValue().equals(sourceApp)) {
            if (ArchiveOrgCodeEnum.SP.getValue().equals(archiveOrgCode)) {
                kafkaSend.send(TopicsEnum.SP.getValue(),msg);
            } else if (ArchiveOrgCodeEnum.WZ.getValue().equals(archiveOrgCode)) {
                kafkaSend.send(TopicsEnum.WZ.getValue(),msg);
            } else if (ArchiveOrgCodeEnum.CZ.getValue().equals(archiveOrgCode)) {
                kafkaSend.send(TopicsEnum.CZ.getValue(),msg);
            }
        }
    }

    /**
     * 消息是否重复消费
     * 重复消费返回true, 未重复消费放回false
     */
    @Override
    public Boolean checkMqSaveExist(String sourceApp, String sourcePaperId, String archiveOrgCode) {
        LambdaQueryWrapper<MqSaveDO> lqw = new LambdaQueryWrapper<>();
        lqw.eq(MqSaveDO::getSourceApp, sourceApp);
        lqw.eq(MqSaveDO::getSourcePaperId, sourcePaperId);
        lqw.eq(MqSaveDO::getArchiveOrgCode, archiveOrgCode);
        List<MqSaveDO> result = mqSaveMapper.selectList(lqw);
        return !result.isEmpty();
    }

    @Override
    public Boolean checkMqSaveExist(String paperId) {
        LambdaQueryWrapper<JobLogDO> lqw = new LambdaQueryWrapper<>();
        lqw.eq(JobLogDO::getPaperId, paperId);
        JobLogDO jobLogDO = this.list(lqw).stream().findAny().orElse(null);
        return jobLogDO != null && this.checkMqSaveExist(jobLogDO.getSourceAppId(), jobLogDO.getSourcePaperId(), jobLogDO.getArchiveOrgCode());

    }

    /**
     * 主表是否存在
     */
    public Boolean checkJobLogExist(String sourceApp, String sourcePaperId, String archiveOrgCode) {
        LambdaQueryWrapper<JobLogDO> lqw = new LambdaQueryWrapper<>();
        lqw.eq(JobLogDO::getSourceAppId, sourceApp);
        lqw.eq(JobLogDO::getSourcePaperId, sourcePaperId);
        lqw.eq(JobLogDO::getArchiveOrgCode, archiveOrgCode);
        lqw.eq(JobLogDO::getStatus, JobLogStatusEnum.STATUS_N.getValue());
        List<JobLogDO> result = getBaseMapper().selectList(lqw);
        return null != result && !result.isEmpty();
    }


    public void clearChangeLog(String logId) {
        JobLogDO jobLog = this.getById(logId);
        List<JobLogDO> jobloglist = this.list(new LambdaQueryWrapper<JobLogDO>()
                .eq(JobLogDO::getSourceAppId, jobLog.getSourceAppId())
                .eq(JobLogDO::getSourcePaperId, jobLog.getSourcePaperId())
                .eq(JobLogDO::getArchiveOrgCode, jobLog.getArchiveOrgCode()));
        try {
            List<JobLogDO> updatelist= jobloglist.stream()
                    .filter(log->log.getLogId().equals(logId))
                    .collect(Collectors.toList());
            for (JobLogDO log : updatelist) {
                this.updateStatus(log.getLogId(),JobLogStatusEnum.STATUS_Y.getValue());
            }
            List<JobLogDO> removelist= jobloglist.stream()
                    .filter(log-> !log.getLogId().equals(logId) && log.getStatus().equals(JobLogStatusEnum.STATUS_E.getValue()))
                    .collect(Collectors.toList());
            for (JobLogDO log : removelist) {
                this.updateStatus(log.getLogId(),JobLogStatusEnum.STATUS_IGNOR.getValue());
                mqChangelogMapper.deleteById(log.getLogId());
                archiveChangelogMapper.deleteById(log.getLogId());
                docConvertChangelogMapper.deleteById(log.getLogId());
                docIndexChangelogMapper.deleteById(log.getLogId());
                docPublishChangelogMapper.deleteById(log.getLogId());
                sendMqChangelogMapper.deleteById(log.getLogId());
            }
        } catch (Exception ex) {
            log.error("clearChangeLog==========", ex);
        }
    }

    @Override
    public Boolean updateStatus(String logId, String status) {
        JobLogDO jobLog = new JobLogDO();
        jobLog.setLogId(logId);
        jobLog.setStatus(status);
        return this.updateById(jobLog);
    }

    @Override
    public void updateError(String logId, String msg, Integer stepId, Integer stepStatus, Integer retryTimes) {
        try {
            JobDetailLogEntity jobDetailLogEntity = new JobDetailLogEntity(logId, msg, stepId, stepStatus, new Date());
            jobDetailRepository.updateByStepId(jobDetailLogEntity);
            if (retryTimes >= 4) {
                this.updateStatus(logId, JobLogStatusEnum.STATUS_E.getValue());
            }
            if (JobDetailLogStepIdEnum.STEP_ID_2.getValue().equals(stepId)) {
                MqChangelogDO mqChangelog = new MqChangelogDO();
                mqChangelog.setLogId(logId);
                mqChangelog.setRetryTimes(retryTimes);
                mqChangelogMapper.updateById(mqChangelog);
            } else if (JobDetailLogStepIdEnum.STEP_ID_3.getValue().equals(stepId)) {
                ArchiveChangelogDO archiveChangelog = new ArchiveChangelogDO();
                archiveChangelog.setLogId(logId);
                archiveChangelog.setRetryTimes(retryTimes);
                archiveChangelogMapper.updateById(archiveChangelog);
            } else if (JobDetailLogStepIdEnum.STEP_ID_4.getValue().equals(stepId)) {
                DocConvertChangelogDO docConvertChangelogDO = new DocConvertChangelogDO();
                docConvertChangelogDO.setLogId(logId);
                docConvertChangelogDO.setRetryTimes(retryTimes);
                docConvertChangelogMapper.updateById(docConvertChangelogDO);
            } else if (JobDetailLogStepIdEnum.STEP_ID_5.getValue().equals(stepId)) {
                DocPublishChangelogDO docPublishChangelogDO = new DocPublishChangelogDO();
                docPublishChangelogDO.setLogId(logId);
                docPublishChangelogDO.setRetryTimes(retryTimes);
                docPublishChangelogMapper.updateById(docPublishChangelogDO);
            } else if (JobDetailLogStepIdEnum.STEP_ID_6.getValue().equals(stepId)) {
                DocIndexChangelogDO docIndexChangelogDO = new DocIndexChangelogDO();
                docIndexChangelogDO.setLogId(logId);
                docIndexChangelogDO.setRetryTimes(retryTimes);
                docIndexChangelogMapper.updateById(docIndexChangelogDO);
            } else if (JobDetailLogStepIdEnum.STEP_ID_7.getValue().equals(stepId)) {
                SendMqChangelogDO sendMqChangelogDO = new SendMqChangelogDO();
                sendMqChangelogDO.setLogId(logId);
                sendMqChangelogDO.setRetryTimes(retryTimes);
                sendMqChangelogMapper.updateById(sendMqChangelogDO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
