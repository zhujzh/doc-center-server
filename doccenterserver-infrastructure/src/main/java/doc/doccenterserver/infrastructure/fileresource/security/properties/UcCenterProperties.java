package doc.doccenterserver.infrastructure.fileresource.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author wangxp
 * @description 用户中心
 * @date 2023/6/12 17:57
 */
@Data
@Component
@ConfigurationProperties(prefix = "uc-center")
public class UcCenterProperties {

    private String hostName;

    private String orgBizCode;
}
