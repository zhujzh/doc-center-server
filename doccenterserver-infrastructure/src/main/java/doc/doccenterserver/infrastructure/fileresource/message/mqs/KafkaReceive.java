package doc.doccenterserver.infrastructure.fileresource.message.mqs;

import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * @author wangxp
 * @description 接收云上至云下esb 消息
 * @date 2023/3/13 15:05
 */
@Slf4j
@Component("kafkaListener")
@RequiredArgsConstructor
public class KafkaReceive{
    private final JobLogRepository jobLogRepository;
    @KafkaListener(topics = "${my-kafka.receiveName}", groupId = "1")
    public void listen(ConsumerRecord<?, ?> record, Acknowledgment acknowledgment) {
        Optional<?> kafkaMessage = Optional.ofNullable(record.value());
        if (kafkaMessage.isPresent()) {
            String message = (String) kafkaMessage.get();
            log.info("收到队列消息: {}", message);
            try {
                jobLogRepository.msgIn(message);
            }catch (Exception e){
                log.error("云上消息入库异常: {}",e.getMessage());
                e.printStackTrace();
            }finally {
                acknowledgment.acknowledge();
            }
        }
    }
}

