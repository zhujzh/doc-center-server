package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.ImageEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ImageDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface ImageConvertor {

    ImageConvertor INSTANCE = Mappers.getMapper(ImageConvertor.class);

    @Mappings({})
    ImageDO entityToDO(ImageEntity parameter);

    @Mappings({})
    ImageEntity dOToEntity(ImageDO parameter);

    @Mappings({})
    List<ImageEntity> dOToEntity(List<ImageDO> parameter);

    @Mappings({})
    List<ImageDO> entityToDO(List<ImageEntity> parameter);
}
