package doc.doccenterserver.infrastructure.fileresource.system.converter;

import doc.doccenterserver.domain.fileresource.system.model.ParameterEntry;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ParameterDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * DictToDictDOConverter
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Mapper
public interface ParameterEntryToParameterDOConverter {

    ParameterEntryToParameterDOConverter INSTANCE = Mappers.getMapper(ParameterEntryToParameterDOConverter.class);

    @Mappings({})
    ParameterDO parameterEntryToParameterDO(ParameterEntry parameterEntry);
    @Mappings({})
    ParameterEntry parameterDOToParameterEntry(ParameterDO parameterDO);

    @Mappings({})
    List<ParameterDO> parameterEntryToParameterDOList(List<ParameterEntry> parameterEntry);
    @Mappings({})
    List<ParameterEntry> parameterDOToParameterEntryList(List<ParameterDO> parameterDO);
}
