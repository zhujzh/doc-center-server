package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.AttachmentDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 文档附件表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface AttachmentMapper extends BaseMapper<AttachmentDO> {
    Integer findAttCountByPaperId(String paperId);

    List<AttachmentEntity> findAttachmentListByParams(@Param("params")Map<String, Object> attachment);
}
