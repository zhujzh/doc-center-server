package doc.doccenterserver.infrastructure.fileresource.message.utils;

import cn.hutool.core.util.ObjectUtil;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.io.Writer;
import java.lang.reflect.Method;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author luobo
 */
public class StringUtil {

    /**
     * 检测字符串是否不为空(null,"","null")
     *
     * @param s
     * @return 不为空则返回true，否则返回false
     */
    public static boolean notEmpty(String s) {
        return s != null && !"".equals(s) && !"null".equals(s);
    }

    /**
     * 检测字符串是否为空(null,"","null")
     *
     * @param s
     * @return 为空则返回true，不否则返回false
     */
    public static boolean isEmpty(String s) {
        return s == null || "".equals(s) || "null".equals(s);
    }

    /**
     * 字符串转换为字符串数组
     *
     * @param str        字符串
     * @param splitRegex 分隔符
     * @return
     */
    public static String[] str2StrArray(String str, String splitRegex) {
        if (isEmpty(str)) {
            return null;
        }
        return str.split(splitRegex);
    }

    /**
     * 用默认的分隔符(,)将字符串转换为字符串数组
     *
     * @param str 字符串
     * @return
     */
    public static String[] str2StrArray(String str) {
        return str2StrArray(str, ",\\s*");
    }

    /**
     * 转换成整形，出现数字格式异常默认为defaultValue
     *
     * @param object
     * @param defaultValue
     * @return
     */
    public static int toInt(Object object, int defaultValue) {
        if (null == object) {
            return defaultValue;
        }
        try {
            return Integer.parseInt("" + object);
        } catch (NumberFormatException notint) {
            return defaultValue;
        }
    }

    /**
     * 转换成整形，出现数字格式异常默认�?
     *
     * @param object
     * @return
     */
    public static int toInt(Object object) {
        return toInt(object, 0);
    }


    public static String toNumricStr(Integer num, Integer digits) {
        String numstr = num.toString();
        while (numstr.length() < digits) {
            numstr = "0" + numstr;
        }
        return numstr;
    }


    public static void main(String[] args) {
        //String str="一、卷包车间平面7台综合测试仪需要进行数据采集，要求：1、采集卷接生产过程进行在线质量检测的检测数据。2、将采集数据集成至工艺质量系统，对数据进行分析统计，生成相应报表。3、将采集到的数据集成至卷包数采系统，在卷包数采工控机数采终端能实时查询。\r\n二、检测仪器自动采集与数据应用升级改造：1、新进红外水分安装采集和分析模块，2、填充值采集模块，3、红外校准零点记录与跟踪，4、报表生成，5、对比分析模块，6、目前在用系统升级\r\n三、系统集成";
        //System.out.println(textDecode(str));
        String test = ";2;3;4;;6;8;;";
        List<String> list = strToListWithEmptyString(test, ";");
        System.out.println(list.size());

    }

    /**
     * @方法名: handleTextArea
     * @描述:处理一个实体中的属性，如果是String，则进行转义，以方便前台换行显示
     * @作者:高星
     * @时间:2015-8-7 上午9:51:57
     * @参数:@param obj
     * @返回值：void
     */
    public void handleTextArea(Object obj) {
        if (obj != null && obj instanceof Map) {
            Map<String, Object> map = ((Map<String, Object>) obj);
            Set<String> keySet = map.keySet();
        }
    }


    /**
     * 转换textarea 中提交的数据在页面中按照textarea中一样的格式进行换行显示
     *
     * @param textarea
     * @return
     */
    public static String textarea2html(String textarea) {
        if (null != textarea) {
            return textarea.replaceAll("\n", "<br/>").replace(" ", "&nbsp;&nbsp;");
        }
        return textarea;
    }

    /**
     * 将字符串中HTML字符转换成TEXT类型字符
     *
     * @param text
     * @return
     */
    public static String textEncode(String text) {
        if (text == null) {
            return "";
        }
        text = text.replaceAll("<br/>", "\r\n");
        text = text.replaceAll("<br>", "\r\n");
        text = text.replaceAll("", "");
        text = text.replaceAll("&amp;", "&");
        text = text.replaceAll("&quot;", "\"");
        text = text.replaceAll("&lt;", "<");
        text = text.replaceAll("&gt;", ">");
        text = text.replaceAll("&#146;", "'");
        text = text.replaceAll("&nbsp;", " ");
        text = text.replaceAll("&nbsp;&nbsp;&nbsp;&nbsp;", "\t");
        return text;
    }

    /**
     * 将字符串中TEXT类型字符转换成HTML字符
     *
     * @param text
     * @return
     */
    public static String textDecode(String text) {
        if (text == null) {
            return "";
        }
        text = text.replaceAll("", "");
        text = text.replaceAll("&", "&amp;");
        text = text.replaceAll("\"", "&quot;");
        text = text.replaceAll("<", "&lt;");
        text = text.replaceAll(">", "&gt;");
        text = text.replaceAll("'", "&#146;");
        text = text.replaceAll(" ", "&nbsp;");
        text = text.replaceAll("\\\\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
        text = text.replaceAll("\r\n", "<br/>");
        text = text.replaceAll("\\r\\n", "<br/>");
        text = text.replaceAll("\\\r\\\n", "<br/>");
        text = text.replaceAll("\\\\r\\\\n", "<br/>");
        return text;
    }

    public static void handleText(Object obj) {
        if (obj != null && obj instanceof Map) {
            Map map = (Map) obj;
            Set<String> keySet = map.keySet();
            for (String key : keySet) {
                Object item = map.get(key);
                if (item != null && item instanceof String) {
                    map.put(key, StringUtil.textDecode((String) item));
                }
            }
            obj = map;
        }
    }

    /**
     * 获取字符串的实际长度（按字节长度，如一个中文长度为2）
     *
     * @param str 待返回长度的字符串
     * @return 字符串的字节长度
     */
    public static int getStrRealLength(String str) {

        if (str == null) {
            return 0;
        }

        int length = 0;

        char[] chars = str.toCharArray();

        for (char ch : chars) {

            if (ch <= 127) {
                length++;

            } else {
                length += 2;
            }
        }
        return length;
    }

    //求两个字符串数组的并集，利用set的元素唯一性
    public static String[] union(String[] arr1, String[] arr2) {
        Set<String> set = new HashSet<String>();
        for (String str : arr1) {
            set.add(str);
        }
        for (String str : arr2) {
            set.add(str);
        }
        String[] result = {};
        return set.toArray(result);
    }

    //求两个数组的交集
    public static String[] intersect(String[] arr1, String[] arr2) {
        Map<String, Boolean> map = new HashMap<String, Boolean>();
        LinkedList<String> list = new LinkedList<String>();
        for (String str : arr1) {
            if (!map.containsKey(str)) {
                map.put(str, Boolean.FALSE);
            }
        }
        for (String str : arr2) {
            if (map.containsKey(str)) {
                map.put(str, Boolean.TRUE);
            }
        }

        for (Entry<String, Boolean> e : map.entrySet()) {
            if (e.getValue().equals(Boolean.TRUE)) {
                list.add(e.getKey());
            }
        }

        String[] result = {};
        return list.toArray(result);
    }

    //求两个数组的差集
    public static String[] minus(String[] arr1, String[] arr2) {
        LinkedList<String> list = new LinkedList<String>();
        LinkedList<String> history = new LinkedList<String>();
        String[] longerArr = arr1;
        String[] shorterArr = arr2;
        //找出较长的数组来减较短的数组
        if (arr1.length > arr2.length) {
            longerArr = arr2;
            shorterArr = arr1;
        }
        for (String str : longerArr) {
            if (!list.contains(str)) {
                list.add(str);
            }
        }
        for (String str : shorterArr) {
            if (list.contains(str)) {
                history.add(str);
                list.remove(str);
            } else {
                if (!history.contains(str)) {
                    list.add(str);
                }
            }
        }

        String[] result = {};
        return list.toArray(result);
    }

    public static String convertToString(Object obj, String defaultValue) {
        if (obj == null) {
            return defaultValue;
        } else {
            return obj.toString();
        }
    }

    /**
     * @方法名: strToList
     * @描述:将以特定分隔符分割的字符串转为为List(忽略分割后得到的空字符串和首尾空字符串)
     * @作者:高星
     * @时间:2014-10-13 上午9:51:08
     * @参数:@param ids
     * @参数:@param seperator
     * @参数:@return
     * @返回值：List
     */
    public static List<String> strToList(String ids, String seperator) {
        List<String> result = Lists.newArrayList();
        if (StringUtils.isNotBlank(ids)) {
            return Lists.newArrayList(Splitter.on(seperator).trimResults().omitEmptyStrings().split(ids));
        }
        return result;
    }

    /**
     * @方法名: strToList
     * @描述:将以特定分隔符分割的字符串转为为List(不忽略分割后得到的空字符串和首尾空字符串)
     * @作者:高星
     * @时间:2014-10-13 上午9:51:08
     * @参数:@param ids
     * @参数:@param seperator
     * @参数:@return
     * @返回值：List
     */
    public static List<String> strToListWithEmptyString(String ids, String seperator) {
        List<String> result = Lists.newArrayList();
        if (StringUtils.isNotBlank(ids)) {
            return Lists.newArrayList(Splitter.on(seperator).split(ids));
        }
        return result;
    }

    /**
     * @方法名: clobToStr
     * @描述:将Clob字段转化为字符串
     * @作者:高星
     * @时间:2014-10-31 下午3:36:43
     * @参数:@param clob
     * @参数:@return
     * @返回值：String
     */
    public static String clobToStr(Clob clob) {
        String result = "";
        try {
            result = (clob != null ? clob.getSubString(1, (int) clob.length()) : "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Clob strToClob(String str, Clob lob) throws Exception {
        Method methodToInvoke = lob.getClass().getMethod(
                "getCharacterOutputStream", (Class[]) null);
        Writer writer = (Writer) methodToInvoke.invoke(lob, (Object[]) null);
        writer.write(str);
        writer.close();
        return lob;
    }

    /**
     * 功能: 按照指定字节长度截取字符串并选择在后面是否加...
     *
     * @param str         需要截取的字符串
     * @param offset      需要截取的字节长度，不包括追加的…
     * @param addEllipsis 是否添加省略号…  true 是  false 否
     * @return String 截取后的字符串
     */
    public static String getSubStr(String str, int offset, boolean addEllipsis) {
        if (str == null) {
            return "";
        }
        int realLength = getStrRealLength(str);
        if (realLength <= offset) {
            return str;
        }
        int length = 0;
        StringBuilder sb = new StringBuilder();
        char[] chars = str.toCharArray();
        int ii = 0;
        while (length < offset) {
            char ch = chars[ii++];
            if (ch <= 127) {
                length++;
            } else {
                length += 2;
            }
            sb.append(ch);
        }
        if (addEllipsis == true) {
            sb.append("…");
        }
        return sb.toString();
    }

    public static boolean contains(String str1, String str2, String separator) {
        boolean b = false;
        if (StringUtils.isNotBlank(str1) && StringUtils.isNotBlank(str2)) {
            if (null != separator) {
                b = str1.indexOf(separator + str2 + separator) > -1;
            } else {
                b = str1.indexOf(str2) > -1;
            }
        }
        return b;
    }

    public static void handleMapListData(List<Object> dataList) {
        if (dataList != null && dataList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                Object obj = dataList.get(i);
                handleObjWithClob(obj);
            }
        }
    }

    /**
     * @方法名: handleObjWithClob
     * @描述:处理包含Clob类型的Object（必须是map或者map的子类）
     * @作者:高星
     * @时间:2016年5月5日 上午9:51:00
     * @参数:@param obj
     * @返回值：void
     */
    public static void handleObjWithClob(Object obj) {
        Map<String, Object> map = (Map<String, Object>) obj;
        Set<String> keySet = map.keySet();
        for (String key : keySet) {
            Object value = map.get(key);
            String className = value.getClass().getSimpleName();
            if ("clob".equals(className.toLowerCase()) || className.toLowerCase().indexOf("clob") > -1) {
                map.put(key, StringUtil.clobToStr((Clob) map.get(key)));
            }
        }
        obj = map;
    }

    /**
     * @方法名: processPaper
     * @描述:将返回页面模块的数据进行数据处理
     * @作者:邓敏杰
     * @时间:2016年5月18日 下午17:31:00
     * @参数:String paperName
     * @返回值：void
     */
    public static String processPaper(String papertitle, Integer titlenum) {
        if (papertitle.startsWith("中共湖南中烟工业有限责任公司机关委员会")) {
            papertitle = papertitle.substring(19);
        } else if (papertitle.startsWith("湖南中烟工业有限责任公司董事会办公室")) {
            papertitle = papertitle.substring(18);
        } else if (papertitle.startsWith("中共湖南中烟工业有限责任公司党组")) {
            papertitle = papertitle.substring(16);
        } else if (papertitle.startsWith("中共湖南中烟工业有限责任公司")) {
            papertitle = papertitle.substring(14);
        } else if (papertitle.startsWith("湖南中烟工业有限责任公司办公室")) {
            papertitle = papertitle.substring(15);
        } else if (papertitle.startsWith("湖南中烟工业有限责任公司")) {
            papertitle = papertitle.substring(12);
        }
        if (papertitle.length() > titlenum) {
            papertitle = papertitle.substring(0, titlenum);
            papertitle = papertitle + "...";
        }
        return papertitle;
    }

    /**
     * 去除特殊字符。
     *
     * @param str
     * @return
     * @throws PatternSyntaxException
     */
    public static String StringFilter(String str) throws PatternSyntaxException {
        String regEx = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？-]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll("").trim();
    }

    /**
     * 计算文件大小
     *
     * @param objSize
     * @return
     * @throws PatternSyntaxException
     */
    public static String fileSize(double objSize) throws PatternSyntaxException {
        //格式化文件大小
        int objSizeFlag = 0;
        do {
            objSize = objSize / 1024;
            objSizeFlag++;
        } while (objSize > 99);
        String[] sFlag = {"B", "KB", "MB", "GB", "TB", "PB", "EB"};
        String objs = String.format("%.1f", objSize) + sFlag[objSizeFlag];
        return objs;
    }

    /**
     * 获取浏览器版本
     *
     * @param String
     * @return
     * @throws PatternSyntaxException
     */
    public static String getBrowser(String userAgent) {
        if (userAgent == null || userAgent.trim().length() < 1) {
            return "unknow ";
        }
        String[] brorserEN = new String[]{
                "MyIE2",
                "Firefox",
                "KuGooSoft",
                "LBBROWSER",
                "TheWord",
                "QQ",
                "Maxthon",
                "BIDUPlayerBrowser",
                "Opera",
                "Chrome",
                "Safari",
                "9A334",
                "UCWEB",
                "googlebot",
                "rv:11.0"};
        String[] brorserCN = new String[]{
                "MyIE2",
                "Firefox",
                "酷狗",
                "猎豹",
                "世界之窗",
                "QQ",
                "Maxthon",
                "百度影音",
                "Opera",
                "Chrome",
                "Safari",
                "360",
                "UCWEB",
                "googlebot",
                "IE 11.0"};
        for (int i = 0; i < brorserEN.length; i++) {
            if (userAgent.indexOf(brorserEN[i]) > -1) {
                return brorserCN[i];
            }
        }
        if (userAgent.indexOf("MSIE") > -1) {
            if (userAgent.indexOf("MSIE 9.0") > -1) {
                return "IE 9.0";
            } else if (userAgent.indexOf("MSIE 10.0") > -1) {
                return "IE 10.0";
            } else if (userAgent.indexOf("MSIE 8.0") > -1) {
                return "IE 8.0";
            } else if (userAgent.indexOf("MSIE 7.0") > -1) {
                return "IE 7.0";
            } else if (userAgent.indexOf("MSIE 6.0") > -1) {
                return "IE 6.0";
            }
            return "IE";
        }
        return "unknow Browser";
    }

    public static String removeHTMLTag(String input, int length) {
        if (input == null || "".equals(input.trim())) {
            return "";
        }
        // 去掉所有html元素
        String str = input.replaceAll("\\&[a-zA-Z]{1,10};", "").replaceAll("<[^>]*>", "");
        str = str.replaceAll("[(/>)<]", "");
        int len = str.length();
        if (len <= length || length == 0) {
            return str;
        } else {
            str = str.substring(0, length);
            str += "......";
        }
        return str;
    }

    /**
     * <b>function:</b> 处理oracle sql 语句in子句中（where id in (1, 2, ..., 1000, 1001)），
     * 如果子句中超过1000项就会报错。
     * 这主要是oracle考虑性能问题做的限制。
     * 如果要解决次问题，可以用 where id (1, 2, ..., 1000) or id (1001, ...)
     *
     * @param ids   in语句中的集合对象
     * @param count in语句中出现的条件个数
     * @param field in语句对应的数据库查询字段
     * @return 返回 field in (...) or field in (...) 字符串
     * @author hoojo
     * @createDate 2012-8-31 下午02:36:03
     */
    public static String getOracleSQLIn(List<?> ids, int count, String field) {
        count = Math.min(count, 1000);
        int len = ids.size();
        int size = len % count;
        if (size == 0) {
            size = len / count;
        } else {
            size = (len / count) + 1;
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            int fromIndex = i * count;
            int toIndex = Math.min(fromIndex + count, len);
            //System.out.println(ids.subList(fromIndex, toIndex));
            String productId = StringUtils.defaultIfEmpty(StringUtils.join(ids.subList(fromIndex, toIndex), "','"), "");
            if (i != 0) {
                builder.append(" or ");
            }
            builder.append(field).append(" in ('").append(productId).append("')");
        }

        return StringUtils.defaultIfEmpty(builder.toString(), field + " in ('')");
    }

    /**
     * @方法名: splitList
     * @描述:切割一个集合成嵌套集合，切割后的每个集合包含最多splitSize个元素
     * @作者:高星
     * @时间:2017年1月12日 下午2:33:23
     * @参数:@param targetList
     * @参数:@param splitSize
     * @参数:@return
     * @返回值：List<List<E>>
     */
    public static <E> List<List<E>> splitList(List<E> targetList, Integer splitSize) {
        if (targetList == null) {
            return null;
        }
        Integer size = targetList.size();
        List<List<E>> resultList = new ArrayList<List<E>>();
        if (size <= splitSize) {
            resultList.add(targetList);
        } else {
            for (int i = 0; i < size; i += splitSize) {
                //用于限制最后一部分size小于splitSize的list
                Integer limit = i + splitSize;
                if (limit > size) {
                    limit = size;
                }
                resultList.add(targetList.subList(i, limit));
            }
        }
        return resultList;
    }


    public static String objToStr(Object obj) {
        if(ObjectUtil.isEmpty(obj)){
            return null;
        }
     return String.valueOf(obj);
    }
}
