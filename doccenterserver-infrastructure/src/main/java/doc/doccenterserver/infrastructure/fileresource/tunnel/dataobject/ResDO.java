package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统资源
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_pub_res`")
@NoArgsConstructor
public class ResDO {

    @TableId
    @Field(name = "RES_ID")
    @TableField("res_id")
    private String resId;

    @Field(name = "RES_KEY")
    @TableField("res_key")
    private String resKey;

    @Field(name = "RES_TYPE")
    @TableField("res_type")
    private String resType;

    @Field(name = "RES_VALUE")
    @TableField("res_value")
    private String resValue;

    @Field(name = "RES_PARENT_ID")
    @TableField("res_parent_id")
    private String resParentId;

    @Field(name = "RES_SORT")
    @TableField("res_sort")
    private Integer resSort;

    @Field(name = "AUTH_YES_NO")
    @TableField("auth_yes_no")
    private Integer authYesNo;

    @Field(name = "资源图标")
    @TableField("res_icon")
    private String resIcon;
}
