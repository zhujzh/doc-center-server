package doc.doccenterserver.infrastructure.fileresource.file.converter;

import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.FileDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * DictToDictDOConverter
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Mapper
public interface FileEntityToFileDOConverter {

    FileEntityToFileDOConverter INSTANCE = Mappers.getMapper(FileEntityToFileDOConverter.class);

    @Mappings({})
    FileDO fileEntityToFileDO(FileEntity file);
    @Mappings({  })
    FileEntity fileDOToFileEntity(FileDO fileDO);
    @Mappings({ })
    List<FileEntity> fileDOToFileEntityList(List<FileDO> fileDOList);
    @Mappings({ })
    List<FileDO> fileEntityToFileDOList(List<FileEntity> fileDOList);
}
