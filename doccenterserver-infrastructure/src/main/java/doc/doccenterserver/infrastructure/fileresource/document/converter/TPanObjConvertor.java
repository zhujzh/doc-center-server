package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.file.model.TPanObjEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.TPanObjDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */

@Mapper
public interface TPanObjConvertor {

    TPanObjConvertor INSTANCE = Mappers.getMapper(TPanObjConvertor.class);

    @Mappings({})
    TPanObjDO entityToDO(TPanObjEntity parameter);

    @Mappings({})
    TPanObjEntity dOToEntity(TPanObjDO parameter);

    @Mappings({})
    List<TPanObjEntity> dOToEntity(List<TPanObjDO> parameter);

    @Mappings({})
    List<TPanObjDO> entityToDO(List<TPanObjEntity> parameter);
}
