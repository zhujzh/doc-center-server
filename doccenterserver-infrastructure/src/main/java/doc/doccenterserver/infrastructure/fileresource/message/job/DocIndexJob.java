package doc.doccenterserver.infrastructure.fileresource.message.job;

import com.xxl.job.core.handler.annotation.XxlJob;
import doc.doccenterserver.domain.fileresource.message.repository.DocConvertChangelogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.DocIndexChangelogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文档创建索引
 */
@Component
@Slf4j
public class DocIndexJob {
    @Autowired
    private DocIndexChangelogRepository docIndexChangelogRepository;
    @XxlJob("docIndex")
    public void  docIndex(){
        docIndexChangelogRepository.docIndex();
        log.info("调用了");
    }
}
