package doc.doccenterserver.infrastructure.fileresource.message.repository;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import doc.doccenterserver.domain.fileresource.file.repository.FileRepository;
import doc.doccenterserver.domain.fileresource.message.repository.DocIndexChangelogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobDetailRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import doc.doccenterserver.infrastructure.fileresource.es.SearchDocument;
import doc.doccenterserver.infrastructure.fileresource.es.SearchUtil;
import doc.doccenterserver.infrastructure.fileresource.es.UpdateDocument;
import doc.doccenterserver.infrastructure.fileresource.es.bean.SearchDmlResult;
import doc.doccenterserver.infrastructure.fileresource.file.utils.OfficeTextExtractUtil;
import doc.doccenterserver.infrastructure.fileresource.message.enums.IsMainEnum;
import doc.doccenterserver.infrastructure.fileresource.message.enums.JobDetailLogStepIdEnum;
import doc.doccenterserver.infrastructure.fileresource.message.enums.JobDetailLogStepStatusEnum;
import doc.doccenterserver.infrastructure.fileresource.message.enums.OperationTypeEnum;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.*;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/**
 * 创建索引
 */
@Slf4j
@Component("FILEDocIndexChangelogDORepository")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DocIndexChangelogRepositoryImpl extends ServiceImpl<DocIndexChangelogMapper, DocIndexChangelogDO> implements DocIndexChangelogRepository {

    private final JobDetailRepository jobDetailRepository;
    private final JobLogRepository jobLogRepository;
    private final AttachmentMapper attachmentMapper;
    private final PaperMapper paperMapper;
    private final PaperAuthMapper paperAuthMapper;
    private final UserMapper userMapper;
    private final DocIndexChangelogMapper docIndexChangelogMapper;
    private final FileRepository fileRepository;
    private final FileMapper fileMapper;
    private final ExecutorService executorService;

    @Override
    public void docIndex() {
        LambdaQueryWrapper<DocIndexChangelogDO> lqw = new LambdaQueryWrapper<>();
        lqw.lt(DocIndexChangelogDO::getRetryTimes, 4);
        List<DocIndexChangelogDO> docConvertChangeloglist = getBaseMapper().selectList(lqw);
        for (DocIndexChangelogDO docIndexChangelog : docConvertChangeloglist) {
            try {
                this.processData(docIndexChangelog);
            } catch (Exception e) {
                docIndexChangelog.setRetryTimes(docIndexChangelog.getRetryTimes() + 1);
                jobLogRepository.updateError(docIndexChangelog.getLogId(), e.getMessage(), JobDetailLogStepIdEnum.STEP_ID_6.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_FAILD.getValue(), docIndexChangelog.getRetryTimes());
            }
        }
    }

    @Override
    public boolean insertDocIndexChange(String logId, String paperId, String operation) {
        DocIndexChangelogDO ddo = new DocIndexChangelogDO();
        docIndexChangelogMapper.deleteById(logId);
        ddo.setLogId(logId);
        ddo.setPaperId(paperId);
        ddo.setOperation(operation);
        ddo.setRetryTimes(0);
        ddo.setCreateTime(new Date());
        return docIndexChangelogMapper.insert(ddo) > 0 ? true : false;
    }

    public void processData(DocIndexChangelogDO docIndexChangelog) throws Exception {
        String paperId = docIndexChangelog.getPaperId();
        String logId = docIndexChangelog.getLogId();
        String operation = docIndexChangelog.getOperation();
        //创建的时候  根据应用ID、归档组织、应用文档编号，去统一消息暂存日志表是否存在
        if (OperationTypeEnum.CREATE.getValue().equals(operation) && Boolean.TRUE.equals(jobLogRepository.checkMqSaveExist(paperId))) {
            jobLogRepository.clearDate4Repeated(docIndexChangelog.getLogId(), JobDetailLogStepIdEnum.STEP_ID_6.getValue());
            return;
        }
        senddata2index(paperId, operation);
        //更新任务日志从表《创建索引》任务状态为完成
        jobDetailRepository.updateStatus(logId, JobDetailLogStepIdEnum.STEP_ID_6.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_SUCCESS.getValue());
        //删除创建索引任务表
        this.removeById(logId);
    }

    public void senddata2index(String paperId, String operation) throws Exception {
        Map<String, Object> result = Maps.newHashMap();
        PaperDO paper = paperMapper.selectById(paperId);
        List<AttachmentDO> attlist = attachmentMapper.selectList(new LambdaQueryWrapper<AttachmentDO>()
                .eq(AttachmentDO::getPaperId, paperId).eq(AttachmentDO::getIsMain, IsMainEnum.Y.getValue()));
        SearchUtil searchUtil = SpringUtil.getBean(SearchUtil.class);
        if (OperationTypeEnum.CREATE.getValue().equals(operation)) {
            SearchDocument searchDocument = new SearchDocument();
            searchDocument.setIdx_user_id(paper.getIssueUserId());
            UserDO user = userMapper.selectById(paper.getIssueUserId());
            if (null != user) {
                searchDocument.setIdx_user_name(user.getUserName());
            } else {
                searchDocument.setIdx_user_name(paper.getIssueUserId());
            }
            searchDocument.setIdx_parent_id(paper.getClassId());
            if (1 != paper.getPaperType()) { //网页正文文档
                searchDocument.setId(paperId);
                searchDocument.setIdx_id(paperId);
                searchDocument.setIdx_title(paper.getPaperName());
                searchDocument.setIdx_suffix("html");
                searchDocument.setIdx_desc(paper.getPaperText());
            } else { //附件正文文档
                for (AttachmentDO attachment : attlist) {
                    if (attachment.getIsMain().equalsIgnoreCase("1")) {//正文附件
                        searchDocument.setId(paperId);
                        searchDocument.setIdx_id(paperId);
                        searchDocument.setIdx_title(paper.getPaperName());
                        searchDocument.setIdx_desc(paper.getPaperName());
                        searchDocument.setIdx_suffix(FilenameUtils.getExtension(attachment.getFileName()));
                        break;
                    }
                }
            }
            searchDocument.setIdx_parent_id("p_" + paper.getClassId());
            searchDocument.setIdx_date(paper.getIssueDate());
            searchDocument.setIdx_app_id(paper.getAppId());

            List<String> authstrlist = new ArrayList<String>();
            if ("Y".equalsIgnoreCase(paper.getIsPublic())) {
                authstrlist.add("public");
            } else {
                //非公开文档，指定索引权限
                List<PaperAuthDO> authlist = paperAuthMapper.selectList(new LambdaQueryWrapper<PaperAuthDO>().eq(PaperAuthDO::getPaperId, paperId));
                for (PaperAuthDO pa : authlist) {
                    String tmpstr = pa.getObjectType() + "_" + pa.getAuthObject();
                    authstrlist.add(tmpstr);
                }
                String inputUserId = paper.getInputUserId();
                authstrlist.add("u_" + inputUserId);
            }
            searchDocument.setIdx_auth_value(authstrlist);
            result = searchUtil.create(searchDocument);
            //处理正文附件，将正文附加内容抽取后更新至index, by gj 2023/5/11
            if (Boolean.TRUE == result.get("success")) {
                executorService.execute(() -> {
                    updateTextAttachToIndex(paper, attlist, searchUtil);
                });
            }
        } else {
            //删除索引
            result = searchUtil.delete(paperId);
        }
        Boolean success = (Boolean) result.get("success");
        if (!success) {
            throw new Exception("" + result.get("message"));
        }
    }

    //将正文文档中的文本，抽取并更新至Index索引
    private void updateTextAttachToIndex(PaperDO paper, List<AttachmentDO> attlist, SearchUtil searchUtil) {
        try {
            Thread.sleep(10000);
            log.error("sleep completed");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (CollectionUtil.isEmpty(attlist)) return;
        if (1 != paper.getPaperType()) return; //仅处理附件文档
        for (AttachmentDO attachment : attlist) {
            if (!attachment.getIsMain().equalsIgnoreCase("1")) {//仅处理正文附件
                continue;
            }
            try {
                String fileId = attachment.getFileId();
                FileDO fileDO = fileMapper.selectById(fileId);
                Assert.notNull(fileDO, "找不到文件对象，fileId:" + fileId);
                byte[] byteArray = fileRepository.downloadFile(fileDO.getBigdataFileId());
//                byte[] byteArray = fileRepository.downloadFile("4322230511000001601");
                if (null == byteArray || byteArray.length == 0) {
                    log.error("updateTextAttachToIndex.byteArray (office text) is null, paper id and fileId is ==>"
                            + paper.getPaperId(), "," + fileId);
                }
                //从文档抽取text
                String fileText = OfficeTextExtractUtil.getContent(byteArray, attachment.getFileFormat());
                UpdateDocument document = new UpdateDocument();
                document.setIdx_id(paper.getPaperId());
                document.setIdx_desc(fileText); //正文
                Map<String, Object> result = searchUtil.update(document);
                Boolean success = (Boolean) result.get("success");
                if (!success) {
                    throw new RuntimeException("writeTextAttachmentToIndex error======>" + result.get("message"));
                }
            } catch (Exception e) {
                log.error("updateTextAttachToIndex error===============>");
                e.printStackTrace();
            }
        }
    }
}
