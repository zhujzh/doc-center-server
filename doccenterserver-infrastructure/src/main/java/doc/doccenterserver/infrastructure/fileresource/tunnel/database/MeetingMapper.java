package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.MeetingDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会议通知公告表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface MeetingMapper extends BaseMapper<MeetingDO> {
}
