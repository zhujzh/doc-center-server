package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.OrgDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface OrgEntityToOrgDOConvertor {

    OrgEntityToOrgDOConvertor INSTANCE = Mappers.getMapper(OrgEntityToOrgDOConvertor.class);

    @Mappings({})
    OrgDO entityToDO(OrgEntity orgEntity);

    @Mappings({})
    OrgEntity dOToEntity(OrgDO orgDO);

    @Mappings({})
    List<OrgEntity> dOToEntityList(List<OrgDO> orgDOList);

    @Mappings({})
    List<OrgDO> entityToDOList(List<OrgEntity> orgEntityList);
}
