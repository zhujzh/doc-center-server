package doc.doccenterserver.infrastructure.fileresource.file.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import doc.doccenterserver.domain.fileresource.file.model.TPanCollectFolderEntity;
import doc.doccenterserver.domain.fileresource.file.repository.TPanCollectFolderRepository;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.TPanCollectFolderMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.TPanCollectFolderDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Tangcancan
 */

@Slf4j
@Component("TPanCollectFolderRepository")
public class TPanCollectFolderRepositoryImpl implements TPanCollectFolderRepository {

    @Resource
    private TPanCollectFolderMapper tPanCollectFolderMapper;

    @Override
    public Integer getCountByObjId(TPanCollectFolderEntity tpanCol) {
        QueryWrapper<TPanCollectFolderDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("OBJ_ID", tpanCol.getObjId());
        queryWrapper.eq("USER_ID", tpanCol.getUserId());
        return tPanCollectFolderMapper.selectCount(queryWrapper);
    }
}
