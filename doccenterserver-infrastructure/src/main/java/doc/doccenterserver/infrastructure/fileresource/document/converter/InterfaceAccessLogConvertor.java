package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.InterfaceAccessLogEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.InterfaceAccessLogDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;


/**
 * @author Tangcancan
 */
@Mapper
public interface InterfaceAccessLogConvertor {

    InterfaceAccessLogConvertor INSTANCE = Mappers.getMapper(InterfaceAccessLogConvertor.class);

    @Mappings({})
    InterfaceAccessLogDO entityToDO(InterfaceAccessLogEntity parameter);

    @Mappings({})
    InterfaceAccessLogEntity dOToEntity(InterfaceAccessLogDO parameter);

    @Mappings({})
    List<InterfaceAccessLogEntity> dOToEntity(List<InterfaceAccessLogDO> parameter);

    @Mappings({})
    List<InterfaceAccessLogDO> entityToDO(List<InterfaceAccessLogEntity> parameter);
}
