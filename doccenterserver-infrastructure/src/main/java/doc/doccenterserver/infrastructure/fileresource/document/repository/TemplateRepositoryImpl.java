package doc.doccenterserver.infrastructure.fileresource.document.repository;

import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.base.Splitter;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.TemplateEntity;
import doc.doccenterserver.domain.fileresource.document.repository.TemplateRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.TemplateEntityToTemplateDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.TempletMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.TempletDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("TemplateDORepository")
public class TemplateRepositoryImpl implements TemplateRepository {

    @Resource
    private TempletMapper templateMapper;

    @Override
    public PageResult<List<TemplateEntity>> getListTemplates(TemplateEntity templateEntity) {
        // 分页查询
        LambdaQueryWrapper<TempletDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(TempletDO::getName, templateEntity.getName());
        Page<TempletDO> page = new Page<>(templateEntity.getPageNum(), templateEntity.getPageSize());
        IPage<TempletDO> templates = templateMapper.selectPage(page, wrapper);
        List<TempletDO> templateDOList = templates.getRecords();
        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(templates.getCurrent());
        baseDto.setPageSize(templates.getSize());
        baseDto.setTotal(templates.getTotal());
        baseDto.setTotalPage(templates.getPages());
        return new PageResult<>(TemplateEntityToTemplateDOConvertor.INSTANCE.dOToEntityList(templateDOList), baseDto);
    }

    @Override
    public TemplateEntity getTemplate(Integer id) {
        TempletDO templetDO = templateMapper.selectById(id);
        return TemplateEntityToTemplateDOConvertor.INSTANCE.dOToEntity(templetDO);
    }

    @Override
    public TemplateEntity addTemplate(TemplateEntity templateEntity) {
        TempletDO templetDO = TemplateEntityToTemplateDOConvertor.INSTANCE.entityToDO(templateEntity);
        int uuid = UUID.randomUUID().toString().replaceAll(ConstantSys.HORIZONTAL_BAR, ConstantSys.EMPTY_STRING).hashCode();
        templetDO.setId(uuid < ConstantSys.NUMBER_ZERO ? -uuid : uuid);
        templateMapper.insert(templetDO);
        return TemplateEntityToTemplateDOConvertor.INSTANCE.dOToEntity(templetDO);
    }

    @Override
    public TemplateEntity editTemplate(TemplateEntity templateEntity) {
        TempletDO templetDO = TemplateEntityToTemplateDOConvertor.INSTANCE.entityToDO(templateEntity);
        templateMapper.updateById(templetDO);
        return TemplateEntityToTemplateDOConvertor.INSTANCE.dOToEntity(templetDO);
    }

    @Override
    public TemplateEntity deleteTemplate(String ids) {
        if (StringUtil.notEmpty(ids)) {
            List<String> stringList = Splitter.on(ConstantSys.SEPARATE_COMMA).trimResults().omitEmptyStrings().splitToList(ids);
            templateMapper.deleteBatchIds(stringList);
        }
        return new TemplateEntity();
    }

    @Override
    public List<TemplateEntity> getAllTemplate() {
        QueryWrapper<TempletDO> wrapper = new QueryWrapper<>();
        List<TempletDO> templetDOList = templateMapper.selectList(wrapper);
        return TemplateEntityToTemplateDOConvertor.INSTANCE.dOToEntityList(templetDOList);
    }
}
