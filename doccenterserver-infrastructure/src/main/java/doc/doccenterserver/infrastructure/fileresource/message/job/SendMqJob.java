package doc.doccenterserver.infrastructure.fileresource.message.job;

import com.xxl.job.core.handler.annotation.XxlJob;
import doc.doccenterserver.domain.fileresource.message.repository.SendMqChangelogRepository;
import doc.doccenterserver.domain.fileresource.system.repository.ParameterRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 发送MQ消息
 */
@Component
@Slf4j
public class SendMqJob {
    @Autowired
    private SendMqChangelogRepository sendMqChangelogRepository;
    @XxlJob("sendMq")
    public void  sendMq(){
        sendMqChangelogRepository.sendMq();
        log.info("调用了");
    }
}
