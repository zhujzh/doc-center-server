package doc.doccenterserver.infrastructure.fileresource.system.converter;

import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ParameterDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 22:13
 */
@Mapper
public interface ParamEntityToParamDOConverter {
    ParamEntityToParamDOConverter INSTANCE = Mappers.getMapper(ParamEntityToParamDOConverter.class);


    @Mappings({})
    List<ParameterDO> dOToEntityList(List<ParameterDO> parameterDOList);
    @Mappings({})
    ParameterDO entityToDO(ParamEntity paramEntity);
    @Mappings({})
    ParamEntity dOToEntity(ParameterDO paramDO);
}
