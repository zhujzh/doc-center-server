package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 搜索 - 频道分类表、用于在搜索首页展现分类
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_s_channel`")
@NoArgsConstructor
public class SChannelDO {

    @Field(name = "序号")
    @TableId(type = IdType.INPUT, value = "content_seq")
    private Integer contentSeq;

    @Field(name = "分类编号")
    @TableField("class_id")
    private String classId;

    @Field(name = "分类父编号")
    @TableField("parent_id")
    private String parentId;

    @Field(name = "显示名称")
    @TableField("display_name")
    private String displayName;

    @Field(name = "显示序号")
    @TableField("order_num")
    private Integer orderNum;

    @Field(name = "模板ID")
    @TableField("template_id")
    private Integer templateId;

    @Field(name = "状态")
    @TableField("status")
    private String status;
}
