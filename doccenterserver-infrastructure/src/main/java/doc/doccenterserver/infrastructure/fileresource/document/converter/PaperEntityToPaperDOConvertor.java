package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PaperDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface PaperEntityToPaperDOConvertor {

    PaperEntityToPaperDOConvertor INSTANCE = Mappers.getMapper(PaperEntityToPaperDOConvertor.class);

    @Mappings({})
    PaperDO entityToDO(PaperEntity paperEntity);
    @Mappings({})
    PaperEntity dOToEntity(PaperDO paperDO);
    @Mappings({})
    List<PaperEntity> dOToEntityList(List<PaperDO> paperDOList);
    @Mappings({})
    List<PaperDO> entityToDOList(List<PaperEntity> paperEntityList);
    
}
