package doc.doccenterserver.infrastructure.fileresource.message.converter;

import doc.doccenterserver.domain.fileresource.document.model.ChannelAuthEntity;
import doc.doccenterserver.domain.fileresource.message.model.JobDetailLogEntity;

import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ChannelAuthDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobDetailLogDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface JobDetailLogEntityToJobDetailLogDOConvertor {

    JobDetailLogEntityToJobDetailLogDOConvertor INSTANCE = Mappers.getMapper(JobDetailLogEntityToJobDetailLogDOConvertor.class);

    @Mappings({})
    JobDetailLogDO entityToDO(JobDetailLogEntity channelAuthEntity);

    @Mappings({})
    JobDetailLogEntity dOToEntity(JobDetailLogDO channelAuthDO);

    @Mappings({})
    List<JobDetailLogEntity> dOToEntityList(List<JobDetailLogDO> channelAuthDOList);

    @Mappings({})
    List<JobDetailLogDO> entityToDOList(List<JobDetailLogEntity> channelAuthEntityList);
}
