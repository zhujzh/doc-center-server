package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AppEntity;
import doc.doccenterserver.domain.fileresource.document.model.InterfaceAccessLogEntity;
import doc.doccenterserver.domain.fileresource.document.repository.AppRepository;
import doc.doccenterserver.domain.fileresource.document.repository.InterfaceAccessLogRepository;
import doc.doccenterserver.domain.fileresource.document.repository.RoleRepository;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.infrastructure.fileresource.document.converter.InterfaceAccessLogConvertor;
import doc.doccenterserver.infrastructure.fileresource.message.utils.DateUtil;
import doc.doccenterserver.infrastructure.fileresource.message.utils.IdentifierGenerator;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.security.service.TokenService;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.InterfaceAccessLogMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.InterfaceAccessLogDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("InterfaceAccessLogDORepository")
public class InterfaceAccessLogRepositoryImpl implements InterfaceAccessLogRepository {

    @Resource
    private InterfaceAccessLogMapper interfaceAccessLogMapper;

    @Resource
    private RoleRepository roleRepository;

    @Resource
    private AppRepository appRepository;

    @Resource
    private TokenService tokenService;

    @Override
    public PageResult<List<InterfaceAccessLogEntity>> getInterfaceAccessLogList(
            HttpServletRequest request, InterfaceAccessLogEntity interfaceAccessLogEntity) {
        UserModel user = tokenService.getLoginUser(request).getUserModel();
        AppEntity appEntity = new AppEntity();
        List<String> roleId = roleRepository.getRoleByUserId(user.getUserid());
        if (!roleId.contains(ConstantSys.DOC_FLOW_ADMIN)) {
            appEntity.setUserId(user.getUserid());
            interfaceAccessLogEntity.setUserId(user.getUserid());
        }
        List<AppEntity> appEntityList = appRepository.getAppList(appEntity);
        List<String> appNames = new ArrayList<>();
        appEntityList.forEach(e -> appNames.add(e.getAppName()));
        interfaceAccessLogEntity.setAppNames(appNames);

        // 分页查询
        LambdaQueryWrapper<InterfaceAccessLogDO> wrapper = new LambdaQueryWrapper<>();
        Date date = new Date();
        if (StringUtil.notEmpty(interfaceAccessLogEntity.getStartTime())) {
            wrapper.ge(InterfaceAccessLogDO::getCallTime, interfaceAccessLogEntity.getStartTime());
        } else {
            wrapper.ge(InterfaceAccessLogDO::getCallTime, DateUtil.getBeforeOneWeekDate(date));
        }
        if (StringUtil.notEmpty(interfaceAccessLogEntity.getEndTime())) {
            wrapper.le(InterfaceAccessLogDO::getCallTime, interfaceAccessLogEntity.getEndTime());
        } else {
            wrapper.le(InterfaceAccessLogDO::getCallTime, DateUtil.getShortDateFormat(date));
        }
        wrapper.eq(StringUtil.notEmpty(interfaceAccessLogEntity.getAppid()),
                InterfaceAccessLogDO::getAppname, interfaceAccessLogEntity.getAppid());
        wrapper.eq(StringUtil.notEmpty(interfaceAccessLogEntity.getResultStatus()),
                InterfaceAccessLogDO::getResultStatus, interfaceAccessLogEntity.getResultStatus());
        // 暂无操作类型
        // wrapper.eq(StringUtil.notEmpty(interfaceAccessLogEntity.getOperatetype()), "getOperatetype", interfaceAccessLogEntity.getOperatetype());
        wrapper.in(StringUtil.notEmpty(interfaceAccessLogEntity.getUserId()),
                InterfaceAccessLogDO::getAppname, interfaceAccessLogEntity.getAppNames());
        wrapper.orderByDesc(InterfaceAccessLogDO::getCallTime);
        Page<InterfaceAccessLogDO> page = new Page<>(interfaceAccessLogEntity.getPageNum(), interfaceAccessLogEntity.getPageSize());
        IPage<InterfaceAccessLogDO> pages = interfaceAccessLogMapper.selectPage(page, wrapper);
        List<InterfaceAccessLogDO> paperDOList = pages.getRecords();
        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(pages.getCurrent());
        baseDto.setPageSize(pages.getSize());
        baseDto.setTotal(pages.getTotal());
        baseDto.setTotalPage(pages.getPages());
        List<InterfaceAccessLogEntity> interfaceAccessLogEntityList = InterfaceAccessLogConvertor.INSTANCE.dOToEntity(paperDOList);
        return new PageResult<>(interfaceAccessLogEntityList, baseDto);
    }

    @Override
    public InterfaceAccessLogEntity getInterfaceAccessLog(String logId) {
        LambdaQueryWrapper<InterfaceAccessLogDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(InterfaceAccessLogDO::getLogId, logId);
        InterfaceAccessLogDO interfaceAccessLogDO = interfaceAccessLogMapper.selectOne(queryWrapper);
        return InterfaceAccessLogConvertor.INSTANCE.dOToEntity(interfaceAccessLogDO);
    }

    @Override
    public InterfaceAccessLogEntity getInit(HttpServletRequest request) {
        InterfaceAccessLogEntity interfaceAccessLogEntity = new InterfaceAccessLogEntity();
        // 获取用户能看到的appidList表
        UserModel user = tokenService.getLoginUser(request).getUserModel();
        List<String> roleId = roleRepository.getRoleByUserId(user.getUserid());
        AppEntity appEntity = new AppEntity();
        if (!roleId.contains(ConstantSys.DOC_FLOW_ADMIN)) {
            appEntity.setUserId(user.getUserid());
        }
        List<AppEntity> appEntityList = appRepository.getAppList(appEntity);
        interfaceAccessLogEntity.setAppEntityList(appEntityList);
        return interfaceAccessLogEntity;
    }

    @Override
    public InterfaceAccessLogEntity insert(InterfaceAccessLogEntity interfaceAccessLogEntity) {
        InterfaceAccessLogDO interfaceAccessLogDO = InterfaceAccessLogConvertor.INSTANCE.entityToDO(interfaceAccessLogEntity);
        interfaceAccessLogDO.setLogId(IdentifierGenerator.createPrimaryKeySeq());
        interfaceAccessLogMapper.insert(interfaceAccessLogDO);
        return InterfaceAccessLogConvertor.INSTANCE.dOToEntity(interfaceAccessLogDO);
    }
}
