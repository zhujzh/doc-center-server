package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.AppEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.AppDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface AppConvertor {

    AppConvertor INSTANCE = Mappers.getMapper(AppConvertor.class);

    @Mappings({})
    AppDO entityToDO(AppEntity parameter);

    @Mappings({})
    AppEntity dOToEntity(AppDO parameter);

    @Mappings({})
    List<AppEntity> dOToEntity(List<AppDO> parameter);

    @Mappings({})
    List<AppDO> entityToDO(List<AppEntity> parameter);

}
