package doc.doccenterserver.infrastructure.fileresource.message.enums;

/**
 * 失效标志
 */
public enum PaperTypeEnum {
    HTML(0, "正文HTML文档"),
    FILE(1, "附件文档"),
    ;
    private final Integer value;
    private final String name;
    PaperTypeEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }
    public Integer getValue() {
        return this.value;
    }

}
