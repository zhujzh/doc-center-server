package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户角色关联表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_pub_user2role`")
@NoArgsConstructor
public class User2roleDO {

    @Field(name = "角色ID")
    @TableField("role_id")
    private String roleId;

    @Field(name = "用户ID")
    @TableField("user_id")
    private String userId;

    @Field(name = "备注")
    @TableField("remark")
    private String remark;

    @Field(name = "USER_CODE")
    @TableField("user_code")
    private String userCode;

    @Field(name = "USER_NAME")
    @TableField("user_name")
    private String userName;

    @Field(name = "ORG_ID")
    @TableField("org_id")
    private String orgId;

    @Field(name = "ORG_NAME")
    @TableField("org_name")
    private String orgName;
}
