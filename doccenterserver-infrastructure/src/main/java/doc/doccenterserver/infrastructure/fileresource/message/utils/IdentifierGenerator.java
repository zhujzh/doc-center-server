package doc.doccenterserver.infrastructure.fileresource.message.utils;


/**
 * @author Tangcancan
 */
public class IdentifierGenerator {

    /**
     * 根据长度生成随机字符串
     *
     * @param len
     * @return
     */
    public static String genRandStr(int len) {
        String rand = "";
        int readomWordIndex = 0;
        String[] readomWord = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
                "u", "v", "w", "x", "y", "z", "A", "B", "C", "D",
                "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
                "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X",
                "Y", "Z"
        };
        for (int i = 0; i < len; i++) {
            readomWordIndex = (int) (Math.random() * 52);
            rand += readomWord[readomWordIndex];
        }
        return rand;
    }

    /**
     * 根据长度随机生成数字串
     *
     * @param len
     * @return
     */
    public static String genRandNumber(int len) {
        String rand = "";
        int readomWordIndex = 0;
        String[] readomWord = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        for (int i = 0; i < len; i++) {
            readomWordIndex = (int) (Math.random() * 10);
            rand += readomWord[readomWordIndex];
        }
        return rand;
    }

    public static String createPrimaryKeySeq() {
        String currentTimeMillisStr = new Long(System.currentTimeMillis()).toString();
        return currentTimeMillisStr + genRandStr(25);
    }


    public static void main(String[] args) {
        try {
            System.out.println(IdentifierGenerator.createPrimaryKeySeq());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
