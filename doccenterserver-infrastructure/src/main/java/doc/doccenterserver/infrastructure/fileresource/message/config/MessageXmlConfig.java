package doc.doccenterserver.infrastructure.fileresource.message.config;


public class MessageXmlConfig {

    public static final String SOURCE_APP_ID="/root/head/sender";
    public static final String SOURCE_PAPER_ID="/root/kmdoc/resourceid";
    public static final String CMS_SOURCE_PAPER_ID="/root/kmdoc/id";
    public static final String ARCHIVE_ORG_CODE="/root/kmdoc/archiveorgcode";
    public static final String PERMISION="/root/kmdoc/permision";
    public static final String REMARK="/root/kmdoc/remark";
    public static final String READERS="/root/kmdoc/readers";
    public static final String ATTACHMENT="/root/kmdoc/attachments/attachment";
    public static final String SENDER="/root/head/sender";
    public static final String CATEGORY="/root/kmdoc/category";
    public static final String SUBJECT="/root/kmdoc/subject";
    public static final String OPERATION="/root/kmdoc/operation";
    public static final String CONTEXT="/root/kmdoc/context";
    public static final String ABS_URL="/root/kmdoc/absurl";
    public static final String CREATOR="/root/kmdoc/creator";
    public static final String CREATED= "/root/kmdoc/created";
    public static final String DOCAUTHOR="/root/kmdoc/docauthor";
    public static final String EXTENSION_1= "/root/kmdoc/extension1";
    public static final String EXTENSION_2= "/root/kmdoc/extension2";
    public static final String PAPER_TYPE=  "/root/kmdoc/papertype";








    private MessageXmlConfig() {
    }
}
