package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.domain.fileresource.document.model.RoleEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.RoleDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 角色Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface RoleMapper extends BaseMapper<RoleDO> {
    List<Map<String, Object>> querySelectedRoles(List<List<String>> roleIdList);

    List<String> getRoleByUserId(String userId);

}
