package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.DocConvertChangelogEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.DocConvertChangelogDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface DocConvertChangelogEntityToDocConvertChangelogDOConvertor {

    DocConvertChangelogEntityToDocConvertChangelogDOConvertor INSTANCE = Mappers.getMapper(DocConvertChangelogEntityToDocConvertChangelogDOConvertor.class);

    @Mappings({})
    DocConvertChangelogDO entityToDO(DocConvertChangelogEntity docConvertChangelogEntity);

    @Mappings({})
    DocConvertChangelogEntity dOToEntity(DocConvertChangelogDO docConvertChangelogDO);

    @Mappings({})
    List<DocConvertChangelogEntity> dOToEntityList(List<DocConvertChangelogDO> docConvertChangelogDOList);

    @Mappings({})
    List<DocConvertChangelogDO> entityToDOList(List<DocConvertChangelogEntity> docConvertChangelogEntityList);
}
