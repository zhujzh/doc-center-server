package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 数据字典
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_pub_dict`")
@NoArgsConstructor
public class DictDO {
    @TableId
    @Field(name = "ID")
    @TableField("id")
    private String id;

    @Field(name = "PID")
    @TableField("pid")
    private String pid;

    @Field(name = "DICT_NAME")
    @TableField("dict_name")
    private String dictName;

    @Field(name = "DICT_VALUE")
    @TableField("dict_value")
    private String dictValue;

    @Field(name = "DICT_SORT")
    @TableField("dict_sort")
    private Integer dictSort;

    @Field(name = "DICT_STATUS")
    @TableField("dict_status")
    private Integer dictStatus;
}
