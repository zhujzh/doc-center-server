package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.UserConfigDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户自定义配置Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface UserConfigMapper extends BaseMapper<UserConfigDO> {
}
