package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobDetailLogDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobLogDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobLogDOEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 任务日志主表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface JobLogMapper extends BaseMapper<JobLogDO> {

    IPage<JobLogDOEntity> queryJobLogListPage(Page<JobLogDOEntity> page,@Param(Constants.WRAPPER) Map<String, String> params);

    /**
     *根据id查找一个JobLog
     */
    JobLogDOEntity findJobLogById(String logId);

    List<JobLogDOEntity> findJobLogDetailById(@Param("logId")String logId);

    /**
     * 查看归档信息
     */
    JobLogDOEntity queryJobLogDetail(JobDetailLogDO jobDetailLogDO);

}
