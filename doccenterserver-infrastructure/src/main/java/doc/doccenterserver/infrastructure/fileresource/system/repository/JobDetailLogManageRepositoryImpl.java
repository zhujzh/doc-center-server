package doc.doccenterserver.infrastructure.fileresource.system.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import doc.doccenterserver.domain.fileresource.document.model.JobDetailLogEntity;
import doc.doccenterserver.domain.fileresource.system.model.JobLogEntry;
import doc.doccenterserver.domain.fileresource.system.repository.JobDetailLogManageRepository;
import doc.doccenterserver.infrastructure.fileresource.system.converter.JobDetailLogToJobDetailLogDOConverter;
import doc.doccenterserver.infrastructure.fileresource.system.converter.JoblogToJoblogDOConverter;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.ArchiveChangelogMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.JobDetailLogMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.JobLogMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/19 9:34
 */
@Slf4j
@Component("JobDetailLogManageRepository")
public class JobDetailLogManageRepositoryImpl implements JobDetailLogManageRepository {
    @Resource
    private JobDetailLogMapper jobDetailLogMapper;
    @Resource
    private JobLogMapper jobLogMapper;
    @Resource
    private ArchiveChangelogMapper archiveChangelogMapper;

    @Override
    public Map<String, Object> savejobdetaillog(JobDetailLogEntity jobDetailLogEntity) {
        Map<String, Object> result= Maps.newHashMap();
        try {
            JobDetailLogDO jobDetailLogDO = JobDetailLogToJobDetailLogDOConverter.INSTANCE.entityToDO(jobDetailLogEntity);
            int successNum = 0;
            if(jobDetailLogDO.getLogId()!=null){
                QueryWrapper<JobDetailLogDO> wrapper = new QueryWrapper<>();
                wrapper.eq("LOG_ID",jobDetailLogDO.getLogId());
                wrapper.eq("STEP_ID",jobDetailLogDO.getStepId());
                wrapper.eq("STEP_STATUS",jobDetailLogDO.getStepStatus());
                successNum =jobDetailLogMapper.update(jobDetailLogDO,wrapper);
            }else{
                successNum =jobDetailLogMapper.insert(jobDetailLogDO);
            }

            JobLogEntry jobLog = new JobLogEntry();
            jobLog.setLogId(jobDetailLogEntity.getLogId());
            jobLog.setContent(jobDetailLogEntity.getContent());
            if(jobLog.getLogId()!=null && !"".equals(jobLog.getLogId())){
                JobLogDO jobLogDO =  JoblogToJoblogDOConverter.INSTANCE.entityToDO(jobLog);
                jobLogMapper.updateById(jobLogDO);
            }
            ArchiveChangelogDO aceDO=new ArchiveChangelogDO();
            aceDO.setLogId(jobDetailLogEntity.getLogId());
            aceDO.setContent(jobDetailLogEntity.getContent());
            aceDO.setCreateTime(new Date());
            if(aceDO.getLogId()!=null && !"".equals(aceDO.getLogId())){
                archiveChangelogMapper.insert(aceDO);
            }

            if(successNum==1){
                result.put("info", "操作成功！");
                result.put("success", true);
            }
        } catch(Exception e){
            log.error("saveJobDetailLog==========jobDetailLog="+jobDetailLogEntity,e);
        }
        return result;
    }
}
