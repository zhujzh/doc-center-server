package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import doc.doccenterserver.domain.fileresource.document.model.GroupMemberEntity;
import doc.doccenterserver.domain.fileresource.document.repository.GroupMemberRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.GroupMemberConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.GroupMemberMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.GroupMemberDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("GroupMemberDORepository")
public class GroupMemberRepositoryImpl implements GroupMemberRepository {

    @Resource
    private GroupMemberMapper groupMemberMapper;

    @Override
    public List<GroupMemberEntity> getGroupAuth(String groupId) {
        return groupMemberMapper.getGroupAuth(groupId);
    }

    @Override
    public Integer deleteGroupMemberByGroupId(String groupId) {
        LambdaQueryWrapper<GroupMemberDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(GroupMemberDO::getGroupId, groupId);
        return groupMemberMapper.delete(queryWrapper);
    }

    @Override
    public Integer insertGroupMember(GroupMemberEntity member) {
        return groupMemberMapper.insert(GroupMemberConvertor.INSTANCE.entityToDO(member));
    }

    @Override
    public Integer updateGroupMember(GroupMemberEntity member) {
        return groupMemberMapper.updateById(GroupMemberConvertor.INSTANCE.entityToDO(member));
    }
}
