package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.Role2resEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.Role2resDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface Role2resConvertor {

    Role2resConvertor INSTANCE = Mappers.getMapper(Role2resConvertor.class);

    @Mappings({})
    Role2resDO entityToDO(Role2resEntity parameter);

    @Mappings({})
    Role2resEntity dOToEntity(Role2resDO parameter);

    @Mappings({})
    List<Role2resEntity> dOToEntity(List<Role2resDO> parameter);

    @Mappings({})
    List<Role2resDO> entityToDO(List<Role2resEntity> parameter);
}
