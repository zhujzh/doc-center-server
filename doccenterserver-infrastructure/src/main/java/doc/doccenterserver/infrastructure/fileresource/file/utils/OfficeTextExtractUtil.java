package doc.doccenterserver.infrastructure.fileresource.file.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hslf.extractor.PowerPointExtractor;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xssf.extractor.XSSFExcelExtractor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;


/**
 * word 内容提取工具
 */
@Slf4j
public class OfficeTextExtractUtil {

    /**
     * 根据文件远程路径，获取文件文本内容（office文档）
     *
     * @param fileURL 文件远程路径地址
     * @param suffix  文件后缀名
     * @return
     * @throws Exception
     */
    public static String getContent(String fileURL, String suffix) throws Exception {
        String text = "";
        BufferedInputStream is = null;
        try {
            URL u = new URL(fileURL);
            URLConnection conn = u.openConnection();
            is = new BufferedInputStream(conn.getInputStream());
            if (is == null) return "";
            text = doGetContent(is, suffix);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return text;
    }

    /**
     * 根据文件远程路径，获取文件文本内容（office文档）
     *
     * @param byteArray 附件二进制格式
     * @param suffix    文件后缀名
     * @return
     * @throws Exception
     */
    public static String getContent(byte[] byteArray, String suffix) throws Exception {
        String text = "";
        ByteArrayInputStream is = null;
        try {
            is = new ByteArrayInputStream(byteArray);
            text = doGetContent(is, suffix);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return text;
    }


    public static String doGetContent(InputStream is, String suffix) throws IOException {
        String text = null;
        try {
            switch (suffix) {
                case "doc":
                    WordExtractor ex1 = new WordExtractor(is);
                    text = ex1.getText();
                    ex1.close();
                    break;
                case "docx":
                    XWPFDocument doc = new XWPFDocument(is);
                    XWPFWordExtractor ex2 = new XWPFWordExtractor(doc);
                    text = ex2.getText();
                    ex2.close();
                    break;
                case "xls":
                    ExcelExtractor ex3 = new ExcelExtractor(new HSSFWorkbook(is));
                    text = ex3.getText();
                    ex3.close();
                    break;
                case "xlsx":
                    XSSFExcelExtractor ex4 = new XSSFExcelExtractor(new XSSFWorkbook(is));
                    text = ex4.getText();
                    ex4.close();
                    break;
                case "ppt":
                    PowerPointExtractor ex5 = new PowerPointExtractor(is);
                    text = ex5.getText();
                    ex5.close();
                    break;
                case "pptx":
                    XSLFPowerPointExtractor ex6 = new XSLFPowerPointExtractor(new XMLSlideShow(is));
                    text = ex6.getText();
                    ex6.close();
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static void main(String[] args) throws Exception {
        String fileUrl = "http://192.168.1.169/doc-cloud-service/file/download/15396b0a846100eab5967a531089a399";
        String test = OfficeTextExtractUtil.getContent(fileUrl, "xlsx");
        System.out.println(test);
        //String text=getContent(fileUrl, "pptx");
        //String exam = getContent("D:\\四平卷烟厂易地技改统一集成平台（综合数据门户）项目技术要求－l.doc");
        //System.out.println("test\n" + exam);
    }
}
