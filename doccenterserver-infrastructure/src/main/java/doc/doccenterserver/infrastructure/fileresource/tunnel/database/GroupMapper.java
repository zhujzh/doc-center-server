package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.GroupDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 自定义组Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface GroupMapper extends BaseMapper<GroupDO> {
    List<Map<String, Object>> querySelectedGroups(List<List<String>> groupIdList);

    Integer selectGroupMaxSortNum(String userId);

}
