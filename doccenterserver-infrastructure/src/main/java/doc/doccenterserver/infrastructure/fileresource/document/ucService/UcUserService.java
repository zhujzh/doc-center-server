package doc.doccenterserver.infrastructure.fileresource.document.ucService;

import com.tobacco.mp.uc.kz.client.api.dto.PageQueryEmployeeExtResponse;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;

import java.util.Collection;
import java.util.List;

/**
 * @author wangxp
 * @description 用户中心扩展 - 用户
 * @date 2023/6/13 16:39
 */
public interface UcUserService {

    /**
     * @param userName 姓名
     * @param orgUnitIds 组织id
     *               QueryMode 1 查询本层级  2 查询下级
     * @return
     * @description 根据姓名  查询 orgIds 下的 用户
     * 户
     * @author wangxp
     * @date 2023/6/13 16:41
     */
    Collection<PageQueryEmployeeExtResponse> queryUserListByOrgIds(String userName, List<String> orgUnitIds, Integer QueryMode);


    /**
     * @param userName 用户名
     * @param orgUnitIds 组织id
     * @param QueryMode 查询模式
     * @return  Collection<PageQueryEmployeeExtResponse>
     * @description 根据姓名  查询 orgIds 下的 用户 只查20条
     * @author wangxp
     * @date 2023/6/13 18:35
     */
    Collection<PageQueryEmployeeExtResponse> pageUserListByOrgIds(String userName, List<String> orgUnitIds, Integer QueryMode);



    /**
     * @param userCode 用户编码
     * @return UserEntity
     * @description 通过用户编码查询用户信息
     * @author wangxp
     * @date 2023/6/16 14:20
     */
    UserEntity getUserInfoByUserCode(String userCode);


    /**
     * @param ids staffIds
     * @return  List<UserEntity>
     * @description 根据staffIds 查询 user
     * @author wangxp
     * @date 2023/6/16 16:30
     */
    List<UserEntity> getUsersByIds(List<String> ids);
}
