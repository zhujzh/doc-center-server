package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文档附件表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_d_attachment`")
@NoArgsConstructor
public class AttachmentDO {

    @Field(name = "附件名称")
    @TableField("file_name")
    private String fileName;

    @Field(name = "附件描述")
    @TableField("file_desc")
    private String fileDesc;

    @Field(name = "所属实体的id")
    @TableField("paper_id")
    private String paperId;

    @Field(name = "文件保存路径")
    @TableField("file_path")
    private String filePath;

    @Field(name = "大数据平台文件ID")
    @TableField("file_id")
    private String fileId;

    @Field(name = "大数据平台对应文件访问地址")
    @TableField("file_url")
    private String fileUrl;

    @Field(name = "是否已经被转换成PDF")
    @TableField("is_pdf")
    private Integer isPdf;

    @Field(name = "附件大小")
    @TableField("file_size")
    private Double fileSize;

    @Field(name = "附件格式")
    @TableField("file_format")
    private String fileFormat;

    @Field(name = "附件下载次数")
    @TableField("download_count")
    private Integer downloadCount;

    @Field(name = "是否是主附件")
    @TableField("is_main")
    private String isMain;

    @Field(name = "PDF文件路径")
    @TableField("pdf_url")
    private String pdfUrl;

    @Field(name = "排序号")
    @TableField("order_by")
    private Integer orderBy;

    @Field(name = "上传时间")
    @TableField("upload_date")
    private Date uploadDate;

    @Field(name = "PDF文档大数据平台主键")
    @TableField("pdf_file_id")
    private String pdfFileId;

    @Field(name = "附件id")
    @TableId(type = IdType.INPUT, value = "atta_id")
    private String attaId;

    @Field(name = "租户id")
    @TableField("app_id")
    private String appId;

    @Field(name = "是否上传")
    @TableField("is_upload")
    private String isUpload;

    @Field(name = "SWF文件id")
    @TableField("swf_file_id")
    private String swfFileId;

    @Field(name = "文件MD5")
    @TableField("file_md5")
    private String fileMd5;

    @Field(name = "PDF文件MD5")
    @TableField("pdf_file_md5")
    private String pdfFileMd5;
}
