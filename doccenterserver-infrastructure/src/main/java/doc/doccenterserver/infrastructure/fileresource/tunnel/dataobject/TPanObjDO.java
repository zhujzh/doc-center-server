package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * ????????(TPanObj)实体类
 *
 * @author makejava
 * @since 2023-04-11 10:02:19
 */
@Data
@TableName("`t_pan_obj`")
@NoArgsConstructor
public class TPanObjDO {
    @Field(name = "OBJ_ID")
    @TableField("OBJ_ID")
    private String objId;

    @Field(name = "OBJ_NAME")
    @TableField("OBJ_NAME")
    private String objName;

    @Field(name = "OBJ_TYPE")
    @TableField("OBJ_TYPE")
    private String objType;

    @Field(name = "OBJ_PATH")
    @TableField("OBJ_PATH")
    private String objPath;

    @Field(name = "PARENT_OBJ_ID")
    @TableField("PARENT_OBJ_ID")
    private String parentObjId;

    @Field(name = "USER_ID")
    @TableField("USER_ID")
    private String userId;

    @Field(name = "OBJ_AUTH")
    @TableField("OBJ_AUTH")
    private Double objAuth;

    @Field(name = "OBJ_SORT")
    @TableField("OBJ_SORT")
    private Double objSort;

    @Field(name = "OBJ_SIZE")
    @TableField("OBJ_SIZE")
    private Double objSize;

    @Field(name = "IS_DELETE")
    @TableField("IS_DELETE")
    private String isDelete;

    @Field(name = "OBJ_ICON")
    @TableField("OBJ_ICON")
    private String objIcon;

    @Field(name = "CREATE_TIME")
    @TableField("CREATE_TIME")
    private Date createTime;

    @Field(name = "UPDATE_TIME")
    @TableField("UPDATE_TIME")
    private Date updateTime;

    @Field(name = "RESERVED_1")
    @TableField("RESERVED_1")
    private String reserved1;

    @Field(name = "RESERVED_2")
    @TableField("RESERVED_2")
    private String reserved2;

    @Field(name = "RESERVED_14")
    @TableField("RESERVED_14")
    private String reserved14;

    @Field(name = "RESERVED_13")
    @TableField("RESERVED_13")
    private String reserved13;

    @Field(name = "RESERVED_12")
    @TableField("RESERVED_12")
    private String reserved12;

    @Field(name = "RESERVED_11")
    @TableField("RESERVED_11")
    private String reserved11;

    @Field(name = "RESERVED_10")
    @TableField("RESERVED_10")
    private String reserved10;

    @Field(name = "RESERVED_9")
    @TableField("RESERVED_9")
    private String reserved9;

    @Field(name = "RESERVED_8")
    @TableField("RESERVED_8")
    private String reserved8;

    @Field(name = "RESERVED_7")
    @TableField("RESERVED_7")
    private String reserved7;

    @Field(name = "RESERVED_6")
    @TableField("RESERVED_6")
    private String reserved6;

    @Field(name = "RESERVED_5")
    @TableField("RESERVED_5")
    private String reserved5;

    @Field(name = "RESERVED_4")
    @TableField("RESERVED_4")
    private String reserved4;

    @Field(name = "RESERVED_3")
    @TableField("RESERVED_3")
    private String reserved3;

    @Field(name = "OBJ_TAG")
    @TableField("OBJ_TAG")
    private String objTag;

    @Field(name = "THUMB_PATH")
    @TableField("THUMB_PATH")
    private String thumbPath;

    @Field(name = "OBJ_DESC")
    @TableField("OBJ_DESC")
    private String objDesc;

}

