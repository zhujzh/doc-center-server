package doc.doccenterserver.infrastructure.fileresource.es;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import doc.doccenterserver.infrastructure.fileresource.message.utils.DateUtil;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Data
public class SearchParam {
    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public SearchParam() {

    }

    public SearchParam(String text, List<String> authlist, String objType, Date startDate, Date endDate, long rows, long start) {
        super();
        this.text = text;
        this.authlist = authlist;
        this.objType = objType;
        this.startDate = startDate;
        this.endDate = endDate;
        this.rows = rows;
        this.start = start;
    }

    public SearchParam(String text, List<String> authlist, String objType, Date startDate, Date endDate, long rows,
                       long start, List<Map<String, String>> sorts) {
        super();
        this.text = text;
        this.authlist = authlist;
        this.objType = objType;
        this.startDate = startDate;
        this.endDate = endDate;
        this.rows = rows;
        this.start = start;
        this.sorts = sorts;
    }

    public SearchParam(String title, List<String> authlist, String objType, Date startDate, Date endDate,
                       int rows, int start, List<Map<String, String>> sortlist) {
        this.title = title;
        this.authlist = authlist;
        this.objType = objType;
        this.startDate = startDate;
        this.endDate = endDate;
        this.rows = rows;
        this.start = start;
        this.sorts = sortlist;

    }

    private String title;//按标题搜索
    private String content;//按内容搜索
    private String text;//搜索标题+内容
    private List<String> authlist = Lists.newArrayList();//权限过滤信息
    private String objType;//索引类别
    private Date startDate;//按时间范围过滤，开始时间
    private Date endDate;//按时间范围过滤，截止时间
    private long rows = 10;//每页获取记录数
    private long start = 0;//记录开始序号
    private long curPage = 1;//当前页数
    private String appId;//索引所属应用id
    private String suffix;//文件后缀
    private String publishman;//发布人
    public static final String SEARCH_TYPE_TITLE = "0";//按标题搜索
    public static final String SEARCH_TYPE_CONTENT = "1";//按内容搜索
    public static final String SEARCH_TYPE_CONTENTANDTITLE = "2";//按标题、内容搜索
    public static final String SEARCH_TYPE_OFFICE = "1";//按office文档搜索
    public static final String SEARCH_TYPE_HTML = "0";//按网页文档搜索
    List<Map<String, String>> sorts = Lists.newArrayList();
    private boolean isLocal;

    public List<String> getAuthlist() {
        return authlist;
    }

    public void setAuthlist(List<String> authlist) {
        this.authlist = authlist;
    }

    public String getPublishman() {
        return publishman;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public void setPublishman(String publishman) {
        this.publishman = publishman;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getObjType() {
        return objType;
    }

    public void setObjType(String objType) {
        this.objType = objType;
    }

    public long getRows() {
        return rows;
    }

    public void setRows(long rows) {
        this.rows = rows;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public boolean isLocal() {
        return isLocal;
    }

    public void setIsLocal(boolean isLocal) {
        this.isLocal = isLocal;
    }

    public String toString() {
        JSONArray filter = new JSONArray();
        JSONArray sort = null;
        JSONObject queryObj = new JSONObject();
        JSONObject json = null;

        if (StringUtils.isNotBlank(this.content)) {
            json = new JSONObject();
            json.put("name", "idx_desc");
            json.put("value", "\"" + this.content + "\"");
            filter.add(json);
        }
        if (StringUtils.isNotBlank(this.publishman)) {
            json = new JSONObject();
            json.put("name", "idx_user_id");
            json.put("value", "\"" + this.publishman + "\"");
            filter.add(json);
        }
        if (StringUtils.isNotBlank(this.title)) {
            json = new JSONObject();
            json.put("name", "idx_title");
            json.put("value", "\"" + this.title + "\"");

            filter.add(json);
        }
        //只选择后缀查询时，按文件名查询，不管界面选择什么查询方式
        if (StringUtils.isNotBlank(this.suffix)) {
            json = new JSONObject();
            json.put("name", "idx_suffix");
            json.put("value", "\"" + this.suffix + "\"");
            filter.add(json);
        }
        if (StringUtils.isNotBlank(this.text)) {
            json = new JSONObject();
            json.put("name", "idx_text");
            json.put("value", this.text);
            filter.add(json);
        }
        if (StringUtils.isNotBlank(this.objType)) {
            json = new JSONObject();
            json.put("name", "idx_parent_id");
            json.put("value", this.objType);
            filter.add(json);
        }
        if (StringUtils.isNotBlank(this.appId)) {
            json = new JSONObject();
            json.put("name", "idx_app_id");
            json.put("value", this.appId);
            filter.add(json);
        }
        if (authlist.size() > 0) {
            json = new JSONObject();

            json.put("name", "idx_auth_value");
            json.put("value", "(" + Joiner.on(" OR ").join(authlist) + ")");
            filter.add(json);
        }
        if (startDate != null && endDate != null) {
            json = new JSONObject();
            json.put("name", "idx_date");
            String start = DateUtil.getDateFormat(startDate, "yyyy-MM-dd HH:mm:ss");
            String end = DateUtil.getDateFormat(endDate, "yyyy-MM-dd HH:mm:ss");
            if (isLocal) {
                json.put("start", start);
                json.put("end", end);
            } else {
                json.put("value", Joiner.on(",").join(start, end));
            }
            filter.add(json);
        }
        if (filter.size() > 0) {
            queryObj.put("filter", filter);
        }
        if (sorts.size() > 0) {
            sort = new JSONArray();
            for (int i = 0; i < sorts.size(); i++) {
                sort.add(sorts.get(i));
            }
            queryObj.put("sort", sort);
        }
        queryObj.put("rows", rows);
        queryObj.put("start", start);
        return queryObj.toJSONString();
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public long getCurPage() {
        return curPage;
    }

    public void setCurPage(long curPage) {
        this.curPage = curPage;
        this.start = UtilPage.getFromIndex(curPage, rows);
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public List<Map<String, String>> getSorts() {
        return sorts;
    }

    public void setSorts(List<Map<String, String>> sorts) {
        this.sorts = sorts;
    }
}
