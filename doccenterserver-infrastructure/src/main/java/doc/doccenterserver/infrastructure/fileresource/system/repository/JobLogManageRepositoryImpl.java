package doc.doccenterserver.infrastructure.fileresource.system.repository;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.document.repository.AttachmentRepository;
import doc.doccenterserver.domain.fileresource.message.repository.DocIndexChangelogRepository;
import doc.doccenterserver.domain.fileresource.system.model.JobLogEntry;
import doc.doccenterserver.domain.fileresource.system.repository.JobLogManageRepository;
import doc.doccenterserver.domain.fileresource.system.repository.ParameterRepository;
import doc.doccenterserver.infrastructure.fileresource.message.config.MessageXmlConfig;
import doc.doccenterserver.infrastructure.fileresource.message.enums.ArchiveOrgCodeEnum;
import doc.doccenterserver.infrastructure.fileresource.message.enums.SourceAppEnum;
import doc.doccenterserver.infrastructure.fileresource.message.enums.TopicsEnum;
import doc.doccenterserver.infrastructure.fileresource.message.mqs.KafkaSend;
import doc.doccenterserver.infrastructure.fileresource.message.utils.JdomXmlUtil;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.system.converter.JoblogToJoblogDOConverter;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.*;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jdom2.Document;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/19 9:34
 */
@Slf4j
@Component("JobLogManageDORepository")
public class JobLogManageRepositoryImpl  implements JobLogManageRepository {
    private static final Object DOMAIN_FILE_API_INFO = "{\"md5URL\":\"https://api.hngytobacco.com/oss-service-sso/file/md5/check/\",\"uploadURL\":\"https://api.hngytobacco.com/oss-service-sso/file/upload\",\"deleteURL\":\"https://api.hngytobacco.com/oss-service-sso/file/delete/\",\"fileTag\":{\"image\":\"jpg,jpeg,bmp,png,gif\",\"document\":\"doc,docx,xls,xlsx,ppt,pptx,txt,pdf\",\"video\":\"avi,wma,rmvb,rm,swf,mp4,mid,3gp\",\"music\":\"mp3,wma,wav,mod,aac,ape,ogg,m4a\"},\"supportedViewTypes\":\"doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,bmp,png,gif\"}";
    @Resource
    private KafkaSend kafkaSend;
    @Resource
    private JobLogMapper jobLogMapper;
    @Resource
    private JobDetailLogMapper jobDetailLogMapper;
    @Resource
    private MqChangelogMapper mqChangelogEntryMapper;
    @Resource
    private ArchiveChangelogMapper archiveChangelogEntryMapper;
    @Resource
    private DocConvertChangelogMapper docConvertChangelogEntryMapper;
    @Resource
    private DocPublishChangelogMapper docPublishChangelogEntryMapper;
    @Resource private DocIndexChangelogMapper docIndexChangelogEntryMapper;
    @Resource private SendMqChangelogMapper sendMqChangelogEntryMapper;

    @Resource
    private PaperMapper paperMapper;
    @Resource
    private AttachmentRepository attachmentRepository;
    @Resource
    private ParameterRepository parameterRepository;
    @Resource
    private DocIndexChangelogRepository docIndexChangelogRepository;

//    @Override
//    public PageResult<List<JobLogEntry>> listJobLogs(JobLogEntry jobLogEntry) {
//        QueryWrapper<JobLogDOEntity> wrapper = new QueryWrapper<>();
//        Page<JobLogDOEntity> page = new Page<>(jobLogEntry.getPageNum(), jobLogEntry.getPageSize());
////暂时屏蔽，待优化
//        wrapper.ge(StringUtil.notEmpty(jobLogEntry.getCreateStartDate()), "CREATE_TIME", jobLogEntry.getCreateStartDate()+" 00:00:00");
//        wrapper.le(StringUtil.notEmpty(jobLogEntry.getCreateEndDate()), "CREATE_TIME", jobLogEntry.getCreateEndDate()+" 23:59:59");
//        wrapper.like(StringUtil.notEmpty(jobLogEntry.getPaperId()),"PAPER_ID",jobLogEntry.getPaperId());
//        wrapper.eq(StringUtil.notEmpty(jobLogEntry.getSourceAppId()),"SOURCE_APP_ID",jobLogEntry.getSourceAppId());
//        wrapper.eq(StringUtil.notEmpty(jobLogEntry.getOperation()),"OPERATION",jobLogEntry.getOperation());
//        wrapper.eq(StringUtil.notEmpty(jobLogEntry.getStatus()),"STATUS",jobLogEntry.getStatus());
//        wrapper.like(StringUtil.notEmpty(jobLogEntry.getLogName()),"LOG_NAME",jobLogEntry.getLogName());
//        wrapper.like(StringUtil.notEmpty(jobLogEntry.getLogId()),"LOG_ID",jobLogEntry.getLogId());
//        wrapper.like(StringUtil.notEmpty(jobLogEntry.getSourcePaperId()),"SOURCE_PAPER_ID",jobLogEntry.getSourcePaperId());
//        wrapper.orderByDesc("CREATE_TIME desc,OPERATION");
//
//
//        IPage<JobLogDOEntity> paramDoPage = jobLogManageMapper.selectPage(page,wrapper);
//        List<JobLogDOEntity> result=paramDoPage.getRecords();
//
//
//        Map<String,String> queryMap = Maps.newHashMap();
//        result.forEach(JobLogDOEntity -> {
//            if(jobLogEntry.getStepId() != null){
//                queryMap.put("stepId",jobLogEntry.getStepId().toString());
//            }
//            queryMap.put("logId", JobLogDOEntity.getLogId());
//            List<JobDetailLogDO> detailLogEntrys =jobDetailLogMapper.findJobDetailLogListByParams(queryMap);
//            if(detailLogEntrys!=null&&detailLogEntrys.size()>0){
//                JobDetailLogDO detailLogDo = detailLogEntrys.get(0);
//                JobLogDOEntity.setStepId(detailLogDo.getStepId().toString());
//                JobLogDOEntity.setStepStatus(detailLogDo.getStepStatus().toString());
//                LocalDateTime ldt = detailLogDo.getCreateTime().toInstant()
//                        .atZone( ZoneId.systemDefault() )
//                        .toLocalDateTime();
//                JobLogDOEntity.setCreateTime(ldt);
//            }
//            queryMap.clear();
//        });
//
//        BaseDTO baseDTO = new BaseDTO();
//        baseDTO.setPageNum(page.getCurrent());
//        baseDTO.setPageSize(page.getSize());
//        baseDTO.setTotal(page.getTotal());
//        baseDTO.setTotalPage(page.getPages());
//
//
//        return new PageResult(JoblogToJoblogDOConverter.INSTANCE.entityToDOList(result),baseDTO);
//    }


    @Override
    public PageResult<List<JobLogEntry>> listJobLogs(JobLogEntry jobLogEntry) {
        QueryWrapper<JobLogDO> wrapper = new QueryWrapper<>();
        Page<JobLogDOEntity> page = new Page<>(jobLogEntry.getPageNum(), jobLogEntry.getPageSize());
        JobLogDOEntity jobLogDOEntity = JoblogToJoblogDOConverter.INSTANCE.jobLogDOentityToEntity(jobLogEntry);
//暂时屏蔽，待优化
//        wrapper.ge(StringUtil.notEmpty(jobLogEntry.getCreateStartDate()), "CREATE_TIME", jobLogEntry.getCreateStartDate()+" 00:00:00");
//        wrapper.le(StringUtil.notEmpty(jobLogEntry.getCreateEndDate()), "CREATE_TIME", jobLogEntry.getCreateEndDate()+" 23:59:59");
//        wrapper.like(StringUtil.notEmpty(jobLogEntry.getPaperId()),"PAPER_ID",StringUtil.notEmpty(jobLogEntry.getPaperId()));
//        wrapper.eq(StringUtil.notEmpty(jobLogEntry.getSourceAppId()),"SOURCE_APP_ID",StringUtil.notEmpty(jobLogEntry.getSourceAppId()));
//        wrapper.eq(StringUtil.notEmpty(jobLogEntry.getOperation()),"OPERATION",StringUtil.notEmpty(jobLogEntry.getOperation()));
//        wrapper.eq(StringUtil.notEmpty(jobLogEntry.getStatus()),"STATUS",StringUtil.notEmpty(jobLogEntry.getStatus()));
//        wrapper.eq(StringUtil.notEmpty(jobLogEntry.getLogName()),"LOG_NAME",StringUtil.notEmpty(jobLogEntry.getLogName()));
//        wrapper.like("LOG_NAME",jobLogEntry.getLogName());
//        wrapper.like(StringUtil.notEmpty(jobLogEntry.getLogId()),"LOG_ID",StringUtil.notEmpty(jobLogEntry.getLogId()));
//        wrapper.like(StringUtil.notEmpty(jobLogEntry.getSourcePaperId()),"SOURCE_PAPER_ID",StringUtil.notEmpty(jobLogEntry.getSourcePaperId()));

        Map<String,String> map = Maps.newHashMap();
        map.put("logName",jobLogEntry.getLogName());
        map.put("paperId",jobLogEntry.getPaperId());
        map.put("sourceAppId",jobLogEntry.getSourceAppId());
        map.put("operation",jobLogEntry.getOperation());
        map.put("status",jobLogEntry.getStatus());
        map.put("logId",jobLogEntry.getLogId());
        map.put("sourcePaperId",jobLogEntry.getSourcePaperId());
        if(jobLogEntry.getStepId() != null){
            map.put("stepId",jobLogEntry.getStepId().toString());
        }else{
            map.put("stepId",null);
        }
        if(StringUtil.notEmpty(jobLogEntry.getCreateStartDate())){
            map.put("createStartDate",jobLogEntry.getCreateStartDate()+" 00:00:00");
        }
        if(StringUtil.notEmpty(jobLogEntry.getCreateEndDate())){
            map.put("createEndDate",jobLogEntry.getCreateEndDate()+" 23:59:59");
        }

        IPage<JobLogDOEntity> paramDoPage = jobLogMapper.queryJobLogListPage(page, map);

//        IPage<JobLogDO> paramDoPage = jobLogMapper.selectPage(page,wrapper);
        List<JobLogDOEntity> result=paramDoPage.getRecords();

        Map<String,String> queryMap = Maps.newHashMap();
        result.forEach(JobLogDOEntity -> {
            if(jobLogEntry.getStepId() != null){
                queryMap.put("stepId",jobLogEntry.getStepId().toString());
            }
            queryMap.put("logId", JobLogDOEntity.getLogId());
            List<JobDetailLogDO> detailLogEntrys =jobDetailLogMapper.findJobDetailLogListByParams(queryMap);
            if(detailLogEntrys!=null&&detailLogEntrys.size()>0){
                JobDetailLogDO detailLogDo = detailLogEntrys.get(0);
                JobLogDOEntity.setStepId(detailLogDo.getStepId().toString());
                JobLogDOEntity.setStepStatus(detailLogDo.getStepStatus().toString());
                LocalDateTime ldt = detailLogDo.getCreateTime().toInstant()
                        .atZone( ZoneId.systemDefault() )
                        .toLocalDateTime();
                JobLogDOEntity.setCreateTime(ldt);
            }
            queryMap.clear();
        });

        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(page.getCurrent());
        baseDto.setPageSize(page.getSize());
        baseDto.setTotal(page.getTotal());
        baseDto.setTotalPage(page.getPages());


        return new PageResult(JoblogToJoblogDOConverter.INSTANCE.entityToDOList(result),baseDto);
    }

    @Override
    public Map<String, Object> lookLogDetail(String logId) {
        Map<String, Object> map = Maps.newHashMap();
        map.put("fileDownloadPath",parameterRepository.getByKey("FILE_DOWNLOAD_PATH").getPValue());

        Map<String, String> fileApiMap = JSON.parseObject(parameterRepository.getByKey("FILE_API_INFO").getPValue(),
                new TypeReference<HashMap<String, String>>() {
                });
        String imageTypes = com.alibaba.fastjson.JSONObject.parseObject(fileApiMap.get("fileTag")).getString("image");
        map.put("imageTypes", imageTypes);

        JobLogDO mainJobDo = jobLogMapper.findJobLogById(logId);
        String paperId = mainJobDo.getPaperId();// 对象在文档中心的主键
        if (StringUtils.isNotBlank(paperId)) {
            PaperEntity paper = paperMapper.findPaperById(paperId);
            if (paper != null && 1 == paper.getPaperType()) {
                AttachmentEntity mainFile = attachmentRepository.findMainAttachmentByPaperId(paperId);
                mainFile.setExt(mainFile.getFileFormat());
                map.put("mainFile", mainFile);
            }
        }
        if (StringUtils.isNotBlank(logId) && !"-1".equals(logId)) {
            List<JobLogDOEntity> result = jobLogMapper.findJobLogDetailById(logId);
            JobLogDOEntity jobLog = jobLogMapper.findJobLogById(logId);
            map.put("jobLog", jobLog);
            map.put("list", result);
        }
        map.put("DOC_CLOUD_FRONT_URL", parameterRepository.getByKey("DOC_CLOUD_FRONT_URL").getPValue());
        map.put("DOMAIN_PDF_FILE_PATH",parameterRepository.getByKey("DOMAIN_FILE_DOWNLOAD_PATH").getPValue());
        return  map;
    }

    //编辑归档消息
    @Override
    public Map<String, Object> viewarchmsg(String logId, Integer stepId) {
        Map<String, Object> map = Maps.newHashMap();
        if (StringUtils.isNotBlank(logId) && !"-1".equals(logId)){
            try {
                JobDetailLogDO jobDetailLogDO  = new JobDetailLogDO();
                jobDetailLogDO.setLogId(logId);
                jobDetailLogDO.setStepId(stepId);
                JobLogDOEntity result = queryJobLogDetail(jobDetailLogDO);
                String content = result.getContent();
                String xmlContent = JdomXmlUtil.precorrectxml(content);
                result.setContent(content);
                result.setXmlContent(xmlContent);
                map.put("logDetail",result);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("查看环节信息失败=======：stepId：" + stepId + "logId：" + logId + "\n" + e);
            }
        }
        return map;
    }

    @Override
    public Map<String, Object> viewerrmsg(String logId, Integer stepId) {
        Map<String, Object> map = Maps.newHashMap();
        if (StringUtils.isNotBlank(logId) && !"-1".equals(stepId)) {
            JobDetailLogDO jobDetailLogDO  = new JobDetailLogDO();
            jobDetailLogDO.setLogId(logId);
            jobDetailLogDO.setStepId(stepId);
            JobLogDOEntity result = queryJobLogDetail(jobDetailLogDO);
            map.put("logDetail",result);
        }
        return map;
    }

    /**
     *任务错误重试
     * @param logId
     * @param stepId
     * @return
     */
    @Override
    public boolean retry(String logId, Integer stepId) {
        boolean result=false;
        JobLogDO jlDo = new JobLogDO();
        jlDo.setLogId(logId);
        jlDo.setStatus("N");//变更主日志状态为处理中
        jobLogMapper.updateById(jlDo);
        switch (stepId) {
            //MQ消息处理，无按钮
            case 2:
//                MqChangelogEntry entry1=new MqChangelogEntry();
                MqChangelogDO entry1 = new MqChangelogDO();
                entry1.setRetryTimes(0);
                entry1.setLogId(logId);
                result=mqChangelogEntryMapper.updateById(entry1)>0?true:false;
                break;
            //消息归档	，有原文件上传与下载按钮
            case 3:
                ArchiveChangelogDO entry2 = new ArchiveChangelogDO();
                entry2.setRetryTimes(0);
                entry2.setLogId(logId);
                result=archiveChangelogEntryMapper.updateById(entry2)>0?true:false;
                break;
            //文档中心文档转换，有原文件，pdf文件，swf文件上传与下载按钮
            case 4:
//                DocConvertChangelogEntry entry3=new DocConvertChangelogEntry();
                DocConvertChangelogDO entry3 = new DocConvertChangelogDO();
                entry3.setRetryTimes(0);
                entry3.setLogId(logId);
                result=docConvertChangelogEntryMapper.updateById(entry3)>0?true:false;
                break;
            //文档发布，无按钮
            case 5:
                DocPublishChangelogDO entry4 = new DocPublishChangelogDO();
                entry4.setRetryTimes(0);
                entry4.setLogId(logId);
                result=docPublishChangelogEntryMapper.updateById(entry4)>0?true:false;
                break;
            //文档中心创建（或）删除索引，有创建(或删除)索引按钮和错误重试按钮
            case 6:
//                DocIndexChangelogEntry entry5=new DocIndexChangelogEntry();
                DocIndexChangelogDO entry5 = new DocIndexChangelogDO();
                entry5.setRetryTimes(0);
                entry5.setLogId(logId);
                result=docIndexChangelogEntryMapper.updateById(entry5)>0?true:false;
                break;
            //发送ESB消息，有编辑统一消息，推送（或推送删除）统一消息按钮
            case 7:
                SendMqChangelogDO entry6 = new SendMqChangelogDO();
                entry6.setRetryTimes(0);
                entry6.setLogId(logId);
                result=sendMqChangelogEntryMapper.updateById(entry6)>0?true:false;
                break;
            //网盘文件文档转换处理，有原文件上传与下载，pdf文件上传与下载按钮    屏蔽
//            case 9:
//                FileChangelogEntry entry7=new FileChangelogEntry();
//                Filech`
//                entry7.putAll(params);
//                result=fileChangelogEntryMapper.updateFileChangelog(entry7)>0?true:false;
//                break;
            default:
                break;
        }
        return result;
    }


    //重新入库
    @Override
    public Map<String, Object> rearchive(String logId) {
        Map<String, Object> result = Maps.newHashMap();
        boolean rs = false;
        try {
            JobLogDOEntity jobLog = jobLogMapper.findJobLogById(logId);
            String contentxml = generateRetriveXml(jobLog.getSourcePaperId(), "", jobLog.getSourceAppId(),
                    jobLog.getLogName(), jobLog.getArchiveOrgCode());
            log.debug("xml:" + contentxml);

            String newmqmsg = JdomXmlUtil.precorrectxml(contentxml);
            Document doc = JdomXmlUtil.xml2Document(newmqmsg);
            String sourceApp = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SENDER);
            String archiveOrgCode = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.ARCHIVE_ORG_CODE);
            // 发送删除
//            rs = IbmMq.send(contentxml);
            //将集团消息进行复制分发到对应厂部文档中心MQ接收队列进行归档
            this.distributionOrgMsg(sourceApp, archiveOrgCode, contentxml);
//             发送新增
//            rs = IbmMq.send(jobLog.getContent());
            this.distributionOrgMsg(sourceApp, archiveOrgCode, jobLog.getContent());

            rs=true;
        } catch (Exception e) {
            e.printStackTrace();
            result.put("info", e.getMessage());
        }
        result.put("success", rs);
        return result;
    }

    private void distributionOrgMsg(String sourceApp, String archiveOrgCode, String msg) {
        if (SourceAppEnum.OA.getValue().equals(sourceApp) || SourceAppEnum.ZYOA.getValue().equals(sourceApp)) {
            if (ArchiveOrgCodeEnum.SP.getValue().equals(archiveOrgCode)) {
                kafkaSend.send(TopicsEnum.SP.getValue(),msg);
            }
            if (SourceAppEnum.ZYOA.getValue().equals(sourceApp) && ArchiveOrgCodeEnum.CZ.getValue().equals(archiveOrgCode)) {
                kafkaSend.send(TopicsEnum.CZ.getValue(),msg);
            }
        } else if (SourceAppEnum.ZYSTDD.getValue().equals(sourceApp)) {
            if (ArchiveOrgCodeEnum.SP.getValue().equals(archiveOrgCode)) {
                kafkaSend.send(TopicsEnum.SP.getValue(),msg);
            } else if (ArchiveOrgCodeEnum.WZ.getValue().equals(archiveOrgCode)) {
                kafkaSend.send(TopicsEnum.WZ.getValue(),msg);
            } else if (ArchiveOrgCodeEnum.CZ.getValue().equals(archiveOrgCode)) {
                kafkaSend.send(TopicsEnum.CZ.getValue(),msg);
            }
        }
    }

    @Override
    public Map<String, Object> handlefiles(String sourcePaperId, String paperId, String appId, String optType, HttpServletRequest request) {
        Map<String, Object> result = Maps.newHashMap();
        if (request.getHeader("iv-user") != null) {
            result.put("fileDownloadPath",parameterRepository.getByKey("DOMAIN_PDF_FILE_PATH").getPValue());
            result.put("fileApiInfo",DOMAIN_FILE_API_INFO);
        } else {
            result.put("fileDownloadPath",parameterRepository.getByKey("PDF_FILE_PATH").getPValue());
            result.put("fileApiInfo",parameterRepository.getByKey("FILE_API_INFO").getPValue());
        }
        List<Map<String,String>> fileList = new ArrayList<Map<String,String>>();
        // 文档中心以及其他系统归档到文档中心的所有附件
        if (StringUtils.isNotBlank(paperId)) {
            PaperEntity paper = paperMapper.findPaperById(paperId);
            Map<String, Object> params = Maps.newHashMap();
            params.put("paperId", paperId);
            List<AttachmentEntity> attachList = attachmentRepository.findAttachmentListByParams(params);
            for (AttachmentEntity atta : attachList) {
                Map<String,String> item = Maps.newHashMap();
                item.put("attaId", atta.getAttaId());
                item.put("fileName", atta.getFileName());
                item.put("fileId", atta.getFileId());
                item.put("pdfFileId", atta.getPdfFileId());
                item.put("appId", paper.getAppId());
                item.put("paperId", atta.getPaperId());
                item.put("isMain", atta.getIsMain());
                fileList.add(item);
            }
        }
        appId = fileList.size() > 0 ? fileList.get(0).get("appId").toString() : "";
        result.put("fileList",fileList);
        result.put("optType",optType);
        result.put("appId",appId);
        return result;
    }

    /**
     * @方法名: handleIndex
     * @描述:对文档的索引进行创建或删除操作
     * @参数:@param paperId 文档id
     * @参数:@param operation 操作类型 ：create 创建 retrieve 删除
     * @参数:@return
     * @返回值：Map<String,Object>
     */
    @Override
    public Map<String, Object> handleIndex(String logId, String paperId, String operation, String appId) {
        Map<String, Object> result = Maps.newHashMap();
        result.put("success", false);
        boolean b = false;
        try {
            if ("yunpan".equals(appId)) {
//                屏蔽云盘
//                b = fileChangelogService.insertFileChangelog(logId, paperId, operation);
            } else {
                b = docIndexChangelogRepository.insertDocIndexChange(logId, paperId, operation);
            }
            result.put("success", b);
        } catch (Exception e) {
            log.error("======执行handleIndex出错：paperId:" + paperId + ",operation:" + operation + "\n" + e);
        }
        return result;
    }

    @Override
    public Map<String, Object> sendmq(String logId, Integer stepId) {
        Map<String, Object> result = Maps.newHashMap();
        result.put("success", false);
        try {
            LambdaQueryWrapper<JobDetailLogDO> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(JobDetailLogDO::getLogId,logId);
            wrapper.eq(JobDetailLogDO::getStepId,stepId);
            JobDetailLogDO jddldo = jobDetailLogMapper.selectOne(wrapper);
            log.error("发送消息开始:" + jddldo.getContent());
            if (null != jddldo.getContent() && !"".equals(jddldo.getContent())) {
                log.error("发送消息开始:" + jddldo.getContent());
                JobLogDOEntity jobLog = jobLogMapper.findJobLogById(logId);
//                IbmMqUtil.send(jobdetail.getContent());
//                kafkaSend.send(null,jddldo.getContent());//暂时为空
                this.distributionOrgMsg(jobLog.getSourceAppId(), jobLog.getArchiveOrgCode(), jddldo.getContent());
                log.error("发送消息结束:" + jddldo.getContent());
            }
            result.put("success", true);
        } catch (Exception e) {
            result.put("message", e.getMessage());
            log.error("======执行sendMQ出错：logId:" + logId + ",stepId:" + stepId + "\n" + e, e);
        }
        return result;
    }


    public JobLogDOEntity queryJobLogDetail(JobDetailLogDO jobDetailLogDO) {
        JobLogDOEntity result=jobLogMapper.queryJobLogDetail(jobDetailLogDO);
        return result;
    }

    public String generateRetriveXml(String docsourceid, String servicecode, String sender, String papername,
                                     String archiveorgcode) throws Exception {
        StringBuffer buffer = new StringBuffer();
        // 流水号
        long changlogid = System.currentTimeMillis();

        String ECM_IP = "" + parameterRepository.getByKey("ECM_IP").getPValue();
        if ("OA".equals(sender)) {
            servicecode = "OATOEKP";
        } else if ("ZYSTDD".equals(sender)) {
            servicecode = "ZYSTDDTOZYECM";
        } else if ("CMS".equals(sender)) {
            servicecode = "CMSTOEKP";
        }

        buffer.append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
        buffer.append("<root >");
        // 消息头
        buffer.append("<head>");
        buffer.append("<servicecode>" + servicecode + "</servicecode>");
        buffer.append("<sender>" + sender + "</sender>");
        buffer.append("<bizid>0003</bizid>");
        buffer.append("<serialnumber>" + changlogid + "</serialnumber>");
        buffer.append("</head>");

        // 消息体
        buffer.append("<kmdoc>");
        buffer.append("<ip>" + ECM_IP + "</ip>");
        buffer.append("<id>" + docsourceid + "</id>");
        buffer.append("<from>ZYECM</from>");
        buffer.append("<operation>retrieve</operation>");
        buffer.append("<category>4</category>");
        buffer.append("<subject><![CDATA[" + papername + "]]></subject>");
        buffer.append("<archiveorgcode>" + archiveorgcode + "</archiveorgcode>");
        buffer.append("<resourceid>").append(docsourceid).append("</resourceid>");
        buffer.append("<changlogid>").append(changlogid).append("</changlogid>");
        buffer.append("</kmdoc>");
        buffer.append("</root>");
        return buffer.toString();
    }

}
