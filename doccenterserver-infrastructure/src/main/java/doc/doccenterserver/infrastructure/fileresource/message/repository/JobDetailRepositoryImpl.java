package doc.doccenterserver.infrastructure.fileresource.message.repository;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import doc.doccenterserver.domain.fileresource.message.model.JobDetailLogEntity;
import doc.doccenterserver.domain.fileresource.message.repository.JobDetailRepository;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.*;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.rmi.ServerException;
import java.util.Date;

/**
 * 任务详情
 */
@Slf4j
@Component("FILEJobDetailDORepository")
public class JobDetailRepositoryImpl extends ServiceImpl<JobDetailLogMapper, JobDetailLogDO> implements JobDetailRepository {

    @Override
    public boolean updateStatus(String logId, Integer stepId, Integer stepStatus) {
        LambdaUpdateWrapper<JobDetailLogDO> luw=new LambdaUpdateWrapper<>();
        luw.eq(JobDetailLogDO::getLogId,logId);
        luw.eq(JobDetailLogDO::getStepId,stepId);
        luw.set(JobDetailLogDO::getUpdateTime,new Date());
        luw.set(JobDetailLogDO::getStepStatus,stepStatus);
        return this.update(luw);
    }

    @Override
    public boolean updateByStepId(JobDetailLogEntity jobDetailLogEntity) throws ServerException {
        if(StringUtils.isEmpty(jobDetailLogEntity.getLogId()) || StringUtils.isEmpty(jobDetailLogEntity.getStepId()) ){
            throw new ServerException("参数缺失");
        }
        LambdaUpdateWrapper<JobDetailLogDO> luw=new LambdaUpdateWrapper<>();
        luw.eq(JobDetailLogDO::getLogId,jobDetailLogEntity.getLogId());
        luw.eq(JobDetailLogDO::getStepId,jobDetailLogEntity.getStepId());
        luw.set(JobDetailLogDO::getUpdateTime,new Date());
        if(!StringUtils.isEmpty(jobDetailLogEntity.getStepStatus())){
            luw.set(JobDetailLogDO::getStepStatus,jobDetailLogEntity.getStepStatus());
        }
        if(!StringUtils.isEmpty(jobDetailLogEntity.getMessage())){
            luw.set(JobDetailLogDO::getMessage,jobDetailLogEntity.getMessage());
        }
        return this.update(luw);
    }


    @Override
    public boolean insert(String logId, String mqmsg,String message, Integer stepId, Integer stepStatus) {
        JobDetailLogDO detailLogDO = new JobDetailLogDO(logId,mqmsg,new Date(),null,message,
                stepId, stepStatus,new Date());
        return this.save(detailLogDO);
    }


}
