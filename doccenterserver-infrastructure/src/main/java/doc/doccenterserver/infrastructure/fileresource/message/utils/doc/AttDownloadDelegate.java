package doc.doccenterserver.infrastructure.fileresource.message.utils.doc;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface AttDownloadDelegate extends Remote {
    String getAttachmentById(String var1, String var2, String var3) throws RemoteException;
}

