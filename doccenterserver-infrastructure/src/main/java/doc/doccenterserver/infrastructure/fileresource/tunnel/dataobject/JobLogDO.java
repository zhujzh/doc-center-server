package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 任务日志主表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_job_log`")
@NoArgsConstructor
public class JobLogDO {

    @Field(name = "日志流水号")
    @TableId(type = IdType.INPUT, value = "log_id")
    private String logId;

    @Field(name = "日志名称")
    @TableField("log_name")
    private String logName;

    @Field(name = "文档ID")
    @TableField("paper_id")
    private String paperId;

    @Field(name = "消息来源")
    @TableField("source_app_name")
    private String sourceAppName;

    @Field(name = "消息来源的应用编号")
    @TableField("source_app_id")
    private String sourceAppId;

    @Field(name = "源应用文档编号")
    @TableField("source_paper_id")
    private String sourcePaperId;

    @Field(name = "消息来源的应用流水号")
    @TableField("source_seq")
    private String sourceSeq;

    @Field(name = "文档操作")
    @TableField("operation")
    private String operation;

    @Field(name = "消息体")
    @TableField("content")
    private String content;

    @Field(name = "日志主状态")
    @TableField("status")
    private String status;

    @Field(name = "归档组织ID")
    @TableField("archive_org_code")
    private String archiveOrgCode;

    @Field(name = "日志创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @Field(name = "备用字段1")
    @TableField("reserved_1")
    private String reserved1;

    @Field(name = "备用字段2")
    @TableField("reserved_2")
    private String reserved2;

    @Field(name = "备用字段3")
    @TableField("reserved_3")
    private String reserved3;

    @Field(name = "备用字段4")
    @TableField("reserved_4")
    private String reserved4;

    @Field(name = "备用字段5")
    @TableField("reserved_5")
    private String reserved5;

    @Field(name = "备用字段6")
    @TableField("reserved_6")
    private String reserved6;

    @Field(name = "备用字段7")
    @TableField("reserved_7")
    private String reserved7;

    @Field(name = "备用字段8")
    @TableField("reserved_8")
    private String reserved8;

    @Field(name = "备用字段9")
    @TableField("reserved_9")
    private String reserved9;

    @Field(name = "备用字段10")
    @TableField("reserved_10")
    private String reserved10;

    @Field(name = "备用字段11")
    @TableField("reserved_11")
    private String reserved11;

    @Field(name = "备用字段12")
    @TableField("reserved_12")
    private String reserved12;

    @Field(name = "备用字段13")
    @TableField("reserved_13")
    private String reserved13;

    @Field(name = "备用字段14")
    @TableField("reserved_14")
    private String reserved14;


    public JobLogDO(String logId, String content, String logName,
                    String operation, String sourceAppId, String sourceAppName,
                    String sourcePaperId, String status,String archiveOrgCode) {
        super();
        this.setLogId(logId);
        this.setContent( content);
        this.setLogName (logName);
        this.setOperation (operation);
        this.setSourceAppId (sourceAppId);
        this.setSourceAppName (sourceAppName);
        this.setSourcePaperId (sourcePaperId);
        this.setStatus (status);
        this.setArchiveOrgCode(archiveOrgCode);
    }
}
