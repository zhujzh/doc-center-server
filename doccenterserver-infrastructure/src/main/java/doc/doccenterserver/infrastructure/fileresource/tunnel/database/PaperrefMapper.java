package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PaperrefDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 引用文档表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface PaperrefMapper extends BaseMapper<PaperrefDO> {
}
