package doc.doccenterserver.infrastructure.fileresource.system.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import doc.doccenterserver.domain.fileresource.system.repository.ResRepository;
import doc.doccenterserver.infrastructure.fileresource.security.model.LoginUser;
import doc.doccenterserver.infrastructure.fileresource.security.service.TokenService;
import doc.doccenterserver.infrastructure.fileresource.system.converter.ResEntityToResDOConverter;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.ResMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ResDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 9:24
 */
@Slf4j
@Component("ResDORepository")
public class ResRepositoryImpl implements ResRepository {
    @Resource
    private ResMapper resMapper;
    @Resource
    private TokenService tokenService;

    @Override
    public ResEntity getByParentId(String s) {
        ResDO resDO = resMapper.getByParentId(s);
        return ResEntityToResDOConverter.INSTANCE.dOToEntity(resDO);
    }

    @Override
    public Integer findChildrenNumById(String resId) {
        return resMapper.findChildrenNumById(resId);
    }

    @Override
    public List<ResEntity> getInItTree(String s) {
        List<ResDO> resDOList = resMapper.getInItTree(s);
        return ResEntityToResDOConverter.INSTANCE.dOToEntityList(resDOList);
    }

    @Override
    public ResEntity getById(String id) {
        ResDO resDO = resMapper.getById(id);
        return ResEntityToResDOConverter.INSTANCE.dOToEntity(resDO);
    }

    @Override
    public ResEntity addRes(ResEntity resEntity) {
        ResDO resDO = ResEntityToResDOConverter.INSTANCE.entityToDO(resEntity);
        resMapper.insert(resDO);
        return ResEntityToResDOConverter.INSTANCE.dOToEntity(resDO);
    }

    @Override
    public ResEntity editRes(ResEntity resEntity) {
        ResDO resDO = ResEntityToResDOConverter.INSTANCE.entityToDO(resEntity);
        resMapper.updateById(resDO);
        return ResEntityToResDOConverter.INSTANCE.dOToEntity(resDO);
    }

    @Override
    public ResEntity deleteRes(String id) {
        resMapper.deleteById(id);
        return null;
    }

    @Override
    public List<ResEntity> getAllRes() {
        List<ResDO> resDOList = resMapper.selectList(new QueryWrapper<>());
        return ResEntityToResDOConverter.INSTANCE.dOToEntityList(resDOList);
    }

    @Override
    public List<ResEntity> getInItUserTree(HttpServletRequest request) {
        LoginUser loginuser= tokenService.getLoginUser(request);
        List<ResDO> resDOList = resMapper.getInItUserTree(loginuser.getUserModel().getUserid());
        return ResEntityToResDOConverter.INSTANCE.dOToEntityList(resDOList);
    }
}
