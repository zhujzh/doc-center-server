package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.User2roleEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.User2roleDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */

@Mapper
public interface User2roleConvertor {

    User2roleConvertor INSTANCE = Mappers.getMapper(User2roleConvertor.class);


    @Mappings({})
    User2roleDO entityToDO(User2roleEntity parameter);

    @Mappings({})
    User2roleEntity dOToEntity(User2roleDO parameter);

    @Mappings({})
    List<User2roleEntity> dOToEntity(List<User2roleDO> parameter);

    @Mappings({})
    List<User2roleDO> entityToDO(List<User2roleEntity> parameter);

}
