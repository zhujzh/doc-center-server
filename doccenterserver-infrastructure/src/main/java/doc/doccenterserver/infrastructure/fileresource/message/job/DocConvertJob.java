package doc.doccenterserver.infrastructure.fileresource.message.job;

import com.xxl.job.core.handler.annotation.XxlJob;
import doc.doccenterserver.domain.fileresource.message.repository.ArchiveChangelogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.DocConvertChangelogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文档转换
 */
@Component
@Slf4j
public class DocConvertJob {
    @Autowired
    private DocConvertChangelogRepository convertChangelogRepository;
    @XxlJob("docConvert")
    public void  docConvert(){
        convertChangelogRepository.docConvert();
        log.info("调用了");
    }
}
