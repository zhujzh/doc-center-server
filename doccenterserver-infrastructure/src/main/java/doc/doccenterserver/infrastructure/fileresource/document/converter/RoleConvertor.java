package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.RoleEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.RoleDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface RoleConvertor {

    RoleConvertor INSTANCE = Mappers.getMapper(RoleConvertor.class);


    @Mappings({})
    RoleDO entityToDO(RoleEntity parameter);

    @Mappings({})
    RoleEntity dOToEntity(RoleDO parameter);

    @Mappings({})
    List<RoleEntity> dOToEntity(List<RoleDO> parameter);

    @Mappings({})
    List<RoleDO> entityToDO(List<RoleEntity> parameter);
}
