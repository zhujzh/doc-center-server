package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_pub_user`")
@NoArgsConstructor
public class UserDO {

    @Field(name = "USER_ID")
    @TableId(type = IdType.INPUT, value = "user_id")
    private String userId;

    @Field(name = "USER_CODE")
    @TableField("user_code")
    private String userCode;

    @Field(name = "USER_NAME")
    @TableField("user_name")
    private String userName;

    @Field(name = "USER_SEX")
    @TableField("user_sex")
    private String userSex;

    @Field(name = "USER_AGE")
    @TableField("user_age")
    private Integer userAge;

    @Field(name = "COMPANY_ID")
    @TableField("company_id")
    private String companyId;

    @Field(name = "ORG_ID")
    @TableField("org_id")
    private String orgId;

    @Field(name = "USER_MOBILE")
    @TableField("user_mobile")
    private String userMobile;

    @Field(name = "USER_MAIL")
    @TableField("user_mail")
    private String userMail;

    @Field(name = "USER_WORK_ADDRESS")
    @TableField("user_work_address")
    private String userWorkAddress;

    @Field(name = "USER_WORK_PHONE")
    @TableField("user_work_phone")
    private String userWorkPhone;

    @Field(name = "USER_HOME_ADDREE")
    @TableField("user_home_addree")
    private String userHomeAddree;

    @Field(name = "USER_HOME_PHONE")
    @TableField("user_home_phone")
    private String userHomePhone;

    @Field(name = "POSITION_ID")
    @TableField("position_id")
    private String positionId;

    @Field(name = "PLURALITY_POSITION_ID")
    @TableField("plurality_position_id")
    private String pluralityPositionId;

    @Field(name = "TITLE_ID")
    @TableField("title_id")
    private String titleId;

    @Field(name = "PLURALITY_TITLE_ID")
    @TableField("plurality_title_id")
    private String pluralityTitleId;

    @Field(name = "USER_TYPE")
    @TableField("user_type")
    private String userType;

    @Field(name = "USER_STATUS")
    @TableField("user_status")
    private String userStatus;

    @Field(name = "USER_SORT")
    @TableField("user_sort")
    private Integer userSort;

    @Field(name = "USER_PWD")
    @TableField("user_pwd")
    private String userPwd;

    @Field(name = "USER_CREATE_TIME")
    @TableField("user_create_time")
    private Date userCreateTime;

    @Field(name = "USER_UPDATE_TIME")
    @TableField("user_update_time")
    private Date userUpdateTime;

    @Field(name = "USER_CREATOR")
    @TableField("user_creator")
    private String userCreator;

    @Field(name = "REMARK")
    @TableField("remark")
    private String remark;
    @Field(name = "DEPT_ID")
    @TableField("dept_id")
    private String deptId;

    @Field(name = "COMPANY_NAME")
    @TableField("company_name")
    private String companyName;
    @Field(name = "DEPT_NAME")
    @TableField("dept_name")
    private String deptName;
    @Field(name = "ORG_NAME")
    @TableField("org_name")
    private String orgName;
}
