package doc.doccenterserver.infrastructure.fileresource.file.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * 文档转换
 */
public class DocConvertUtil {

    public static void main(String[] args) throws Exception {
        //文件转换
        //待转文件地址
        String inputFilePath="e:\\appdata\\content\\data\\Mon_2304\\attachments\\convert\\国家烟草专卖局办公室关于转发电子商务统计报表制度的通知.doc";
        convert(inputFilePath);
    }
    /**
     * 获取脚本文件路径
     * @return
     */
    public static String getDoc2PdfPath(){
        String shellPath = MultipartFileUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        shellPath = StringUtils.replace(shellPath, "\\", "/");
        int index = shellPath.indexOf("/lib/");
        String os = System.getProperty("os.name");
        String shName= "doc2img.sh";
        if(os.toLowerCase().startsWith("win")){
            shellPath = shellPath.substring(1);
            shName= "doc2pdf.vbs";
        }
        if(index>-1){
            shellPath = shellPath.substring(0, index);
        }
        if(!shellPath.endsWith("/")){
            shellPath = shellPath+"/";
        }
//        return shellPath+shName;
        return "C:\\xxl-job\\"+shName;
//        return "E:\\ywzt-doc\\doc-center-server\\doccenterserver-infrastructure\\lib\\"+shName;
    }
    public static void convert(String inputFilePath){
        ProcessHelper.executeScript("doc2pdf","wscript",getDoc2PdfPath(), "\""+inputFilePath+"\"");
    }
}
