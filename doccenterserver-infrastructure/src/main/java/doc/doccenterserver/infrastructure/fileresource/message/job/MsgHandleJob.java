package doc.doccenterserver.infrastructure.fileresource.message.job;

import com.xxl.job.core.handler.annotation.XxlJob;
import doc.doccenterserver.domain.fileresource.message.repository.MqChangelogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息处理
 */
@Component
@Slf4j
public class MsgHandleJob {
    @Autowired
    private MqChangelogRepository mqChangelogRepository;
    @XxlJob("msgHandle")
    public void  msgHandle(){
        mqChangelogRepository.msgHandle();
    }
}
