package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.DictDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 数据字典Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface DictMapper extends BaseMapper<DictDO> {
    DictDO getByParentId(@Param("pid")String pid);

    List<DictDO> getInItTree(@Param("pid")String pid);

    Integer findChildrenNumById(@Param("id")String id);
}
