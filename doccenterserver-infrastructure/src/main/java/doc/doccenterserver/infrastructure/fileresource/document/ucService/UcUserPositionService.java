package doc.doccenterserver.infrastructure.fileresource.document.ucService;

import com.tobacco.mp.uc.kz.client.api.dto.ListUserPositionResponse;

import java.util.Collection;

/**
 * @author wangxp
 * @description 人员 定职列表
 * @date 2023/6/15 18:35
 */
public interface UcUserPositionService {

    /**
     * @param loginUid 登录 id
     * @return    Collection<ListUserPositionResponse>
     * @description 根据 用户登录id查询人员定制列表
     * @author wangxp
     * @date 2023/6/15 18:58
     */
    Collection<ListUserPositionResponse> queryUserPositionByLoginId(String loginUid);
}
