package doc.doccenterserver.infrastructure.fileresource.message.repository;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.blueland.ecm.wsinterface.ECMWSTool_New;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.document.model.InterfaceAccessLogEntity;
import doc.doccenterserver.domain.fileresource.document.repository.InterfaceAccessLogRepository;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import doc.doccenterserver.domain.fileresource.file.repository.FileRepository;
import doc.doccenterserver.domain.fileresource.message.repository.ArchiveChangelogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobDetailRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import doc.doccenterserver.infrastructure.fileresource.file.utils.MultipartFileUtil;
import doc.doccenterserver.infrastructure.fileresource.message.config.MessageXmlConfig;
import doc.doccenterserver.infrastructure.fileresource.message.enums.*;
import doc.doccenterserver.infrastructure.fileresource.message.utils.DateUtil;
import doc.doccenterserver.infrastructure.fileresource.message.utils.IdentifierGenerator;
import doc.doccenterserver.infrastructure.fileresource.message.utils.JdomXmlUtil;
import doc.doccenterserver.infrastructure.fileresource.message.utils.doc.ECMWSTool_New;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.*;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.rmi.ServerException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static doc.doccenterserver.infrastructure.fileresource.message.utils.JdomXmlUtil.getMeetingEntry;

/**
 * 消息归档
 */
@Slf4j
@Component("FILEArchiveChangelogDORepository")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ArchiveChangelogRepositoryImpl extends ServiceImpl<ArchiveChangelogMapper, ArchiveChangelogDO> implements ArchiveChangelogRepository {
    private final JSONObject parameterMap;
    private final JobDetailRepository jobDetailRepository;
    private final JobLogRepository jobLogRepository;
    private final PaperMapper paperMapper;
    private final MeetingMapper meetingMapper;
    private final ImageMapper imageMapper;
    private final DocConvertChangelogMapper docConvertChangelogMapper;
    private final PaperAuthMapper paperAuthMapper;
    private final OrgMapper orgMapper;
    private final UserMapper userMapper;
    private final AttachmentMapper attachmentMapper;
    private final JobLogMapper jobLogMapper;
    private final FileRepository fileRepository;

    private final InterfaceAccessLogRepository interfaceAccessLogRepository;

    @Value("${xxl.job.admin.addresses}")
    private String adminAddresses;

    @Value("${xxl.job.executor.ip}")
    private String ip;

    @Override
    public void msgArchive() {
        LambdaQueryWrapper<ArchiveChangelogDO> lqw = new LambdaQueryWrapper<>();
        lqw.lt(ArchiveChangelogDO::getRetryTimes, 4);
        List<ArchiveChangelogDO> archiveChangeloglist = getBaseMapper().selectList(lqw);
        for (ArchiveChangelogDO archiveChangelog : archiveChangeloglist) {
            InterfaceAccessLogEntity interfaceAccessLogEntity = new InterfaceAccessLogEntity();
            interfaceAccessLogEntity.setLogId(IdentifierGenerator.createPrimaryKeySeq());
            interfaceAccessLogEntity.setParams(JSON.toJSONString(archiveChangelog));
            interfaceAccessLogEntity.setCallTime(new Date());
            interfaceAccessLogEntity.setRequestUrl(adminAddresses);
            interfaceAccessLogEntity.setRequestIp(ip);
            interfaceAccessLogEntity.setAppname("文档中心扩展");
            try {
                this.processData(archiveChangelog);
                interfaceAccessLogEntity.setResultStatus("Y");
                interfaceAccessLogRepository.insert(interfaceAccessLogEntity);
            } catch (Exception e) {
                e.printStackTrace();
                archiveChangelog.setRetryTimes(archiveChangelog.getRetryTimes() + 1);
                jobLogRepository.updateError(archiveChangelog.getLogId(), e.getMessage(), JobDetailLogStepIdEnum.STEP_ID_3.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_FAILD.getValue(), archiveChangelog.getRetryTimes());
                interfaceAccessLogEntity.setResultStatus("N");
                interfaceAccessLogEntity.setFailReason(e.getMessage());
                interfaceAccessLogRepository.insert(interfaceAccessLogEntity);
            }
        }
    }

    public void processData(ArchiveChangelogDO archiveChangelog) throws Exception {
        //预处理xml
        String content = JdomXmlUtil.precorrectxml(archiveChangelog.getContent());
        Document doc = JdomXmlUtil.xml2Document(content);
        String sourceApp = archiveChangelog.getSourceApp();
        String sourcePaperId = archiveChangelog.getSourcePaperId();
        String operation = archiveChangelog.getOperation();
        String archiveOrgCode = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.ARCHIVE_ORG_CODE);
        //CMS文档特殊处理
        if ("CMS".equals(sourceApp)) {
            ChannelEntity channel = jobLogRepository.cmsCl(doc);
            if (channel != null) {
                archiveOrgCode = channel.getOrgId();
            }
        }
        //根据应用ID、归档组织、应用文档编号，去统一消息暂存日志表是否存在
        if (OperationTypeEnum.CREATE.getValue().equals(operation) && Boolean.TRUE.equals(jobLogRepository.checkMqSaveExist(sourceApp, sourcePaperId, archiveOrgCode))) {
            jobLogRepository.clearDate4Repeated(archiveChangelog.getLogId(), JobDetailLogStepIdEnum.STEP_ID_3.getValue());
            return;
        }
        LambdaQueryWrapper<PaperDO> lqwPaper = new LambdaQueryWrapper<>();
        lqwPaper.eq(PaperDO::getAppId, sourceApp);
        lqwPaper.eq(PaperDO::getArchiveOrgCode, archiveOrgCode);
        lqwPaper.eq(PaperDO::getSourceDocId, sourcePaperId);
        PaperDO paperDO = paperMapper.selectList(lqwPaper).stream().findAny().orElse(null);
        //消息插入到文档表、文档附件表，下载文档附件，判断附件是否下载成功
        JobLogDO jobLog = new JobLogDO();
        String logId = archiveChangelog.getLogId();
        jobLog.setLogId(logId);
        String paperId = "";
        if (OperationTypeEnum.CREATE.getValue().equals(operation)) {
            paperId = this.createPaper(doc, paperDO);
            jobLog.setPaperId(paperId);
        }
        if (OperationTypeEnum.RETRIEVE.getValue().equals(operation) && null != paperDO) {
            PaperDO paper = new PaperDO();
            paperId = paperDO.getPaperId();
            //更新为删除标识
            paper.setDelFlag(DelFlagEnum.Y.getValue());
            paper.setPaperId(paperId);
            paperMapper.updateById(paper);
            //清除创建的文档已经存在于大数据平台的原文件
            deleteBigDataFileByPaperId(paperId);
            jobLog.setPaperId(paperId);
        }

        //删除消息归档任务changlog表
        this.removeById(logId);
        //更新任务日志从表《消息归档》任务状态为完成
        jobDetailRepository.updateStatus(logId, JobDetailLogStepIdEnum.STEP_ID_3.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_SUCCESS.getValue());
        if (StringUtils.isNoneBlank(paperId)) {
            //插入任务日志从表《文档转换》，任务状态为未完成
            jobDetailRepository.insert(logId, null, null, JobDetailLogStepIdEnum.STEP_ID_4.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_HANDLING.getValue());
            //写入文档转换任务change表
            DocConvertChangelogDO docConvertChangelog = new DocConvertChangelogDO(logId, new Date(), operation, paperId, 0);
            docConvertChangelogMapper.insert(docConvertChangelog);
        } else {
            jobLog.setStatus(JobLogStatusEnum.STATUS_IGNOR.getValue());
        }
        jobLogMapper.updateById(jobLog);
    }

    public String createPaper(Document doc, PaperDO paperDO) throws Exception {
        String permision = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.PERMISION);
        String remark = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.REMARK);
        String readers = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.READERS);
        //解析文档
        PaperDO paper = handlePaper(doc);
        String archiveOrgCode = paper.getArchiveOrgCode();
        String sourceApp = paper.getAppId();
        String paperId = paper.getPaperId();
        if (StringUtils.isBlank(paperId)) {
            throw new ServerException("生成文档编号失败。。。。。。");
        }
        //解析文档权限
        List<PaperAuthDO> authList = savePaperRole(paperId, permision, sourceApp, archiveOrgCode, remark, readers);
        paper.setIsPublic(authList.isEmpty() ? "Y" : "N");
        if (null != paperDO) {
            paper.setDelFlag(DelFlagEnum.N.getValue());
            paperId = paperDO.getPaperId();
            paper.setPaperId(paperId);
            //删除历史数据
            paperAuthMapper.delete(new LambdaQueryWrapper<PaperAuthDO>().eq(PaperAuthDO::getPaperId, paperId));
            //清除创建的文档已经存在于大数据平台的原文件
            deleteBigDataFileByPaperId(paperId);
            LambdaQueryWrapper<AttachmentDO> lqwAtt = Wrappers.lambdaQuery();
            lqwAtt.eq(AttachmentDO::getPaperId, paperId);
            attachmentMapper.delete(lqwAtt);
            LambdaQueryWrapper<ImageDO> lqwImg = Wrappers.lambdaQuery();
            lqwImg.eq(ImageDO::getPaperId, paperId);
            imageMapper.delete(lqwImg);
            paperMapper.updateById(paper);
        } else {
            //新增文档默认PAPER_ORDER的值
            Date orderDate = DateUtil.parseDateFormat("1900", "yyyy");
            paper.setPaperOrder(orderDate);
            paperMapper.insert(paper);
        }
        //会议通告
        MeetingDO meetingDO = getMeetingEntry(doc);
        if (meetingDO != null) {
            MeetingDO meeting = meetingMapper.selectById(paperId);
            meetingDO.setPaperId(paperId);
            if (null == meeting) {
                meetingMapper.insert(meetingDO);
            } else {
                meetingMapper.updateById(meetingDO);
            }
        }
        //保存文档权限
        for (PaperAuthDO paperAuthEntry : authList) {
            paperAuthMapper.insert(paperAuthEntry);
        }
        //新增：文档文件附件及图片信息；修改：文档的首页图片地址及文档内容
        this.createAttachment(doc, paper);
        return paperId;
    }

    public void createAttachment(Document doc, PaperDO paper) throws Exception {
        String sourceApp = paper.getAppId();
        String paperId = paper.getPaperId();
        log.info("处理文档附件");
        List<Element> attachmentlist = JdomXmlUtil.getElementListFromDocument(doc, MessageXmlConfig.ATTACHMENT);
        if(attachmentlist==null|| attachmentlist.isEmpty()){
            log.info("无附件");
            return;
        }
        String fileMapJSON = saveAttachment(attachmentlist, sourceApp, paper.getPaperType(), paperId);
        log.info("附件集合"+fileMapJSON);
        JSONObject fileMap =  JSONUtil.parseObj(fileMapJSON);
        List<AttachmentDO> attList = fileMap.getJSONArray("attList").toList(AttachmentDO.class);
        for (AttachmentDO att : attList) {
            att.setAttaId(IdentifierGenerator.createPrimaryKeySeq());
            att.setAppId(sourceApp);
            attachmentMapper.insert(att);
        }
        List<ImageDO> imgAttList = fileMap.getJSONArray("imgAttList").toList(ImageDO.class);
        for (ImageDO imgAtt : imgAttList) {
            imgAtt.setAppId(sourceApp);
            imageMapper.insert(imgAtt);
        }
    }
    /**
     * 保存附件
     */
    private String saveAttachment(List<Element> elementlist, String sourceApp, Integer paperType, String paperId) throws Exception {
        String picPath;
        Map<String, Object> retMap = Maps.newHashMap();
        List<AttachmentDO> attList = Lists.newArrayList();
        List<ImageDO> imgAttList = Lists.newArrayList();
        int imgNums = 0;
        int attNums = 0;
        //是否存在主附件
        boolean hasMainAtt = false;
        for (int i = 0; i < elementlist.size(); i++) {
            Element element = elementlist.get(i);
            //附件名称
            String attName = "";
            //附件编号（通过此编号从指定WEBSERVICE中获取附件文件流）
            String attId = "";
            //附件类型（1、2：表示附件文档的附件；0：表示正文的附件；3；表示图片新闻的附件）
            String attType = "";
            //附件的描述
            String attDescript = "";
            Element name = JdomXmlUtil.getElementFromElement(element, "name");
            if (name != null) attName = name.getTextTrim();
            Element access = JdomXmlUtil.getElementFromElement(element, "access");
            if (access != null) {
                attId = access.getTextTrim();
            } else {
                return null;
            }
            Element description = JdomXmlUtil.getElementFromElement(element, "descript");
            if (description != null) attDescript = description.getTextTrim();
            Element type = JdomXmlUtil.getElementFromElement(element, "type");
            if (type != null) attType = type.getTextTrim();
            if (null == attId || "".equals(attId.trim())) {
                log.info("无附件!");
                return null;
            }
            AttachmentDO att = new AttachmentDO();
            att.setFileName(attName);
            att.setPaperId(paperId);
            att.setFileDesc(attDescript);
            att.setOrderBy(i);
            String fileformat = "tmp";
            if (attName.contains(".")) {
                fileformat = attName.substring(attName.lastIndexOf(".") + 1).toLowerCase();
            }
            String filepath = "";
            long filesize = 0L;

            //附件或图片的相对路径，如：/content/data/Mon_1304/attachments/cmsUploadFile/07_1365319245031.doc
            filepath = generateAttPath(attName, attType);
            //通过webservice接口，获取附件流，并保存成物理文件
            String basePath = StringUtils.replace(parameterMap.getStr("BIG_DATA_DIR_PATH"), "\\", "/");
            if (basePath.endsWith("/")) {
                basePath = basePath.substring(0, basePath.length() - 2);
            }
            log.info("下载附件"+basePath + filepath);
            filesize = getAttFile(sourceApp, attId, attType, basePath + filepath);
            File file = new File(basePath + filepath);
            FileEntity fileDto = new FileEntity();
            if (file.exists()) {
                try {
                    String fileId=fileRepository.uploadFile(MultipartFileUtil.getMultipartFile(file));
                    String ext = FilenameUtils.getExtension(attName);
                    fileDto.setFileId(IdentifierGenerator.createPrimaryKeySeq());
                    fileDto.setFileFormat(ext);
                    fileDto.setFileSize((double) filesize);
                    fileDto.setFilename(attName);
                    fileDto.setAppId(sourceApp);
                    fileDto.setLabelName(sourceApp);
                    fileDto.setBigdataFileId(fileId);
                    fileDto.setCreateDate(new Date());
                    fileRepository.insertFile(fileDto);
                } catch (Exception e) {
                    log.error("归档附件上传基线版文档中心失败");
                    throw new ServerException("归档附件上传基线版文档中心失败");
                }

            } else {
                log.error("归档附件下载失败");
                throw new ServerException("归档附件下载失败");
            }
            if (fileDto.getBigdataFileId() != null) {
                att.setFileMd5(fileDto.getFileId());
                att.setFileId(fileDto.getFileId());
                try {
                    log.info("删除下载的文件");
                    FileUtils.deleteQuietly(new File(basePath + filepath));
                } catch (Exception ex) {
                    throw new ServerException("归档文件删除失败");
                }
            }
            att.setFileSize((double) filesize);
            //图片类型的附件
            if ("3".equals(attType)) {
                ImageDO image = new ImageDO();
                image.setImgId(IdentifierGenerator.createPrimaryKeySeq());
                image.setPaperId(paperId);
                image.setOrderBy(i);
                image.setImgName(attName);
                image.setImgDesc(attDescript);
                image.setFileId(att.getFileId());
                image.setFileMd5(att.getFileId());
                if (imgNums == 0) {
                    image.setIsMain("1");
                    //压缩首页图片
                    picPath = filepath;
                    image.setImgPath(picPath);
                } else {
                    image.setIsMain("0");
                    image.setImgPath(filepath);
                }
                imgNums++;
                imgAttList.add(image);
            } else {
                attNums++;
                att.setFileFormat(fileformat);
                //cms特殊处理：当只有一个附件的时候默认为 主附件，多个的话，默认第一个为主附件
                if ("CMS".equals(sourceApp)) {
                    if (attNums == 1) {
                        att.setIsMain(IsMainEnum.Y.getValue());
                        hasMainAtt = true;
                    } else {
                        att.setIsMain(IsMainEnum.N.getValue());
                    }
                } else {
                    if ("".equals(attType)) {
                        att.setIsMain(IsMainEnum.N.getValue());
                    } else {
                        att.setIsMain(attType);
                        if ("1".equals(attType)) {
                            hasMainAtt = true;
                        }
                    }
                }
                attList.add(att);
            }
        }
        if (!hasMainAtt && 1 == paperType) {
            throw new ServerException("文档类型为office附件文档时，必须带正文附件！");
        }
        retMap.put("imgAttList", imgAttList);
        retMap.put("attList", attList);
        return JSONUtil.toJsonStr(retMap);
    }

    /**
     * 获取归档消息附件
     */
    private long getAttFile(String sourceApp, String attId, String attType, String filepath) throws Exception {
        File attfile = new File(filepath);
        if (!attfile.getParentFile().exists()) {
            attfile.getParentFile().mkdirs();
        }
        String url = "";
        if ("OA".equals(sourceApp)) {
            url = parameterMap.getStr("OAFILESERVICE_URL");
        } else if ("ZYOA".equals(sourceApp)) {
            url = parameterMap.getStr("ZYOAFILESERVICE_URL");
        } else if ("CZOA".equals(sourceApp)) {
            url = parameterMap.getStr("CZ_OAFILESERVICE_URL");
        } else if ("CZCOP".equals(sourceApp)) {
            url = parameterMap.getStr("CZ_COPFILESERVICE_URL");
        } else if ("CZSTDD".equals(sourceApp) || "CZECM".equals(sourceApp)) {
            url = parameterMap.getStr("CZ_STDDFILESERVICE_URL");
        } else if ("CMS".equals(sourceApp)) {
            url = parameterMap.getStr("PHPCMSFILESERVICE_URL");
        } else if ("ZYECM".equals(sourceApp)) {
            url = parameterMap.getStr("JT_ECMFILESERVICE_URL");
        } else if ("WZECM".equals(sourceApp)) {
            url = parameterMap.getStr("WZ_ECMFILESERVICE_URL");
        } else if ("SPECM".equals(sourceApp)) {
            url = parameterMap.getStr("SP_ECMFILESERVICE_URL");
        } else if ("ZYSTDD".equals(sourceApp)) {
            url = parameterMap.getStr("ZY_STDDFILESERVICE_URL");
        } else if ("ZYPOST".equals(sourceApp)) {
            url = parameterMap.getStr("ZYPOSTFILESERVICE_URL");
        }
        log.info("调用脚本下载文件");
        ECMWSTool_New ecmwsToolNew = new ECMWSTool_New();
        ecmwsToolNew.getAttFile(sourceApp, attId, attType, filepath, url);
        if (!attfile.exists() || attfile.length() == 0) {
            throw new ServerException("file get failed! filepath:" + filepath);
        }
        log.info("调用脚本下载文件完成");
        return attfile.length();
    }

    /**
     * 获取路径
     */
    public String generateAttPath(String fileName, String attType) {
        String filePath = "";
        LocalDate today = LocalDate.now();
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyMM");
        String timePath = today.format(fmt);
        if ("3".equals(attType)) {
            //上传后的图片路径
            filePath = "/content/data/Mon_" + timePath + "/images/" + fileName;
        } else {
            //上传后的附件路径
            filePath = "/content/data/Mon_" + timePath + "/attachments/cmsUploadFile/" + fileName;
        }
        return filePath;
    }

    /**
     * 保存文档权限
     */
    private List<PaperAuthDO> savePaperRole(String paperId, String permision, String sourceApp, String archiveorgcode, String remark, String readers) throws ServerException {
        List<PaperAuthDO> authList = Lists.newArrayList();
        //新增：文档权限信息（OA遵循新规范）
        if (!"CMS".equals(sourceApp)) {
            //判断此文档是否允许公开
            if ("all".equals(permision)) {
                //如果readers判断后是公开，还需要判断以下条件：
                //如果归属地是集团总部，则表示此文档是公开的；
                //如果是各烟厂，则表示此文档只能归属地用户能看
                String jtCompanyId=parameterMap.getStr("JT_COMPANYID");
                if (jtCompanyId==null || !jtCompanyId.equals(archiveorgcode)) {
                    PaperAuthDO paperAuth = new PaperAuthDO(archiveorgcode, 1, "g", paperId);
                    authList.add(paperAuth);
                }
            } else {
                //消息的阅读范围
                //权限，使用u:xxx;u:xxx;g:xxx;a:xxx;e:xxx；
                //其中u为人员、g为部门、a为自定义组、e为扩展。这样可以同时满足对人、部门、分组、扩展信息的授权。如果该帖对所有人公开，则传入all。
                if (null != permision && !"".equals(permision)) {
                    String[] roles = permision.split(";");
                    //去掉重复的权限
                    HashMap<String,String> uni = new HashMap<>();
                    for (String role : roles) {
                        if (uni.containsKey(role)) {
                            continue;
                        } else {
                            uni.put(role, role);
                        }
                        if (role.length() > 0) {
                            PaperAuthDO paperAuth = new PaperAuthDO();
                            paperAuth.setAuthValue(1);
                            paperAuth.setPaperId(paperId);
                            if (role.startsWith("u:")) {
                                role = role.replace("u:", "");
                                paperAuth.setObjectType("u");
                            } else if (role.startsWith("a:")) {
                                paperAuth.setObjectType("a");
                                role = role.replace("a:", "");
                            } else if (role.startsWith("g:")) {
                                paperAuth.setObjectType("g");
                                role = role.replace("g:", "");
                                OrgDO org = orgMapper.selectById(role);
                                if (null == org) {
                                    throw new ServerException("组织【" + role + "】不存在...");
                                }
                            }
                            paperAuth.setAuthObject(role);
                            authList.add(paperAuth);
                        }
                    }
                }
            }
        } else {
            if (null != remark && (remark.contains("可公开")
                    || remark.contains("此件可上网")
                    || remark.contains("是")
                    || remark.contains("YES")
                    || remark.contains("yes"))) {
                if (!parameterMap.getStr("JT_COMPANYID").equals(archiveorgcode)) {
                    PaperAuthDO paperAuth = new PaperAuthDO(archiveorgcode, 1, "g", paperId);
                    paperAuthMapper.insert(paperAuth);
                }
            } else {
                if (readers.length() > 0) {
                    String[] roles = readers.split(";");
                    //去掉重复的权限
                    HashMap<String,String> uni = new HashMap<>();
                    for (String role : roles) {
                        if (uni.containsKey(role)) {
                            continue;
                        } else {
                            uni.put(role, role);
                        }
                        if (role.trim().length() > 0) {
                            PaperAuthDO paperAuth = new PaperAuthDO();
                            paperAuth.setAuthValue(1);
                            paperAuth.setPaperId(paperId);
                            //如果出现LDAP不整的情况（如：cn=内网技术标准付催专用管理组,,dc=HNGYTOBACCO,dc=COM）
                            if (role.contains(",,")) {
                                throw new ServerException("当前消息的阅读范围【" + readers + "】中LDAP目录不完整..." + role);
                            }
                            if (role.startsWith("uid=")) {
                                paperAuth.setObjectType("u");
                            } else {
                                if (role.toLowerCase().contains("cn=groups")) {
                                    //将用户组转换成所有用户信息
                                    role = role.substring(0, role.indexOf(","));
                                    role = role.replace("cn=", "");
                                    paperAuth.setObjectType("a");
                                } else {
                                    role = role.substring(0, role.indexOf(","));
                                    role = role.replace("cn=", "");
                                    OrgDO org = orgMapper.selectById(role);
                                    if (org == null) {
                                        throw new ServerException("组织【" + role + "】不存在...");
                                    }
                                    paperAuth.setObjectType("g");
                                }
                                paperAuth.setAuthObject(role);
                                authList.add(paperAuth);
                            }
                        }
                    }
                }
            }
        }
        return authList;
    }
    /**
     * 消息转文档
     */
    private PaperDO handlePaper(Document doc) throws Exception {
        String sourceApp = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SENDER);
        String sourcePaperId = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SOURCE_PAPER_ID);
        String archiveorgcode = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.ARCHIVE_ORG_CODE);
        String context = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.CONTEXT);
        String appurl = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.ABS_URL);
        String paperName = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.SUBJECT);
        String paperId = IdentifierGenerator.createPrimaryKeySeq();
        PaperDO paper = new PaperDO();
        paper.setPaperName(paperName);
        paper.setPaperId(paperId);
        paper.setAppId(sourceApp);
        paper.setAppUrl(appurl);
        paper.setArchiveOrgCode(archiveorgcode);
        paper.setPublicFlag(0);
        paper.setDelFlag(DelFlagEnum.N.getValue());
        paper.setIsChannel("0");
        paper.setIsPublic("N");

        String creator = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.CREATOR);
        String inputUserId = ConstantSys.CZ_ADMIN;
        if (null != creator) {
            if (creator.startsWith("uid=")) {
                creator = creator.substring(0, creator.indexOf(","));
                inputUserId = creator.replace("uid=", "");
            } else {
                inputUserId = creator;
            }
        }
        paper.setInputUserId(inputUserId);

        //消息所属的频道编号（OA遵循新的规范）
        if ("CMS".equals(sourceApp)) {
            ChannelEntity channel = jobLogRepository.cmsCl(doc);
            if (channel!=null) {
                paper.setClassId(channel.getId());
            }
            sourcePaperId = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.CMS_SOURCE_PAPER_ID);
            if (sourcePaperId.startsWith("CMS_")) {
                sourcePaperId = sourcePaperId.replace("CMS_", "");
                String[] cmsIds = sourcePaperId.split("_");
                sourcePaperId = "CMS_" + cmsIds[0];
            }

        } else {
            //直接获取channelid节点数据
            ChannelEntity channel = jobLogRepository.cmsCl(doc);
            if (channel!=null) {
                paper.setClassId(channel.getId());
            }
        }
        //消息内容
        if (null != context && !"".equals(context)) {
            context = context.replace("&lt;", "<");
            context = context.replace("&gt;", ">");
            if ("<br/>".equals(context) || "<br />".equals(context)) {
                context = "";
            }
            context = context.replace("&amp;", "&");
            context = context.replace("\\[attachment=.+\\]", "");
            context = context.replace("\\[upload=.+\\]", "");
            paper.setPaperText(context);
        }
        //找出此用户的部门及公司编号
        UserDO user = userMapper.selectById(inputUserId);
        if (null != user) {
            paper.setInputDeptId(user.getDeptId());
            paper.setInputOrgId(user.getOrgId());
        } else {
            paper.setInputDeptId(archiveorgcode);
            paper.setInputOrgId("-1");
        }
        //消息起草时间，如：2013-04-18 15:30
        String created = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.CREATED);
        if (null != created) {
            LocalDateTime now = LocalDateTime.now();
            if (created.length() > 0) {
                String createdate = created;
                String[] inputtimes = createdate.split(":");
                if (inputtimes.length == 1) {
                    createdate += " 00:00:00";
                } else if (inputtimes.length == 2) {
                    createdate += ":00";
                }
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                now = LocalDateTime.parse(createdate, formatter);
            }
            paper.setInputTime(now);
        }
        //消息发布人编号，如：uid=sumy1206,cn=employees,dc=hngytobacco,dc=com
        String docauthor = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.DOCAUTHOR);
        if (null != docauthor) {
            String issueUserId = ConstantSys.CZ_ADMIN;
            if (docauthor.length() > 0) {
                if (docauthor.startsWith("uid=")) {
                    docauthor = docauthor.substring(0, docauthor.indexOf(","));
                    issueUserId = docauthor.replace("uid=", "");
                } else {
                    issueUserId = docauthor;
                }
            }
            paper.setIssueUserId(issueUserId);
        }

        //OA的扩展信息，用于桌面端，于归档无用
        if (!"OA".equals(sourceApp) && !"ZYOA".equals(sourceApp)) {
            //消息落款一  对应的会议 已经单独处理
            String extension1 = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.EXTENSION_1);
            if (null != extension1 && extension1.length() > 0) {
                paper.setSignOrgName(extension1);
            }

            //消息落款二
            String extension2 = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.EXTENSION_2);
            if (null != extension2 && extension2.length() > 0) {
                paper.setSignDate(extension2);
            }
        }
        //文档类型（PAPERTYPE）
        String paperType = JdomXmlUtil.getTextvalueFromDocument(doc, MessageXmlConfig.PAPER_TYPE);
        if (null != paperType && !"".equals(paperType)) {
            paper.setPaperType(Integer.valueOf(paperType));
        } else {
            if (null == context || "".equals(context)) {
                //附件文档
                paper.setPaperType(PaperTypeEnum.FILE.getValue());
            } else {
                //正文HTML文档
                paper.setPaperType(PaperTypeEnum.HTML.getValue());
            }
        }
        //判断待归档文档在文档中心中是否已经存在
        //判断条件：归属地+资源ID + create
        paper.setSourceDocId(sourcePaperId);
        return paper;
    }

    /**
     * 删除文件
     */
    private void deleteBigDataFileByPaperId(String paperId) throws IOException {
        //根据fileid和pdffileid删除原文件和pdf文件
        LambdaQueryWrapper<AttachmentDO> lqw = new LambdaQueryWrapper<>();
        lqw.eq(AttachmentDO::getPaperId, paperId);
        List<AttachmentDO> attachmentList = attachmentMapper.selectList(lqw);
        //删除附件
        List<AttachmentDO> deleteFileList = attachmentList.stream().filter(item -> StringUtils.isNotEmpty(item.getFileId())).collect(Collectors.toList());
        for (AttachmentDO item : deleteFileList) {
            log.error("---开始删除fileid为：" + item.getFileId() + "的文件");
            fileRepository.deleteFile(item.getFileId());
            log.error("---成功删除fileid为：" + item.getFileId() + "的文件");
        }
        //删除转PDF文件
        List<AttachmentDO> isMainList = attachmentList.stream()
                .filter(item -> IsMainEnum.Y.getValue().equals(item.getIsMain()) && StringUtils.isNotEmpty(item.getPdfFileId()))
                .collect(Collectors.toList());
        for (AttachmentDO item : isMainList) {
            log.error("---开始删除fileid为：" + item.getPdfFileId() + "的pdf文件");
            fileRepository.deleteFile(item.getPdfFileId());
            log.error("---成功删除fileid为：" + item.getPdfFileId() + "的pdf文件");
        }
    }
}
