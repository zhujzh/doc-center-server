package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.document.model.AttachmentEntity;
import doc.doccenterserver.domain.fileresource.document.repository.AttachmentRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.AttachmentEntityToAttachmentDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.AttachmentMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.AttachmentDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("AttachmentDORepository")
public class AttachmentRepositoryImpl implements AttachmentRepository {

    @Resource
    private AttachmentMapper attachmentMapper;

    @Override
    public Integer deleteAttachmentByPaperId(String paperId) {
        LambdaQueryWrapper<AttachmentDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AttachmentDO::getPaperId, paperId);
        return attachmentMapper.delete(wrapper);
    }

    @Override
    public Integer insertAttachment(AttachmentEntity attachmentEntity) {
        return attachmentMapper.insert(AttachmentEntityToAttachmentDOConvertor.INSTANCE.entityToDO(attachmentEntity));
    }

    @Override
    public Integer findAttCountByPaperId(String paperId) {
        return attachmentMapper.findAttCountByPaperId(paperId);
    }

    @Override
    public List<AttachmentEntity> getAttachmentList(AttachmentEntity attachmentEntity) {
        LambdaQueryWrapper<AttachmentDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AttachmentDO::getPaperId, attachmentEntity.getPaperId());
        queryWrapper.eq(AttachmentDO::getIsMain, attachmentEntity.getIsMain());

        List<AttachmentDO> attachmentDOList = attachmentMapper.selectList(queryWrapper);
        List<AttachmentEntity> attachmentEntityList = AttachmentEntityToAttachmentDOConvertor.INSTANCE.dOToEntityList(attachmentDOList);
        attachmentEntityList.forEach(e ->{
            e.setName(e.getFileName());
            e.setUid(e.getFileId());
        });
        return attachmentEntityList;
    }

    @Override
    public AttachmentEntity findMainAttachmentByPaperId(String paperId) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("paperId", paperId);
        params.put("isMain", "1");
        List<AttachmentEntity> attachList = findAttachmentListByParams(params);
        if (attachList.size() > 0) {
            return attachList.get(0);
        }
        return new AttachmentEntity();
    }

    @Override
    public List<AttachmentEntity> findAttachmentListByParams(Map<String, Object> attachment) {
        QueryWrapper<AttachmentDO> queryWrapper = new QueryWrapper<>();
        if (attachment.containsKey("paperId")) {
            queryWrapper.eq("PAPER_ID", attachment.get("paperId").toString());
        }
        if (attachment.containsKey("isMain")) {
            queryWrapper.eq("IS_MAIN", attachment.get("isMain").toString());
        }
        List<AttachmentDO> attachmentDOList = attachmentMapper.selectList(queryWrapper);
        return AttachmentEntityToAttachmentDOConvertor.INSTANCE.dOToEntityList(attachmentDOList);
    }

    @Override
    public AttachmentEntity getAttachmentByPaperId(String paperId) {
        LambdaQueryWrapper<AttachmentDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AttachmentDO::getPaperId, paperId);
        wrapper.eq(AttachmentDO::getIsMain, ConstantSys.IS_MAIN_ONE);
        return AttachmentEntityToAttachmentDOConvertor.INSTANCE.dOToEntity(attachmentMapper.selectOne(wrapper));
    }

    @Override
    public List<AttachmentEntity> getAttachmentsByPaperIdList(List<String> paperIdList) {
        LambdaQueryWrapper<AttachmentDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(AttachmentDO::getPaperId, paperIdList);
        wrapper.eq(AttachmentDO::getIsMain, ConstantSys.IS_MAIN_ONE);
        return AttachmentEntityToAttachmentDOConvertor.INSTANCE.dOToEntityList(attachmentMapper.selectList(wrapper));
    }
}
