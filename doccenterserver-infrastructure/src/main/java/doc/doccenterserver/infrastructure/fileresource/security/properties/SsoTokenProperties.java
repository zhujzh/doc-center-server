package doc.doccenterserver.infrastructure.fileresource.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author wangxp
 * @description sso 认证配置
 * @date 2023/3/29 16:58
 */
@Data
@Component
@ConfigurationProperties(prefix = "token")
public class SsoTokenProperties {


    // 令牌自定义标识
    private String header;

    //协议密钥
    private String secret;

    //客户端id
    private String clientId;

    // 认证地址
    private String ssoApiUrl;

    // 过期时间
    private Integer expireTime;
}
