package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.domain.fileresource.document.model.GroupMemberEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.GroupMemberDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 自定义组成员Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface GroupMemberMapper extends BaseMapper<GroupMemberDO> {
    List<GroupMemberEntity> getGroupAuth(String groupId);

}
