package doc.doccenterserver.infrastructure.fileresource.message.enums;

public enum ArchiveOrgCodeEnum {

    SP("2043000156", "四平", "sp"),
    CZ("2043000153", "郴州", "cz"),
    WZ("2043000155", "吴忠", "wz"),
    ;
    private final String value;
    private final String name;
    private final String sign;


    ArchiveOrgCodeEnum(String value, String name, String sign) {
        this.value = value;
        this.name = name;
        this.sign = sign;
    }
    public String getValue() {
        return this.value;
    }

    public String getSign() {
        return this.sign;
    }

}
