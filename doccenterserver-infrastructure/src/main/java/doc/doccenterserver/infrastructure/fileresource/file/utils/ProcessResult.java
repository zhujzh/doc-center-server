package doc.doccenterserver.infrastructure.fileresource.file.utils;


public class ProcessResult {


    private String process;
    private String command;
    private String exception;
    private String error;
    private String exitValue;
    private String out;

    public ProcessResult() {

    }

    public ProcessResult (String process,
                                   String error, Exception ex) {
        setProcess(process);
        setException(ex.toString());
        setError(error);
        setExitValue("-1");
    }

    public ProcessResult(String error) {
        setError(error);
        setExitValue("-1");
    }

    public String getOut() {
        return out;
    }
    public void setOut(String out) {
        this.out = out;
    }
    public String getCommand() {
        return command;
    }
    public void setCommand(String command) {
        this.command = command;
    }
    public String getProcess() {
        return process;
    }
    public void setProcess(String process) {
        this.process = process;
    }
    public String getException() {
        return exception;
    }
    public void setException(String exception) {
        this.exception = exception;
    }
    public String getError() {
        return error;
    }
    public void setError(String error) {
        this.error = error;
    }
    public String getExitValue() {
        return exitValue;
    }
    public void setExitValue(String exitValue) {
        this.exitValue = exitValue;
    }

    public String buildLogMessage() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("{process: ");
        strBuilder.append(getProcess());
        strBuilder.append(",");
        strBuilder.append("command; ");
        strBuilder.append(getCommand());
        strBuilder.append(",");
        strBuilder.append("exitValue: ");
        strBuilder.append(getExitValue());


        if (getException() != null) {
            strBuilder.append(",");
            strBuilder.append("exception: ");
            strBuilder.append(getException());

        }
        if (getError() != null) {
            strBuilder.append(",");
            strBuilder.append("error: ");
            strBuilder.append(getError());
        }
        if (getOut() != null) {
            strBuilder.append(",");
            strBuilder.append("out: ");
            strBuilder.append(getOut());
        }
        strBuilder.append("}");
        return strBuilder.toString();
    }


    @Override
    public String toString() {
           return buildLogMessage();
    }
}
