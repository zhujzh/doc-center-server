package doc.doccenterserver.infrastructure.fileresource.document.repository;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ImageEntity;
import doc.doccenterserver.domain.fileresource.document.repository.ImageRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.ImageConvertor;
import doc.doccenterserver.infrastructure.fileresource.document.converter.ImageEntityToImageDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.message.utils.IdentifierGenerator;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.ImageMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ImageDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("ImageDORepository")
public class ImageRepositoryImpl implements ImageRepository {

    @Resource
    private ImageMapper imageMapper;

    private static final String ATTA_FILE_DOWLOAD_IP_URL = "http://192.168.1.170:8000/doc-cloud-file/file/download/";

    private static final String DOC_FILE_URL = "";

    @Override
    public Integer deleteImagesByPaperId(String paperId) {
        LambdaQueryWrapper<ImageDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ImageDO::getPaperId, paperId);
        return imageMapper.delete(wrapper);
    }

    @Override
    public Integer insertImage(ImageEntity imageEntity) {
        return imageMapper.insert(ImageEntityToImageDOConvertor.INSTANCE.entityToDO(imageEntity));
    }

    @Override
    public List<ImageEntity> getImageList(ImageEntity imageEntity) {
        LambdaQueryWrapper<ImageDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ImageDO::getPaperId, imageEntity.getPaperId());
        wrapper.orderByAsc();
        List<ImageDO> imageDOList = imageMapper.selectList(wrapper);
        List<ImageEntity> imageEntityList = ImageEntityToImageDOConvertor.INSTANCE.dOToEntityList(imageDOList);
        imageEntityList.forEach(e ->{
            e.setName(e.getImgName());
            e.setUid(e.getFileId());
        });
        return imageEntityList;
    }

    @Override
    public ImageEntity addInit() {
        return null;
    }

    @Override
    public ImageEntity editInit(String imgId) {
        return ImageConvertor.INSTANCE.dOToEntity(imageMapper.selectById(imgId));
    }

    @Override
    public ImageEntity save(ImageEntity imageEntity) {
        ImageDO imageDO = ImageConvertor.INSTANCE.entityToDO(imageEntity);
        String imgId = imageDO.getImgId();
        if (StringUtils.isBlank(imgId)) {
            imgId = IdentifierGenerator.createPrimaryKeySeq();
            imageEntity.setImgId(imgId);
            imageMapper.insert(imageDO);
        } else {
            imageMapper.updateById(imageDO);
        }
        return ImageConvertor.INSTANCE.dOToEntity(imageDO);
    }

    @Override
    public PageResult<List<ImageEntity>> getList(ImageEntity imageEntity) {
        LambdaQueryWrapper<ImageDO> queryWrapper = new LambdaQueryWrapper<>();
        Page<ImageDO> page = new Page<>(imageEntity.getPageNum(), imageEntity.getPageSize());
        IPage<ImageDO> pages = imageMapper.selectPage(page, queryWrapper);
        List<ImageDO> imageDOList = pages.getRecords();
        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(pages.getCurrent());
        baseDto.setPageSize(pages.getSize());
        baseDto.setTotal(pages.getTotal());
        baseDto.setTotalPage(pages.getPages());
        List<ImageEntity> imageEntityList = ImageConvertor.INSTANCE.dOToEntity(imageDOList);
        return new PageResult<>(imageEntityList, baseDto);
    }

    @Override
    public ImageEntity delete(String imgIds) {
        List<String> idList = StringUtil.strToList(imgIds, ConstantSys.SEPARATE_SEMICOLON);
        imageMapper.deleteBatchIds(idList);
        return new ImageEntity();
    }

    @Override
    public ImageEntity getDetail(String imgId) {
        return null;
    }

    @Override
    public ImageEntity downloadFile(String imgId, HttpServletRequest request, HttpServletResponse response) {
        ImageDO imageDO = imageMapper.selectById(imgId);
        if (null == imageDO) {
            String fileName = imageDO.getImgName();
            String uploadUrl = imageDO.getImgPath();
            try {
                // FileOperateUtil.downloadFile(request, response, uploadUrl, imageDO.getFileId(), fileName, null);
            } catch (Exception ex) {
                log.error("downloadFile===========imgId" + imgId, ex);
            }
        }
        return new ImageEntity();
    }

    @Override
    public ImageEntity showImageTailorPage(ImageEntity imageEntity) {
        ImageEntity image = new ImageEntity();
        Map<String, Object> result = Maps.newHashMap();
        if (StringUtils.isNotBlank(imageEntity.getImgId())) {
            // 有id，则根据id获取图片路径
            // ImageEntry img = imageService.findImageById(imageEntity.getImgId());
            ImageDO imageDO = imageMapper.selectById(imageEntity.getImgId());
            result.put("tailParams", imageDO.getTailParams());// 获取裁剪参数
        }
        String fileId = imageEntity.getFileId();
        // String imgPath = ATTA_FILE_DOWLOAD_IP_URL + fileId;

        String imgUrl = DOC_FILE_URL + fileId;
        // 获取图片的宽高大小信息
        if (StringUtils.isNotBlank(imageEntity.getTailParams())) {
            result.put("tailParams", imageEntity.getTailParams());// 以当前裁剪参数为准
        }
        result.put("fileId", fileId);
        result.put("imgPath", imgUrl);
        result.put("fromType", imageEntity.getFromType());

        image.setResult(result);
        image.setMinwidth(imageEntity.getMinwidth());
        image.setHeight(imageEntity.getHeight());
        return image;
    }

    @Override
    public ImageEntity getImageInfo(String imgId, HttpServletResponse response) {
        ImageEntity imageEntity = new ImageEntity();
        Map<String, Object> result = Maps.newHashMap();
        // 获取图片的宽高大小信息
        try {
            String downurl = ATTA_FILE_DOWLOAD_IP_URL;
            log.error("图片：" + downurl + imgId);
//            BufferedInputStream bis = FileUtil.getInputStreamByURL(response, downurl + imgId);
//            Map<String, Object> imgInfo = FileUtil.getImgInfo(bis);
//            imgInfo.put("fileUrl", DOC_FILE_URL + imgId);
//            result.putAll(imgInfo);

            imageEntity.setResult(result);
        } catch (Exception e) {
            log.error("getimageinfo=======获取图片信息失败", e);
        }
        return imageEntity;
    }

    @Override
    public ImageEntity cutImage(ImageEntity imageEntity) {
        Map<String, Object> result = Maps.newHashMap();
        ImageEntity image = new ImageEntity();
        try {
            /*String fileId = imageEntity.getFileId();
            int x = imageEntity.getX();
            int y = imageEntity.getY();
            int height = imageEntity.getHeight();
            int width = imageEntity.getWidth();
            String imgPath = ATTA_FILE_DOWLOAD_IP_URL + fileId;
            BufferedInputStream bis = FileUtil.getInputStreamByURL(imgPath);
            // 保存剪切的图片到服务器、剪切图片名称=原文件名_tailor.后缀
            Map<String, Object> map = FileUtil.saveTailorImageToBigData(bis, x, y, width, height, fileId, YUN_STORAGE_SPACCE);
            Joiner joiner = Joiner.on(":").skipNulls();
            // 用:号拼接裁剪参数
            result.put("tailParams", joiner.join(x, y, width, height));
            // 剪裁图片在大数据平台的fileId，保存为paper的picPath字段
            result.put("picPath", map.get("fileId"));
            image.setResult(result);*/
        } catch (Exception e) {
            log.error("cutImage===裁剪图片出错(/image/cutImage)============" + JSON.toJSONString(imageEntity), e);
        }
        return image;
    }

    @Override
    public List<ImageEntity> getImages(List<String> paperIds) {
        LambdaQueryWrapper<ImageDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(CollectionUtil.isNotEmpty(paperIds), ImageDO:: getPaperId, paperIds);
        wrapper.eq(ImageDO:: getIsMain, ConstantSys.IS_MAIN_ONE);
        List<ImageDO> imageDOList = imageMapper.selectList(wrapper);
        List<ImageEntity> imageEntity = ImageEntityToImageDOConvertor.INSTANCE.dOToEntityList(imageDOList);
        // imageEntity.setName(imageEntity.getImgName());
        return imageEntity;
    }

}
