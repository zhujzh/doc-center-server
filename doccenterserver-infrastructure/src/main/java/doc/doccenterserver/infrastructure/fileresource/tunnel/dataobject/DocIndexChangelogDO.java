package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建索引任务处理
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_doc_index_changelog`")
@NoArgsConstructor
public class DocIndexChangelogDO {

    @Field(name = "日志流水号")
    @TableId(type = IdType.INPUT, value = "log_id")
    private String logId;

    @Field(name = "文档ID")
    @TableField("paper_id")
    private String paperId;

    @Field(name = "信息归档操作类型")
    @TableField("operation")
    private String operation;

    @Field(name = "错误重试次数")
    @TableField("retry_times")
    private Integer retryTimes;

    @Field(name = "信息产生时间")
    @TableField("create_time")
    private Date createTime;

    public DocIndexChangelogDO(String logId, Date createTime,
                                  String operation, String paperId, Integer retryTimes) {
        super();
        this.setLogId ( logId);
        this.setCreateTime ( createTime);
        this.setOperation ( operation);
        this.setPaperId ( paperId);
        this.setRetryTimes ( retryTimes);
    }
}
