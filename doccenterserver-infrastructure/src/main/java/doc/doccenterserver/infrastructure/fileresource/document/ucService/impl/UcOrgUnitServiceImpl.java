package doc.doccenterserver.infrastructure.fileresource.document.ucService.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.bizworks.core.runtime.common.MultiResponse;
import com.alibaba.bizworks.core.runtime.common.SingleResponse;
import com.tobacco.mp.uc.client.api.OrgServiceAPI;
import com.tobacco.mp.uc.client.api.org.dto.OrgNodeUnitDTO;
import com.tobacco.mp.uc.client.api.org.dto.QueryParentOrgUnitsInOrgDTO;
import com.tobacco.mp.uc.client.api.org.req.ListOrgUnitByBizCodeRequest;
import com.tobacco.mp.uc.client.api.org.req.QueryParentOrgUnitsInOrgRequest;
import com.tobacco.mp.uc.kz.client.api.OrgExtServiceAPI;
import com.tobacco.mp.uc.kz.client.api.dto.ListOrgRootOrgUnitsResponse;
import com.tobacco.mp.uc.kz.client.api.dto.OrgUnitResponse;
import com.tobacco.mp.uc.kz.client.api.dto.QueryChildrenOrgUnitsInOrgResponse;
import com.tobacco.mp.uc.kz.client.api.req.GetOrgUnitByBizCodeRequest;
import com.tobacco.mp.uc.kz.client.api.req.ListOrgRootOrgUnitsRequest;
import com.tobacco.mp.uc.kz.client.api.req.PageQueryChildrenOrgUnitsInOrgRequest;
import com.tobacco.mp.uc.kz.client.api.req.QueryChildrenOrgUnitsInOrgRequest;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcOrgUnitService;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.constant.Constants;
import doc.doccenterserver.infrastructure.fileresource.security.properties.UcCenterProperties;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wangxp
 * @description 用户中心扩展-组织
 * @date 2023/6/13 16:51
 */
@Service
public class UcOrgUnitServiceImpl implements UcOrgUnitService {

    @Resource
    private UcCenterProperties ucCenterProperties;

    @Resource
    private OrgExtServiceAPI orgExtServiceAPI;


    @Resource
    private OrgServiceAPI orgServiceAPI;

    @Override
    public Collection<QueryChildrenOrgUnitsInOrgResponse> getOrgByParentId(String parentId) {
        QueryChildrenOrgUnitsInOrgRequest queryChildrenOrgUnitsInOrgRequest = new QueryChildrenOrgUnitsInOrgRequest();
        queryChildrenOrgUnitsInOrgRequest.setOrgBizCode(ucCenterProperties.getOrgBizCode());
        queryChildrenOrgUnitsInOrgRequest.setOrgUnitId(parentId);
        MultiResponse<QueryChildrenOrgUnitsInOrgResponse> response = orgExtServiceAPI.queryChildrenOrgUnitsInOrg(queryChildrenOrgUnitsInOrgRequest);
        if (ObjectUtil.isNotEmpty(response.getData()) && CollUtil.isNotEmpty(response.getData().getItems())) {
            // 筛选启用
            return response.getData().getItems().stream().filter(i->Constants.ORG_START.equals(i.getStatus())).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public OrgUnitResponse queryUcOrgNodeByBizCode(String bizCode) {
        GetOrgUnitByBizCodeRequest getOrgUnitByBizCodeRequest = new GetOrgUnitByBizCodeRequest();
        getOrgUnitByBizCodeRequest.setOrgBizCode(ucCenterProperties.getOrgBizCode());
        getOrgUnitByBizCodeRequest.setBizCode(bizCode);
        SingleResponse<OrgUnitResponse> response = orgExtServiceAPI.getOrgUnitByBizCode(getOrgUnitByBizCodeRequest);
        if(ObjectUtil.isNotEmpty(response.getData())){
            return response.getData();
        }
        return null;
    }


    @Override
    public Collection<ListOrgRootOrgUnitsResponse> queryOrgRootOrgUnits() {
        ListOrgRootOrgUnitsRequest listOrgRootOrgUnitsRequest = new ListOrgRootOrgUnitsRequest();
        listOrgRootOrgUnitsRequest.setOrgBizCode(ucCenterProperties.getOrgBizCode());
        MultiResponse<ListOrgRootOrgUnitsResponse> response = orgExtServiceAPI.listOrgRootOrgUnits(listOrgRootOrgUnitsRequest);
        if(ObjectUtil.isNotEmpty(response.getData()) && CollUtil.isNotEmpty(response.getData().getItems())){
            // 筛选启用
            return response.getData().getItems().stream().filter(i->Constants.ORG_START.equals(i.getStatus())).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public Collection<OrgNodeUnitDTO> queryOrgUnitByBizCode(List<String> bizCodes) {
        ListOrgUnitByBizCodeRequest listOrgUnitByBizCodeRequest = new ListOrgUnitByBizCodeRequest();
        listOrgUnitByBizCodeRequest.setBizCodes(bizCodes);
        listOrgUnitByBizCodeRequest.setOrgBizCode(ucCenterProperties.getOrgBizCode());
        MultiResponse<OrgNodeUnitDTO> response = orgServiceAPI.listOrgUnitByBizCode(listOrgUnitByBizCodeRequest);
        if(ObjectUtil.isNotEmpty(response.getData()) && CollUtil.isNotEmpty(response.getData().getItems())){
            return response.getData().getItems().stream().filter(OrgNodeUnitDTO::getStatus).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public Collection<QueryParentOrgUnitsInOrgDTO> queryOrgParentOrgUnits(String orgUnitIds) {
        QueryParentOrgUnitsInOrgRequest queryParentOrgUnitsInOrgRequest = new QueryParentOrgUnitsInOrgRequest();
        queryParentOrgUnitsInOrgRequest.setOrgBizCode(ucCenterProperties.getOrgBizCode());
        queryParentOrgUnitsInOrgRequest.setOrgUnitId(orgUnitIds);
        QueryParentOrgUnitsInOrgRequest.QueryConditionDTO queryConditionDTO = new QueryParentOrgUnitsInOrgRequest.QueryConditionDTO();
        queryConditionDTO.setIncludeSelf(Boolean.TRUE);
        queryParentOrgUnitsInOrgRequest.setQueryCondition(queryConditionDTO);
        MultiResponse<QueryParentOrgUnitsInOrgDTO> response =  orgServiceAPI.queryParentOrgUnitsInOrg(queryParentOrgUnitsInOrgRequest);
        if(ObjectUtil.isNotEmpty(response.getData()) && CollUtil.isNotEmpty(response.getData().getItems())){
            return response.getData().getItems();
        }
        return null;
    }
}
