package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 引用文档表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_d_paperref`")
@NoArgsConstructor
public class PaperrefDO {

    @Field(name = "新频道ID")
    @TableField("channel_nid")
    private String channelNid;

    @Field(name = "新频道名称")
    @TableField("channel_nname")
    private String channelNname;

    @Field(name = "原始频道ID")
    @TableField("channel_id")
    private String channelId;

    @Field(name = "文档编号")
    @TableField("paper_id")
    private String paperId;

    @Field(name = "引用用户ID")
    @TableField("ref_userid")
    private String refUserid;

    @Field(name = "引用时间")
    @TableField("ref_date")
    private Date refDate;

    @Field(name = "文档排序")
    @TableField("paperorder")
    private Date paperorder;

    @Field(name = "备注")
    @TableField("remark")
    private String remark;
}
