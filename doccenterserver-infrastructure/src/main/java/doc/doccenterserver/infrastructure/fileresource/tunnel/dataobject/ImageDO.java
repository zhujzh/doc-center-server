package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文档图片表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_cms_d_image`")
@NoArgsConstructor
public class ImageDO {

    @Field(name = "附件id")
    @TableField("img_id")
    private String imgId;

    @Field(name = "附件名称")
    @TableField("img_name")
    private String imgName;

    @Field(name = "描述")
    @TableField("img_desc")
    private String imgDesc;

    @Field(name = "所属文档id")
    @TableField("paper_id")
    private String paperId;

    @Field(name = "文件保存路径")
    @TableField("img_path")
    private String imgPath;

    @Field(name = "上传时间")
    @TableField("upload_date")
    private Date uploadDate;

    @Field(name = "是否为首页图片")
    @TableField("is_main")
    private String isMain;

    @Field(name = "排序号")
    @TableField("order_by")
    private Integer orderBy;

    @Field(name = "裁剪参数")
    @TableField("tail_params")
    private String tailParams;

    @Field(name = "大数据平台文件ID")
    @TableField("file_id")
    private String fileId;

    @Field(name = "租户id")
    @TableField("app_id")
    private String appId;

    @Field(name = "是否上传")
    @TableField("is_upload")
    private String isUpload;

    @Field(name = "大数据平台文件MD5")
    @TableField("file_md5")
    private String fileMd5;
}
