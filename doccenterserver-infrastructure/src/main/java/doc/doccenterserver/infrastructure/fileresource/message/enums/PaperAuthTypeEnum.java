package doc.doccenterserver.infrastructure.fileresource.message.enums;

public enum PaperAuthTypeEnum {

    OBJECT_TYPE_U("u", "人员"),
    OBJECT_TYPE_G("g", "部门"),
    OBJECT_TYPE_A("a", "自定义组"),
    OBJECT_TYPE_E("e", "扩展"),
    ;
    private final String value;
    private final String name;
    PaperAuthTypeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }
    public String getValue() {
        return this.value;
    }

}
