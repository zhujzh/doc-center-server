package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import doc.doccenterserver.domain.fileresource.document.model.User2roleEntity;
import doc.doccenterserver.domain.fileresource.document.repository.User2RoleRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.User2roleConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.User2roleMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.User2roleDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */

@Slf4j
@Component("User2RoleDORepository")
public class User2RoleRepositoryImpl implements User2RoleRepository {

    @Resource
    private User2roleMapper user2roleMapper;

    @Override
    public List<User2roleEntity> findAccountInfoListByUserId(String userId) {
        return user2roleMapper.findAccountInfoListByUserId(userId);
    }

    @Override
    public List<User2roleEntity> getUserByRoleId(String roleId) {
        LambdaQueryWrapper<User2roleDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User2roleDO::getRoleId, roleId);
        List<User2roleDO> user2roleDOList = user2roleMapper.selectList(wrapper);
        return User2roleConvertor.INSTANCE.dOToEntity(user2roleDOList);
    }

    @Override
    public Integer delete(String roleId) {
        LambdaQueryWrapper<User2roleDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User2roleDO::getRoleId, roleId);
        return user2roleMapper.delete(wrapper);
    }

    @Override
    public Integer insert(User2roleEntity user2role) {
        User2roleDO user2roleDO = User2roleConvertor.INSTANCE.entityToDO(user2role);
        return user2roleMapper.insert(user2roleDO);
    }
}
