package doc.doccenterserver.infrastructure.fileresource.message.repository;

import cn.hutool.json.JSONObject;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import doc.doccenterserver.domain.fileresource.document.model.InterfaceAccessLogEntity;
import doc.doccenterserver.domain.fileresource.document.repository.InterfaceAccessLogRepository;
import doc.doccenterserver.domain.fileresource.message.model.JobDetailLogEntity;
import doc.doccenterserver.domain.fileresource.message.repository.JobDetailRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.SendMqChangelogRepository;
import doc.doccenterserver.infrastructure.fileresource.message.enums.*;
import doc.doccenterserver.infrastructure.fileresource.message.mqs.KafkaReceive;
import doc.doccenterserver.infrastructure.fileresource.message.mqs.KafkaSend;
import doc.doccenterserver.infrastructure.fileresource.message.utils.DateUtil;
import doc.doccenterserver.infrastructure.fileresource.message.utils.IdentifierGenerator;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.*;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.rmi.ServerException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

/**
 * 消息下发
 */
@Slf4j
@Component("FILESendMqChangelogDORepository")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SendMqChangelogRepositoryImpl extends ServiceImpl<SendMqChangelogMapper, SendMqChangelogDO> implements SendMqChangelogRepository {
    private final JSONObject parameterMap;
    private final JobDetailRepository jobDetailRepository;
    private final JobLogRepository jobLogRepository;
    private final JobLogMapper jobLogMapper;
    private final ChannelMapper channelMapper;
    private final MeetingMapper meetingMapper;
    private final JobDetailLogMapper jobDetailLogMapper;
    private final AttachmentMapper attachmentMapper;
    private final ImageMapper imageMapper;
    private final PaperMapper paperMapper;
    private final PaperAuthMapper paperAuthMapper;
    private final KafkaSend kafkaSend;

    private final InterfaceAccessLogRepository interfaceAccessLogRepository;

    @Value("${xxl.job.admin.addresses}")
    private String adminAddresses;

    @Value("${xxl.job.executor.ip}")
    private String ip;

    @Override
    public void sendMq(){
        LambdaQueryWrapper<SendMqChangelogDO> lqw=new LambdaQueryWrapper<>();
        lqw.lt(SendMqChangelogDO::getRetryTimes,4);
        List<SendMqChangelogDO> docConvertChangeloglist = getBaseMapper().selectList(lqw);
        for (SendMqChangelogDO sendMqChangelog : docConvertChangeloglist) {
            InterfaceAccessLogEntity interfaceAccessLogEntity = new InterfaceAccessLogEntity();
            interfaceAccessLogEntity.setLogId(IdentifierGenerator.createPrimaryKeySeq());
            interfaceAccessLogEntity.setParams(JSON.toJSONString(sendMqChangelog));
            interfaceAccessLogEntity.setCallTime(new Date());
            interfaceAccessLogEntity.setRequestUrl(adminAddresses);
            interfaceAccessLogEntity.setRequestIp(ip);
            interfaceAccessLogEntity.setAppname("文档中心扩展");
            try {
                this.processData(sendMqChangelog);
                interfaceAccessLogEntity.setResultStatus("Y");
                interfaceAccessLogRepository.insert(interfaceAccessLogEntity);
            } catch (Exception e) {
                sendMqChangelog.setRetryTimes(sendMqChangelog.getRetryTimes() + 1);
                jobLogRepository.updateError(sendMqChangelog.getLogId(), e.getMessage(), JobDetailLogStepIdEnum.STEP_ID_7.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_FAILD.getValue(), sendMqChangelog.getRetryTimes());
                interfaceAccessLogEntity.setResultStatus("N");
                interfaceAccessLogEntity.setFailReason(e.getMessage());
                interfaceAccessLogRepository.insert(interfaceAccessLogEntity);
            }
        }
    }
    public void processData(SendMqChangelogDO sendMqChangelog) throws ServerException {
        String paperId = sendMqChangelog.getPaperId();
        String logId = sendMqChangelog.getLogId();
        String operation = sendMqChangelog.getOperation();
        //根据应用ID、归档组织、应用文档编号，去统一消息暂存日志表是否存在
        if(OperationTypeEnum.CREATE.getValue().equals(operation) && Boolean.TRUE.equals(jobLogRepository.checkMqSaveExist(paperId))){
            jobLogRepository.clearDate4Repeated(sendMqChangelog.getLogId(),JobDetailLogStepIdEnum.STEP_ID_7.getValue());
            return;
        }
        //判断是否已成功推送过ESB消息，如已成功推送过，则不做任何处理
        List<JobDetailLogDO> jobDetailLogEntryList = jobDetailLogMapper.selectList(new LambdaQueryWrapper<JobDetailLogDO>().eq(JobDetailLogDO::getLogId,logId).eq(JobDetailLogDO::getStepId,JobDetailLogStepIdEnum.STEP_ID_7.getValue()));
        if(jobDetailLogEntryList.isEmpty() || jobDetailLogEntryList.get(0).getStepStatus().intValue()==JobDetailLogStepStatusEnum.STEP_STATUS_SUCCESS.getValue()) {
            return;
        }
        //提交索引数据（标题、正文、附件ID）到大数据平台
        JobLogDO jobLog = jobLogMapper.selectById(logId);
        List<JobLogDO> jobloglist = jobLogMapper.selectList(new LambdaQueryWrapper<JobLogDO>()
                .eq(JobLogDO::getSourceAppId,jobLog.getSourceAppId())
                .eq(JobLogDO::getSourcePaperId,jobLog.getSourcePaperId())
                .eq(JobLogDO::getArchiveOrgCode,jobLog.getArchiveOrgCode()));
        String msgXml = senddata2mq(jobLog,paperId,jobloglist);
        //3.更新任务日志从表《发送消息》，任务状态为完成
        JobDetailLogEntity jobDetailLog=new JobDetailLogEntity();
        jobDetailLog.setLogId(logId);
        jobDetailLog.setStepId(JobDetailLogStepIdEnum.STEP_ID_7.getValue());
        jobDetailLog.setStepStatus(JobDetailLogStepStatusEnum.STEP_STATUS_SUCCESS.getValue());
        jobDetailLog.setContent(msgXml);
        jobDetailRepository.updateByStepId(jobDetailLog);
        //4.更新任务主表，状态为已完成
        jobLogRepository.clearChangeLog(logId);
        //5.删除发送消息任务changlog表
        this.removeById(logId);
    }
    private String senddata2mq(JobLogDO jbolog,String paperId,List<JobLogDO> jobloglist){
        int recordCount = jobloglist.size();
        if(jbolog.getOperation().equals(OperationTypeEnum.CREATE.getValue()) ){
            recordCount = (recordCount + 1)/2;
        }
        String url = parameterMap.getStr("ECM_VIEWURL_MOBILEPORTAL") + paperId+"?r="+recordCount;
        String msgXml = generateDbInfoXml( paperId,  parameterMap.getStr("ECM_2DB_SERVICECODE"),  parameterMap.getStr("ECM_LOCATION"),  url,  jbolog.getOperation(),  recordCount+"");
        //下发消息内容\
        kafkaSend.send(TopicsEnum.ZB.getValue(),msgXml);
        return msgXml;
    }
    /**
     * 获取写入的统一消息体
     */
    public String generateDbInfoXml(String paperId, String serviceCode, String sender, String url, String operation, String recordCount){
        StringBuilder buffer = new StringBuilder();
        //流水号
        long changLogId = System.currentTimeMillis();
        //当前文档的基本信息
        PaperDO paper = paperMapper.selectById(paperId);
        ChannelDO channel = channelMapper.selectById(paper.getClassId());
        //当前文档对应的category
        String category = channel.getMemo();
        //当前文档所属频道名称
        String channelName = "无";
        if(channel != null){
            channelName = channel.getName();
        }
        channelName = "[" + channelName + "]";
        //内容
        //会议起始时间
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        String meetingstarttime = "";
        MeetingDO meetingEntry = meetingMapper.selectById(paperId);
        if(meetingEntry!=null && meetingEntry.getStartTime()!=null){
//            meetingstarttime = DateUtil.getDateFormat(meetingEntry.getStartTime(), "yyyy-MM-dd HH:mm:ss");
            meetingstarttime = df.format(meetingEntry.getStartTime());

        }
        String content = "<meetingstarttime>"+meetingstarttime+"</meetingstarttime>";
        //会议结束时间
        String meetingendtime = "";//(paper.getMeet_endtime()==null?"":paper.getMeet_endtime());
        if(meetingEntry!=null && meetingEntry.getEndTime()!=null){
//            meetingendtime = DateUtil.getDateFormat(meetingEntry.getEndTime(), "yyyy-MM-dd HH:mm:ss");
            meetingendtime = df.format(meetingEntry.getEndTime());
        }
        content += "<meetingendtime>"+meetingendtime+"</meetingendtime>";
        String address = "";
        if(meetingEntry!=null){
            address = StringUtils.isNoneBlank(meetingEntry.getAddress())?meetingEntry.getAddress():"";
        }
        content += "<meetingplace><![CDATA["+address+"]]</meetingplace>";
        //扩展节点内容
        String addtion ="<element id=\"meetingstarttime\">"+meetingstarttime+"</element>";
        //会议结束时间
        addtion += "<element id=\"meetingendtime\">"+meetingendtime+"</element>";
        //会议地址
        addtion += "<element id=\"meetingplace\"><![CDATA["+address+"]]></element>";
        //附属参数
        //<element id='attributelable'>bodyfile,attachment,bodyimage,receipt </element>
        //bodyfile 正文有文件；attachment 有附件文件；
        //bodyimage 正文有图片；receipt 需要回执； 多个值用逗号隔开
        addtion += "<element id=\"attributelable\">"+getAttachment(paper)+"</element>";
        //统一消息的唯一标识编号
        String xmlID = sender + "_" + paperId + "_" + recordCount;
        if(OperationTypeEnum.RETRIEVE.getValue().equalsIgnoreCase(operation)){//如果是删除消息
            if(null!=paper.getReserved1() && !"".equals(paper.getReserved1())){
                xmlID = paper.getReserved1();//从数据库获取创建消息的标识编号
            }else{//如果暂时没有存储统一消息标识的数据
                int record = (Integer.parseInt(recordCount) + 1)/2-1;
                xmlID = sender + "_" + paperId + "_" + record;
            }
        }else{//如果是创建消息，保存这条创建消息的标识编号
            PaperDO paperDO = new PaperDO();
            paperDO.setReserved1(xmlID);
            paperDO.setPaperId(paper.getPaperId());
            paperMapper.updateById(paperDO);
        }
        buffer.append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
        if(OperationTypeEnum.RETRIEVE.getValue().equals(operation)){
            buffer.append("<root classify=\"3\">");
        }else{
            buffer.append("<root classify=\"1\">");
        }
        //消息头
        buffer.append("<head>");
        buffer.append("<servicecode>").append(serviceCode).append("</servicecode>");
        buffer.append("<sender>").append(sender).append("</sender>");
        buffer.append("<bizid>0001</bizid>");
        buffer.append("<serialnumber>").append(changLogId).append("</serialnumber>");
        buffer.append("</head>");
        //消息体（通用）
        buffer.append("<format>");
        if(OperationTypeEnum.RETRIEVE.getValue().equals(operation)){
            buffer.append("<from>").append(sender).append("</from>");
            buffer.append("<id>").append(xmlID).append("</id>");
            buffer.append("<category>").append(category).append("</category>");
            buffer.append("<owner />");
        }else{
            buffer.append("<ip>").append(parameterMap.getStr("ECM_IP")).append("</ip>");
            buffer.append("<from>").append(sender).append("</from>");
            buffer.append("<id>").append(xmlID).append("</id>");
            buffer.append("<category>").append(category).append("</category>");
            buffer.append("<owner><![CDATA[").append(getPaperOwner(paper)).append("]]></owner>");
            buffer.append("<title><![CDATA[").append(channelName).append(paper.getPaperName()).append("]]></title>");
            String description = paper.getPaperName()==null?"":paper.getPaperName();
            buffer.append("<abstract><![CDATA[").append(description).append("]]></abstract>");
            buffer.append("<content><![CDATA[").append(content).append("]]></content>");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String inputTime = paper.getInputTime()==null? LocalDateTime.now().format(formatter):paper.getInputTime().format(formatter);
            buffer.append("<url><![CDATA[").append(url).append("]]></url>");
            buffer.append("<time>").append(inputTime.replace(".0", "")).append("</time>");
            buffer.append("<org />");
            buffer.append("<permission>").append(getPaperPermission(paper)).append("</permission>");
            buffer.append("<resourceid>").append(paperId).append("</resourceid>");
            buffer.append("<channelid>").append(paper.getClassId()).append("</channelid>");
            buffer.append("<addition>").append(addtion).append("</addition>");
        }
        buffer.append("</format>");
        buffer.append("</root>");
        return buffer.toString();
    }

    public String getAttachment(PaperDO paper){
        Integer paperType = paper.getPaperType();
        String paperId = paper.getPaperId();
        StringBuilder buffer = new StringBuilder();
        //文档附件列表
        List<AttachmentDO> attList =  attachmentMapper.selectList(new LambdaQueryWrapper<AttachmentDO>()
                .eq(AttachmentDO::getPaperId,paperId));
        int attSize = attList.size();
        //如果是附件文档
        if(paperType==1){
            if(attSize == 1){
                buffer.append("bodyfile");
            }
            else if(attSize > 1){
                buffer.append("bodyfile,attachment");
            }
        }else{
            if(attSize > 0){
                buffer.append("attachment");
            }
        }
        //图片附件列表
        List<ImageDO> imgList =  imageMapper.selectList(new LambdaQueryWrapper<ImageDO>()
                .eq(ImageDO::getPaperId,paperId));
        if(!imgList.isEmpty()){
            String result = buffer.toString();
            if(result.length() > 0){
                buffer.append(",bodyimage");
            }else{
                buffer.append("bodyimage");
            }
        }
        return buffer.toString();
    }

    public String getPaperOwner(PaperDO paper){
        StringBuilder ldap = new StringBuilder();
        List<PaperAuthDO> pauthlist = paperAuthMapper.selectList(new LambdaQueryWrapper<PaperAuthDO>().eq(PaperAuthDO::getPaperId,paper.getPaperId()));
        if(pauthlist.isEmpty()){
            String isPublic = paper.getIsPublic();
            if("Y".equals(isPublic)){
                ldap = new StringBuilder("cn=43,cn=0,dc=hngytobacco,dc=com");
            }else {
                ldap = new StringBuilder(paper.getArchiveOrgCode());
            }
        }else{

            for(PaperAuthDO paperAuthEntry:pauthlist ){
                if(PaperAuthTypeEnum.OBJECT_TYPE_U.getValue().equals(paperAuthEntry.getObjectType())){//用户
                    ldap.append(paperAuthEntry.getAuthObject()).append("§");
                }else  if(PaperAuthTypeEnum.OBJECT_TYPE_G.getValue().equals(paperAuthEntry.getObjectType())){//公司 组织
                    ldap.append("cn=").append(paperAuthEntry.getAuthObject()).append(",cn=43,cn=0,DC=HNGYTOBACCO,DC=COM").append("§");
                }else  if(PaperAuthTypeEnum.OBJECT_TYPE_A.getValue().equals(paperAuthEntry.getObjectType())){//自定义组
                    ldap.append("a:").append(paperAuthEntry.getAuthObject()).append("§");
                }
            }
            if(ldap.toString().endsWith("§")){
                ldap = new StringBuilder(ldap.substring(0, ldap.length() - 1));
            }
        }
        return ldap.toString();
    }
    public String getPaperPermission(PaperDO paper) {
        StringBuilder ldap = new StringBuilder();
        List<PaperAuthDO> pauthlist = paperAuthMapper.selectList(new LambdaQueryWrapper<PaperAuthDO>().eq(PaperAuthDO::getPaperId,paper.getPaperId()));
        if(pauthlist.isEmpty()){
            String isPublic = paper.getIsPublic();
            if("Y".equals(isPublic)){
                ldap = new StringBuilder("all");
            }else {
                ldap = new StringBuilder("g:" + paper.getArchiveOrgCode() + ";");
            }
        }else{
            for(PaperAuthDO paperAuthEntry:pauthlist ){
                if(PaperAuthTypeEnum.OBJECT_TYPE_U.getValue().equals(paperAuthEntry.getObjectType())){//用户
                    ldap.append("u:").append(paperAuthEntry.getAuthObject()).append(";");
                }else  if(PaperAuthTypeEnum.OBJECT_TYPE_G.getValue().equals(paperAuthEntry.getObjectType())){//公司 组织
                    ldap.append("g:").append(paperAuthEntry.getAuthObject()).append(";");
                }else  if(PaperAuthTypeEnum.OBJECT_TYPE_A.getValue().equals(paperAuthEntry.getObjectType())){//自定义组
                    ldap.append("a:").append(paperAuthEntry.getAuthObject()).append(";");
                }
            }
        }
        return ldap.toString();
    }
}
