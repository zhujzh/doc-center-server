package doc.doccenterserver.infrastructure.fileresource.es;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 处理日期时间的工具类
 */
public class UtilDate {

	private static String pattern = "yyyy-MM-dd HH:mm:ss";

	/**
	 * @Description: 默认采用 yyyy-MM-dd格式
	 * @param
	 * @return String
	 */
	public static String dateToString(Date date) {

		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return formatter.format(date);
	}

	public static String dateToString(Date date, String pattern) {

		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return formatter.format(date);
	}

	/**
	 * 将短时间格式字符串转换为时间 yyyy-MM-dd
	 *
	 * @param strDate
	 * @return
	 */
	public static Date strToDate(String strDate) {
		return strToDate(strDate, pattern);
	}

	/**
	 * 给定字符串和字符串格式转换为时间
	 *
	 * @param strDate
	 * @param format
	 * @return
	 */
	public static Date strToDate(String strDate, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	public static String getWeekOfDate(Date date) {
		String[] weekDaysName = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五",
				"星期六" };
		String[] weekDaysCode = { "0", "1", "2", "3", "4", "5", "6" };
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int intWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		return weekDaysName[intWeek];
	}

	public static String getNowDate(){

		Date date = new Date();

		String now = dateToString(date, "yyyy年MM月dd日") + " " + getWeekOfDate(date);

		return now;
	}

	public static Date getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		Date currDate = cal.getTime();
		return currDate;
	}

	public static void main(String[] args) {
		Date now = new Date();

		System.out.println(getNowDate());
	}

}
