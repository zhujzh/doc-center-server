package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.ImageEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ImageDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface ImageEntityToImageDOConvertor {

    ImageEntityToImageDOConvertor INSTANCE = Mappers.getMapper(ImageEntityToImageDOConvertor.class);

    @Mappings({})
    ImageDO entityToDO(ImageEntity imageEntity);

    @Mappings({})
    ImageEntity dOToEntity(ImageDO imageDO);

    @Mappings({})
    List<ImageEntity> dOToEntityList(List<ImageDO> imageDOList);

    @Mappings({})
    List<ImageDO> entityToDOList(List<ImageEntity> imageEntityList);

}
