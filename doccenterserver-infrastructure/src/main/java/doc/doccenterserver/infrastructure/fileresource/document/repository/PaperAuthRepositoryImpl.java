package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import doc.doccenterserver.domain.fileresource.document.model.PaperAuthEntity;
import doc.doccenterserver.domain.fileresource.document.repository.PaperAuthRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.PaperAuthEntityToPaperAuthDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.PaperAuthMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PaperAuthDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("PaperAuthDORepository")
public class PaperAuthRepositoryImpl implements PaperAuthRepository {

    @Resource
    private PaperAuthMapper paperAuthMapper;

    @Override
    public Integer deletePaperAuthById(String paperId) {
        LambdaQueryWrapper<PaperAuthDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(PaperAuthDO::getPaperId,paperId);
        return  paperAuthMapper.delete(queryWrapper);
    }

    @Override
    public Integer insertPaperAuth(PaperAuthEntity paperAuthEntity) {
        return paperAuthMapper.insert(PaperAuthEntityToPaperAuthDOConvertor.INSTANCE.entityToDO(paperAuthEntity));
    }

    @Override
    public List<PaperAuthEntity> getPaperAuthList(String paperId) {
        LambdaQueryWrapper<PaperAuthDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(PaperAuthDO::getPaperId, paperId);
        List<PaperAuthDO> paperAuthDOList = paperAuthMapper.selectList(queryWrapper);
        return PaperAuthEntityToPaperAuthDOConvertor.INSTANCE.dOToEntityList(paperAuthDOList);
    }

    @Override
    public Boolean getPaperAuth(String userId, String paperId) {
        LambdaQueryWrapper<PaperAuthDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(PaperAuthDO::getAuthObject, userId);
        queryWrapper.eq(PaperAuthDO::getPaperId, paperId);
        Integer num = paperAuthMapper.selectCount(queryWrapper);
        return num > 0;
    }
}
