package doc.doccenterserver.infrastructure.fileresource.es.bean;

import org.springframework.context.annotation.Configuration;

/**
 * <p>搜索引擎：SearchChannelBean </p>
 * <p>文件描述: 用于搜索引擎的频道信息</p>
 * <p>版权所有: blueland(C)2010</p>
 * <p>公   司: blueland</p>
 * <p>内容摘要: 用于搜索引擎的频道信息</p>
 * <p>其他说明: 对应附件表T_CMS_S_CHANNEL</p>
 * <p>完成日期：2010年11月17日</p>
 * @author  汪正中
 */

@Configuration
public class SearchChannelBean {
	/**
	 * 序号(序列号生成器SEQ_CMS_S_CHANNEL生成)
	 */
	private String content_seq;
	/**
	 * 分类编号
	 */
	private String class_id;
	/**
	 * 分类父编号
	 */
	private String parent_id;
	/**
	 * 显示名称
	 */
	private String display_name;
	/**
	 * 显示序号
	 */
	private String order_num;
	/**
	 * 标题模板
	 */
	private String header_template;
	/**
	 * 内容模板
	 */
	private String content_template;
	/**
	 * 页脚模板
	 */
	private String footer_template;
	/**
	 * 状态（A 新增；M 修改； D 删除）
	 */
	private String status;

	public String getContent_seq() {
		return content_seq;
	}

	public void setContent_seq(String contentSeq) {
		content_seq = contentSeq;
	}

	public String getClass_id() {
		return class_id;
	}

	public void setClass_id(String classId) {
		class_id = classId;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parentId) {
		parent_id = parentId;
	}

	public String getDisplay_name() {
		return display_name;
	}

	public void setDisplay_name(String displayName) {
		display_name = displayName;
	}

	public String getOrder_num() {
		return order_num;
	}

	public void setOrder_num(String orderNum) {
		order_num = orderNum;
	}

	public String getHeader_template() {
		return header_template;
	}

	public void setHeader_template(String headerTemplate) {
		header_template = headerTemplate;
	}

	public String getContent_template() {
		return content_template;
	}

	public void setContent_template(String contentTemplate) {
		content_template = contentTemplate;
	}

	public String getFooter_template() {
		return footer_template;
	}

	public void setFooter_template(String footerTemplate) {
		footer_template = footerTemplate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
