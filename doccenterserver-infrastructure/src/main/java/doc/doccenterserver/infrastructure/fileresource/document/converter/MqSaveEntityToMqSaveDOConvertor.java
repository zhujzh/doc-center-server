package doc.doccenterserver.infrastructure.fileresource.document.converter;

import doc.doccenterserver.domain.fileresource.document.model.MqSaveEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.MqSaveDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author Tangcancan
 */
@Mapper
public interface MqSaveEntityToMqSaveDOConvertor {

    MqSaveEntityToMqSaveDOConvertor INSTANCE = Mappers.getMapper(MqSaveEntityToMqSaveDOConvertor.class);

    @Mappings({})
    MqSaveDO entityToDO(MqSaveEntity mqSaveEntity);

    @Mappings({})
    MqSaveEntity dOToEntity(MqSaveDO mqSaveDO);

    @Mappings({})
    List<MqSaveEntity> dOToEntityList(List<MqSaveDO> mqSaveDOList);

    @Mappings({})
    List<MqSaveDO> entityToDOList(List<MqSaveEntity> mqSaveEntityList);
}
