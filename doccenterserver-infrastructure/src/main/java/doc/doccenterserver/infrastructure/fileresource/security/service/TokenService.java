package doc.doccenterserver.infrastructure.fileresource.security.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import doc.doccenterserver.domain.fileresource.config.Constants;
import doc.doccenterserver.domain.fileresource.exception.BusinessException;
import doc.doccenterserver.domain.fileresource.exception.ExceptionEnum;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.redis.RedisCache;
import doc.doccenterserver.infrastructure.fileresource.security.model.LoginUser;
import doc.doccenterserver.infrastructure.fileresource.security.model.SsoModel;
import doc.doccenterserver.infrastructure.fileresource.security.properties.SsoTokenProperties;
import doc.doccenterserver.infrastructure.fileresource.sso.util.SsoUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * @author wangxp
 * @description TokenService
 * @date 2023/3/29 9:15
 */
@Component
@Slf4j
public class TokenService {


    @Resource
    private SsoTokenProperties ssoTokenProperties;


    private static final String TOKEN_PREFIX = "DcToken ";


    // refreshToken 过期时间 30 天
    private static final Integer REFRESH_TOKEN_EXPIRATION = 30 * 24 * 60;


    @Resource
    private RedisCache redisCache;

    @Resource
    private HttpServletRequest request;

    /**
     * 获取用户身份信息
     *
     * @return 用户信息
     */
    public LoginUser getLoginUser(HttpServletRequest request) {
        // 获取请求携带的令牌
        String token = getToken(request);
        if (StrUtil.isNotEmpty(token)) {
            //存在token
            //通过token去redis拿用户信息
            String userKey = Constants.LOGIN_USER_KEY + token;
            return redisCache.getCacheObject(userKey);
        }
        return null;
    }

    /**
     * 获取用户身份信息 - v2
     * 通过在当前线程内获取请求 request, 之后再获取用户
     * by gj, 2023/5/12
     * @return 用户信息
     */
    public LoginUser getLoginUserV2() {
        return getLoginUser(request);
    }

    /**
     * @param ssoModel  认证信息
     * @param userModel 用户信息
     * @description 存储信息
     * @author wangxp
     * @date 2023/3/30 9:42
     */
    public void saveCache(SsoModel ssoModel, UserModel userModel) {
        String userKey = getUserInfoKey(ssoModel.getAccess_token());
        String refreshKey = getRefreshKey(ssoModel.getAccess_token());
        //存储
        LoginUser loginUser = new LoginUser();
        loginUser.setUserModel(userModel);
        loginUser.setToken(ssoModel.getAccess_token());
        loginUser.setLoginTime(System.currentTimeMillis());
        loginUser.setExpireTime(loginUser.getLoginTime() + ssoTokenProperties.getExpireTime() * 1000);
        // 存储 用户信息
        redisCache.setCacheObject(userKey, loginUser, ssoTokenProperties.getExpireTime(), TimeUnit.SECONDS);
        // 存储 refreshToken
        redisCache.setCacheObject(refreshKey, ssoModel.getRefresh_token(), REFRESH_TOKEN_EXPIRATION, TimeUnit.MINUTES);

    }

    /**
     * 设置用户身份信息
     */
    public void setLoginUser(LoginUser loginUser) {
        if (ObjectUtil.isNotNull(loginUser) && StrUtil.isNotEmpty(loginUser.getToken())) {
            refreshToken(loginUser);
        }
    }

    /**
     * 删除用户身份信息
     */
    public void delLoginUser(String token) {
        if (StrUtil.isNotEmpty(token)) {
            String userKey = getUserInfoKey(token);
            redisCache.deleteObject(userKey);
        }
    }


    /**
     * 验证令牌有效期，相差不足20分钟，自动刷新缓存
     *
     * @param loginUser
     * @return 令牌
     */
    public void verifyToken(LoginUser loginUser) {
        long expireTime = loginUser.getExpireTime();
        long currentTime = System.currentTimeMillis();
        if (expireTime - currentTime <= 0) {
            refreshToken(loginUser);
        }
    }

    /**
     * 刷新令牌有效期
     *
     * @param loginUser 登录信息
     */
    public void refreshToken(LoginUser loginUser) {
        String refreshKey = getRefreshKey(loginUser.getToken());
        if (StrUtil.isEmpty(refreshKey)) {
            throw new BusinessException(ExceptionEnum.NOT_FOUND.getCode(), ExceptionEnum.NOT_FOUND.getMsg());
        }
        //调用单点登录 刷新令牌
        loginUser.setLoginTime(System.currentTimeMillis());
        loginUser.setExpireTime(loginUser.getLoginTime() + ssoTokenProperties.getExpireTime() * 1000);
        // 根据uuid将loginUser缓存
        String userKey = getUserInfoKey(loginUser.getToken());
        redisCache.setCacheObject(userKey, loginUser, ssoTokenProperties.getExpireTime(), TimeUnit.SECONDS);
        String refreshToken = redisCache.getCacheObject(refreshKey);
        SsoModel ssoModel = SsoUtils.refreshToken(refreshToken);
        if (ObjectUtil.isEmpty(ssoModel)) {
            throw new BusinessException(ExceptionEnum.NOT_FOUND.getCode(), ExceptionEnum.NOT_FOUND.getMsg());
        }
    }


    /**
     * 获取请求token
     *
     * @param request
     * @return token
     */
    private String getToken(HttpServletRequest request) {
        String token = request.getHeader(ssoTokenProperties.getHeader());
        if (StrUtil.isNotEmpty(token) && token.startsWith(TOKEN_PREFIX)) {
            token = token.replace(TOKEN_PREFIX, "");
        }
        return token;
    }

    private String getUserInfoKey(String token) {
        return Constants.LOGIN_USER_KEY + token;
    }

    private String getRefreshKey(String token) {
        return Constants.LOGIN_REFRESH_KEY + token;
    }

    /**
     * @param request
     * @description 退出登录
     * @author wangxp
     * @date 2023/4/27 14:24
     */
    public void loginOut(HttpServletRequest request){
        String token = getToken(request);
        String userInfoKey =getUserInfoKey(token);
        String refreshKey = getRefreshKey(token);
        // 删除用户消息
        redisCache.deleteObject(userInfoKey);
        // 删除refreshToken
        redisCache.deleteObject(refreshKey);

    }

}
