package doc.doccenterserver.infrastructure.fileresource.message.enums;

public enum JobLogStatusEnum {
    STATUS_Y("Y", "完成"),
    STATUS_N("N", "处理中"),
    STATUS_Q("Q", "排队"),
    STATUS_E("E", "异常"),
    STATUS_IGNOR("I", "忽略")
    ;
    private final String value;
    private final String name;
    JobLogStatusEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }
    public String getValue() {
        return this.value;
    }

}
