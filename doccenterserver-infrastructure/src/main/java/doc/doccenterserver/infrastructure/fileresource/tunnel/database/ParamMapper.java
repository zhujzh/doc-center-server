package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ParameterDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ch
 * @date 2023/4/11 14:27
 */
@Mapper
public interface ParamMapper  extends BaseMapper<ParameterDO> {
}
