package doc.doccenterserver.infrastructure.fileresource.message.job;

import com.xxl.job.core.handler.annotation.XxlJob;
import doc.doccenterserver.domain.fileresource.message.repository.ArchiveChangelogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息归档
 */
@Component
@Slf4j
public class MsgArchiveJob {

    @Autowired
    private ArchiveChangelogRepository archiveChangelogRepository;
    @XxlJob("msgArchive")
    public void  msgArchive(){
        archiveChangelogRepository.msgArchive();
        log.info("调用了");
    }
}
