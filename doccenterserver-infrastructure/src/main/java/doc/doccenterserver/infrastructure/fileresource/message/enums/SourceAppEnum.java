package doc.doccenterserver.infrastructure.fileresource.message.enums;

public enum SourceAppEnum {

    OA("OA", "老OA"),
    ZYOA("ZYOA", "新OA"),
    ZYSTDD("ZYSTDD", "集团标准化"),
    ;
    private final String value;
    private final String name;
    SourceAppEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }
    public String getValue() {
        return this.value;
    }

}
