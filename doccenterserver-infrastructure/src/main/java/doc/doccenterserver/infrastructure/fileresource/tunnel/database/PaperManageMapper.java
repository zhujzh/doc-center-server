package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PaperDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 16:15
 */
@Mapper
public interface PaperManageMapper extends BaseMapper<PaperDO> {

    List<Map<String, Object>> queryPaperAuthObjects(String paperId);
}
