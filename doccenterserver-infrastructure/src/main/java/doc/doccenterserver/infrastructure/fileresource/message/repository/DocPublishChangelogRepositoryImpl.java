package doc.doccenterserver.infrastructure.fileresource.message.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import doc.doccenterserver.domain.fileresource.message.repository.DocPublishChangelogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobDetailRepository;
import doc.doccenterserver.domain.fileresource.message.repository.JobLogRepository;
import doc.doccenterserver.infrastructure.fileresource.message.enums.*;
import doc.doccenterserver.infrastructure.fileresource.message.utils.DateUtil;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.*;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.rmi.ServerException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * 文档发布
 */
@Slf4j
@Component("FILEDocPublishChangelogDORepository")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DocPublishChangelogRepositoryImpl extends ServiceImpl<DocPublishChangelogMapper, DocPublishChangelogDO> implements DocPublishChangelogRepository {
    private final JobDetailRepository jobDetailRepository;
    private final JobLogRepository jobLogRepository;
    private final PaperMapper paperMapper;
    private final DocIndexChangelogMapper docIndexChangelogMapper;
    private final SendMqChangelogMapper sendMqChangelogMapper;

    @Override
    public void docPublish() {
        LambdaQueryWrapper<DocPublishChangelogDO> lqw = new LambdaQueryWrapper<>();
        lqw.lt(DocPublishChangelogDO::getRetryTimes, 4);
        List<DocPublishChangelogDO> docConvertChangeloglist = getBaseMapper().selectList(lqw);
        for (DocPublishChangelogDO docPublishChangelog : docConvertChangeloglist) {
            try {
                this.processData(docPublishChangelog);
            } catch (Exception e) {
                docPublishChangelog.setRetryTimes(docPublishChangelog.getRetryTimes() + 1);
                jobLogRepository.updateError(docPublishChangelog.getLogId(), e.getMessage(), JobDetailLogStepIdEnum.STEP_ID_5.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_FAILD.getValue(), docPublishChangelog.getRetryTimes());
            }
        }
    }

    public void processData(DocPublishChangelogDO docPublishChangelog) throws ServerException {
        String logId = docPublishChangelog.getLogId();
        String paperId = docPublishChangelog.getPaperId();
        String operation = docPublishChangelog.getOperation();
        PaperDO oldPaper = paperMapper.selectById(paperId);

        //根据应用ID、归档组织、应用文档编号，去统一消息暂存日志表 查询是否存在 比当前消息后入库的消息
        if (OperationTypeEnum.CREATE.getValue().equals(operation) && Boolean.TRUE.equals(jobLogRepository.checkMqSaveExist(paperId))) {
            //当前消息已过期，执行忽略操作
            jobLogRepository.clearDate4Repeated(docPublishChangelog.getLogId(), JobDetailLogStepIdEnum.STEP_ID_5.getValue());
            return;
        }
        PaperDO paper = new PaperDO();
        paper.setPaperId(paperId);
        if (OperationTypeEnum.CREATE.getValue().equals(operation)) {
            paper.setPublicFlag(PublicFlagEnum.PAPER_PUBLIC_FLAG_ISSUE.getValue());
            paper.setIssueDate(LocalDateTime.now());
            Date orderDate = DateUtil.parseDateFormat("1900", "yyyy");
            paper.setReserved8(0);
            paper.setPaperOrder(orderDate);
        } else {
            paper.setPublicFlag(PublicFlagEnum.PAPER_PUBLIC_FLAG_CANCEL.getValue());
        }
        paperMapper.updateById(paper);
        //4.更新任务日志从表《文档发布》任务状态为完成
        jobDetailRepository.updateStatus(logId, JobDetailLogStepIdEnum.STEP_ID_5.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_SUCCESS.getValue());

        //5.插入任务日志从表《创建索引》，任务状态为未完成
        jobDetailRepository.insert(logId, null, null, JobDetailLogStepIdEnum.STEP_ID_6.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_HANDLING.getValue());

        //6.写入创建索引任务change表
        DocIndexChangelogDO docIndexChangelog = new DocIndexChangelogDO(logId, new Date(), operation, paperId, 0);
        docIndexChangelogMapper.insert(docIndexChangelog);
        //7.写入发送消息任务change表
        //发送统一消息
        if (isSend(oldPaper)) {
            //插入任务日志从表《发送消息》，任务状态为未完成
            jobDetailRepository.insert(logId, null, null, JobDetailLogStepIdEnum.STEP_ID_7.getValue(), JobDetailLogStepStatusEnum.STEP_STATUS_HANDLING.getValue());
            SendMqChangelogDO sendMqChangelog = new SendMqChangelogDO(logId, new Date(), operation, paperId, 0);
            sendMqChangelogMapper.insert(sendMqChangelog);
        }
        //7.删除文档发布任务changlog表
        this.removeById(logId);
        //更新任务主表，状态为已完成
        jobLogRepository.clearChangeLog(logId);
    }

    /**
     * 是否发消息
     */
    public boolean isSend(PaperDO oldPaper) {
        String appid = oldPaper.getAppId();
        String archiveOrgCode = oldPaper.getArchiveOrgCode();
        boolean isSend = true;
        if ("OA".equals(appid) || "ZYOA".equals(appid)) {
            //只在四平 吴忠 2个地方不发统一消息
            if (ArchiveOrgCodeEnum.SP.getValue().equals(archiveOrgCode) || ArchiveOrgCodeEnum.WZ.getValue().equals(archiveOrgCode)) {
                isSend = false;
            }
        } else if ("ZYSTDD".equals(appid)) {
            //非集团不发统一消息
            if (!"20430001".equals(archiveOrgCode)) {
                isSend = false;
            }
        } else if ("CMS".equals(appid)) {//cms 暂时不发消息
            isSend = false;
        }
        return isSend;
    }
}
