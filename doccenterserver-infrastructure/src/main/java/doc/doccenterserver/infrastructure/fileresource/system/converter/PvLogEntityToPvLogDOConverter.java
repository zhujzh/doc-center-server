package doc.doccenterserver.infrastructure.fileresource.system.converter;

import doc.doccenterserver.domain.fileresource.system.model.ParamEntity;
import doc.doccenterserver.domain.fileresource.system.model.PvLogEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ParameterDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PvlogDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author ch
 * @date 2023/4/11 22:13
 */
@Mapper
public interface PvLogEntityToPvLogDOConverter {
    PvLogEntityToPvLogDOConverter INSTANCE = Mappers.getMapper(PvLogEntityToPvLogDOConverter.class);
    @Mappings({})
    List<PvLogEntity> dOToEntityList(List<PvlogDO> pvlogDOS);
    @Mappings({})
    PvlogDO entityToDO(PvLogEntity paramEntity);
    @Mappings({})
    PvLogEntity dOToEntity(PvlogDO paramDO);
}
