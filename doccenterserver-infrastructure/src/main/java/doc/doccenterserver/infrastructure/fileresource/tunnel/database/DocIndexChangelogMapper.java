package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.DocIndexChangelogDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 创建索引任务处理Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface DocIndexChangelogMapper extends BaseMapper<DocIndexChangelogDO> {
}
