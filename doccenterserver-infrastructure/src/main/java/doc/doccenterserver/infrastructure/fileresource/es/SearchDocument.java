package doc.doccenterserver.infrastructure.fileresource.es;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
public class SearchDocument{

    private String idx_id;//业务主键 - 全局唯一
    private String idx_title;//标题
    private String idx_user_id;//创建者ID
    private String idx_user_name;//创建者名称
    private String idx_parent_id;//上级业务主键
    private String idx_desc;//业务描述
    private List<String> idx_auth_value;//授权值
    private String idx_auth_value_string;//授权值-字符串格式
    private String idx_app_id;//所属模块或应用ID
    private String idx_attachment_id;//附件ID,多个附件id以”,”分割
    private LocalDateTime idx_date;//索引创建时间
    private String id;//索引主键
    private String idx_suffix;//文件名后缀
    private String idx_text;//文件文本内容（用于全文索引）

}


