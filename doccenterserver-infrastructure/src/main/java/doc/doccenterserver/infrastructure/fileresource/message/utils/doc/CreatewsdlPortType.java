package doc.doccenterserver.infrastructure.fileresource.message.utils.doc;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CreatewsdlPortType extends Remote {
    String create(String var1) throws RemoteException;
}

