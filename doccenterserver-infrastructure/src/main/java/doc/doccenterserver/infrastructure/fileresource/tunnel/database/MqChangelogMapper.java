package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.MqChangelogDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 消息入库Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface MqChangelogMapper extends BaseMapper<MqChangelogDO> {
}
