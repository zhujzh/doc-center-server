package doc.doccenterserver.infrastructure.fileresource.document.repository;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.bizworks.core.runtime.common.MultiResponse;
import com.alibaba.bizworks.core.runtime.common.SingleResponse;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.tobacco.mp.uc.client.api.org.dto.OrgNodeUnitDTO;
import com.tobacco.mp.uc.kz.client.api.EmployeeExtServiceAPI;
import com.tobacco.mp.uc.kz.client.api.OrgExtServiceAPI;
import com.tobacco.mp.uc.kz.client.api.dto.ListOrgRootOrgUnitsResponse;
import com.tobacco.mp.uc.kz.client.api.dto.OrgUnitResponse;
import com.tobacco.mp.uc.kz.client.api.dto.PageQueryEmployeeExtResponse;
import com.tobacco.mp.uc.kz.client.api.dto.QueryChildrenOrgUnitsInOrgResponse;
import com.tobacco.mp.uc.kz.client.api.req.*;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;
import doc.doccenterserver.domain.fileresource.document.repository.OrgRepository;
import doc.doccenterserver.domain.fileresource.system.model.ParameterEntry;
import doc.doccenterserver.domain.fileresource.system.repository.ParameterRepository;
import doc.doccenterserver.domain.fileresource.system.service.ParameterDomainService;
import doc.doccenterserver.infrastructure.fileresource.document.converter.OrgEntityToOrgDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcOrgUnitService;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcUserService;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.constant.Constants;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.security.properties.UcCenterProperties;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.OrgMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.OrgDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("OrgDORepository")
public class OrgRepositoryImpl implements OrgRepository {

    @Resource
    private OrgMapper orgMapper;
    @Resource
    private ParameterRepository parameterRepository;

    @Resource
    private OrgExtServiceAPI orgExtServiceAPI;
    @Resource
    private UcCenterProperties ucCenterProperties;
    @Resource
    private EmployeeExtServiceAPI employeeExtServiceAPI;

    @Resource
    private UcUserService ucUserService;
    @Resource
    private UcOrgUnitService ucOrgUnitService;


    @Override
    public OrgEntity getById(String id) {
        OrgUnitResponse response =  ucOrgUnitService.queryUcOrgNodeByBizCode(id);
        if(ObjectUtil.isEmpty(response)){
             return OrgEntity.builder()
                     .orgId(response.getBizCode())
                     .orgName(response.getOrgUnitName())
                     .build();
        }
        return null;
      //  return OrgEntityToOrgDOConvertor.INSTANCE.dOToEntity(orgMapper.selectById(id));
    }

    @Override
    public List<Map<String, Object>> querySelectedOrgs(List<List<String>> orgMutilList) {
        return orgMapper.querySelectedOrgs(orgMutilList);
    }

    @Override
    public List<OrgEntity> getAllOrg() {
        LambdaQueryWrapper<OrgDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(OrgDO::getOrgStatus, "QY");
        wrapper.orderByAsc(OrgDO::getOrgSort);
        List<OrgDO> orgDOList = orgMapper.selectList(wrapper);
        return OrgEntityToOrgDOConvertor.INSTANCE.dOToEntityList(orgDOList);
    }

    @Override
    public List<OrgEntity> getOrgByOrgId(String parentId,List<String> queryType) {
        List<OrgEntity> orgEntities = new ArrayList<>();
        if (StrUtil.isEmpty(parentId)) { // 查询顶级节点
            // 获取参数字典表里的配置值
            ParameterEntry parameterEntry = parameterRepository.getByKey(ConstantSys.TOP_ORG_ID);
            if (StrUtil.isNotEmpty(parameterEntry.getPValue())) {
             // 查询顶级节点
                List<String> orgIds = Arrays.asList(parameterEntry.getPValue().split(","));
                Collection<ListOrgRootOrgUnitsResponse> rootUnits = ucOrgUnitService.queryOrgRootOrgUnits();
                if(CollUtil.isNotEmpty(rootUnits)){
                    List<ListOrgRootOrgUnitsResponse> list = rootUnits.stream().filter(i->orgIds.contains(i.getBizCode())).collect(Collectors.toList());
                    for (ListOrgRootOrgUnitsResponse orgUnit : list) {
                        OrgEntity item = OrgEntity.builder()
                                .orgId(orgUnit.getBizCode())
                                .orgName(orgUnit.getOrgNodeName())
                                .orgParentId(null)
                                .dataType(Constants.QUERY_ORG)
                                .orgLeaf(false)
                                .build();
                        orgEntities.add(item);
                    }
                }
            }
        } else {
            // 通过orgId 查询组织节点
            OrgEntity org = BuildOrgNode(parentId);
            if(ObjectUtil.isNotEmpty(org)){
                // 查询下级
                Collection<QueryChildrenOrgUnitsInOrgResponse> orgs=  ucOrgUnitService.getOrgByParentId(org.getOrgUnitId());
                if (CollUtil.isNotEmpty(orgs)) {
                    for (QueryChildrenOrgUnitsInOrgResponse orgUnit : orgs) {
                        OrgEntity item = OrgEntity.builder()
                                .orgId(orgUnit.getBizCode())
                                .orgName(orgUnit.getOrgNodeName())
                                .orgParentId(org.getOrgId())
                                .dataType(Constants.QUERY_ORG)
                               // .orgLeaf(false)
                                .build();
                        // 如果只查询 org  orgLeaf就使用 用户中心返回的 leaf
                        if(CollUtil.isNotEmpty(queryType) && queryType.size() == 1 && Constants.QUERY_ORG.equals(queryType.get(0))){
                            item.setOrgLeaf(orgUnit.getLeafNode());
                        }else {
                            item.setOrgLeaf(false);
                        }
                        orgEntities.add(item);
                    }

                }
                if(queryType.contains(Constants.QUERY_USER)){
                    // 查询人员
                    Collection<PageQueryEmployeeExtResponse> users = ucUserService.queryUserListByOrgIds("",Collections.singletonList(org.getOrgUnitId()),1);
                    if (CollUtil.isNotEmpty(users)) {
                        for (PageQueryEmployeeExtResponse user:users){
                            OrgEntity item = OrgEntity.builder()
                                    .orgId(user.getBizCode())
                                    .orgName(user.getName())
                                    .deptName(user.getOrgNodeName())
                                    .orgParentId(org.getOrgId())
                                    .dataType(Constants.QUERY_USER)
                                    .orgLeaf(true)
                                    .build();
                            orgEntities.add(item);
                        }
                    }
                }
            }
        }
        return orgEntities;
    }


    @Override
    public List<OrgEntity> getOrgByUserName(String name, List<String> bizIds) {
        // 查询出 根组织节点属性
        List<OrgEntity> orgEntities = new ArrayList<>();
        Collection<OrgNodeUnitDTO> nodeUnitDTOS =  ucOrgUnitService.queryOrgUnitByBizCode(bizIds);
        if(CollUtil.isNotEmpty(nodeUnitDTOS)){
            // 获取主节点
            List<String> unitIds = nodeUnitDTOS.stream().map(OrgNodeUnitDTO::getOrgId).collect(Collectors.toList());
            // 根据这些节点查询出 组织

        }

        return orgEntities;
    }



    private OrgEntity BuildOrgNode(String parentId) {
        OrgEntity orgEntity = null;
        OrgUnitResponse orgUnitResponse =  ucOrgUnitService.queryUcOrgNodeByBizCode(parentId);
        //response
        if (ObjectUtil.isNotEmpty(orgUnitResponse)) {
            orgEntity = OrgEntity.builder()
                    .orgId(orgUnitResponse.getBizCode())
                    .orgName(orgUnitResponse.getOrgUnitName())
                    .orgUnitId(orgUnitResponse.getId())
                    .orgParentId(null)
                    .build();
        }
        return orgEntity;
    }


}


