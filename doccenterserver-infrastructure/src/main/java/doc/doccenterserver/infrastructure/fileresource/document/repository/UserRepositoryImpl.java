package doc.doccenterserver.infrastructure.fileresource.document.repository;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.tobacco.mp.uc.client.api.org.dto.OrgNodeUnitDTO;
import com.tobacco.mp.uc.client.api.org.dto.QueryParentOrgUnitsInOrgDTO;
import com.tobacco.mp.uc.kz.client.api.dto.OrgUnitResponse;
import com.tobacco.mp.uc.kz.client.api.dto.PageQueryEmployeeExtResponse;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.document.repository.GroupRepository;
import doc.doccenterserver.domain.fileresource.document.repository.OrgRepository;
import doc.doccenterserver.domain.fileresource.document.repository.RoleRepository;
import doc.doccenterserver.domain.fileresource.document.repository.UserRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.UserEntityToUserDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcOrgUnitService;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcUserService;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.UserMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.UserDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("UserDORepository")
public class UserRepositoryImpl implements UserRepository {

    @Resource
    private UserMapper userMapper;

    @Resource
    private OrgRepository orgRepository;

    @Resource
    private RoleRepository roleRepository;

    @Resource
    private GroupRepository groupRepository;

    @Resource
    private UcUserService ucUserService;
    @Resource
    private UcOrgUnitService ucOrgUnitService;

    @Override
    public UserEntity getUserInfo(String userId) {
        return ucUserService.getUserInfoByUserCode(userId);
       // UserDO userDO = userMapper.selectById(userId);
     //  return UserEntityToUserDOConvertor.INSTANCE.dOToEntity(userDO);
    }

    @Override
    public List<Map<String, Object>> querySelectedData(String authObjIds) {
        // 用户id集合
        List<String> userIdList = new ArrayList<>();
        // 组织id集合
        List<String> orgIdList = new ArrayList<>();
        // 角色id集合
        List<String> roleIdList = new ArrayList<>();
        // 自定义组id集合
        List<String> groupIdList = new ArrayList<>();
        List<Map<String, Object>> resultMapList = Lists.newArrayList();
        Map<String, Integer> orderMap = new HashMap<>();
        String[] ids = authObjIds.split(",");
        for (int i = 0; i < ids.length; i++) {
            // 拆分id，区分组织id和用户id
            if (ids[i].contains("^USER")) {
                // 去除^USER,属于用户id
                ids[i] = ids[i].substring(0, ids[i].length() - 5);
                userIdList.add(ids[i]);
            } else if (ids[i].contains("^ORG")) {
                // 去除^ORG,属于组织id
                ids[i] = ids[i].substring(0, ids[i].length() - 4);
                orgIdList.add(ids[i]);
            } else if (ids[i].contains("^ROLE")) {
                ids[i] = ids[i].substring(0, ids[i].length() - 5);
                roleIdList.add(ids[i]);
            } else if (ids[i].contains("^GROUP")) {
                // 去除^GROUP,属于自定义组id
                ids[i] = ids[i].substring(0, ids[i].length() - 6);
                groupIdList.add(ids[i]);
            }
            orderMap.put(ids[i], i);
            resultMapList.add(new HashMap<>());
        }

        if (userIdList.size() > 0) {
            // 存在用户id集合，查询用户信息
            log.error("------------------------------用户id列表数据" + JSON.toJSONString(userIdList));
           // List<Map<String, Object>> userMapList = userMapper.querySelectedUsers(userIdList);
            List<Map<String, Object>> userMapList = querySelectedUsers(userIdList);
            for (Map<String, Object> user : userMapList) {
                resultMapList.set(orderMap.get(user.get("ID")), user);
            }
        }

        if (orgIdList.size() > 0) {
            // 存在组织id集合，查询组织信息
            List<List<String>> orgMutilList = StringUtil.splitList(orgIdList, 1000);
            List<Map<String, Object>> orgMapList = querySelectedOrgs(orgMutilList);
    //        List<Map<String, Object>> orgMapList = orgRepository.querySelectedOrgs(orgMutilList);

            for (Map<String, Object> org : orgMapList) {
                resultMapList.set(orderMap.get(org.get("ID")), org);
            }
        }


        if (roleIdList.size() > 0) {
            // 存在角色id集合，查询role信息
            List<List<String>> roleMutilList = StringUtil.splitList(roleIdList, 1000);
            List<Map<String, Object>> roleMapList = roleRepository.querySelectedRoles(roleMutilList);

            for (Map<String, Object> role : roleMapList) {
                resultMapList.set(orderMap.get(role.get("id")), role);
            }
        }

        if (groupIdList.size() > 0) {
            // 存在自定义组id集合，查询group信息
            List<List<String>> groupMutilList = StringUtil.splitList(groupIdList, 1000);
            List<Map<String, Object>> groupMapList = groupRepository.querySelectedGroups(groupMutilList);
            for (Map<String, Object> group : groupMapList) {
                resultMapList.set(orderMap.get(group.get("id")), group);

            }
        }

        for (int i = resultMapList.size() - 1; i >= 0; i--) {
            // 去除无内容数据
            Map<String, Object> item = resultMapList.get(i);
            if (MapUtils.isEmpty(item)) {
                resultMapList.remove(i);
            }
        }

        return resultMapList;
    }

    private List<Map<String, Object>> querySelectedOrgs(List<List<String>> orgMutilList) {
        List<String> ids = new ArrayList<>();
        orgMutilList.forEach(i->{
            ids.addAll(i);
        });
        List<Map<String,Object>> mapList = new ArrayList<>();
        Collection<OrgNodeUnitDTO> orgNodeUnitDTOS =  ucOrgUnitService.queryOrgUnitByBizCode(ids);
        if(CollUtil.isNotEmpty(orgNodeUnitDTOS)){
            for (OrgNodeUnitDTO unitDTO:orgNodeUnitDTOS){
                Map<String,Object> map = new HashMap<>();
                map.put("ID",unitDTO.getRefOrgUnit().getBizCode());
                map.put("NAME",unitDTO.getRefOrgUnit().getOrgUnitName());
                map.put("DATA_TYPE","ORG");
                mapList.add(map);
            }
        }
        return mapList;
    }

    /**
     * @param userIdList 用户id集合
     * @return  List<Map<String, Object>> 继承原有代码
     * @description 根据用户id集合查询 用户
     * @author wangxp
     * @date 2023/6/16 14:51
     */
    private List<Map<String, Object>> querySelectedUsers(List<String> userIdList) {
        List<Map<String, Object>> mapList = new ArrayList<>();
        List<UserEntity> userEntities = ucUserService.getUsersByIds(userIdList);
        for (UserEntity user:userEntities){
            Map<String,Object> map = new HashMap<>();
            map.put("USER_CODE",user.getUserId());
            map.put("COMPANY_NAME",user.getCompanyName());
            map.put("DEPT_NAME",user.getDeptName());
            map.put("ORG_ID",user.getOrgId());
            map.put("USER_ID",user.getUserId());
            map.put("ID",user.getUserId());
            map.put("USER_NAME",user.getUserName());
            map.put("DATA_TYPE","USER");
            map.put("NAME",user.getUserName());
            mapList.add(map);
        }
        return mapList;
    }

    @Override
    public List<UserEntity> getUserListByGroupId(List<String> groupUserId) {
        LambdaQueryWrapper<UserDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(CollectionUtil.isNotEmpty(groupUserId), UserDO::getUserId, groupUserId);
        List<UserDO> userDOList = userMapper.selectList(queryWrapper);
        return UserEntityToUserDOConvertor.INSTANCE.dOToEntityList(userDOList);
    }

    @Override
    public List<UserEntity> getUserListByOrgIds(List<String> orgIds) {
        LambdaQueryWrapper<UserDO> queryWrapper = new LambdaQueryWrapper<>();
        if(orgIds.size()>1){
            queryWrapper.in(UserDO::getOrgId, orgIds);
        } else if(orgIds.size()==1){
            queryWrapper.eq(UserDO::getOrgId, orgIds.get(0));
        }
        queryWrapper.eq(UserDO::getUserStatus, ConstantSys.QY);
        queryWrapper.orderByAsc(UserDO::getUserSort);
        List<UserDO> userDOList = userMapper.selectList(queryWrapper);
        return UserEntityToUserDOConvertor.INSTANCE.dOToEntityList(userDOList);
    }
    @Override
    public List<UserEntity> getUserByUserName(String userName,List<String> orgIds) {
        // 查询 组织单元
        Collection<OrgNodeUnitDTO> orgs = ucOrgUnitService.queryOrgUnitByBizCode(orgIds);
        List<UserEntity> userEntities = new ArrayList<>();
        if(CollUtil.isNotEmpty(orgs)){
            List<String> orgUnitIds = orgs.stream().map(i->i.getRefOrgUnit().getId()).collect(Collectors.toList());
            Collection<PageQueryEmployeeExtResponse> users = ucUserService.pageUserListByOrgIds(userName,orgUnitIds,2);
            for (PageQueryEmployeeExtResponse employeeExtResponse: users){
                UserEntity item = UserEntity.builder()
                       // .orgId(employeeExtResponse.get)
                        .employeeId(employeeExtResponse.getId())
                        .userName(employeeExtResponse.getName())
                        .userId(employeeExtResponse.getBizCode())
                        .userCode(employeeExtResponse.getBizCode())
                        .orgId(ObjectUtil.isNotEmpty(employeeExtResponse.getExtendProps().get("orgId"))?String.valueOf(employeeExtResponse.getExtendProps().get("orgId")):"")
                        .orgName(employeeExtResponse.getOrgNodeName())
                        .orgUnitId(employeeExtResponse.getOrgUnitId())
                        .build();
                userEntities.add(item);
            }
        }
        return userEntities;
    }


    /**
     * @param orgUnitIds 组织单元ids
     * @return List<OrgEntity>
     * @description 查询所有 上级 包含本身
     * @author wangxp
     * @date 2023/6/14 9:14
     */
    @Override
    public List<OrgEntity> getOrgListTreeByUnitIds(List<String> orgUnitIds) {
        List<QueryParentOrgUnitsInOrgDTO> list = new ArrayList<>();
        for (String unitId:orgUnitIds){
            Collection<QueryParentOrgUnitsInOrgDTO> orgUnits = ucOrgUnitService.queryOrgParentOrgUnits(unitId);
            if(CollUtil.isNotEmpty(orgUnits)){
                list.addAll(orgUnits);
            }
        }
        // 去重
        list =  list.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(QueryParentOrgUnitsInOrgDTO::getOrgUnitId))),ArrayList::new));
        List<OrgEntity>  orgEntities = new ArrayList<>();
        for (QueryParentOrgUnitsInOrgDTO unit:list){
            // 筛选出父级
            QueryParentOrgUnitsInOrgDTO parentOrg = null;
            if(StrUtil.isNotEmpty(unit.getParentOrgUnitId())){ // 存在父级id
                 parentOrg = list.stream().filter(i->i.getOrgUnitId().equals(unit.getParentOrgUnitId())).findAny().get();
            }
            orgEntities.add( OrgEntity.builder()
                    .orgId(unit.getBizCode())
                    .orgName(unit.getOrgNodeName())
                    .orgUnitId(unit.getOrgUnitId())
                    .orgParentId(ObjectUtil.isNotEmpty(parentOrg)?parentOrg.getBizCode():"")
                 //    .parentOrgUnitId(unit.getParentOrgUnitId())
                    .orgLeaf(false)
                    .build());
        }
        return orgEntities;
    }

}
