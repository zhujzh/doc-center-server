package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import doc.doccenterserver.domain.fileresource.document.model.MeetingEntity;
import doc.doccenterserver.domain.fileresource.document.repository.MeetingRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.MeetingEntityToMeetingDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.MeetingMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.MeetingDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("MeetingDORepository")
public class MeetingRepositoryImpl implements MeetingRepository {

    @Resource
    private MeetingMapper meetingMapper;

    @Override
    public Integer saveMeeting(MeetingEntity meeting) {
        MeetingDO meetingDO = meetingMapper.selectById(meeting.getPaperId());
        if (null == meetingDO) {
            return meetingMapper.insert(MeetingEntityToMeetingDOConvertor.INSTANCE.entityToDO(meeting));
        } else {
            return meetingMapper.updateById(MeetingEntityToMeetingDOConvertor.INSTANCE.entityToDO(meeting));
        }
    }

    @Override
    public MeetingEntity getMeetingById(String paperId) {
        MeetingDO meetingDO = meetingMapper.selectById(paperId);
        return MeetingEntityToMeetingDOConvertor.INSTANCE.dOToEntity(meetingDO);
    }

    @Override
    public List<MeetingEntity> getMeeting(List<String> paperIds) {
        LambdaQueryWrapper<MeetingDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(MeetingDO:: getPaperId, paperIds);
        List<MeetingDO> meetingDOList = meetingMapper.selectList(wrapper);
        return MeetingEntityToMeetingDOConvertor.INSTANCE.dOToEntityList(meetingDOList);
    }
}
