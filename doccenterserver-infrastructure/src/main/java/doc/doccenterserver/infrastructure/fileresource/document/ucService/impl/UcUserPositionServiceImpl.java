package doc.doccenterserver.infrastructure.fileresource.document.ucService.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.bizworks.core.runtime.common.MultiResponse;
import com.tobacco.mp.uc.kz.client.api.UserPositionServiceAPI;
import com.tobacco.mp.uc.kz.client.api.dto.ListUserPositionResponse;
import com.tobacco.mp.uc.kz.client.api.req.ListUserPositionRequest;
import doc.doccenterserver.infrastructure.fileresource.document.ucService.UcUserPositionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;

/**
 * @author wangxp
 * @description 人员 定职列表
 * @date 2023/6/15 18:38
 */
@Service
public class UcUserPositionServiceImpl implements UcUserPositionService {

    @Resource
    private UserPositionServiceAPI userPositionServiceAPI;

    @Override
    public Collection<ListUserPositionResponse> queryUserPositionByLoginId(String loginUid) {
        ListUserPositionRequest listUserPositionRequest = new ListUserPositionRequest();
        listUserPositionRequest.setLoginUid(loginUid);
        listUserPositionRequest.setStatus("1");
        MultiResponse<ListUserPositionResponse> response = userPositionServiceAPI.listUserPosition(listUserPositionRequest);
        if(ObjectUtil.isNotEmpty(response.getData()) && CollUtil.isNotEmpty(response.getData().getItems())){
            return response.getData().getItems();
        }
        return null;
    }
}
