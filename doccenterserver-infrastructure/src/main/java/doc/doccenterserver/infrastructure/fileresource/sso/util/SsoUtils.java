package doc.doccenterserver.infrastructure.fileresource.sso.util;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import doc.doccenterserver.infrastructure.fileresource.security.model.SsoModel;
import doc.doccenterserver.infrastructure.fileresource.security.properties.SsoTokenProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wangxp
 * @description 单点认证工具类
 * @date 2023/3/29 16:27
 */
public class SsoUtils {


    // 获取token type
    private static final String AUTHORIZATION_CODE = "authorization_code";


    // 刷新token type
    private static final String REFRESH_TOKEN = "refresh_token";

    /**
     * @param code sso验证码
     * @return SsoModel 认证信息
     * @description 获取token
     * @author wangxp
     * @date 2023/3/29 16:55
     */
    public static SsoModel getToken(String code){
        SsoTokenProperties ssoTokenProperties = SpringUtil.getBean(SsoTokenProperties.class);
        Map<String,Object> formMap = new HashMap<>();
        formMap.put("client_id",ssoTokenProperties.getClientId());
        formMap.put("client_secret",ssoTokenProperties.getSecret());
        formMap.put("code",code);
        formMap.put("grant_type",AUTHORIZATION_CODE);
        HttpResponse response = HttpRequest.post(ssoTokenProperties.getSsoApiUrl() + "access_token")
                .form(formMap)
                .execute();
        SsoModel ssoModel = null;
        if(response.isOk()){
            JSONObject obj = JSONUtil.parseObj(response.body());
            if("0".equals(obj.getStr("code"))){
                ssoModel = JSONUtil.toBean(obj.getJSONObject("data"), SsoModel.class);
            }
        }
        return ssoModel;
    }


    public static SsoModel refreshToken(String refreshToken){
        SsoTokenProperties ssoTokenProperties = SpringUtil.getBean(SsoTokenProperties.class);
        Map<String,Object> formMap = new HashMap<>();
        formMap.put("client_id",ssoTokenProperties.getClientId());
        formMap.put("client_secret",ssoTokenProperties.getSecret());
        formMap.put("grant_type",REFRESH_TOKEN);
        formMap.put("refresh_token",refreshToken);
        formMap.put("scope","appdata");
        HttpResponse response = HttpRequest.post(ssoTokenProperties.getSsoApiUrl() + "access_token")
                .form(formMap)
                .execute();
        SsoModel ssoModel = null;
        if(response.isOk()){
            JSONObject obj = JSONUtil.parseObj(response.body());
            if("0".equals(obj.getStr("code"))){
                ssoModel = JSONUtil.toBean(obj.getJSONObject("data"), SsoModel.class);
            }
        }
        return ssoModel;
    }


}
