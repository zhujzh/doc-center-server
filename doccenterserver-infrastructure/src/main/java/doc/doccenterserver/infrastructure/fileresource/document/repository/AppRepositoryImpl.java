package doc.doccenterserver.infrastructure.fileresource.document.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.AppEntity;
import doc.doccenterserver.domain.fileresource.document.repository.AppRepository;
import doc.doccenterserver.domain.fileresource.document.repository.PaperRepository;
import doc.doccenterserver.domain.fileresource.document.repository.RoleRepository;
import doc.doccenterserver.domain.fileresource.pan.repository.TPanObjRepository;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.infrastructure.fileresource.document.converter.AppConvertor;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.security.service.TokenService;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.AppMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.AppDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * @author Tangcancan
 */
@Slf4j
@Component("AppDORepository")
public class AppRepositoryImpl implements AppRepository {

    @Resource
    private AppMapper appMapper;

    @Resource
    private PaperRepository paperRepository;

    @Resource
    private RoleRepository roleRepository;

    @Resource
    private TPanObjRepository tpanObjRepository;

    @Resource
    private TokenService tokenService;

    @Override
    public PageResult<List<AppEntity>> getUseDetailList(HttpServletRequest request, AppEntity appEntity) {
        // 获取当前登录用户
        String userId = tokenService.getLoginUser(request).getUserModel().getUserid();
        // 判断是否是系统管理员
        List<String> roleId = roleRepository.getRoleByUserId(userId);
        if (!roleId.contains(ConstantSys.DOC_FLOW_ADMIN)) {
            appEntity.setUserId(userId);
        }
        // 查询启用的租户
        appEntity.setStatus(ConstantSys.STATUS_ONE);
        PageResult<List<AppEntity>> listPageResult = pageListForApp(appEntity);
        List<AppEntity> appEntityList = listPageResult.getData();
        // 取出appId 查询每个应用所占配额
        for (AppEntity app : appEntityList) {
            setQuotaValue(app.getAppId(), app);
        }
        listPageResult.setData(appEntityList);
        return listPageResult;
    }

    @Override
    public List<AppEntity> getAppList(AppEntity appEntity) {
        LambdaQueryWrapper<AppDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtil.notEmpty(appEntity.getUserId()), AppDO::getUserId, appEntity.getUserId());
        return AppConvertor.INSTANCE.dOToEntity(appMapper.selectList(wrapper));
    }

    @Override
    public AppEntity editInit(String appId) {
        return AppConvertor.INSTANCE.dOToEntity(appMapper.selectById(appId));
    }

    @Override
    public AppEntity save(AppEntity appEntity) {
        // add 新增 edit 编辑
        String operate = appEntity.getOperate();
        AppDO appDO = AppConvertor.INSTANCE.entityToDO(appEntity);
        if (ConstantSys.OPERATE_ADD.equals(operate)) {
            appMapper.insert(appDO);
            // BucketUtil.createBucket(appDO.getAppId());
        } else {
            appMapper.updateById(appDO);
        }
        return AppConvertor.INSTANCE.dOToEntity(appDO);
    }

    @Override
    public PageResult<List<AppEntity>> getAppLists(AppEntity appEntity) {
        return pageListForApp(appEntity);
    }

    @Override
    public AppEntity delete(String appIds) {
        List<String> idList = StringUtil.strToList(appIds, ConstantSys.SEPARATE_SEMICOLON);
        appMapper.deleteBatchIds(idList);
        return new AppEntity();
    }

    @Override
    public AppEntity getAppDetail(String appId) {
        AppDO appDO = appMapper.selectById(appId);
        AppEntity app = AppConvertor.INSTANCE.dOToEntity(appDO);
        setQuotaValue(appId, app);
        return app;
    }

    private PageResult<List<AppEntity>> pageListForApp(AppEntity appEntity) {
        // 分页查询
        LambdaQueryWrapper<AppDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtil.notEmpty(appEntity.getAppId()), AppDO::getAppId, appEntity.getAppId());
        wrapper.like(StringUtil.notEmpty(appEntity.getAppName()), AppDO::getAppName, appEntity.getAppName());
        wrapper.eq(StringUtil.notEmpty(appEntity.getStatus()), AppDO::getStatus, appEntity.getStatus());
        wrapper.eq(StringUtil.notEmpty(appEntity.getUserId()), AppDO::getUserId, appEntity.getUserId());
        Page<AppDO> page = new Page<>(appEntity.getPageNum(), appEntity.getPageSize());
        IPage<AppDO> pages = appMapper.selectPage(page, wrapper);
        List<AppDO> paperDOList = pages.getRecords();
        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(pages.getCurrent());
        baseDto.setPageSize(pages.getSize());
        baseDto.setTotal(pages.getTotal());
        baseDto.setTotalPage(pages.getPages());
        List<AppEntity> appEntityList = AppConvertor.INSTANCE.dOToEntity(paperDOList);
        return new PageResult<>(appEntityList, baseDto);
    }

    private void setQuotaValue(String appId, AppEntity app) {
        double sumquota = 0.0;
        double usedquota = 0.0;
        if (null == app.getQuota()) {
            sumquota = 0.0;
        } else {
            sumquota = app.getQuota();
        }
        if ("yunpan".equals(appId)) {
            usedquota = tpanObjRepository.getTpanObjSize();
        } else if ("ZYECM".equals(appId)) {
            usedquota = paperRepository.findsumpaperfilesize(appId);
        } else {
            usedquota = paperRepository.sumtpanfilesize(appId);
        }
        BigDecimal b = new BigDecimal(usedquota / ConstantSys.ONE_THOUSAND_AND_TWENTY_FOUR / ConstantSys.ONE_THOUSAND_AND_TWENTY_FOUR);
        usedquota = b.setScale(ConstantSys.NUMBER_ONE, RoundingMode.HALF_UP).doubleValue();
        app.setUsedQuota(usedquota);
        // 剩余配额
        b = new BigDecimal(sumquota - usedquota);
        double surplusQuota = b.setScale(ConstantSys.NUMBER_ONE, RoundingMode.HALF_UP).doubleValue();
        app.setSurplusQuota(surplusQuota);
        app.setSumquota(sumquota);
    }
}
