package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ResDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统资源Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface ResMapper extends BaseMapper<ResDO> {
    ResDO getByParentId(@Param("resParentId")String s);

    Integer findChildrenNumById(@Param("resId") String resId);

    List<ResDO> getInItTree(@Param("resParentId") String s);

    ResDO getById(@Param("resId")String id);

    List<ResDO> getInItUserTree(@Param("userId")String userId);
}
