package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ParameterDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统配置参数Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface ParameterMapper extends BaseMapper<ParameterDO> {
}
