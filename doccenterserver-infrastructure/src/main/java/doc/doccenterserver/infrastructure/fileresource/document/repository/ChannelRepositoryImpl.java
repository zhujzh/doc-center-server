package doc.doccenterserver.infrastructure.fileresource.document.repository;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.ChannelEntity;
import doc.doccenterserver.domain.fileresource.document.repository.ChannelRepository;
import doc.doccenterserver.domain.fileresource.system.model.UserModel;
import doc.doccenterserver.infrastructure.fileresource.document.converter.ChannelEntityToChannelDOConverter;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.ChannelMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ChannelDO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Slf4j
@Component("ChannelDORepository")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChannelRepositoryImpl implements ChannelRepository {
    private final JSONObject parameterMap;
    private final ChannelMapper channelMapper;

    @Override
    public ChannelEntity getById(String id) {
        LambdaQueryWrapper<ChannelDO> wrap = new LambdaQueryWrapper<>();
        wrap.eq(ChannelDO::getId, id);
        ChannelDO fileDO = channelMapper.selectOne(wrap);
        return ChannelEntityToChannelDOConverter.INSTANCE.dOToEntity(fileDO);
    }

    @Override
    public List<ChannelEntity> selectList(ChannelEntity channel) {
        ChannelDO channelDO = ChannelEntityToChannelDOConverter.INSTANCE.entityToDO(channel);
        List<ChannelDO> doList = channelMapper.selectList(new QueryWrapper<>(channelDO));
        return ChannelEntityToChannelDOConverter.INSTANCE.dOToEntityList(doList);
    }

    @Override
    public PageResult<List<ChannelEntity>> selectByPage(ChannelEntity channelEntity) {
        LambdaQueryWrapper<ChannelDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ChannelDO::getParentId, channelEntity.getId());
        wrapper.orderByAsc(ChannelDO::getSort);
        Page<ChannelDO> page = new Page<>(channelEntity.getPageNum(), channelEntity.getPageSize());
        IPage<ChannelDO> channels = channelMapper.selectPage(page, wrapper);
        List<ChannelDO> channelDOList = channels.getRecords();
        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(channels.getCurrent());
        baseDto.setPageSize(channels.getSize());
        baseDto.setTotal(channels.getTotal());
        baseDto.setTotalPage(channels.getPages());
        return new PageResult<>(ChannelEntityToChannelDOConverter.INSTANCE.dOToEntityList(channelDOList), baseDto);
    }

    @Override
    public List<ChannelEntity> getInItTree(String parentId) {
        LambdaQueryWrapper<ChannelDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.likeRight(ChannelDO:: getId, parentId);
        wrapper.orderByAsc(ChannelDO::getSort);
        List<ChannelDO> channelDOList = channelMapper.selectList(wrapper);
        return ChannelEntityToChannelDOConverter.INSTANCE.dOToEntityList(channelDOList);
    }

    @Override
    public String getLastestChannelId(String parentId) {
        return channelMapper.getLatestChannelId(parentId);
    }

    @Override
    public String initChannelPath(String parentId, String curId) {
        StringBuilder sb = new StringBuilder();
        List<ChannelDO> list = channelMapper.queryParentChannels(parentId);
        for (ChannelDO c : list) {
            sb.append(ConstantSys.BACKSLASH).append(c.getId());
        }
        sb.append(ConstantSys.BACKSLASH).append(curId);
        return sb.toString();
    }

    @Override
    public List<ChannelEntity> initChannelPath(String classId) {
        List<ChannelDO> list = channelMapper.queryParentChannels(classId);
        return ChannelEntityToChannelDOConverter.INSTANCE.dOToEntityList(list);
    }

    @Override
    public ChannelEntity deleteChannel(String ids) {
        if (StringUtils.isNotBlank(ids)) {
            List<String> stringList = Splitter.on(ConstantSys.SEPARATE_COMMA).trimResults().omitEmptyStrings().splitToList(ids);
            channelMapper.deleteBatchIds(stringList);
        }
        return new ChannelEntity();
    }

    @Override
    public ChannelEntity addChannel(ChannelEntity channelEntity) {
        ChannelDO channelDO = ChannelEntityToChannelDOConverter.INSTANCE.entityToDO(channelEntity);
        channelDO.setDeleteFlag(ConstantSys.DELETE_FLAG_ZERO);
        channelMapper.insert(channelDO);
        return ChannelEntityToChannelDOConverter.INSTANCE.dOToEntity(channelDO);
    }

    @Override
    public ChannelEntity editChannel(ChannelEntity channelEntity) {
        ChannelDO channelDO = ChannelEntityToChannelDOConverter.INSTANCE.entityToDO(channelEntity);
        channelMapper.updateById(channelDO);
        return ChannelEntityToChannelDOConverter.INSTANCE.dOToEntity(channelDO);
    }

    @Override
    public String queryRefChannels() {
        LambdaQueryWrapper<ChannelDO> wrap = new LambdaQueryWrapper<>();
        wrap.eq(ChannelDO::getGrade, ChannelEntity.CHANNEL_GRADE_REF);
        List<ChannelDO> list = channelMapper.selectList(wrap);
        StringBuilder sb = new StringBuilder(ConstantSys.SEPARATE_COMMA);
        for (ChannelDO channel : list) {
            sb.append(channel.getId()).append(ConstantSys.SEPARATE_COMMA);
        }
        return sb.toString();
    }

    @Override
    public List<ChannelEntity> queryChildChannlesById(String classid) {
        LambdaQueryWrapper<ChannelDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByAsc(ChannelDO::getSort);
        wrapper.eq(ChannelDO::getParentId,classid);
        wrapper.eq(ChannelDO::getDeleteFlag,"0");
        List<ChannelDO> channelDOList = channelMapper.selectList(wrapper);
        return ChannelEntityToChannelDOConverter.INSTANCE.dOToEntityList(channelDOList);
    }

    @Override
    public ChannelEntity findChannelById(String classid) {
        ChannelDO channelDO = channelMapper.findChannelById(classid);
        return ChannelEntityToChannelDOConverter.INSTANCE.dOToEntity(channelDO);
    }

    @Override
    public boolean isChannelAuthList(String classId, UserModel curUser) {
        boolean isChannel =false;
        Map<String,Object> channel = Maps.newHashMap();
        if(curUser.getUserid().equals(ConstantSys.CZ_ADMIN)){
            isChannel=true;
        }else{
            channel.put("userId", curUser.getUserid());
            channel.put("orgId", curUser.getOrgId());
            channel.put("deptId", curUser.getDeptId());
            channel.put("companyId", curUser.getCompanyId());
            if(null!=classId && !"".equals(classId)){
                List<ChannelDO> childList=channelMapper.queryChildChannles(classId);
                for(ChannelEntity item:ChannelEntityToChannelDOConverter.INSTANCE.dOToEntityList(childList)){
                    channel.put("classId",item.getId());
                    List<String> isChannelAuth= channelMapper.isChannelAuthList(channel);
                    if(isChannelAuth!=null && isChannelAuth.size()>0 ){
                        isChannel = true;
                        break;
                    }
                }
                if(!isChannel){
                    List<ChannelDO> parentClassList=channelMapper.queryParentChannles(classId);
                    for(ChannelEntity item:ChannelEntityToChannelDOConverter.INSTANCE.dOToEntityList(parentClassList)){
                        channel.put("classId",item.getId());
                        List<String> isChannelAuth= channelMapper.isChannelAuthList(channel);
                        if(isChannelAuth!=null && isChannelAuth.size()>0 ){
                            isChannel = true;
                            break;
                        }
                    }
                }
            }else{//判断用户是否有文档管理的任一频道权限 ，如果有则返回true
                List<String> isChannelAuth= channelMapper.isChannelAuthList(channel);
                if(null!=isChannelAuth && isChannelAuth.size()>0){
                    isChannel = true;
                }
            }
        }
        return isChannel;
    }

    @Override
    public List<String> initChannelPathName(String classId) {
        List<String> listname=new ArrayList<>();
        List<ChannelDO> list=channelMapper.queryParentChannles(classId);
        for(ChannelDO c:list){
            listname.add(c.getName());
        }
        return listname;
    }

    @Override
    public Boolean isHasChildrenChannel(String classId) {
        LambdaQueryWrapper<ChannelDO> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ChannelDO:: getParentId, classId);
        return channelMapper.selectCount(wrapper) > 0;
    }


    @Override
    public String getChannelId(String name, String parentId, String fromAppName) {
        String channel_id = null;
        try {
            if (parentId == null) {
                if ("OA".equals(fromAppName) || "ZYOA".equals(fromAppName)) {
                    if ("[会议通知]".equals(name)) {
                        return parameterMap.getStr("MeetingChannelID");
                    } else {
                        parentId = parameterMap.getStr("OAChannelID");
                    }
                } else {
                    parentId = parameterMap.getStr("WebsiteID");
                }
            }
            //去掉前后的中括号
            name = name.substring(1);
            name = name.substring(0, name.length() - 1);
            name = replaceChannelName(name);
            //开始查找CMS中的 频道信息
            String[] channelNames = name.split(ConstantSys.BACKSLASH);
            for (int i = 0; i < channelNames.length; i++) {
                String channelName = channelNames[i].trim();
                if (channelName.length() > 0) {

                    ChannelEntity channel = new ChannelEntity();
                    channel.setName(channelName);
                    channel.setParentId(parentId);
                    List<ChannelEntity> channellist = this.selectList(channel);
                    if (channellist == null || channellist.isEmpty()) {
                        log.error("频道名称不存在name........... " + channelName);
                        break;
                    }
                    parentId = channellist.get(0).getId();
                }
            }
            channel_id = parentId;
            //如果OA的子栏目在ECM中没有找到，则将它归纳到其它类别中
            if (("OA".equals(fromAppName) || "ZYOA".equals(fromAppName)) && channel_id.equals(parameterMap.getStr("OAChannelID"))) {
                channel_id = parameterMap.getStr("OA_Other_ChannelID");
            }
        } catch (Exception e) {
            log.info("ArchiveJob.getChannelID(" + name + ")：", e);
            //e.printStackTrace();
        }
        return channel_id;
    }

    /**
     * 对于频道名称，作特殊处理
     *
     * @param name
     * @return
     */
    private String replaceChannelName(String name) {
        if ("吴忠卷烟厂/通知公告/通知".equals(name)) {
            name = "吴忠卷烟厂/通知公告/烟厂通知";
        }

        if ("零陵卷烟厂/考核反馈".equals(name)) {
            name = "零陵卷烟厂/烟厂专栏/考核反馈";
        }

        if ("原料采购中心/内部刊物/中心工作要情".equals(name)) {
            name = "原料采购中心/内部刊物/工作要情";
        }

        if ("公司本部/内部刊物/工作要情".equals(name)) {
            name = "公司本部/公司简报/工作要情";
        }

        if ("公司本部/内部刊物/经济运行通报".equals(name)) {
            name = "公司本部/公司简报/经济运行通报";
        }

        if ("公司本部/资料库/领导讲话".equals(name)) {
            name = "公司本部/公司简报/领导讲话";
        }

        if ("公司本部/综合信息/实时动态".equals(name)) {
            name = "公司本部/资料库/实时动态";
        }

        //2013年11月19日客户要求修改频次名称---------begin
//		if("公司本部/公司新闻".equals(name)){
//			name = "公司本部/综合信息";
//		}
//		if("公司本部/公司新闻/公司动态".equals(name)){
//			name = "公司本部/综合信息/政务信息";
//		}
//		if("公司本部/公司新闻/图片新闻".equals(name)){
//			name = "公司本部/综合信息/图片新闻";
//		}
        //2013年11月19日客户要求修改频次名称---------end

        if ("整顿规范专题/领导讲话".equals(name) || "<a href=\"html/stopic/zdgf.html\">整顿规范专题</a>/领导讲话".equals(name)) {
            name = "共享专栏/整顿规范专题/领导讲话";
        }

        if ("整顿规范专题/动态报道".equals(name) || "<a href=\"html/stopic/zdgf.html\">整顿规范专题</a>/动态报道".equals(name)) {
            name = "共享专栏/整顿规范专题/动态报道";
        }

        if ("整顿规范专题/正式文件".equals(name) || "<a href=\"html/stopic/zdgf.html\">整顿规范专题</a>/正式文件".equals(name)) {
            name = "共享专栏/整顿规范专题/正式文件";
        }

        if ("整顿规范专题/图片新闻".equals(name) || "<a href=\"html/stopic/zdgf.html\">整顿规范专题</a>/图片新闻".equals(name)) {
            name = "共享专栏/整顿规范专题/图片新闻";
        }

        if ("整顿规范专题/行业精神".equals(name) || "<a href=\"html/stopic/zdgf.html\">整顿规范专题</a>/行业精神".equals(name)) {
            name = "共享专栏/整顿规范专题/行业精神";
        }

        if ("整顿规范专题/专项工作".equals(name) || "<a href=\"html/stopic/zdgf.html\">整顿规范专题</a>/专项工作".equals(name)) {
            name = "共享专栏/整顿规范专题/专项工作";
        }

        if ("整顿规范专题/整顿办通知".equals(name) || "<a href=\"html/stopic/zdgf.html\">整顿规范专题</a>/整顿办通知".equals(name)) {
            name = "共享专栏/整顿规范专题/整顿办通知";
        }

        if (name.startsWith("吴忠卷烟厂11111")) {
            name = name.replaceAll("11111", "");
        }

        if (name.startsWith("清洁生产及五项现代管理推广应用")) {
            name = "共享专栏/" + name;
        }
        return name;
    }

}
