package doc.doccenterserver.infrastructure.fileresource.document.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.ConstantSys;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;
import doc.doccenterserver.domain.fileresource.document.model.Role2resEntity;
import doc.doccenterserver.domain.fileresource.document.model.RoleEntity;
import doc.doccenterserver.domain.fileresource.document.model.User2roleEntity;
import doc.doccenterserver.domain.fileresource.document.repository.OrgRepository;
import doc.doccenterserver.domain.fileresource.document.repository.Role2resRepository;
import doc.doccenterserver.domain.fileresource.document.repository.RoleRepository;
import doc.doccenterserver.domain.fileresource.document.repository.User2RoleRepository;
import doc.doccenterserver.domain.fileresource.document.repository.UserRepository;
import doc.doccenterserver.domain.fileresource.system.model.ResEntity;
import doc.doccenterserver.domain.fileresource.system.model.UserEntity;
import doc.doccenterserver.domain.fileresource.system.repository.ResRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.RoleConvertor;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.RoleMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.RoleDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Tangcancan
 */

@Slf4j
@Component("RoleDORepository")
public class RoleRepositoryImpl implements RoleRepository {

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private ResRepository resRepository;

    @Resource
    private Role2resRepository role2ResRepository;

    @Resource
    private User2RoleRepository user2RoleRepository;

    @Resource
    private OrgRepository orgRepository;

    @Resource
    private UserRepository userRepository;

    @Override
    public List<Map<String, Object>> querySelectedRoles(List<List<String>> roleMutilList) {
        return roleMapper.querySelectedRoles(roleMutilList);
    }

    @Override
    public List<String> getRoleByUserId(String userId) {
        return roleMapper.getRoleByUserId(userId);
    }

    @Override
    public RoleEntity editInit(String roleId) {
        RoleDO roleDO = roleMapper.selectById(roleId);
        return RoleConvertor.INSTANCE.dOToEntity(roleDO);
    }

    @Override
    public RoleEntity save(RoleEntity roleEntity) {
        RoleDO roleDO = roleMapper.selectById(roleEntity.getRoleId());
        if (null == roleDO) {
            // 补上角色类型（默认业务系统角色）
            roleEntity.setRoleType(ConstantSys.BIZ);
            // 默认为DOC_SYS
            roleEntity.setRoleAppId(ConstantSys.ROLE_APP_ID);
            roleEntity.setRoleCreateTime(new Date());
            roleMapper.insert(RoleConvertor.INSTANCE.entityToDO(roleEntity));
        } else {
            roleMapper.updateById(RoleConvertor.INSTANCE.entityToDO(roleEntity));
        }
        return roleEntity;
    }

    @Override
    public PageResult<List<RoleEntity>> getList(RoleEntity roleEntity) {
        roleEntity.setRoleAppId(ConstantSys.ROLE_APP_ID);
        LambdaQueryWrapper<RoleDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(RoleDO::getRoleId, roleEntity.getRoleId());
        queryWrapper.like(RoleDO::getRoleName, roleEntity.getRoleName());
        queryWrapper.eq(RoleDO::getRoleAppId, roleEntity.getRoleAppId());
        Page<RoleDO> page = new Page<>(roleEntity.getPageNum(), roleEntity.getPageSize());
        IPage<RoleDO> pages = roleMapper.selectPage(page, queryWrapper);
        List<RoleDO> roleDOList = pages.getRecords();
        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(pages.getCurrent());
        baseDto.setPageSize(pages.getSize());
        baseDto.setTotal(pages.getTotal());
        baseDto.setTotalPage(pages.getPages());
        List<RoleEntity> roleEntityList = RoleConvertor.INSTANCE.dOToEntity(roleDOList);
        return new PageResult<>(roleEntityList, baseDto);
    }

    @Override
    public RoleEntity getById(String roleId) {
        return RoleConvertor.INSTANCE.dOToEntity(roleMapper.selectById(roleId));
    }

    @Override
    public RoleEntity delete(String roleIds) {
        List<String> idList = StringUtil.strToList(roleIds, ";");
        roleMapper.deleteBatchIds(idList);
        return new RoleEntity();
    }

    @Override
    public List<ResEntity> getInitTree(String resId) {
        return resRepository.getAllRes();
    }

    @Override
    public RoleEntity getRes(String roleId) {
        List<Role2resEntity> role2resEntity = role2ResRepository.getRes(roleId);
        List<String> resList = new ArrayList<>();
        role2resEntity.forEach(e -> resList.add(e.getResId()));
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setRoleId(roleId);
        roleEntity.setResList(resList);
        return roleEntity;
    }

    @Override
    public RoleEntity updateRes(RoleEntity roleDto) {
        String roleId = roleDto.getRoleId();
        role2ResRepository.delete(roleId);
        List<String> resList = roleDto.getResList();
        for (String resId : resList) {
            Role2resEntity role2resEntity = new Role2resEntity();
            role2resEntity.setRoleId(roleId);
            role2resEntity.setResId(resId);
            role2ResRepository.insert(role2resEntity);
        }
        return roleDto;
    }

    @Override
    public RoleEntity getUser(String roleId) {
        List<User2roleEntity> user2roleEntityList = user2RoleRepository.getUserByRoleId(roleId);
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setRoleId(roleId);
        roleEntity.setUserList(user2roleEntityList);
        return roleEntity;
    }

    @Override
    public RoleEntity updateUser(RoleEntity roleEntity) {
        String roleId = roleEntity.getRoleId();
        user2RoleRepository.delete(roleId);
        List<User2roleEntity> userList = roleEntity.getUserList();
        for (User2roleEntity user : userList) {
            User2roleEntity user2role = new User2roleEntity();
            user2role.setRoleId(roleId);
            UserEntity userInfo = userRepository.getUserInfo(user.getUserId());
            OrgEntity orgEntity = orgRepository.getById(userInfo.getOrgId());
            if(ObjectUtil.isNotEmpty(userInfo)){
                user2role.setUserId(userInfo.getUserId());
                user2role.setUserCode(userInfo.getUserCode());
                user2role.setUserName(userInfo.getUserName());
            }
            if(ObjectUtil.isNotEmpty(orgEntity)){
                user2role.setOrgId(orgEntity.getOrgId());
                user2role.setOrgName(orgEntity.getOrgName());
            }
            user2RoleRepository.insert(user2role);
        }
        return roleEntity;
    }

    @Override
    public List<OrgEntity> getInitOrgTree(String orgId) {
        return orgRepository.getAllOrg();
    }
    public List<OrgEntity> getInitOrg(String parentId,List<String> queryType) {
        return orgRepository.getOrgByOrgId(parentId,queryType);
    }
    @Override
    public List<UserEntity> getUserByOrgIds(List<String> orgIds) {
        return userRepository.getUserListByOrgIds(orgIds);
    }

    @Override
    public List<UserEntity> getUserByUserName(String userName,List<String> orgIds) {
        return userRepository.getUserByUserName(userName,orgIds);
    }

    @Override
    public List<OrgEntity> getOrgListTreeByUnitIds(List<String> orgUnitIds) {
        return userRepository.getOrgListTreeByUnitIds(orgUnitIds);
    }

    @Override
    public List<OrgEntity> getOrgByUserName(String name, List<String> bizIds) {
        return orgRepository.getOrgByUserName(name,bizIds);
    }


}
