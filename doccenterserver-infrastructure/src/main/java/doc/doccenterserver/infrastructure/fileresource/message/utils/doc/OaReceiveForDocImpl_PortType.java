package doc.doccenterserver.infrastructure.fileresource.message.utils.doc;


import java.rmi.Remote;
import java.rmi.RemoteException;

public interface OaReceiveForDocImpl_PortType extends Remote {
    String receiveForDoc(String var1, String var2) throws RemoteException;

    String dealOpinion(String var1, String var2, String var3, String var4, String var5) throws RemoteException;

    String getIssueInfoForPhonePortal(String var1, String var2, String var3, String var4) throws RemoteException;

    String dealOpinionCountersign(String var1, String var2, String var3, String var4, String var5, String var6) throws RemoteException;

    String getHandleSteps(String var1, String var2, String var3, String var4) throws RemoteException;

    String getParticipants(String var1, String var2, String var3, String var4, String var5) throws RemoteException;

    String next4DoneProcess(String var1, String var2, String var3, String var4, String[] var5, String var6) throws RemoteException;

    String getIdom(String var1, String var2) throws RemoteException;

    String getAllNotes(String var1, String var2, String var3) throws RemoteException;
}


