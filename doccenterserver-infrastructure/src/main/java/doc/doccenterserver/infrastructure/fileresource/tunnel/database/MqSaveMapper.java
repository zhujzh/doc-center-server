package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.MqSaveDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 统一消息暂存日志Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface MqSaveMapper extends BaseMapper<MqSaveDO> {
}
