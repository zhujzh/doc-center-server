package doc.doccenterserver.infrastructure.fileresource.message.enums;

public enum JobDetailLogStepStatusEnum {
    STEP_STATUS_FAILD(0, "失败"),
    STEP_STATUS_SUCCESS(1, "成功"),
    STEP_STATUS_WATING(2, "待查"),
    STEP_STATUS_IGNOR(3, "忽略"),
    STEP_STATUS_HANDLING(4, "处理中")
    ;
    private final Integer value;
    private final String name;
    JobDetailLogStepStatusEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }
    public Integer getValue() {
        return this.value;
    }

}
