package doc.doccenterserver.infrastructure.fileresource.message.job;

import com.xxl.job.core.handler.annotation.XxlJob;
import doc.doccenterserver.domain.fileresource.message.repository.ArchiveChangelogRepository;
import doc.doccenterserver.domain.fileresource.message.repository.MqSaveRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息异常处理
 */
@Component
@Slf4j
public class MsgErrJob {
    @Autowired
    private MqSaveRepository mqSaveRepository;
    @XxlJob("msgErr")
    public void  msgErr() throws Exception {
        mqSaveRepository.msgErr();
        log.info("调用了");
    }
}
