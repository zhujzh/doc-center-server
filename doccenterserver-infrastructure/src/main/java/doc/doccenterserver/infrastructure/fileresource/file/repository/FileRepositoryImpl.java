package doc.doccenterserver.infrastructure.fileresource.file.repository;

import com.alibaba.bizworks.core.runtime.common.ResponseCodeEnum;
import com.alibaba.bizworks.core.runtime.common.SingleResponse;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tobacco.mp.dc.client.api.BaseFileServiceAPI;
import com.tobacco.mp.dc.client.api.FileServiceAPI;
import com.tobacco.mp.dc.client.api.basefile.req.DeleteFileRequest;
import com.tobacco.mp.dc.client.api.file.req.DownloadFileRequest;
import doc.doccenterserver.domain.fileresource.document.repository.AttachmentRepository;
import doc.doccenterserver.domain.fileresource.file.model.FileEntity;
import doc.doccenterserver.domain.fileresource.file.repository.FileRepository;
import doc.doccenterserver.domain.fileresource.pan.repository.TPanObjRepository;
import doc.doccenterserver.infrastructure.fileresource.file.converter.FileEntityToFileDOConverter;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.FileMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.FileDO;
import feign.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.rmi.ServerException;
import java.util.List;

/**
 * 文件
 * repository实现类
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@Slf4j
@Component("FILEFileDORepository")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FileRepositoryImpl extends ServiceImpl<FileMapper, FileDO> implements FileRepository {

    private final BaseFileServiceAPI baseFileServiceAPI;
    private final FileServiceAPI fileServiceAPI;

    private final TPanObjRepository tPanObjRepository;

    private final AttachmentRepository attachmentRepository;

    @Override
    public FileEntity getFileById(String pid) {
        FileDO fileDO = this.getById(pid);
        return FileEntityToFileDOConverter.INSTANCE.fileDOToFileEntity(fileDO);
    }

    @Override
    public List<FileEntity> md5Check(String md5) {
        List<FileDO> fileDOList = this.list(new LambdaQueryWrapper<FileDO>().eq(FileDO::getFileMd5, md5));
        return FileEntityToFileDOConverter.INSTANCE.fileDOToFileEntityList(fileDOList);
    }

    @Override
    public boolean insertFile(FileEntity fileEntity) {
        return this.save(FileEntityToFileDOConverter.INSTANCE.fileEntityToFileDO(fileEntity));
    }

    @Override
    public List<FileEntity> downloadFileList(String fileId) {
        LambdaQueryWrapper<FileDO> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(FileDO::getFileId, fileId).or(qw -> qw.eq(FileDO::getBigdataFileId, fileId)).or(qw -> qw.eq(FileDO::getFileMd5, fileId));
        List<FileDO> fileDOList = this.list(lambdaQueryWrapper);
        return FileEntityToFileDOConverter.INSTANCE.fileDOToFileEntityList(fileDOList);
    }

    @Override
    public String uploadFile(MultipartFile file) throws ServerException {
        SingleResponse response = baseFileServiceAPI.uploadFile(file);
        if (ResponseCodeEnum.SUCCESS.code.equals(response.getCode())) {
            return response.getData().toString();
        } else {
            throw new ServerException("上传失败" + response.getMessage());
        }
    }

    @Override
    public void downloadFile(HttpServletResponse httpServletResponse, String fileId) throws ServerException {
        DownloadFileRequest request = new DownloadFileRequest();
        request.setFileId(fileId);
        Response response = fileServiceAPI.downloadFile(request);
        try (InputStream inputStream = response.body().asInputStream();
             OutputStream out = httpServletResponse.getOutputStream();) {
            int size = 0;
            int lent = 0;
            byte[] buf = new byte[1024];
            while ((size = inputStream.read(buf)) != -1) {
                lent += size;
                out.write(buf, 0, size);
            }
            inputStream.close();
            out.close();
        } catch (IOException e) {
            log.error("下载文件失败", e.getMessage());
            throw new ServerException("下载文件失败");
        }
    }

    @Override
    public void downloadFilePath(String filePath, String fileName, String fileId) throws ServerException {
        File attfile = new File(filePath + fileName);
        if (!attfile.getParentFile().exists()) {
            attfile.getParentFile().mkdirs();
        }
        DownloadFileRequest request = new DownloadFileRequest();
        request.setFileId(fileId);
        Response response = fileServiceAPI.downloadFile(request);
        try (InputStream inputStream = response.body().asInputStream();
             BufferedInputStream bis = new BufferedInputStream(inputStream);
             FileOutputStream fileOut = new FileOutputStream(filePath + fileName);
             BufferedOutputStream bos = new BufferedOutputStream(fileOut);) {
            byte[] buf = new byte[1024];
            int length = bis.read(buf);
            //保存文件
            while (length != -1) {
                bos.write(buf, 0, length);
                length = bis.read(buf);
            }
            inputStream.close();
            bos.close();
            bis.close();
        } catch (IOException e) {
            log.error("下载文件失败", e.getMessage());
            throw new ServerException("下载文件失败");
        }
    }

    @Override
    public Boolean deleteFile(String fileId) throws IOException {
        DeleteFileRequest request = new DeleteFileRequest();
        request.setFileId(fileId);
        SingleResponse response = fileServiceAPI.deleteFile(request);
        if (ResponseCodeEnum.SUCCESS.code.equals(response.getCode())) {
            return true;
        } else {
            throw new ServerException("删除失败" + response.getMessage());
        }
    }

    @Override
    public boolean updateFile(FileEntity fileEntity) {
        return this.updateById(FileEntityToFileDOConverter.INSTANCE.fileEntityToFileDO(fileEntity));
    }

    @Override
    public byte[] downloadFile(String fileId) throws IOException {
        DownloadFileRequest request = new DownloadFileRequest();
        request.setFileId(fileId);
        Response response = fileServiceAPI.downloadFile(request);
        if (response.status() != 200) {
            throw new RuntimeException("download file from baseLine error....." + JSON.toJSONString(response));
        }
        InputStream inputStream = response.body().asInputStream();
        byte[] buf = new byte[1024];
        try {
            buf = IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != inputStream) {
                inputStream.close();
            }
        }
        return buf;
    }
}
