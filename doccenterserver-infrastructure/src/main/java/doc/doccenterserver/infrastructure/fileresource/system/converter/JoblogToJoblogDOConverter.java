package doc.doccenterserver.infrastructure.fileresource.system.converter;

import doc.doccenterserver.domain.fileresource.system.model.JobLogEntry;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobLogDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.JobLogDOEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author ch
 * @date 2023/4/12 19:16
 */
@Mapper
public interface JoblogToJoblogDOConverter {
    JoblogToJoblogDOConverter INSTANCE = Mappers.getMapper(JoblogToJoblogDOConverter.class);

    @Mappings({  })
    List<JobLogEntry> dOToEntityList(List<JobLogEntry> joblogDoPageList);
//    @Mappings({  })
//    List<JobLogEntry> entityToDOList(List<JobLogDO> joblogDoPageList);
    @Mappings({  })
    List<JobLogDOEntity> entityToDOList(List<JobLogDOEntity> joblogDoPageList);
    @Mappings({  })
    JobLogDO entityToDO(JobLogEntry jobLog);
    @Mappings({  })
    JobLogDOEntity jobLogDOentityToEntity(JobLogEntry jobLog);
}
