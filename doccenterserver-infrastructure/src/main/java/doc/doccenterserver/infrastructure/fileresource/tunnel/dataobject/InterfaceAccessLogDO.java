package doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject;

import com.alibaba.bizworks.core.specification.Field;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 接口调用日志表
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Data
@TableName("`t_interface_access_log`")
@NoArgsConstructor
public class InterfaceAccessLogDO {

    @Field(name = "日志流水号")
    @TableField("log_id")
    private String logId;

    @Field(name = "接口调用参数")
    @TableField("params")
    private String params;

    @Field(name = "调用人appid")
    @TableField("appid")
    private String appid;

    @Field(name = "调用人密码")
    @TableField("password")
    private String password;

    @Field(name = "接口调用时间")
    @TableField("call_time")
    private Date callTime;

    @Field(name = "接口调用结果状态")
    @TableField("result_status")
    private String resultStatus;

    @Field(name = "接口调用失败原因")
    @TableField("fail_reason")
    private String failReason;

    @Field(name = "请求路径")
    @TableField("request_url")
    private String requestUrl;

    @Field(name = "调用人ip")
    @TableField("request_ip")
    private String requestIp;

    @Field(name = "appname")
    @TableField("appname")
    private String appname;
}
