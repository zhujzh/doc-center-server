package doc.doccenterserver.infrastructure.fileresource.es.search.elasticsearch;

import doc.doccenterserver.infrastructure.fileresource.es.HwRestClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.hwclient.HwRestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.io.File;

/**
 * @Description: elastic search连接、配置等初始化
 * @author: gj
 * @date 2023/4/11
 */
@Configuration
@Slf4j
public class ElasticInitializer {

    @Autowired
    private Environment environment;

    private static RestHighLevelClient highLevelClient = null;

    @Bean
    public RestHighLevelClient getHighLevelClient() {
        if (highLevelClient != null) {
            return highLevelClient;
        }
        try {
            //HwRestClient hwRestClient = HwRestClientUtils.getHwRestClient(null);
            String[] args = new String[]{buildEsConfigPath()};
            HwRestClient hwRestClient = HwRestClientUtils.getHwRestClient(args);
            highLevelClient = new RestHighLevelClient(hwRestClient.getRestClientBuilder());
        } catch (Exception e) {
            log.error("RestHighLevelClient.error:", e);
        }

        return highLevelClient;
    }

    private String buildEsConfigPath() {
        String confName = environment.getProperty("esconf.confName");
        log.error("configName===>" + confName);
        return System.getProperty("user.dir") + File.separator + confName + File.separator;
    }

    /**
     * 关闭RestHighLevelClient
     *
     * @return
     */
    public void closeHighLevelClient(RestHighLevelClient client) {
        try {
            if (client != null) {
                highLevelClient.close();
            }
        } catch (Exception e) {
            log.error("Failed to close RestHighLevelClient.", e);
        }
    }
}
