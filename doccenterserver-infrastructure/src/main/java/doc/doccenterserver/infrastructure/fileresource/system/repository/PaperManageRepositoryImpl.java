package doc.doccenterserver.infrastructure.fileresource.system.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import doc.doccenterserver.domain.fileresource.base.BaseDto;
import doc.doccenterserver.domain.fileresource.base.PageResult;
import doc.doccenterserver.domain.fileresource.document.model.MeetingEntity;
import doc.doccenterserver.domain.fileresource.document.model.PaperAuthEntity;
import doc.doccenterserver.domain.fileresource.document.model.PaperEntity;
import doc.doccenterserver.domain.fileresource.document.repository.MeetingRepository;
import doc.doccenterserver.domain.fileresource.document.repository.PaperAuthRepository;
import doc.doccenterserver.domain.fileresource.document.repository.PaperRepository;
import doc.doccenterserver.domain.fileresource.document.repository.UserRepository;
import doc.doccenterserver.domain.fileresource.system.repository.PaperManageRepository;
import doc.doccenterserver.infrastructure.fileresource.document.converter.PaperEntityToPaperDOConvertor;
import doc.doccenterserver.infrastructure.fileresource.message.utils.DateUtil;
import doc.doccenterserver.infrastructure.fileresource.message.utils.StringUtil;
import doc.doccenterserver.infrastructure.fileresource.security.model.LoginUser;
import doc.doccenterserver.infrastructure.fileresource.security.service.TokenService;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.ChannelMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.MeetingMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.database.PaperManageMapper;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.ChannelDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.MeetingDO;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.PaperDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ch
 * @date 2023/4/23 16:02
 */
@Slf4j
@Component("PaperManageDORepository")
public class PaperManageRepositoryImpl  extends ServiceImpl<PaperManageMapper, PaperDO> implements PaperManageRepository {

    private static final String DOMAIN_FILE_API_INFO = "{\"downloadURL\":\"https://www.rzdata.net/doc-cloud-file/file/download/\",\"md5URL\":\"https://www.rzdata.net/doc-cloud-file/file/md5/check/\",\"uploadURL\":\"https://www.rzdata.net/doc-cloud-file/file/upload\",\"updateURL\":\"https://www.rzdata.net/doc-cloud-file/file/get\",\"deleteURL\":\"https://www.rzdata.net/doc-cloud-file/file/delete\",\"fileTag\":{\"image\":\"jpg,jpeg,bmp,png,gif\",\"document\":\"doc,docx,xls,xlsx,ppt,pptx,txt,pdf\",\"video\":\"avi,wma,rmvb,rm,swf,mp4,mid,3gp\",\"music\":\"mp3,wma,wav,mod,aac,ape,ogg,m4a\"},\"supportedViewTypes\":\"doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,bmp,png,gif\"}";
    private static final String FILE_API_INFO = "{\"downloadURL\":\"http://10.158.134.38:9081/doc-cloud-file/file/download/\",\"md5URL\":\"http://10.158.134.38:9081/doc-cloud-file/file/md5/check/\",\"uploadURL\":\"http://10.158.134.38:9081/doc-cloud-file/file/upload\",\"updateURL\":\"http://10.158.134.38:9081/doc-cloud-file/file/update\",\"deleteURL\":\"http://10.158.134.38:9081/doc-cloud-file/file/del/\",\"fileTag\":{\"image\":\"jpg,jpeg,bmp,png,gif\",\"document\":\"doc,docx,xls,xlsx,ppt,pptx,txt,pdf\",\"video\":\"avi,wma,rmvb,rm,swf,mp4,mid,3gp\",\"music\":\"mp3,wma,wav,mod,aac,ape,ogg,m4a\"},\"supportedViewTypes\":\"doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,bmp,png,gif\"}";
    private static final String OBJ_TYPE_U="u";//用户权限
    private static final String OBJ_TYPE_G="g";//组织权限
    private static final String OBJ_TYPE_A="a";//自定义组权限
    private static final String OBJ_TYPE_E="e";//扩展权限

    @Resource
    private PaperManageMapper paperManageMapper;
    @Resource
    private TokenService tokenService;
    @Resource
    private ChannelMapper channelMapper;
    @Resource
    private MeetingMapper meetingMapper;
    @Resource
    private UserRepository userRepository;
    @Resource
    private PaperAuthRepository paperAuthRepository;
    @Resource
    private PaperRepository paperRepository;
    @Resource
    private MeetingRepository meetingRepository;



    @Override
    public PageResult<List<PaperEntity>> listpapers(PaperEntity paperEntity) {

        QueryWrapper<PaperDO> wrapper = new QueryWrapper<>();
        if(StringUtil.notEmpty(paperEntity.getIssueStartDate())){
            wrapper.ge("ISSUE_DATE", paperEntity.getIssueStartDate()+" 00:00:00");
        }
        if(StringUtil.notEmpty(paperEntity.getIssueEndDate())){
            wrapper.le( "ISSUE_DATE", paperEntity.getIssueEndDate()+" 23:59:59");
        }


        wrapper.like(StringUtil.notEmpty(paperEntity.getPaperName()), "PAPER_NAME", paperEntity.getPaperName());
        if(paperEntity.getPublicFlag() != null){
            wrapper.eq("PUBLIC_FLAG", paperEntity.getPublicFlag());
        }
        if(paperEntity.getPaperType() != null){
            wrapper.eq("PAPER_TYPE", paperEntity.getPaperType());
        }
        wrapper.like(StringUtil.notEmpty(paperEntity.getPaperId()), "PAPER_ID", paperEntity.getPaperId());
        wrapper.eq("DEL_FLAG",0);
        wrapper.orderByDesc("PAPER_ORDER","INPUT_TIME","ISSUE_DATE");

        Page<PaperDO> page = new Page<>(paperEntity.getPageNum(), paperEntity.getPageSize());
        IPage<PaperDO> paperDoPage = paperManageMapper.selectPage(page, wrapper);
        List<PaperDO> paperDoPageList = paperDoPage.getRecords();


        BaseDto baseDto = new BaseDto();
        baseDto.setPageNum(page.getCurrent());
        baseDto.setPageSize(page.getSize());
        baseDto.setTotal(page.getTotal());
        baseDto.setTotalPage(page.getPages());

        List<PaperEntity> paperEntityList = PaperEntityToPaperDOConvertor.INSTANCE.dOToEntityList(paperDoPageList);

        //根据PAPER_ORDER，添加置顶状态标识topStatus，true：已置顶  false：未置顶
        for (PaperEntity paper : paperEntityList) {
            Date date = paper.getPaperOrder();
            paper.setTopStatus(null != date && !"1900".equals(DateUtil.getDateFormat(date, "yyyy")));
        }

        return new PageResult(paperEntityList,baseDto);
    }

    @Override
    public Map<String, Object> topdoc(String paperId, Boolean topStatus) {
        Map<String,Object> map= Maps.newHashMap();
        map.put("success",false);
        try{
            boolean b=this.updateDocTopStatus(paperId,topStatus);
            if(b){
                map.put("success",true);
            }
        }catch(Exception e){
            log.error("topDoc==========paperId="+paperId,e);
        }
        return map;
    }

    @Override
    public Map<String, Object> editpaper(String paperId, HttpServletRequest request) {
        Map<String,Object> map = Maps.newHashMap();
        LoginUser loginuser= tokenService.getLoginUser(request);
        PaperDO pedo = this.getById(paperId);
        PaperEntity paperEntity = PaperEntityToPaperDOConvertor.INSTANCE.dOToEntity(pedo);
        String fileApi = DOMAIN_FILE_API_INFO;
        if(loginuser==null){
            fileApi = FILE_API_INFO;
        }
        //频道
        List<ChannelDO> channelList =channelMapper.queryParentChannels(paperEntity.getClassId());//子频道列表

        String paperText=paperEntity.getPaperText();
        if(StringUtils.isNotBlank(paperText)){
            //处理正文内容
            paperEntity.setPaperText(StringUtil.textEncode(paperText));
        }
        paperEntity.setPaperText(StringUtil.textEncode(paperText));
//初始化权限信息
//        StringBuffer authObjectIds=new StringBuffer();
//        List<Map<String,Object>> paperAuthList=paperManageMapper.queryPaperAuthObjects(paperId);
        StringBuilder authObjectIds = new StringBuilder();
        List<PaperAuthEntity> paperAuthList = paperAuthRepository.getPaperAuthList(paperId);

        for (int i = 0; i < paperAuthList.size(); i++) {
            PaperAuthEntity paperAuthEntity = paperAuthList.get(i);
            String objectType = paperAuthEntity.getObjectType();
            String authObject = paperAuthEntity.getAuthObject();

            if (OBJ_TYPE_A.equals(objectType)) {
                authObject += "^GROUP";
            } else if (OBJ_TYPE_G.equals(objectType)) {
                authObject += "^ORG";
            } else if (OBJ_TYPE_U.equals(objectType)) {
                authObject += "^USER";
            }
            if (i < paperAuthList.size() - 1) {
                authObjectIds.append(authObject).append(",");
            } else {
                authObjectIds.append(authObject);
                paperEntity.setAuthValue(String.valueOf(paperAuthEntity.getAuthValue()));
            }
        }
        String authObjIds=authObjectIds.toString();
        if(StringUtils.isNotBlank(authObjIds)){
            List<Map<String,Object>> authList=userRepository.querySelectedData(authObjIds);
            StringBuffer sb=new StringBuffer();
            for(int i=0;i<authList.size();i++){
                Map<String,Object> item=authList.get(i);
                String name=item.get("NAME").toString();
                String orgName=item.get("DEPT_NAME").toString();
                String userId=item.get("ID").toString();
                String type=item.get("DATA_TYPE").toString();
                sb.append(name+","+orgName+","+userId+","+type);
                if(i<authList.size()-1){
                    sb.append("||");
                }

            }
//            map.put("authList",sb.toString());
            map.put("authList",authList);
        }
        map.put("channelList",channelList);
        paperEntity.setAuthObjectIds(authObjIds);

        //
        paperRepository.setPaperType(paperEntity);

        MeetingDO meetingDo=meetingMapper.selectById(paperId);

        map.put("meeting",meetingDo);
        map.put("paper",paperEntity);
        map.put("curUser",loginuser);
        map.put("fileApiInfo",fileApi);

        return map;
    }

    /**
     * 文档维护界面修改文档
     * @param paper
     * @param request
     * @return
     */
    @Override
    public Map<String, Object> savePaper(PaperEntity paper, HttpServletRequest request) {
        Map<String, Object> result=Maps.newHashMap();
        result.put("info", "操作失败！");
        result.put("success",false);//默认操作失败，false为失败，true为成功
        try {

            PaperDO paperDo= PaperEntityToPaperDOConvertor.INSTANCE.entityToDO(paper);
            int successNum=paperManageMapper.updateById(paperDo);

            if(successNum==1){
                //处理权限数据
                paperAuthRepository.deletePaperAuthById(paper.getPaperId());
                // 再插入最新的权限
                updatePaperAuth(paper);
                MeetingEntity meeting =  paper.getMeeting();
                if(meeting!=null){
                    meetingRepository.saveMeeting(meeting);
                }
//                MeetingEntry meeting = new MeetingEntry();
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                if(startTime!=null){
//                    Date date = sdf.parse(startTime);
//                    meeting.setStartTime(date);
//                }
//                if(endTime!=null){
//                    Date date = sdf.parse(endTime);
//                    meeting.setEndTime(date);
//                }
//                if(address!=null){
//                    meeting.setAddress(address);
//                    meeting.setTypeValue("meeting");
//                }
//                if(meeting!=null&&meeting.size()>0){
//                    meeting.setPaperId(paper.getPaperId());
//                    meetingService.saveMeeting(meeting);
//                }
                result.put("info", "操作成功！");
                result.put("success", true);
                result.put("paper",paper);
            }
        } catch (Exception e) {
            log.error("=======修改文档失败=========paperId:"+paper.getPaperId(),e);
        }
        return result;
    }

    private void updatePaperAuth(PaperEntity paper) {
        String paperId = paper.getPaperId();
        PaperAuthEntity pae = new PaperAuthEntity();
        Map<String, Object> params = Maps.newHashMap();
        List<String> authObjectArr = StringUtil.strToList(paper.getAuthObjectIds(), ",");// 获取权限对象
        for (int i = 0; i < authObjectArr.size(); i++) {
            String authObject =authObjectArr.get(i);

            params.put("paperId", paperId);
            params.put("authObject", authObject);
            params.put("authValue", "1");

            pae.setPaperId(paperId);
            pae.setAuthValue(1);//暂时默认1
            if (authObject.indexOf("^ORG") > -1) {//组织
                pae.setAuthObject(authObject.substring(0, authObject.length() - 4));
                pae.setObjectType("g");
            } else if (authObject.indexOf("^GROUP") > -1) {//自定义组
                pae.setAuthObject(authObject.substring(0, authObject.length() - 6));
                pae.setObjectType("a");
            } else {//用户，后缀^USER
                pae.setAuthObject(authObject.substring(0, authObject.length() - 5));
                pae.setObjectType("u");
            }
            paperAuthRepository.insertPaperAuth(pae);
            params.clear();
        }
    }

    public boolean updateDocTopStatus(String paperId,Boolean topStatus) {
        PaperDO pdo = new PaperDO();
        pdo.setPaperId(paperId);
        if (topStatus.equals(true)) {// 已经置顶，则是取消置顶操作
            pdo.setPaperOrder(DateUtil.parseDateFormat("1900","yyyy"));
        } else {//未置顶，则是置顶操作
            pdo.setPaperOrder(new Date());
        }
        return this.updateById(pdo);
    }
}
