package doc.doccenterserver.infrastructure.fileresource.message.utils;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class TrustAllManager implements X509TrustManager {

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return null;
    }

    @Override
    public void checkServerTrusted(X509Certificate[] certs,
                                   String authType)
            throws CertificateException {
    }

    @Override
    public void checkClientTrusted(X509Certificate[] certs,
                                   String authType)
            throws CertificateException {
    }

}
