package doc.doccenterserver.infrastructure.fileresource.tunnel.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import doc.doccenterserver.domain.fileresource.document.model.OrgEntity;
import doc.doccenterserver.infrastructure.fileresource.tunnel.dataobject.OrgDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 组织表Mapper
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@Mapper
public interface OrgMapper extends BaseMapper<OrgDO> {
    List<Map<String, Object>> querySelectedOrgs(List<List<String>> orgMutilList);

    List<OrgEntity> getChildOrgList(String orgId);

    Integer hasChild(String orgId);

}
