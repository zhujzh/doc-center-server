package doc.doccenterserver.infrastructure.fileresource.es;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @Description: 业务层面使用的查询对象
 * @author: gj
 * @date 2023/4/13
 */
public class SearchResult {

    /** 查询耗时 */
    private Double qTime;
    /** 最大相关度 */
    private String maxScore;
    /** 符合条件的结果数目 */
    private long numFound;
    /** 总页数 */
    private long pageCount;
    /** 当前页 */
    private long currentPage;
    /** 分页大小 */
    private long pageSize;
    /** 分页html */
    private String pagination;

    /** 一次获取的结果集合 */
    private List<SearchDocument> entities = Lists.newArrayList();

    public Double getqTime() {
        return qTime;
    }

    public void setqTime(Double qTime) {
        this.qTime = qTime;
    }

    public String getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(String maxScore) {
        this.maxScore = maxScore;
    }

    public long getNumFound() {
        return numFound;
    }

    public void setNumFound(long numFound) {
        this.numFound = numFound;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(long currentPage) {
        this.currentPage = currentPage;
    }

    public String getPagination() {
        return pagination;
    }

    public void setPagination(String pagination) {
        this.pagination = pagination;
    }

    public List<SearchDocument> getEntities() {
        return entities;
    }

    public void setEntities(List<SearchDocument> entities) {
        this.entities = entities;
    }



    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
        this.pageCount=(numFound+pageSize-1)/pageSize;
    }

    @Override
    public String toString() {
        return "SearchResult [qTime=" + qTime + ", maxScore=" + maxScore
                + ", numFound=" + numFound + ", pageCount=" + pageCount
                + ", currentPage=" + currentPage + ", pagination=" + pagination
                + ", entities=" + entities + "]";
    }
}
