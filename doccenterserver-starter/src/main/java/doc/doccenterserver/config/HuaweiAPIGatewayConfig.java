package doc.doccenterserver.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Slf4j
@Configuration
@ComponentScan("com.tobacco.mp.huawei.apiclient")
@ConditionalOnExpression("'huawei'.equals('${bizworks-api-gateway.type}') and 'true'.equals('${bizworks-api-gateway.enable}') ")
public class HuaweiAPIGatewayConfig implements EnvironmentAware {
    @Override
    public void setEnvironment(Environment environment) {
        log.info("开启华为网关模式");
    }
}
