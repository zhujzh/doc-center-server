package doc.doccenterserver.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * swagger properties
 *
 * @author bizworks: 管理员
 * @date 2023.03.08
 */
@ConfigurationProperties(prefix = "spring.swagger")
@Data
public class SwaggerProperties {
    private Boolean enable;
    private String basePackage;
    private String version;
    private String title;
    private String description;
}