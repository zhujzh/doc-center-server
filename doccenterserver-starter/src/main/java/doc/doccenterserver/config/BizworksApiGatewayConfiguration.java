package doc.doccenterserver.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnExpression("'ali'.equals('${bizworks-api-gateway.type}') and 'true'.equals('${bizworks-api-gateway.enable}') ")
@ComponentScan("com.bizworks.ubcp.apiclient.feign")
public class BizworksApiGatewayConfiguration {
    @Bean
    public String init(){
        System.out.println("开启阿里网关模式");
        return null;
    }
}
