package doc.doccenterserver.config;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import doc.doccenterserver.app.fileresource.system.api.ParameterService;
import doc.doccenterserver.client.fileresource.system.api.dto.ParameterDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author bug毁灭者
 */
@Slf4j
@Configuration
public class ParameterConfig {

    @Autowired
    private ParameterService parameterService;
    @Bean
    public JSONObject parameterMap() {
        List<ParameterDto> list= parameterService.getList();
        List<ParameterDto> distinctList = list.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getPKey()))), ArrayList::new));
        Map<String,String> map = distinctList.stream().collect(Collectors.toMap(ParameterDto::getPKey,ParameterDto::getPValue));
        log.info("初始化配置",map);
        return JSONUtil.parseObj(map);
    }
}
