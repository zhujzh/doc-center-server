package doc.doccenterserver;

import com.alibaba.bizworks.traffic.logging.configuration.BWLoggingLogbackListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * doccenterserver启动类
 *
 * @author bizworks: 朱君正
 * @date 2023.03.24
 */
@SpringBootApplication(scanBasePackages = { "doc.doccenterserver", "com.alibaba.bizworks" })
@EnableFeignClients(basePackages = {
		"com.tobacco.mp.dc.client.api",
		"com.tobacco.mp.uc.kz.client.api",
		"com.tobacco.mp.uc.client.api"
})
public class Application {

    public static void main(String[] args) {
    	SpringApplication.run(Application.class, args);

	}
}
