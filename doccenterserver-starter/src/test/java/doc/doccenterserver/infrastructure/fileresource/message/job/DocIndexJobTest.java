package doc.doccenterserver.infrastructure.fileresource.message.job;

import doc.doccenterserver.domain.fileresource.message.repository.DocIndexChangelogRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Description:
 * @author: gj
 * @date 2023/4/14
 */
class DocIndexJobTest {

    @Autowired
    private DocIndexChangelogRepository docIndexChangelogRepository;

    @Test
    void docIndex() {
        docIndexChangelogRepository.docIndex();
    }
}
