package com.alibaba.bizworks.demo.message;


import doc.doccenterserver.Application;
import doc.doccenterserver.app.fileresource.system.api.ParameterService;
import doc.doccenterserver.domain.fileresource.message.repository.*;
import doc.doccenterserver.infrastructure.fileresource.es.RawSearchResult;
import doc.doccenterserver.infrastructure.fileresource.es.search.elasticsearch.ElasticSearchImpl;
import doc.doccenterserver.infrastructure.fileresource.message.enums.TopicsEnum;
import doc.doccenterserver.infrastructure.fileresource.message.mqs.KafkaReceive;
import doc.doccenterserver.infrastructure.fileresource.message.mqs.KafkaSend;
import lombok.RequiredArgsConstructor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes =Application.class, webEnvironment =SpringBootTest.WebEnvironment.DEFINED_PORT)
public class MessageTest {
    @Autowired
    private JobLogRepository jobLogRepository;
    @Autowired
    private MqChangelogRepository mqChangelogRepository;
    @Autowired
    private ArchiveChangelogRepository archiveChangelogRepository;
    @Autowired
    private DocPublishChangelogRepository docPublishChangelogRepository;
    @Autowired
    private DocConvertChangelogRepository docConvertChangelogRepository;
    @Autowired
    private DocIndexChangelogRepository docIndexChangelogRepository;
    @Autowired
    private MqSaveRepository mqSaveRepository;
    @Autowired
    private KafkaSend kafkaSend;
    @Autowired
    ElasticSearchImpl elasticSearch;

    //消息入库
    @Test
    public void msgIn() throws Exception {
        String mqmsg = "<root><head><servicecode>OATOEKP</servicecode><sender>OA</sender><serialnumber>1464569749166</serialnumber><bizid>0003</bizid><SENDQ>BZ.OA.SEND</SENDQ></head><kmdoc><ip>10.158.129.29</ip><from>OA</from><id>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c999</id><category><![CDATA[[收文]]><![CDATA[]]]></category><subject><![CDATA[国家烟草专卖局办公室关于转发电子商务统计报表制度的通知]]></subject><creator>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</creator><created>2016-05-30 08:55:46</created><docauthor>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</docauthor><keywords/><useyears>0</useyears><description/><context/><printcopy>0</printcopy><readers>uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=jintl0124,cn=employees,dc=hngytobacco,dc=com;uid=yangzhm0819,cn=employees,dc=hngytobacco,dc=com;uid=liujf1103,cn=employees,dc=hngytobacco,dc=com;uid=lil0801,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com;uid=zhoujl0617,cn=employees,dc=hngytobacco,dc=com</readers><authors/><attcopy/><copyusers/><attprint/><printusers/><attdownload/><dwldusers/><attachments><attachment><name><![CDATA[国家烟草专卖局办公室关于转发电子商务统计报表制度的通知.doc]]></name><access>459d7065-15925515a49-8a52a0d51510de2bef6158d80746c8c4</access><type>1</type></attachment></attachments><operation>create</operation><publish>1</publish><toporgcode>cn=20430001,cn=43,cn=0,dc=hngytobacco,dc=com</toporgcode><archiveorgcode>20430001</archiveorgcode><remark>（不公开）</remark><meetingstarttime/><meetingendtime/><formname>收文</formname><permision>u:zhoujl0617;u:jintl0124;u:yangzhm0819;u:liujf1103;u:lil0801;u:zhoujl0617;u:zhoujl0617;u:zhoujl0617</permision><resourceid>d7c189d-154755da2af-8a52a0d51510de2bef6158d80746c0021</resourceid><channelid>101080106</channelid><changlogid>1464569749166</changlogid><addition><element id=\"channelname\">收文</element></addition><extension1/></kmdoc></root>";
        jobLogRepository.msgIn(mqmsg);
    }
    //消息处理
    @Test
    public void msgHandle() {
        mqChangelogRepository.msgHandle();
    }
    //消息归档
    @Test
    public void msgArchive() {
        archiveChangelogRepository.msgArchive();
    }
    //文档转换
    @Test
    public void docConvert(){
        docConvertChangelogRepository.docConvert();
    }
    //文档发布
    @Test
    public void docPublish(){
        docPublishChangelogRepository.docPublish();
    }
    //创建索引
    @Test
    public void docIndex(){
        docIndexChangelogRepository.docIndex();
    }
    //异常任务处理
    @Test
    public void msgErr(){
        mqSaveRepository.msgErr();
    }
    //向云上消息队列发送消息
    @Test
    public void sendKafka(){
        kafkaSend.send(TopicsEnum.ZB.getValue(), "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n" +
                "<root classify=\"1\">\n" +
                "    <head>\n" +
                "        <servicecode>ZYECMTOCPORTAL0001</servicecode>\n" +
                "        <sender>ZYECM</sender>\n" +
                "        <bizid>0001</bizid>\n" +
                "        <serialnumber>1480930915138</serialnumber>\n" +
                "    </head>\n" +
                "    <format>\n" +
                "        <ip>10.158.133.209</ip>\n" +
                "        <from>ZYECM</from>\n" +
                "        <id>ZYECM_1480930880253068ClMf9ojKrcNJIOB3K9F32y_1</id>\n" +
                "        <category>3</category>\n" +
                "        <owner>\n" +
                "            <![CDATA[chenchm0219]]>\n" +
                "        </owner>\n" +
                "        <title>\n" +
                "            <![CDATA[[烟厂发文]湖南中烟工业有限责任公司长沙卷烟厂关于购置科研试验线烟草专用机械的请示]]>\n" +
                "        </title>\n" +
                "        <abstract>\n" +
                "            <![CDATA[湖南中烟工业有限责任公司长沙卷烟厂关于购置科研试验线烟草专用机械的请示]]>\n" +
                "        </abstract>\n" +
                "        <content>\n" +
                "            <![CDATA[<meetingstarttime></meetingstarttime><meetingendtime></meetingendtime><meetingplace>\n" +
                "            <![CDATA[]]</meetingplace>]]>\n" +
                "        </content>\n" +
                "        <url>\n" +
                "            <![CDATA[https://tam.hngytobacco.com/dcloud/doc-cloud-service/content/paper/1480930880253068ClMf9ojKrcNJIOB3K9F32y?r=1]]>\n" +
                "        </url>\n" +
                "        <time>2016-12-05 17:40:00</time>\n" +
                "        <org />\n" +
                "        <permission>u:chenchm0219;</permission>\n" +
                "        <resourceid>1480930880253068ClMf9ojKrcNJIOB3K9F32y</resourceid>\n" +
                "        <channelid>102020101</channelid>\n" +
                "        <addition>\n" +
                "            <element id=\"meetingstarttime\"></element>\n" +
                "            <element id=\"meetingendtime\"></element>\n" +
                "            <element id=\"meetingplace\">\n" +
                "                <![CDATA[]]>\n" +
                "            </element>\n" +
                "            <element id=\"attributelable\">bodyfile</element>\n" +
                "        </addition>\n" +
                "    </format>\n" +
                "</root>");
    }
    //测试ES检索
    @Test
    public void search(){
        elasticSearch.testSearch();
    }
}
